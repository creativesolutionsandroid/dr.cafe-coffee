package com.cs.drcafe;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.adapter.CountriesAdapter;
import com.cs.drcafe.model.Countries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by brahmam on 28/12/15.
 */
public class RegistrationActivity extends Activity implements View.OnClickListener{
    private String SOAP_ACTION = "http://tempuri.org/InsertRegistration";
    private String METHOD_NAME = "InsertRegistration";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapPrimitive result;
    private String response12 = null;

    private String SOAP_ACTION_LOGIN = "http://tempuri.org/userLogin";
    private String METHOD_NAME_LOGIN = "userLogin";
    private SoapObject result12;
    private String response13 = null;

    Button backBtn, submit;
    EditText mFirstName, mFamilyName, mNickName, mCountryCode, mPhoneNumber, mEmail, mPassword, mConfirmPassword;
    TextView mGenderMale, mGenderFemale, mTermsTextView;
    CheckBox mTermsOfService;
    private LinearLayout mCountryPicker;
    private ImageView mCountryFlag;
    private ArrayList<Countries> countriesList = new ArrayList<>();
    private Countries countries;
    private CountriesAdapter mCountriesAdapter;
    private String firstName, familyName,nickName, countryCode = "+966", phoneNumber, email, password, confirmPassword, gender = "Male";
    private int countryFlag;
    private int phoneNumberLength;
    ProgressDialog dialog;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String loginUserName, loginPwd;
    String language;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.registration_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.registration_layout_arabic);
        }
        prepareArrayLits();
        countryFlag = R.drawable.sa;
        mCountriesAdapter = new CountriesAdapter(this,countriesList);
        mCountryPicker = (LinearLayout) findViewById(R.id.country_picker);
        mCountryFlag = (ImageView) findViewById(R.id.country_flag);
        mFirstName = (EditText) findViewById(R.id.register_first_name);
        mFamilyName = (EditText) findViewById(R.id.register_family_name);
        mNickName = (EditText) findViewById(R.id.register_nick_name);
        mCountryCode = (EditText) findViewById(R.id.register_country_code);
        mPhoneNumber = (EditText) findViewById(R.id.register_mobile_number);
        mEmail = (EditText) findViewById(R.id.register_email);
        mPassword = (EditText) findViewById(R.id.register_password);
        mConfirmPassword = (EditText) findViewById(R.id.register_confirm_password);
        mGenderMale = (TextView) findViewById(R.id.gender_male);
        mGenderFemale = (TextView) findViewById(R.id.gender_female);
        mTermsOfService = (CheckBox) findViewById(R.id.register_terms_check);
        mTermsTextView = (TextView) findViewById(R.id.register_terms_text);
        backBtn = (Button) findViewById(R.id.back_btn);
        submit = (Button) findViewById(R.id.register_submit);

        if(countryCode.equalsIgnoreCase("+966")){
            int maxLength = 9;
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(maxLength);
            mPhoneNumber.setFilters(fArray);
            phoneNumberLength = 9;
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        mPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = mPhoneNumber.getText().toString();
                if(!mPhoneNumber.hasWindowFocus() || mPhoneNumber.hasFocus() || s == null){
//                    if(countryCode.equalsIgnoreCase("+966")){
                        if(text.startsWith("0")){
                            mPhoneNumber.setText("");
                            return;
//                        }
                    }
                    return;
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        submit.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        mCountryPicker.setOnClickListener(this);
        mGenderMale.setOnClickListener(this);
        mGenderFemale.setOnClickListener(this);
        mTermsTextView.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_btn:
                onBackPressed();
                break;
            case R.id.country_picker:
                dialog();
                break;
            case R.id.gender_male:
                gender = "Male";
                mGenderMale.setBackgroundColor(Color.parseColor("#00000000"));
                mGenderFemale.setBackgroundColor(Color.parseColor("#F1F1F1"));
                break;
            case R.id.gender_female:
                gender = "Female";
                mGenderMale.setBackgroundColor(Color.parseColor("#F1F1F1"));
                mGenderFemale.setBackgroundColor(Color.parseColor("#00000000"));
                break;

            case R.id.register_terms_text:
                Intent loginIntent = new Intent(RegistrationActivity.this, WebViewActivity.class);
                loginIntent.putExtra("webview_toshow", "register_terms");
                startActivity(loginIntent);
                break;


            case R.id.register_submit:
                firstName = mFirstName.getText().toString();
                familyName = mFamilyName.getText().toString();
                nickName = mNickName.getText().toString();
                phoneNumber = mPhoneNumber.getText().toString();
                email = mEmail.getText().toString();
                password = mPassword.getText().toString();
                confirmPassword = mConfirmPassword.getText().toString();
                if(firstName.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mFirstName.setError("Please enter First Name");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mFirstName.setError("من فضلك ادخل الاسم الاول");
                    }
                }else if(phoneNumber.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mPhoneNumber.setError("Please enter Mobile Number");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPhoneNumber.setError("من فضلك أدخل رقم الجوال");
                    }
                }else if(phoneNumber.length() != phoneNumberLength){
                    if(language.equalsIgnoreCase("En")) {
                        mPhoneNumber.setError("Please enter valid Mobile Number");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPhoneNumber.setError("من فضلك أدخل رقم الجوال");
                    }

                }else if(email.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please enter Email");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError("من فضلك ادخل البريد الالكتروني");
                    }
                }else if(!isValidEmail(email)){
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please use Email format (example - abc@abc.com");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError("ادخل البريد الالكتروني كما هو موضح");
                    }

                }else if(password.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mPassword.setError("Please enter Password");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPassword.setError("من فضلك ادخل كلمة المرور");
                    }
                }else if(password.length() < 8 || password.length() > 20){
                    if(language.equalsIgnoreCase("En")) {
                        mPassword.setError("Password must be at least 8 - 20 characters");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPassword.setError("كلمة المرور يجب أن تتكون من 8 إلى 20 حرف على الأقل");
                    }
                }else if (!confirmPassword.equals(password)){
                    if(language.equalsIgnoreCase("En")) {
                        mConfirmPassword.setError("Passwords not match, please retype");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mConfirmPassword.setError("كلمة المرور غير مطابقة ، من فضلك اعد الادخال");
                    }

                }else if(!mTermsOfService.isChecked()){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Please review and accept the terms of service")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("نرجوا مراجعة شروط الخدمة وقبولها والموافقة عليها")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                }else{

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("We will be verifying the mobile number: "+countryCode+" "+phoneNumber+"\n\nIs this OK, or would you like to edit the number?")
                            .setCancelable(false)
                            .setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mPhoneNumber.requestFocus();
                                    mPhoneNumber.setSelection(mPhoneNumber.length());
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    new InsertRegistration().execute();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
                break;
        }
    }

    public class InsertRegistration extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(RegistrationActivity.this);
            dialog = ProgressDialog.show(RegistrationActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("countryId", 190);
                    request.addProperty("fullName", firstName);
                    request.addProperty("fullAddress", "riydh,Saudhi");
                    request.addProperty("email", email);
                    request.addProperty("phone", countryCode+"-"+phoneNumber);
                    request.addProperty("mobile", countryCode+"-"+phoneNumber);
                    request.addProperty("username", email);
                    request.addProperty("password", password);
                    request.addProperty("city", "riyadh");
                    request.addProperty("state", "riyadh");
                    request.addProperty("zip", "500201");
                    request.addProperty("NickName", nickName);
                    request.addProperty("Gender", gender);
                    request.addProperty("DeviceToken", SplashScreen.regid);
                    request.addProperty("FamilyName", familyName);



                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapPrimitive) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    if(dialog!=null) {
                        dialog.dismiss();
                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    if (response12.equals("user alredy exist.")) {
                        if(dialog!=null) {
                            dialog.dismiss();
                        }
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);



                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(RegistrationActivity.this, "Email / Mobile Number is already registered", Toast.LENGTH_SHORT).show();
                            // set title
//                            alertDialogBuilder.setTitle("dr.CAFE");
//
//                            // set dialog message
//                            alertDialogBuilder
//                                    .setMessage("Email / Mobile Number is already registered")
//                                    .setCancelable(false)
//                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.dismiss();
//                                        }
//                                    });
                        }else if(language.equalsIgnoreCase("Ar")){
                            Toast.makeText(RegistrationActivity.this, "البريد الالكتروني / رقم الجوال بالفعل مسجل", Toast.LENGTH_SHORT).show();

                            // set title
//                            alertDialogBuilder.setTitle("د. كيف");
//
//                            // set dialog message
//                            alertDialogBuilder
//                                    .setMessage("البريد الالكتروني / رقم الجوال بالفعل مسجل")
//                                    .setCancelable(false)
//                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.dismiss();
//                                        }
//                                    });
                        }

                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
//                        alertDialog.show();
                    } else if(response12.equals("success fully register.")){
                        if(dialog!=null) {
                            dialog.dismiss();
                        }
                        loginUserName = email;
                        loginPwd = password;
                        new CheckLoginDetails().execute();
                    }
                }
            }

            if(dialog!=null) {
                dialog.dismiss();
            }
            super.onPostExecute(result1);
        }
    }



    public class CheckLoginDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(RegistrationActivity.this);
            dialog = ProgressDialog.show(RegistrationActivity.this, "",
                    "Please wait...");
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
            Log.i("TAG USE", email+"  "+password);
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_LOGIN);
                    request.addProperty("username", email);
                    request.addProperty("password", password);
                    request.addProperty("deviceToken", SplashScreen.regid);
                    request.addProperty("language", "En");

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_LOGIN, envelope);
                    result12 = (SoapObject) envelope.getResponse();
                    response13 = result12.toString();
                    Log.v("TAG", response13);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result12.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response13);
            }else {
                return "no internet";
            }
            return response13;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    if (response13.equals("anyType{}")) {
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                    } else {

                        try {

                            Object property = result12.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject loginObject = (SoapObject) property;
                                    SoapObject userObject = (SoapObject) loginObject.getProperty(0);
                                    SoapObject userObject1 = (SoapObject) userObject.getProperty(0);
                                    String userId = userObject1.getPropertyAsString("userId");
                                    Log.i("TAG", "" + userId);
                                    if (userId.equalsIgnoreCase("0")) {
                                        dialog.dismiss();
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);

                                        // set title
                                        alertDialogBuilder.setTitle("dr.CAFE");

                                        // set dialog message
                                        alertDialogBuilder
                                                .setMessage("Invalid email or password")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                });

                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();

                                        // show it
                                        alertDialog.show();
                                    } else {
                                        String fullName = userObject1.getPropertyAsString("fullName");
                                        String fullAddress = userObject1.getPropertyAsString("fullAddress");
                                        String phone = userObject1.getPropertyAsString("phone");
                                        String countryId = userObject1.getPropertyAsString("countryId");
                                        String country = userObject1.getPropertyAsString("country");
                                        String username = userObject1.getPropertyAsString("username");
                                        String mobile = userObject1.getPropertyAsString("mobile");
                                        String NickName = userObject1.getPropertyAsString("NickName");
                                        if(NickName.equalsIgnoreCase("anytype{}")){
                                            NickName = "";
                                        }
                                        String Gender = userObject1.getPropertyAsString("Gender");
                                        String FamilyName = userObject1.getPropertyAsString("FamilyName");
                                        if(FamilyName.equalsIgnoreCase("anytype{}")){
                                            FamilyName = "";
                                        }

                                        try {
                                            JSONObject parent = new JSONObject();
                                            JSONObject jsonObject = new JSONObject();
                                            JSONArray jsonArray = new JSONArray();
                                            jsonArray.put("lv1");
                                            jsonArray.put("lv2");

                                            jsonObject.put("userId", userId);
                                            jsonObject.put("fullName", fullName);
                                            jsonObject.put("phone", phone);
                                            jsonObject.put("mobile", mobile);
                                            jsonObject.put("FamilyName", FamilyName);
                                            jsonObject.put("Gender", Gender);
                                            jsonObject.put("country", country);
                                            jsonObject.put("NickName", NickName);
                                            jsonObject.put("username", username);
                                            jsonObject.put("country_flag", countryFlag);
                                            jsonObject.put("user_details", jsonArray);

                                            parent.put("profile", jsonObject);
                                            Log.d("output", parent.toString());
                                            userPrefEditor.putString("user_profile", parent.toString());
//                                            userPrefEditor.putString("login_status","loggedin");
                                            userPrefEditor.putString("userId", userId);
                                            userPrefEditor.commit();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        dialog.dismiss();

                                        Intent loginIntent = new Intent(RegistrationActivity.this, VerifyRandomNumber.class);
                                        loginIntent.putExtra("phone_number", phone);
                                        loginIntent.putExtra("country_flag", countryFlag);
                                        loginIntent.putExtra("userId", userId);
                                        startActivity(loginIntent);
                                        finish();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
//                    }

                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }



                    }
                }
            }

            super.onPostExecute(result1);
        }
    }




    /* Method used to prepare the ArrayList,
     * Same way, you can also do looping and adding object into the ArrayList.
     */
    public void prepareArrayLits()
    {

        AddObjectToList(R.drawable.bh, "Bahrain", "+973", "18");
        AddObjectToList(R.drawable.bd, "Bangladesh", "+880", "19");
        AddObjectToList(R.drawable.cn, "China", "+86", "45");
        AddObjectToList(R.drawable.cy, "Cyprus", "+537" ,"57");
        AddObjectToList(R.drawable.eg, "Egypt", "+20", "64");
        AddObjectToList(R.drawable.fr, "France", "+33", "74");
        AddObjectToList(R.drawable.de, "Germany", "+49", "81");
        AddObjectToList(R.drawable.in, "India", "+91", "101");
        AddObjectToList(R.drawable.id, "Indonesia", "+62", "102");

        AddObjectToList(R.drawable.ir, "Iran", "+98", "103");
        AddObjectToList(R.drawable.iq, "Iraq", "+964", "104");
        AddObjectToList(R.drawable.jo, "Jordan", "+962", "112");
        AddObjectToList(R.drawable.kw, "Kuwait", "+965", "118");
        AddObjectToList(R.drawable.lb, "Lebanon", "+961", "122");
        AddObjectToList(R.drawable.my, "Malaysia", "+60", "133");
        AddObjectToList(R.drawable.ma, "Morocco", "+212", "147");
        AddObjectToList(R.drawable.np, "Nepal", "+977", "152");
        AddObjectToList(R.drawable.om, "Oman", "+968", "164");

        AddObjectToList(R.drawable.pk, "Pakistan", "+92", "165");
        AddObjectToList(R.drawable.ps, "Palestinian Territories", "", "167");
        AddObjectToList(R.drawable.ph, "Philippines", "+63", "172");
        AddObjectToList(R.drawable.qa, "Qatar", "+974", "177");
        AddObjectToList(R.drawable.sa, "Saudi Arabia", "+966", "190");
        AddObjectToList(R.drawable.sg, "Singapore", "+65", "195");
        AddObjectToList(R.drawable.es, "Spain", "+34", "202");
        AddObjectToList(R.drawable.lk, "Sri Lanka", "+94", "203");
        AddObjectToList(R.drawable.sd, "Sudan", "+249", "204");

        AddObjectToList(R.drawable.tr, "Syria", "+963", "210");
        AddObjectToList(R.drawable.tr, "Turkey", "+90", "221");
        AddObjectToList(R.drawable.ae, "United Arab Emirates", "+971", "227");
        AddObjectToList(R.drawable.gb, "United Kingdom", "+44", "228");
        AddObjectToList(R.drawable.us, "United States", "+1", "229");
        AddObjectToList(R.drawable.ye, "Yemen", "+967", "240");
    }

    // Add one item into the Array List
    public void AddObjectToList(int image, String countryName, String countryCode, String countrrId)
    {
        countries = new Countries();
        countries.setCountryName(countryName);
        countries.setCountryFlag(image);
        countries.setCountryCode(countryCode);
        countries.setCountryID(countrrId);
        countriesList.add(countries);
    }

    public void dialog() {

        final Dialog dialog2 = new Dialog(this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.setContentView(R.layout.countries_list);
        dialog2.setCanceledOnTouchOutside(true);
        ListView lv = (ListView) dialog2.findViewById(R.id.countries_list);
        lv.setAdapter(mCountriesAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                countryCode = countriesList.get(arg2).getCountryCode();
                countryFlag = countriesList.get(arg2).getCountryFlag();

                if(countryCode.equalsIgnoreCase("+966")){
                    int maxLength = 9;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mPhoneNumber.setFilters(fArray);
                    phoneNumberLength = 9;
                }else{
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mPhoneNumber.setFilters(fArray);
                    phoneNumberLength = 10;
                }

                mCountryFlag.setImageResource(countriesList.get(arg2).getCountryFlag());
                mCountryCode.setText("" + countriesList.get(arg2).getCountryCode());

                dialog2.dismiss();

            }
        });


        dialog2.show();

    }

    public final static boolean isValidEmail(CharSequence target) {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }

}
