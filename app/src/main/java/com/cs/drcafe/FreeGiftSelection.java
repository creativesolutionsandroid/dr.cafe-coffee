package com.cs.drcafe;

import android.database.SQLException;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.drcafe.adapter.FreeDrinkSizeAdapter;
import com.cs.drcafe.adapter.FreeGiftsItemsAdapter;
import com.cs.drcafe.model.FreeDrinkSizes;
import com.cs.drcafe.model.MyGifts;
import com.cs.drcafe.model.SubCategories;
import com.cs.drcafe.widget.CustomListView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 08-03-2018.
 */

public class FreeGiftSelection extends Fragment {

    private ArrayList<MyGifts> giftsList = new ArrayList<>();
    private ArrayList<SubCategories> giftItemsList = new ArrayList<>();
    public static ArrayList<String> selectedGiftsCount = new ArrayList<>();
    private ArrayList<FreeDrinkSizes> freeDrinkSizes = new ArrayList<>();
    String itemIds;
    int position;
    LinearLayout buttonsLayout;
    Boolean isSingleItem = true;
    TextView giftTitle, giftDescription, giftvalidText, giftDate, giftLocation;
    TextView mBackButton, mRedeemButton, cartCount;
    CustomListView giftsListView;
    GridView sizesGridView;
    FreeDrinkSizeAdapter mSizeAdapter;
    private DataBaseHelper myDbHelper;
    FreeGiftsItemsAdapter mItemsAdapter;
    public static int drinksSelected = 0;
    public static String drinkSizeSelected = "0";
    int drinksAvailable;
    Button backBtn;
    String sizesAvailable;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.free_gifts_selection, container, false);

        myDbHelper = new DataBaseHelper(getContext());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
        drinkSizeSelected = "0";
        selectedGiftsCount.clear();
        giftsList = (ArrayList<MyGifts>) getArguments().getSerializable("array");
        position = getArguments().getInt("position");

        itemIds = giftsList.get(position).getItemId();

        giftTitle = (TextView) rootView.findViewById(R.id.giftTitle);
        giftDescription = (TextView) rootView.findViewById(R.id.giftDescription);
        giftvalidText = (TextView) rootView.findViewById(R.id.giftvalidText);
        giftDate = (TextView) rootView.findViewById(R.id.giftDate);
        giftLocation = (TextView) rootView.findViewById(R.id.giftLocation);
        mBackButton = (TextView) rootView.findViewById(R.id.backButton);
        mRedeemButton = (TextView) rootView.findViewById(R.id.RedeemButton);
        cartCount = (TextView) rootView.findViewById(R.id.cart_count);

        buttonsLayout = (LinearLayout) rootView.findViewById(R.id.buttonsLayout);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);

        sizesGridView = (GridView) rootView.findViewById(R.id.sizesGridView);
        giftsListView = (CustomListView) rootView.findViewById(R.id.giftItemsList);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        cartCount.setText("" + myDbHelper.getTotalOrderQty());

        String[] items = itemIds.split(",");
        for (int i = 0; i < items.length; i++){
            ArrayList<SubCategories> localArray= myDbHelper.getItemDetails(items[i]);
            giftItemsList.addAll(localArray);
        }
        if(giftItemsList.size()>1){
            isSingleItem = false;
//            buttonsLayout.setVisibility(View.GONE);
        }

        String[] sizes = giftsList.get(position).getSize().split(",");
        for (int i = 0; i < sizes.length; i++) {
            FreeDrinkSizes free = new FreeDrinkSizes();
            if (sizes[i].equals("1")) {
                free.setId("1");
                free.setSizeName("Short");
//                sizesAvailable = "Short";
            } else if (sizes[i].equals("3")) {
                free.setId("3");
                free.setSizeName("Tall");
//                sizesAvailable = "Tall";
            } else if (sizes[i].equals("2")) {
                free.setId("2");
                free.setSizeName("Grande");
//                sizesAvailable = "Grande";
            } else if (sizes[i].equals("6")) {
                free.setId("6");
                free.setSizeName("Jumbo");
//                sizesAvailable = "Jumbo";
            } else if (sizes[i].equals("7")) {
                free.setId("7");
                free.setSizeName("Single");
//                sizesAvailable = "Single";
            } else if (sizes[i].equals("8")) {
                free.setId("8");
                free.setSizeName("Double");
//                sizesAvailable = "Double";
            } else if (sizes[i].equals("9")) {
                free.setId("9");
                free.setSizeName("Coffee Box");
//                sizesAvailable = "Coffee Box";
            } else if (sizes[i].equals("10")) {
                free.setId("10");
                free.setSizeName("Triple");
//                sizesAvailable = "Triple";
            }
            freeDrinkSizes.add(free);
        }

        mSizeAdapter = new FreeDrinkSizeAdapter(getContext(), freeDrinkSizes);
        sizesGridView.setAdapter(mSizeAdapter);

        drinksAvailable = Integer.parseInt(giftsList.get(position).getRemainingBonus());
        drinksAvailable = 5;
        mItemsAdapter = new FreeGiftsItemsAdapter(getContext(), giftItemsList, isSingleItem, sizesAvailable, drinksAvailable);
        giftsListView.setAdapter(mItemsAdapter);

        giftTitle.setText(giftsList.get(position).getDesc_En());
        giftDescription.setText("Remaining Free drinks " + giftsList.get(position).getRemainingBonus());

        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
        Date dateObj = null;
        try {
            dateObj = curFormater.parse(giftsList.get(position).getEndDate().replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        giftvalidText.setText("Valid until: "+date);
        giftDate.setText("Valid days: "+giftsList.get(position).getValidForDays());


        return rootView;
    }
}
