package com.cs.drcafe;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cs.drcafe.model.Additionals;
import com.cs.drcafe.model.Categories;
import com.cs.drcafe.model.Nutrition;
import com.cs.drcafe.model.Order;
import com.cs.drcafe.model.SubCategories;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseHelper extends SQLiteOpenHelper{
 
    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/com.cs.drcafe/databases/";
 
    private static String DB_NAME = "drCAFE.db";

    private SQLiteDatabase myDataBase;
	SharedPreferences versionPrefs;
	SharedPreferences.Editor versionPrefEditor;
    private final Context myContext;

    private int DB_VERSION = 91;
    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     * @param context
     */
    public DataBaseHelper(Context context) {
 
    	super(context, DB_NAME, null, 91);
        this.myContext = context;

		versionPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
		versionPrefEditor  = versionPrefs.edit();
    }	
 
  /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException{
 
    	boolean dbExist = checkDataBase();
 
    	if(dbExist){
			Log.d("TAG", "dbExist: "+dbExist);
			//do nothing - database already exist
    	}else{
			Log.d("TAG", "dbExist: "+dbExist);

			//By calling this method and empty database will be created into the default system path
               //of your application so we are gonna be able to overwrite that database with our database.
        	this.getReadableDatabase();
        	this.close();
 
        	try {

				Log.d("TAG", "createDataBase: copying database");
    			copyDataBase();
 
    		} catch (IOException e) {
 				e.printStackTrace();
        		throw new Error("Error copying database");
 
        	}
    	}
 
    }
 
    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

    	SQLiteDatabase checkDB = null;
 
    	try{
			if (android.os.Build.VERSION.SDK_INT >= 28) {
				DataBaseHelper helper = new DataBaseHelper(myContext);
				SQLiteDatabase database = helper.getReadableDatabase();
				String filePath = database.getPath();
				database.close();
				checkDB = SQLiteDatabase.openDatabase(filePath, null, SQLiteDatabase.OPEN_READWRITE| SQLiteDatabase.NO_LOCALIZED_COLLATORS);

				if(versionPrefs.getInt("version",0) != DB_VERSION) {
					versionPrefEditor.putInt("version", DB_VERSION);
					versionPrefEditor.commit();
					return false;
				}
			}
			else{
				String myPath = DB_PATH + DB_NAME;
				checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
			}
//			checkDB.disableWriteAheadLogging();
 
    	}catch(SQLiteException e){
    		e.printStackTrace();
    		//database does't exist yet.
    	}

		if(checkDB != null){
			if(checkDB.getVersion() != DB_VERSION){
				checkDB.close();
				return false;
			}else{
				checkDB.close();
				return true;
			}
		}
		return false;
    }
 
    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

    	//Open your local db as the input stream
    	InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = "";
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            DataBaseHelper helper = new DataBaseHelper(myContext);
            SQLiteDatabase database = helper.getReadableDatabase();
            outFileName = database.getPath();
            database.close();
//            checkDB = SQLiteDatabase.openDatabase(filePath, null, SQLiteDatabase.OPEN_READONLY);
        }
        else{
            outFileName = DB_PATH + DB_NAME;
        }

        Log.d("TAG", "copyDataBase: "+outFileName);

    	// Path to the just created empty db
 
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
 
    public void openDataBase() throws SQLException{
    	//Open the database
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            DataBaseHelper helper = new DataBaseHelper(myContext);
            SQLiteDatabase database = helper.getReadableDatabase();
            String filePath = database.getPath();
            database.close();
            myDataBase = SQLiteDatabase.openDatabase(filePath, null, SQLiteDatabase.OPEN_READWRITE| SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			Log.d("TAG", "openDataBase: "+myDataBase);
        }
        else{
            String myPath = DB_PATH + DB_NAME;
            myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        }
    }
 
    @Override
	public synchronized void close() {
    	    if(myDataBase != null)
    		    myDataBase.close();
    	    super.close();
 
	}
 
	@Override
	public void onCreate(SQLiteDatabase db) {

	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 
	}
 
        // Add your public helper methods to access and get content from the database.
       // You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
       // to you to create adapters for your views.

	public Cursor getRMCategoriesList(String catId){
		ArrayList<Categories> catList = new ArrayList<>();
		String selectQuery = "SELECT * FROM  tblNonShopSub where categoryId = " +catId +" AND status = 1 ORDER BY sequenceId ASC";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);

		return cursor;
	}

	public Cursor getCategoriesList(String catId){
		ArrayList<Categories> catList = new ArrayList<>();
		String selectQuery  = "SELECT * FROM  tblNonShopSub where NOT subCategoryId = 71 AND categoryId = " + catId + " AND status = 1 ORDER BY sequenceId ASC";

		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);

		return cursor;
	}

	public Cursor getCategoriesListUsingItemIds(String itemIds){
		ArrayList<Categories> catList = new ArrayList<>();
        Log.d("TAG", "getCategoriesListUsingItemIds: "+itemIds);
		String selectQuery = "select distinct subCat.subCategoryId, subCat.categoryId, subCat.subCategory, subCat.description, subCat.images,subCat.status,subCat.creationDate,subCat.modifiedDate,subCat.sequenceId,subCat.subCategory_ar,subCat.description_ar FROM tblNonShopSub subCat INNER JOIN tblNonShopItem item ON subCat.subcategoryId = item.subcategoryId and subCat.status='1' where item.itemId in ("+ itemIds +") order by subCat.sequenceId";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);

		return cursor;
	}

	public ArrayList<SubCategories> getSubCategoriesList(String catId, String subCatId){
		ArrayList<SubCategories> subCatList = new ArrayList<>();
		String selectQuery = "SELECT * FROM  tblNonShopItem where categoryId=" +catId +" AND subCategoryId="+subCatId + " AND  status=1 order by sequenceId";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				SubCategories sc = new SubCategories();
				sc.setItemId(cursor.getString(0));
				sc.setCatId(cursor.getString(1));
				sc.setSubCatId(cursor.getString(2));
				sc.setItemNo(cursor.getString(3));
				sc.setItemName(cursor.getString(4));
				sc.setDescription(cursor.getString(5));
				sc.setThumbnail(cursor.getString(6));
				sc.setImages(cursor.getString(7));
				sc.setStatus(cursor.getString(8));
				sc.setCreateDate(cursor.getString(9));
				sc.setModifyDate(cursor.getString(10));
				sc.setAdditionalsType(cursor.getString(11));
				sc.setModifiersId(cursor.getString(12));
				sc.setItemNameAr(cursor.getString(13));
				sc.setDescriptionAr(cursor.getString(14));
				subCatList.add(sc);
			} while (cursor.moveToNext());
		}
		return subCatList;
	}

	public ArrayList<SubCategories> getItemDetails(String itemId){
		ArrayList<SubCategories> subCatList = new ArrayList<>();
		String selectQuery = "SELECT * FROM  tblNonShopItem where itemId=" +itemId;
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				SubCategories sc = new SubCategories();
				sc.setItemId(cursor.getString(0));
				sc.setCatId(cursor.getString(1));
				sc.setSubCatId(cursor.getString(2));
				sc.setItemNo(cursor.getString(3));
				sc.setItemName(cursor.getString(4));
				sc.setDescription(cursor.getString(5));
				sc.setThumbnail(cursor.getString(6));
				sc.setImages(cursor.getString(7));
				sc.setStatus(cursor.getString(8));
				sc.setCreateDate(cursor.getString(9));
				sc.setModifyDate(cursor.getString(10));
				sc.setAdditionalsType(cursor.getString(11));
				sc.setModifiersId(cursor.getString(12));
				sc.setItemNameAr(cursor.getString(13));
				sc.setDescriptionAr(cursor.getString(14));
				subCatList.add(sc);
			} while (cursor.moveToNext());
		}
		return subCatList;
	}


	public ArrayList<Nutrition> getNutritionInfo(String catId, String itemId){
		ArrayList<Nutrition> NutrionInfoList = new ArrayList<>();
		String selectQuery = "SELECT * FROM  tblNutrition where itemId="+ itemId;
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Nutrition n = new Nutrition();
				n.setNutritionId(cursor.getString(0));
				n.setCatId(cursor.getString(1));
				n.setSubCatId(cursor.getString(2));
				n.setItemId(cursor.getString(3));
				n.setItemTypeId(cursor.getString(4));
				n.setCalories(cursor.getString(5));
				n.setSaturatedFat(cursor.getString(6));
				n.setTotalFat(cursor.getString(7));
				n.setTransFat(cursor.getString(8));
				n.setCholestrol(cursor.getString(9));
				n.setSodium(cursor.getString(10));
				n.setTotalCarboHydrates(cursor.getString(11));
				n.setDiietaryFiber(cursor.getString(12));
				n.setSugar(cursor.getString(13));
				n.setProtein(cursor.getString(14));
				n.setCaffien(cursor.getString(15));
				n.setPrice(cursor.getString(18));

				NutrionInfoList.add(n);
			} while (cursor.moveToNext());
		}
		return NutrionInfoList;
	}


	public Cursor getModifiers(String additionalType){
		String selectQuery = "SELECT * FROM  tblModifierInfo where modifierId=" +additionalType + " AND status = 1";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);

		return cursor;
	}

	public ArrayList<Additionals> getAdditionalsList(String modifierId){
		ArrayList<Additionals> subCatList = new ArrayList<>();
		String selectQuery = "SELECT * FROM  tblAdditionalsInfo where modifierId=" +modifierId + " AND  status=1";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Additionals sc = new Additionals();
				sc.setAdditionalsId(cursor.getString(0));
				sc.setModifierId(cursor.getString(1));
				sc.setAdditionalName(cursor.getString(2));
				sc.setAdditionalPrice(cursor.getString(3));
				sc.setAdditionalNameAr(cursor.getString(7));
				subCatList.add(sc);
			} while (cursor.moveToNext());
		}
		return subCatList;
	}

	public void insertOrder(HashMap<String, String> queryValues) {
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("itemId", queryValues.get("itemId"));
		values.put("itemTypeId", queryValues.get("itemTypeId"));
		values.put("subCategoryId", queryValues.get("subCategoryId"));
		values.put("additionals", queryValues.get("additionals"));
		values.put("qty", queryValues.get("qty"));
		values.put("price", queryValues.get("price"));
		values.put("additionalsPrice", queryValues.get("additionalsPrice"));
		values.put("additionalsTypeId", queryValues.get("additionalsTypeId"));
		values.put("totalAmount", queryValues.get("totalAmount"));
		values.put("comment", queryValues.get("comment"));
		values.put("status", queryValues.get("status"));
		values.put("creationDate", queryValues.get("creationDate"));
		values.put("modifiedDate", queryValues.get("modifiedDate"));
		values.put("categoryId", queryValues.get("categoryId"));

		database.insert("tblOrderInfo", null, values);
		database.close();
	}

	public ArrayList<Order> getOrderInfo(){
		ArrayList<Order> orderList = new ArrayList<>();
		String selectQuery = "SELECT * FROM  tblOrderInfo";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Order sc = new Order();
				sc.setOrderId(cursor.getString(0));
				sc.setItemId(cursor.getString(1));
				sc.setItemTypeId(cursor.getString(2));
				sc.setSubCatId(cursor.getString(3));
				sc.setAdditionals(cursor.getString(4));
				sc.setQty(cursor.getString(5));
				sc.setPrice(cursor.getString(6));
				sc.setAdditionalsPrice(cursor.getString(7));
				sc.setAdditionalsTypeId(cursor.getString(8));
				sc.setTotalAmount(cursor.getString(9));
				sc.setComment(cursor.getString(10));
				sc.setStatus(cursor.getString(11));
				sc.setCreationDate(cursor.getString(12));
				sc.setModifiedDate(cursor.getString(13));
				sc.setCategoryId(cursor.getString(14));
				sc.setItemName(getItemName(cursor.getString(1), "itemName"));
				sc.setItemNameAr(getItemName(cursor.getString(1), "itemName_ar"));
				orderList.add(sc);
			} while (cursor.moveToNext());
		}
		return orderList;
	}

	public ArrayList<Order> getItemOrderInfo(String itemId){
		ArrayList<Order> orderList = new ArrayList<>();
		String selectQuery = "SELECT * FROM  tblOrderInfo where itemId = "+itemId;
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Order sc = new Order();
				sc.setOrderId(cursor.getString(0));
				sc.setItemId(cursor.getString(1));
				sc.setItemTypeId(cursor.getString(2));
				sc.setSubCatId(cursor.getString(3));
				sc.setAdditionals(cursor.getString(4));
				sc.setQty(cursor.getString(5));
				sc.setPrice(cursor.getString(6));
				sc.setAdditionalsPrice(cursor.getString(7));
				sc.setAdditionalsTypeId(cursor.getString(8));
				sc.setTotalAmount(cursor.getString(9));
				sc.setComment(cursor.getString(10));
				sc.setStatus(cursor.getString(11));
				sc.setCreationDate(cursor.getString(12));
				sc.setModifiedDate(cursor.getString(13));
				sc.setCategoryId(cursor.getString(14));
				sc.setItemName(getItemName(cursor.getString(1), "itemName"));
				sc.setItemNameAr(getItemName(cursor.getString(1), "itemName_ar"));
				orderList.add(sc);
			} while (cursor.moveToNext());
		}
		return orderList;
	}

	public String getItemName(String itemId, String field){

		String itemName = "";
		String selectQuery = "SELECT " + field+" FROM  tblNonShopItem where itemId="+itemId;
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				itemName = cursor.getString(0);
			} while (cursor.moveToNext());
		}

		return itemName;
	}


	public int getItemOrderCount(String itemId){
		int cnt = 0;
		SQLiteDatabase database = this.getWritableDatabase();
		String deleteQuery = "SELECT SUM(qty) FROM  tblOrderInfo where itemId="+itemId;
		Log.d("query", deleteQuery);
		Cursor c=database.rawQuery(deleteQuery, null);
		if(c.moveToFirst())
		{
			cnt  = c.getInt(0);
		}
		return cnt;
	}

	public int getItemOrderPrice(String itemId){
		int cnt = 0;
		SQLiteDatabase database = this.getWritableDatabase();
		String deleteQuery = "SELECT SUM(price) FROM  tblOrderInfo where orderId="+itemId;
		Log.d("query", deleteQuery);
		Cursor c=database.rawQuery(deleteQuery, null);
		if(c.moveToFirst())
		{
			cnt  = c.getInt(0);
		}
		return cnt;
	}

	public int getSubcatOrderCount(String subCategoryId){
		int cnt = 0;
		SQLiteDatabase database = this.getWritableDatabase();
		String deleteQuery = "SELECT SUM(qty) FROM  tblOrderInfo where subCategoryId="+subCategoryId;
		Log.d("query", deleteQuery);
		Cursor c=database.rawQuery(deleteQuery, null);
		if(c.moveToFirst())
		{
			cnt  = c.getInt(0);
		}
		return cnt;
	}

	public ArrayList<String> getSubCatIds(){
		ArrayList<String> subCatIds = new ArrayList<>();
		String selectQuery = "SELECT subCategoryId FROM  tblOrderInfo";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				subCatIds.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		return subCatIds;
	}


	public ArrayList<String> getItemIds(){
		ArrayList<String> itemIds = new ArrayList<>();
		String selectQuery = "SELECT itemId FROM  tblOrderInfo";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				itemIds.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		return itemIds;
	}

	public void updateOrder(String qty, String price, String orderId){
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("qty", qty);
		values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
		String updateQuery = "UPDATE tblOrderInfo SET qty ="+qty+ ",totalAmount ="+ price +" where orderId ="+orderId;
		database.execSQL(updateQuery);
	}

	public void updateComment(String orderId, String comment){
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
		String updateQuery = "UPDATE tblOrderInfo SET comment = '"+comment+"' where orderId ="+orderId;
		database.execSQL(updateQuery);
	}

	public int getTotalOrderQty(){
		int qty = 0;
		String selectQuery = "SELECT itemId FROM  tblOrderInfo";
		SQLiteDatabase database = this.getWritableDatabase();
		try {
			Cursor cur = database.rawQuery("SELECT SUM(qty) FROM tblOrderInfo", null);

			if (cur.moveToFirst()) {
				qty = cur.getInt(0);
			}
		}catch (SQLiteException sqle){
			sqle.printStackTrace();
		}
		return qty;
	}

	public float getTotalOrderPrice(){
		float qty = 0;
		String selectQuery = "SELECT itemId FROM  tblOrderInfo";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cur = database.rawQuery("SELECT SUM(totalAmount) FROM tblOrderInfo", null);
		if(cur.moveToFirst())
		{
			qty  = cur.getFloat(0);
		}

		return qty;
	}

	public String getSubCatId(String itemId){
		String subCatId = "";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cur = database.rawQuery("SELECT subCategoryId, categoryId FROM  tblNonShopItem where itemId = "+itemId, null);
		if(cur.moveToFirst())
		{
			subCatId  = cur.getString(0);
			subCatId  = subCatId +","+cur.getString(1);
		}

		return subCatId;
	}

	public Cursor getNonShopItem(String itemId){
		String subCatId = "";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cur = database.rawQuery("SELECT * FROM  tblNonShopItem where itemId = "+itemId, null);

		return cur;
	}


	public String getPrice(String itemId, String itemTypeId){ //todo
		String subCatId = "";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cur = database.rawQuery("SELECT Price FROM  tblNutrition where itemId = "+itemId + " AND  itemTypeId="+ itemTypeId, null);
		if(cur.moveToFirst())
		{
			subCatId  = cur.getString(0);
		}

		return subCatId;
	}


	public ArrayList<Additionals> getAdditionalDetails(String additionalId){ //todo

		ArrayList<Additionals> subCatList = new ArrayList<>();
		String selectQuery = "SELECT * FROM  tblAdditionalsInfo where additionalsId = "+additionalId + " AND  status=1";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Additionals sc = new Additionals();
				sc.setAdditionalsId(cursor.getString(0));
				sc.setModifierId(cursor.getString(1));
				sc.setAdditionalName(cursor.getString(2));
				sc.setAdditionalPrice(cursor.getString(3));
				sc.setAdditionalNameAr(cursor.getString(7));
				subCatList.add(sc);
			} while (cursor.moveToNext());
		}
		return subCatList;

	}


	public ArrayList<Float> getAdditionalPrices(String additionalId){ //todo

		ArrayList<Float> addPrices = new ArrayList<>();
		String selectQuery = "SELECT additionalsPrice FROM  tblAdditionalsInfo where additionalsId IN ("+additionalId + ") AND  status=1";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				addPrices.add(cursor.getFloat(0));
			} while (cursor.moveToNext());
		}
		return addPrices;

	}

	public void deleteItemFromOrder(String orderId){
		SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
		String updateQuery = "DELETE FROM tblOrderInfo where orderId = "+orderId;
		database.execSQL(updateQuery);

	}

	public void deleteOrderTable(){
		SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
		String updateQuery = "DELETE FROM tblOrderInfo";
		database.execSQL(updateQuery);
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		db.disableWriteAheadLogging();
	}
}