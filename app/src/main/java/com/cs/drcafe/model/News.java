package com.cs.drcafe.model;

/**
 * Created by SKT on 05-01-2016.
 */
public class News {

    String newsId, newsHead, newsDesc, newsDescAr, imagePath, dateCreated;

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getNewsHead() {
        return newsHead;
    }

    public void setNewsHead(String newsHead) {
        this.newsHead = newsHead;
    }

    public String getNewsDesc() {
        return newsDesc;
    }

    public void setNewsDesc(String newsDesc) {
        this.newsDesc = newsDesc;
    }

    public String getNewsDescAr() {
        return newsDescAr;
    }

    public void setNewsDescAr(String newsDescAr) {
        this.newsDescAr = newsDescAr;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }
}
