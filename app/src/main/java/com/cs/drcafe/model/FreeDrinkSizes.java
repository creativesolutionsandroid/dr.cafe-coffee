package com.cs.drcafe.model;

/**
 * Created by CS on 27-03-2018.
 */

public class FreeDrinkSizes {

    String id, sizeName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }
}
