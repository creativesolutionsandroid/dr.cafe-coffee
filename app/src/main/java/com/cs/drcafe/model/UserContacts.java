package com.cs.drcafe.model;

import java.util.Comparator;

/**
 * Created by SKT on 10-12-2015.
 */
public class UserContacts {

    String name, phNo, email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhNo() {
        return phNo;
    }

    public void setPhNo(String phNo) {
        this.phNo = phNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /*Comparator for sorting the list by Student Name*/
    public static Comparator<UserContacts> StuNameComparator = new Comparator<UserContacts>() {

        public int compare(UserContacts s1, UserContacts s2) {
            String StudentName1 = s1.getName().toUpperCase();
            String StudentName2 = s2.getName().toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }};
}
