package com.cs.drcafe.model;

import java.io.Serializable;

/**
 * Created by CS on 13-11-2017.
 */

public class MyGifts implements Serializable{

    String Desc_En, Desc_Ar, endDate, remainingBonus, image, promoType;
    String CategoryId, SubCategoryId, ItemId, Size, Limit, MinSpend, ValidForDays;

    public String getValidForDays() {
        return ValidForDays;
    }

    public void setValidForDays(String validForDays) {
        ValidForDays = validForDays;
    }

    public String getPromoType() {
        return promoType;
    }

    public void setPromoType(String promoType) {
        this.promoType = promoType;
    }

    public String getDesc_En() {
        return Desc_En;
    }

    public void setDesc_En(String desc_En) {
        Desc_En = desc_En;
    }

    public String getDesc_Ar() {
        return Desc_Ar;
    }

    public void setDesc_Ar(String desc_Ar) {
        Desc_Ar = desc_Ar;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRemainingBonus() {
        return remainingBonus;
    }

    public void setRemainingBonus(String remainingBonus) {
        this.remainingBonus = remainingBonus;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getLimit() {
        return Limit;
    }

    public void setLimit(String limit) {
        Limit = limit;
    }

    public String getMinSpend() {
        return MinSpend;
    }

    public void setMinSpend(String minSpend) {
        MinSpend = minSpend;
    }
}
