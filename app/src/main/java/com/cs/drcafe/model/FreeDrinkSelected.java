package com.cs.drcafe.model;

/**
 * Created by CS on 27-03-2018.
 */

public class FreeDrinkSelected {

    String itemId, quantity;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
