package com.cs.drcafe.model;

/**
 * Created by SKT on 16-02-2016.
 */
public class SelectedAddtionals {
    String additionalId, modifierId, additionalName, price;
    int priceRatio;

    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public int getPriceRatio() {
        return priceRatio;
    }

    public void setPriceRatio(int priceRatio) {
        this.priceRatio = priceRatio;
    }
}
