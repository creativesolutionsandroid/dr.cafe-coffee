package com.cs.drcafe.model;

/**
 * Created by CS on 30-06-2016.
 */
public class Promos {

    String CategoryID, SubCategoryId,  PromoID, promoCode, percentage, image, limit, description, descriptionAr, promoTitle, promoTitleAr, itemId, storeId, additionals, size, promoType, remainingBonus,
    dupId, ValidForDays, ValidDays, DailyOnce, endDate, promoQty, Msg_Ar, Msg_En ;

    public String getMsg_Ar() {
        return Msg_Ar;
    }

    public void setMsg_Ar(String msg_Ar) {
        Msg_Ar = msg_Ar;
    }

    public String getMsg_En() {
        return Msg_En;
    }

    public void setMsg_En(String msg_En) {
        Msg_En = msg_En;
    }

    public String getPromoQty() {
        return promoQty;
    }

    public void setPromoQty(String promoQty) {
        this.promoQty = promoQty;
    }

    public String getDupId() {
        return dupId;
    }

    public void setDupId(String dupId) {
        this.dupId = dupId;
    }

    public String getValidForDays() {
        return ValidForDays;
    }

    public void setValidForDays(String validForDays) {
        ValidForDays = validForDays;
    }

    public String getValidDays() {
        return ValidDays;
    }

    public void setValidDays(String validDays) {
        ValidDays = validDays;
    }

    public String getDailyOnce() {
        return DailyOnce;
    }

    public void setDailyOnce(String dailyOnce) {
        DailyOnce = dailyOnce;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public String getPromoID() {
        return PromoID;
    }

    public void setPromoID(String promoID) {
        PromoID = promoID;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getPromoTitle() {
        return promoTitle;
    }

    public void setPromoTitle(String promoTitle) {
        this.promoTitle = promoTitle;
    }

    public String getPromoTitleAr() {
        return promoTitleAr;
    }

    public void setPromoTitleAr(String promoTitleAr) {
        this.promoTitleAr = promoTitleAr;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getAdditionals() {
        return additionals;
    }

    public void setAdditionals(String additionals) {
        this.additionals = additionals;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPromoType() {
        return promoType;
    }

    public void setPromoType(String promoType) {
        this.promoType = promoType;
    }

    public String getRemainingBonus() {
        return remainingBonus;
    }

    public void setRemainingBonus(String remainingBonus) {
        this.remainingBonus = remainingBonus;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }
}
