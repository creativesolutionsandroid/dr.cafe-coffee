package com.cs.drcafe.model;

import java.util.ArrayList;

/**
 * Created by SKT on 15-01-2016.
 */
public class Categories {
    String subCatId, catId, subCat, description, images, status, createDate, modifyDate, sequenceId, subCatAr, descriptionAr;
    ArrayList<SubCategories> childItems;


    public ArrayList<SubCategories> getChildItems() {
        return childItems;
    }

    public void setChildItems(ArrayList<SubCategories> childItems) {
        this.childItems = childItems;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getSubCat() {
        return subCat;
    }

    public void setSubCat(String subCat) {
        this.subCat = subCat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(String sequenceId) {
        this.sequenceId = sequenceId;
    }

    public String getSubCatAr() {
        return subCatAr;
    }

    public void setSubCatAr(String subCatAr) {
        this.subCatAr = subCatAr;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }
}
