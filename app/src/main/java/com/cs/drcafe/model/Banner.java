package com.cs.drcafe.model;

/**
 * Created by CS on 01-06-2016.
 */
public class Banner {

    String image;
    boolean isClickable;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }
}
