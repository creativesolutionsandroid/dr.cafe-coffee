package com.cs.drcafe.model;

import java.util.Comparator;

/**
 * Created by BRAHMAM on 23-11-2015.
 */
public class StoreInfo implements Item{
    String storeId, startTime, endTime, storeName, storeAddress, ItemsJson,
            countryName, cityName, imageURL, dcCountry, dcCity, storeName_ar, storeAddress_ar, message, message_ar,
            DeliveryMessageEn, DeliveryMessageAr, DeliveryItems;
    int openFlag;
    boolean familySection, onlineOrderStatus, wifi, patioSitting, v12, driveThru, meetingSpace, OnlinePayment, CashPayment,
            hospital, university, office, shoppingMall, airPort, dineIn, pickUp, ladies, neighborhood, is24x7, status, Deliverable;
    double latitude, longitude;
    float distance, distanceForDelivery, MinOrderAmount, DeliveryCharge, FreeDeliveryAbove;

    public String getDeliveryItems() {
        return DeliveryItems;
    }

    public void setDeliveryItems(String deliveryItems) {
        DeliveryItems = deliveryItems;
    }

    public String getIemsJson() {
        return ItemsJson;
    }

    public void setIemsJson(String iemsJson) {
        ItemsJson = iemsJson;
    }

    public boolean isOnlinePayment() {
        return OnlinePayment;
    }

    public void setOnlinePayment(boolean onlinePayment) {
        OnlinePayment = onlinePayment;
    }

    public boolean isCashPayment() {
        return CashPayment;
    }

    public void setCashPayment(boolean cashPayment) {
        CashPayment = cashPayment;
    }

    public boolean isOnlineOrderStatus() {
        return onlineOrderStatus;
    }

    public void setOnlineOrderStatus(boolean onlineOrderStatus) {
        this.onlineOrderStatus = onlineOrderStatus;
    }

    public boolean isDeliverable() {
        return Deliverable;
    }

    public void setDeliverable(boolean deliverable) {
        Deliverable = deliverable;
    }

    public float getDistanceForDelivery() {
        return distanceForDelivery;
    }

    public void setDistanceForDelivery(float distanceForDelivery) {
        this.distanceForDelivery = distanceForDelivery;
    }

    public float getMinOrderAmount() {
        return MinOrderAmount;
    }

    public void setMinOrderAmount(float minOrderAmount) {
        MinOrderAmount = minOrderAmount;
    }

    public float getDeliveryCharge() {
        return DeliveryCharge;
    }

    public void setDeliveryCharge(float deliveryCharge) {
        DeliveryCharge = deliveryCharge;
    }

    public float getFreeDeliveryAbove() {
        return FreeDeliveryAbove;
    }

    public void setFreeDeliveryAbove(float freeDeliveryAbove) {
        FreeDeliveryAbove = freeDeliveryAbove;
    }

    public String getDeliveryMessageEn() {
        return DeliveryMessageEn;
    }

    public void setDeliveryMessageEn(String deliveryMessageEn) {
        DeliveryMessageEn = deliveryMessageEn;
    }

    public String getDeliveryMessageAr() {
        return DeliveryMessageAr;
    }

    public void setDeliveryMessageAr(String deliveryMessageAr) {
        DeliveryMessageAr = deliveryMessageAr;
    }

    public boolean isNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(boolean neighborhood) {
        this.neighborhood = neighborhood;
    }

    public boolean isAirPort() {
        return airPort;
    }

    public void setAirPort(boolean airPort) {
        this.airPort = airPort;
    }

    public boolean isPickUp() {
        return pickUp;
    }

    public void setPickUp(boolean pickUp) {
        this.pickUp = pickUp;
    }

    public boolean isDineIn() {
        return dineIn;
    }

    public void setDineIn(boolean dineIn) {
        this.dineIn = dineIn;
    }

    public boolean isLadies() {
        return ladies;
    }

    public void setLadies(boolean ladies) {
        this.ladies = ladies;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getDcCountry() {
        return dcCountry;
    }

    public void setDcCountry(String dcCountry) {
        this.dcCountry = dcCountry;
    }

    public String getStoreName_ar() {
        return storeName_ar;
    }

    public void setStoreName_ar(String storeName_ar) {
        this.storeName_ar = storeName_ar;
    }

    public String getStoreAddress_ar() {
        return storeAddress_ar;
    }

    public void setStoreAddress_ar(String storeAddress_ar) {
        this.storeAddress_ar = storeAddress_ar;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage_ar() {
        return message_ar;
    }

    public void setMessage_ar(String message_ar) {
        this.message_ar = message_ar;
    }

    public boolean isFamilySection() {
        return familySection;
    }

    public void setFamilySection(boolean familySection) {
        this.familySection = familySection;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isPatioSitting() {
        return patioSitting;
    }

    public void setPatioSitting(boolean patioSitting) {
        this.patioSitting = patioSitting;
    }

    public boolean isV12() {
        return v12;
    }

    public void setV12(boolean v12) {
        this.v12 = v12;
    }

    public boolean isDriveThru() {
        return driveThru;
    }

    public void setDriveThru(boolean driveThru) {
        this.driveThru = driveThru;
    }

    public boolean isMeetingSpace() {
        return meetingSpace;
    }

    public void setMeetingSpace(boolean meetingSpace) {
        this.meetingSpace = meetingSpace;
    }

    public boolean isHospital() {
        return hospital;
    }

    public void setHospital(boolean hospital) {
        this.hospital = hospital;
    }

    public boolean isUniversity() {
        return university;
    }

    public void setUniversity(boolean university) {
        this.university = university;
    }

    public boolean isOffice() {
        return office;
    }

    public void setOffice(boolean office) {
        this.office = office;
    }

    public boolean is24x7() {
        return is24x7;
    }

    public void setIs24x7(boolean is24x7) {
        this.is24x7 = is24x7;
    }

    public boolean isShoppingMall() {
        return shoppingMall;
    }

    public void setShoppingMall(boolean shoppingMall) {
        this.shoppingMall = shoppingMall;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDcCity() {
        return dcCity;
    }

    public void setDcCity(String dcCity) {
        this.dcCity = dcCity;
    }

    public double getLatitude() {

        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getOpenFlag() {
        return openFlag;
    }

    public void setOpenFlag(int openFlag) {
        this.openFlag = openFlag;
    }

    /*Comparator for sorting the list by roll no*/
    public static Comparator<StoreInfo> storeDistance = new Comparator<StoreInfo>() {

        public int compare(StoreInfo s1, StoreInfo s2) {

            float rollno1 = s1.getDistance();
            float rollno2 = s2.getDistance();

	   /*For ascending order*/
            return Float.compare(rollno1,rollno2);

	   /*For descending order*/
            //rollno2-rollno1;
        }};


    /*Comparator for sorting the list by roll no*/
    public static Comparator<StoreInfo> storeOpenSort = new Comparator<StoreInfo>() {

        public int compare(StoreInfo s1, StoreInfo s2) {

            int rollno1 = s1.getOpenFlag();
            int rollno2 = s2.getOpenFlag();

	   /*For ascending order*/
            return rollno2-rollno1;

	   /*For descending order*/
            //rollno2-rollno1;
        }};

    @Override
    public boolean isSection() {
        return false;
    }

}
