package com.cs.drcafe.model;

/**
 * Created by CS on 02-03-2018.
 */

public class Tiers {

    String tierName, tierlimit;

    public String getTierName() {
        return tierName;
    }

    public void setTierName(String tierName) {
        this.tierName = tierName;
    }

    public String getTierlimit() {
        return tierlimit;
    }

    public void setTierlimit(String tierlimit) {
        this.tierlimit = tierlimit;
    }
}
