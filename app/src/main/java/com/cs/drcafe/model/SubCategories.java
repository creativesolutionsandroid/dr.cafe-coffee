package com.cs.drcafe.model;

/**
 * Created by SKT on 15-01-2016.
 */
public class SubCategories {
    String itemId, catId, subCatId, itemNo, itemName, description, thumbnail, images, status, createDate, modifyDate,
            additionalsType, modifiersId, itemNameAr, descriptionAr, quantity;

//    ArrayList<Nutrition> nutritionItems;
//
//    public ArrayList<Nutrition> getNutritionItems() {
//        return nutritionItems;
//    }
//
//    public void setNutritionItems(ArrayList<Nutrition> nutritionItems) {
//        this.nutritionItems = nutritionItems;
//    }


    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getAdditionalsType() {
        return additionalsType;
    }

    public void setAdditionalsType(String additionalsType) {
        this.additionalsType = additionalsType;
    }

    public String getModifiersId() {
        return modifiersId;
    }

    public void setModifiersId(String modifiersId) {
        this.modifiersId = modifiersId;
    }

    public String getItemNameAr() {
        return itemNameAr;
    }

    public void setItemNameAr(String itemNameAr) {
        this.itemNameAr = itemNameAr;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }
}
