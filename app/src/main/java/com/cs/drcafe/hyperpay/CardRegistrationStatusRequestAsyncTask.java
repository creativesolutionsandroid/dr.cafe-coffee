package com.cs.drcafe.hyperpay;

import android.os.AsyncTask;
import android.util.Log;

import com.cs.drcafe.Rest.APIInterface;
import com.cs.drcafe.Rest.ApiClient;
import com.cs.drcafe.model.CardRegistration;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Represents an async task to request a payment status from the server.
 */
public class CardRegistrationStatusRequestAsyncTask extends AsyncTask<String, Void, CardRegistration> {

    private CardRegistrationStatusRequestListener listener;
    private CardRegistration cardsResponse = null;

    public CardRegistrationStatusRequestAsyncTask(CardRegistrationStatusRequestListener listener) {
        this.listener = listener;
    }

    @Override
    protected CardRegistration doInBackground(String... params) {
        if (params.length != 1) {
            return null;
        }

        String resourcePath = params[0];

        if (resourcePath != null) {
            // prepare input json
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("resourcePath", params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CardRegistration> call = apiService.addCardStatus(
                    RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
            call.enqueue(new Callback<CardRegistration>() {
                @Override
                public void onResponse(Call<CardRegistration> call, Response<CardRegistration> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        CardRegistration checkOutIdResponse = response.body();
                        try {
                            if (checkOutIdResponse.getStatus()) {
                                cardsResponse = checkOutIdResponse;
                                Log.d("TAG", "sucess: "+cardsResponse);

                                if (listener != null) {
                                    listener.onCardRegistrationStatusReceived(cardsResponse);
                                }
                            } else {
                                Log.d("TAG", "failure: "+cardsResponse);
                                // status false case
//                                if (listener != null) {
//                                    listener.onErrorOccurred();
//                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
//                            if (listener != null) {
//                                listener.onErrorOccurred();
//                            }
                        }
                    } else {
                        Log.d("TAG", "onResponse failure: ");
//                        if (listener != null) {
//                            listener.onErrorOccurred();
//                        }
                    }
                }

                @Override
                public void onFailure(Call<CardRegistration> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (listener != null) {
                        listener.onErrorOccurred();
                    }
                }
            });
        }

        return null;
    }

    @Override
    protected void onPostExecute(CardRegistration paymentStatus) {

    }
}
