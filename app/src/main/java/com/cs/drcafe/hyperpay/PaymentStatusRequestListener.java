package com.cs.drcafe.hyperpay;


import com.cs.drcafe.model.MyCards;

public interface PaymentStatusRequestListener {

    void onErrorOccurred();
    void onPaymentStatusReceived(MyCards paymentStatus);
}
