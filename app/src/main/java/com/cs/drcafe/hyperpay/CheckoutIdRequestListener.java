package com.cs.drcafe.hyperpay;


public interface CheckoutIdRequestListener {

    void onCheckoutIdReceived(String checkoutId);
}
