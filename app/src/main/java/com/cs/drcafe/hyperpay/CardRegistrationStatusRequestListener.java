package com.cs.drcafe.hyperpay;


import com.cs.drcafe.model.CardRegistration;

public interface CardRegistrationStatusRequestListener {

    void onErrorOccurred();
    void onCardRegistrationStatusReceived(CardRegistration paymentStatus);
}
