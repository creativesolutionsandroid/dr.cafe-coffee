package com.cs.drcafe.Rest;

import com.cs.drcafe.model.CardRegistration;
import com.cs.drcafe.model.CheckOutId;
import com.cs.drcafe.model.DeleteCardList;
import com.cs.drcafe.model.MyCards;
import com.cs.drcafe.model.SavedCards;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIInterface {

//    Hyperpay
    @POST("api/HyperpayAPI/CheckoutRequestFromSDK")
    Call<CheckOutId> generateCheckOutId(@Body RequestBody body);

    @POST("api/HyperpayAPI/paymentStatus")
    Call<MyCards> getPaymentStatus(@Body RequestBody body);

    @POST("api/HyperpayAPI/AddCardStatus")
    Call<CardRegistration> addCardStatus(@Body RequestBody body);

    @POST("api/HyperpayAPI/GetCreditCardDetails")
    Call<SavedCards> GetAllSavedCards(@Body RequestBody body);

    @POST("api/HyperpayAPI/deleteCard")
    Call<DeleteCardList> deleteCard(@Body RequestBody body);

 }
