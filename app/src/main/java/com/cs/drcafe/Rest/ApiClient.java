package com.cs.drcafe.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

//    public static final String BASE_URL = "http://www.csadms.com/dcpaymentservice/";
//    public static final String BASE_URL = "http://csadms.com/dcpayments/";
    public static final String BASE_URL = "http://drcafeonline.net/v2payments/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
