package com.cs.drcafe.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.NewsAdapter;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.News;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by SKT on 04-01-2016.
 */
public class NewsFragment extends Fragment {
    private String SOAP_ACTION = "http://tempuri.org/dcNews";
    private String METHOD_NAME = "dcNews";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    Button backBtn;
    ListView newsListView;
    ArrayList<News> newsList = new ArrayList<>();
    NewsAdapter mAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.newsroom, container,
                false);

        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        newsListView = (ListView) rootView.findViewById(R.id.news_listview);
        mAdapter = new NewsAdapter(getActivity(), newsList);

        newsListView.setAdapter(mAdapter);

        new GetNewsDetails().execute();
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment menuFragment = new NewsDetailsFragment();
                Bundle mBundle = new Bundle();
                mBundle.putString("news_header", newsList.get(position).getNewsHead());
                mBundle.putString("news_desc", newsList.get(position).getNewsDesc());
                mBundle.putString("news_image", newsList.get(position).getImagePath());
                menuFragment.setArguments(mBundle);
//                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction menuTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                menuTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                menuTransaction.add(R.id.realtabcontent, menuFragment);
                menuTransaction.hide(NewsFragment.this);
                menuTransaction.addToBackStack(null);

                // Commit the transaction
                menuTransaction.commit();
            }
        });
        return rootView;
    }



    public class GetNewsDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;

        @Override
        protected void onPreExecute() {

//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);


                // request.addProperty("", ""); // incase you need to pass
                // parameters to the web-service

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                androidHttpTransport.call(SOAP_ACTION, envelope);
                result = (SoapObject) envelope.getResponse();
                response12 = result.toString();
                Log.v("TAG", response12);
                // SoapObject root = (SoapObject) result.getProperty(0);
                System.out.println("********Count : " + result.getPropertyCount());

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Responce", "" + response12);

            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (response12 != null) {
                if (response12.equals("anyType{}")) {
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                } else {

                    try {

                        Object property = result.getProperty(1);
                        if (property instanceof SoapObject) {
                            try {
                                SoapObject category_list = (SoapObject) property;
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    News news = new News();
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");
                                    String newsTitle = sObj.getPropertyAsString("newsHead");
                                    String newsDesc = sObj.getPropertyAsString("newsDesc");
                                    String newsImage = sObj.getPropertyAsString("imagePath");
                                    newsImage = newsImage.replace("~", "");
//                                String checkTotal = sObj.getPropertyAsString("CheckTotal");

                                    Log.i("NEWS TAG", "" + newsImage);
                                    news.setNewsHead(newsTitle);
                                    news.setNewsDesc(newsDesc);
                                    news.setImagePath(newsImage);

                                    newsList.add(news);

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//                    }

                    } catch (Exception e) {
                        Log.d("", "Error while parsing the results!");
                        e.printStackTrace();
                    }


                    mAdapter.notifyDataSetChanged();
                    super.onPostExecute(result1);
                }
            }
        }
    }



    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
