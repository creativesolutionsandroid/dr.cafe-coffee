package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

/**
 * Created by SKT on 27-12-2015.
 */
public class RadiusFragment extends Fragment {

    TextView km5, km10, km20, km40, km50;
    Button backBtn;
    SharedPreferences locationPrefs;
    SharedPreferences.Editor locationPrefsEditor;
    String radiusPrefValue, distancePrefValue;
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.radius_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.radius_layout_arabic, container,
                    false);
        }

        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);
        locationPrefsEditor  = locationPrefs.edit();
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        km5 = (TextView) rootView.findViewById(R.id.km5);
        km10 = (TextView) rootView.findViewById(R.id.km10);
        km20 = (TextView) rootView.findViewById(R.id.km20);
        km40 = (TextView) rootView.findViewById(R.id.km40);
        km50 = (TextView) rootView.findViewById(R.id.km50);

        radiusPrefValue = locationPrefs.getString("radius", "50");
        distancePrefValue = locationPrefs.getString("distance","");
        if(distancePrefValue.equalsIgnoreCase("kilometer")){
            if(language.equalsIgnoreCase("En")) {
                km5.setText("5 KM");
                km10.setText("10 KM");
                km20.setText("20 KM");
                km40.setText("40 KM");
                km50.setText("50 KM");
            }else if(language.equalsIgnoreCase("Ar")){
                km5.setText("5كيلو");
                km10.setText("10كيلو");
                km20.setText("20كيلو");
                km40.setText("40كيلو");
                km50.setText("50كيلو");
            }

        }else if(distancePrefValue.equalsIgnoreCase("mile")){
            km5.setText("5 Mi");
            km10.setText("10 Mi");
            km20.setText("20 Mi");
            km40.setText("40 Mi");
            km50.setText("50 Mi");
        }

        if(radiusPrefValue.equalsIgnoreCase("5")){
            if(language.equalsIgnoreCase("En")) {
                km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
            }else if(language.equalsIgnoreCase("Ar")){
                km5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
            }
            km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
            km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }else if(radiusPrefValue.equalsIgnoreCase("10")){
            if(language.equalsIgnoreCase("En")) {
                km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
            }else if(language.equalsIgnoreCase("Ar")){
                km10.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
            }
            km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }else if(radiusPrefValue.equalsIgnoreCase("20")){
            if(language.equalsIgnoreCase("En")) {
                km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
            }else if(language.equalsIgnoreCase("Ar")){
                km20.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
            }
            km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }else if(radiusPrefValue.equalsIgnoreCase("40")){
            if(language.equalsIgnoreCase("En")) {
                km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
            }else if(language.equalsIgnoreCase("Ar")){
                km40.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
            }
            km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }else if(radiusPrefValue.equalsIgnoreCase("50")){
            if(language.equalsIgnoreCase("En")) {
                km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
            }else if(language.equalsIgnoreCase("Ar")){
                km50.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
            }
            km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        km5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPrefsEditor.putString("radius", "5");
                locationPrefsEditor.commit();
                if(language.equalsIgnoreCase("En")) {
                    km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    km5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                }
                km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(new RadiusFragment());
//                trans.remove(new MoreLocation());
                trans.commit();
                FragmentManager manager1 = getActivity().getSupportFragmentManager();
                FragmentTransaction trans1 = manager1.beginTransaction();
                trans1.remove(new MoreLocation());
                trans1.commit();

                manager1.popBackStack();
                manager.popBackStack();

                Fragment radiusFragment = new MoreLocation();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                radiusTransaction.hide(RadiusFragment.this);
                radiusTransaction.addToBackStack(null);

                // Commit the transaction
                radiusTransaction.commit();

            }
        });

        km10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPrefsEditor.putString("radius", "10");
                locationPrefsEditor.commit();
                if(language.equalsIgnoreCase("En")) {
                    km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    km10.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                }
                km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(new RadiusFragment());
//                trans.remove(new MoreLocation());
                trans.commit();
                FragmentManager manager1 = getActivity().getSupportFragmentManager();
                FragmentTransaction trans1 = manager1.beginTransaction();
                trans1.remove(new MoreLocation());
                trans1.commit();

                manager1.popBackStack();
                manager.popBackStack();

                Fragment radiusFragment = new MoreLocation();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                radiusTransaction.hide(RadiusFragment.this);
                radiusTransaction.addToBackStack(null);

                // Commit the transaction
                radiusTransaction.commit();

            }
        });

        km20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPrefsEditor.putString("radius", "20");
                locationPrefsEditor.commit();
                if(language.equalsIgnoreCase("En")) {
                    km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    km20.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                }
                km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(new RadiusFragment());
//                trans.remove(new MoreLocation());
                trans.commit();
                FragmentManager manager1 = getActivity().getSupportFragmentManager();
                FragmentTransaction trans1 = manager1.beginTransaction();
                trans1.remove(new MoreLocation());
                trans1.commit();

                manager1.popBackStack();
                manager.popBackStack();

                Fragment radiusFragment = new MoreLocation();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                radiusTransaction.hide(RadiusFragment.this);
                radiusTransaction.addToBackStack(null);

                // Commit the transaction
                radiusTransaction.commit();

            }
        });


        km40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPrefsEditor.putString("radius", "40");
                locationPrefsEditor.commit();
                if(language.equalsIgnoreCase("En")) {
                    km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    km40.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                }
                km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(new RadiusFragment());
//                trans.remove(new MoreLocation());
                trans.commit();
                FragmentManager manager1 = getActivity().getSupportFragmentManager();
                FragmentTransaction trans1 = manager1.beginTransaction();
                trans1.remove(new MoreLocation());
                trans1.commit();

                manager1.popBackStack();
                manager.popBackStack();

                Fragment radiusFragment = new MoreLocation();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                radiusTransaction.hide(RadiusFragment.this);
                radiusTransaction.addToBackStack(null);

                // Commit the transaction
                radiusTransaction.commit();

            }
        });


        km50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPrefsEditor.putString("radius","50");
                locationPrefsEditor.commit();
                if(language.equalsIgnoreCase("En")) {
                    km50.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    km50.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                }
                km5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km10.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km20.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                km40.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(new RadiusFragment());
//                trans.remove(new MoreLocation());
                trans.commit();
                FragmentManager manager1 = getActivity().getSupportFragmentManager();
                FragmentTransaction trans1 = manager1.beginTransaction();
                trans1.remove(new MoreLocation());
                trans1.commit();

                manager1.popBackStack();
                manager.popBackStack();

                Fragment radiusFragment = new MoreLocation();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                radiusTransaction.hide(RadiusFragment.this);
                radiusTransaction.addToBackStack(null);

                // Commit the transaction
                radiusTransaction.commit();

            }
        });

        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
