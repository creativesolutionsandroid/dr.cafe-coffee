package com.cs.drcafe.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by brahmam on 22/2/16.
 */
public class MessageDetails extends Fragment {

    ImageView icon;
    TextView status, date, message, clear;
    String image, statusStr, dateStr, messageStr;
    Button backBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.message_details, container,
                false);

        statusStr = getArguments().getString("status");
        dateStr = getArguments().getString("date");
        messageStr = getArguments().getString("message");

        icon = (ImageView) rootView.findViewById(R.id.image);
        status = (TextView) rootView.findViewById(R.id.status);
        date = (TextView) rootView.findViewById(R.id.date);
        message = (TextView) rootView.findViewById(R.id.message);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        message.setText(""+messageStr);
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateObj = null;
        String[] parts = dateStr.split(" ");
        try {
            dateObj = curFormater.parse(dateStr.replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a");
        String datetext = postFormater.format(dateObj);

        date.setText(datetext);
        if(statusStr.equals("1")){
            status.setText("Accepted");
            icon.setImageResource(R.drawable.notification1);
        }else if(statusStr.equals("2")){
            status.setText("Ready");
            icon.setImageResource(R.drawable.notification2);
        }else if(statusStr.equals("3")){
            status.setText("Served");
            icon.setImageResource(R.drawable.notification3);
        }else if(statusStr.equals("4")){
            status.setText("Rejected");
            icon.setImageResource(R.drawable.notification4);
        }else if(statusStr.equals("5")){
            status.setText("Promotion");
            icon.setImageResource(R.drawable.notification5);
        }else if(statusStr.equals("6")){
            status.setText("Offer");
            icon.setImageResource(R.drawable.notification5);
        }else if(statusStr.equals("7")){
            status.setText("Wallet");
            icon.setImageResource(R.drawable.notification7);
        }else if(statusStr.equals("8")){
            status.setText("Greeting");
            icon.setImageResource(R.drawable.notification8);
        }else if(statusStr.equals("9")){
            status.setText("Information");
            icon.setImageResource(R.drawable.notification9);
        }else if(statusStr.equals("10")){
            status.setText("Order Placed");
            icon.setImageResource(R.drawable.notification10);
        }

        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
