package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.Rest.APIInterface;
import com.cs.drcafe.Rest.ApiClient;
import com.cs.drcafe.adapter.CreditCardsAdapter;
import com.cs.drcafe.adapter.OrderHistoryAdapterArabic;
import com.cs.drcafe.hyperpay.CardRegistrationStatusRequestAsyncTask;
import com.cs.drcafe.hyperpay.CardRegistrationStatusRequestListener;
import com.cs.drcafe.hyperpay.CheckoutIdRequestAsyncTask;
import com.cs.drcafe.hyperpay.CheckoutIdRequestListener;
import com.cs.drcafe.model.CardRegistration;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.MyCards;
import com.cs.drcafe.model.SavedCards;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.checkout.meta.CheckoutStorePaymentDetailsMode;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.exception.PaymentException;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;
import com.oppwa.mobile.connect.service.ConnectService;
import com.oppwa.mobile.connect.service.IProviderBinder;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by brahmam on 19/2/16.
 */
public class CreditCardsFragment extends Fragment implements CheckoutIdRequestListener,
        CardRegistrationStatusRequestListener {

    private String SOAP_ACTION = "http://tempuri.org/GetCreditCardDetails";
    private String METHOD_NAME = "GetCreditCardDetails";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    private ProgressDialog progressDialog;
    private String cardRegistrationToken = "";

    private String SOAP_ACTION_SETCARD = "http://tempuri.org/InsertCreditCardDetails";
    private String METHOD_NAME_SETCARD = "InsertCreditCardDetails";
    private SoapObject getCardResult;
    private String getCardResponse = null;

    private ArrayList<MyCards.Data> giftsList = new ArrayList<>();
    private ListView giftsListView;
    private CreditCardsAdapter mAdapter;
    private OrderHistoryAdapterArabic mAdapterArabic;
    TextView noCardsAlert;
    Button backBtn, addCard;
    SharedPreferences userPrefs;
    String userId, userFullName, userEmail, userPhone;
    TextView title;
    ProgressDialog dialog;
    String language;
    String CheckoutId = "";
    String merchantId = "";
    String customerEmailId = "";
    private CardRegistration myRegistrationCardDetails = new CardRegistration();

    private static final String STATE_RESOURCE_PATH = "STATE_RESOURCE_PATH";

    protected IProviderBinder providerBinder;
    protected String resourcePath;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CreditCardsFragment.this.onServiceConnected(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            providerBinder = null;
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        View rootView = inflater.inflate(R.layout.my_cards, container,
                false);
        Log.d("lifecycle", "cards fragment onCreateView: ");
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        String response1 = userPrefs.getString("user_profile", null);
        try {
            JSONObject  property = new JSONObject(response1);
            JSONObject userObjuect = property.getJSONObject("profile");

            userEmail = userObjuect.getString("username");
            userPhone = userObjuect.getString("phone");
            userFullName = userObjuect.getString("fullName");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        noCardsAlert = (TextView) rootView.findViewById(R.id.no_cards);
        giftsListView = (ListView) rootView.findViewById(R.id.cards_list_view);
        title = (TextView) rootView.findViewById(R.id.title);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        addCard = (Button) rootView.findViewById(R.id.add_card_btn);

        if(language.equalsIgnoreCase("Ar")){
            title.setText("البطاقات الائتمانية");
            addCard.setText("اضافة بطاقة");
        }
        generateMerchantId();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    requestCheckoutId(DrConstants.SHOPPER_RESULT_URL);
                } catch (Exception e) {
                    Log.e("connect", "error creating the payment page", e);
                }
            }
        });
        new getAllSavedCards().execute();

        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CheckoutActivity.CHECKOUT_ACTIVITY) {
            switch (resultCode) {
                case CheckoutActivity.RESULT_OK:
                    /* Transaction completed. */
                    Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);

                    resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);
                    Log.d("TAG", "onActivityResult: "+resourcePath);

                    /* Check the transaction type. */
                    if (transaction.getTransactionType() == TransactionType.SYNC) {
                        /* Check the status of synchronous transaction. */
                        requestPaymentStatus(resourcePath);
                    } else {
                        /* The on onNewIntent method may be called before onActivityResult
                           if activity was destroyed in the background, so check
                           if the intent already has the callback scheme */
                        if (hasCallbackScheme(getActivity().getIntent())) {
                            requestPaymentStatus(resourcePath);
                        } else {
                            /* The on onNewIntent method wasn't called yet,
                               wait for the callback. */
                            showProgressDialog(R.string.progress_message_please_wait);
                        }
                    }

                    break;
                case CheckoutActivity.RESULT_CANCELED:
                    hideProgressDialog();

                    break;
                case CheckoutActivity.RESULT_ERROR:
                    PaymentError error = data.getParcelableExtra(
                            CheckoutActivity.CHECKOUT_RESULT_ERROR);
                    Log.d("TAG", "onActivityResult: error "+error.getErrorInfo());
                    Log.d("TAG", "onActivityResult: error "+error.getErrorMessage());
                    Log.d("TAG", "onActivityResult: error "+error.getErrorCode());
                    showAlertDialog(R.string.error_message);
            }
        }
    }

    protected boolean hasCallbackScheme(Intent intent) {
        String scheme = intent.getScheme();

        return DrConstants.SHOPPER_RESULT_URL.equals(scheme+"://result");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("lifecycle", "cards fragment onStart: ");
        Intent intent = new Intent(getContext(), ConnectService.class);

        getContext().startService(intent);
        getContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("lifecycle", "cards fragment onStop: ");
        getContext().unbindService(serviceConnection);
        getContext().stopService(new Intent(getContext(), ConnectService.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(STATE_RESOURCE_PATH, resourcePath);
    }

    protected void onServiceConnected(IBinder service) {
        providerBinder = (IProviderBinder) service;

        /* We have a connection to the service. */
        try {
            /* Initialize Payment Provider with test mode. */
            providerBinder.initializeProvider(Connect.ProviderMode.LIVE);
        } catch (PaymentException e) {
            e.printStackTrace();
            showAlertDialog(R.string.error_message);
        }
    }

    protected void requestCheckoutId(String callbackScheme) {
        showProgressDialog(R.string.progress_message_checkout_id);

        new CheckoutIdRequestAsyncTask(this)
                .execute("0.00", DrConstants.SHOPPER_RESULT_URL, "true",
                        merchantId, userEmail, userId, userPhone, userFullName);
    }

    @Override
    public void onCheckoutIdReceived(String checkoutId) {
        hideProgressDialog();

        if (checkoutId == null) {
            Log.d("TAG", "onCheckoutIdReceived: ");
            showAlertDialog(R.string.error_message);
        }

        if (checkoutId != null) {
            CheckoutId = checkoutId;
            openCheckoutUI(checkoutId);
        }
    }

    private void openCheckoutUI(String checkoutId) {
        CheckoutSettings checkoutSettings = createCheckoutSettings(checkoutId);

        /* Set up the Intent and start the checkout activity. */
        Intent intent = new Intent(getContext(), CheckoutActivity.class);
        intent.putExtra(CheckoutActivity.CHECKOUT_SETTINGS, checkoutSettings);

        startActivityForResult(intent, CheckoutActivity.CHECKOUT_ACTIVITY);
    }

    @Override
    public void onErrorOccurred() {
        hideProgressDialog();
        Log.d("TAG", "onErrorOccurred: ");
        showAlertDialog(R.string.error_message);
    }

    @Override
    public void onCardRegistrationStatusReceived(CardRegistration paymentStatus) {
        Log.d("TAG", "onCardRegistrationStatusReceived: "+paymentStatus);
        hideProgressDialog();
        myRegistrationCardDetails = paymentStatus;
//        new saveCardToDb().execute();
        new getAllSavedCards().execute();
    }

    protected void requestPaymentStatus(String resourcePath) {
        showProgressDialog(R.string.progress_message_payment_status);
        Log.d("TAG", "requestPaymentStatus: "+resourcePath);
        resourcePath = resourcePath.replace("registration","payment");
        Log.d("TAG", "requestPaymentStatus: "+resourcePath);
        new CardRegistrationStatusRequestAsyncTask(this).execute(resourcePath);
    }

    /**
     * Creates the new instance of {@link CheckoutSettings}
     * to instantiate the {@link CheckoutActivity}.
     *
     * @param checkoutId the received checkout id
     * @return the new instance of {@link CheckoutSettings}
     */
    protected CheckoutSettings createCheckoutSettings(String checkoutId) {
        return new CheckoutSettings(checkoutId, DrConstants.Config.PAYMENT_BRANDS)
                .setStorePaymentDetailsMode(CheckoutStorePaymentDetailsMode.PROMPT)
                .setShopperResultUrl(DrConstants.SHOPPER_RESULT_URL)
                .setWindowSecurityEnabled(false);
    }

    protected void showProgressDialog(int messageId) {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setCancelable(false);
        }

        progressDialog.setMessage(getString(messageId));
        progressDialog.show();
    }

    protected void hideProgressDialog() {
        if (progressDialog == null) {
            return;
        }

        progressDialog.dismiss();
    }

    protected void showAlertDialog(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, null)
                .setCancelable(false)
                .show();
    }

    protected void showAlertDialog(int messageId) {
        showAlertDialog(getString(messageId));
    }

    public void generateMerchantId(){
        SimpleDateFormat dateFormater = new SimpleDateFormat("ddMMyyyy", Locale.US);
        SimpleDateFormat timeFormater = new SimpleDateFormat("HHmm", Locale.US);

        String dateStr = dateFormater.format(System.currentTimeMillis());
        String timeStr = timeFormater.format(System.currentTimeMillis());

        merchantId = "0" + dateStr + userId + timeStr;
        Log.d("TAG", "generateMerchantId: "+merchantId);

        String response = userPrefs.getString("user_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                String parts[] = userObjuect.getString("phone").split("-");

                String countryCode = parts[0].replace("+","");
                String mobile = parts[1];

                customerEmailId = countryCode+mobile+ "@drcafe.com";

                Log.d("TAG", "generateMerchantId: "+customerEmailId);


            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }

    private class getAllSavedCards extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetCardsJSON();
            showProgressDialog(R.string.progress_message_please_wait);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SavedCards> call = apiService.GetAllSavedCards(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SavedCards>() {
                @Override
                public void onResponse(Call<SavedCards> call, Response<SavedCards> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        SavedCards orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                hideProgressDialog();
                                mAdapter = new CreditCardsAdapter(getContext(), orderItems.getData(), language, getActivity());
                                giftsListView.setAdapter(mAdapter);
                            } else {
                                hideProgressDialog();
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    showAlertDialog(failureResponse);
                                } else {
                                    String failureResponse = orderItems.getMessageAr();
                                    showAlertDialog(failureResponse);
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            hideProgressDialog();
                            if (language.equalsIgnoreCase("En")) {
                                showAlertDialog("Cannot reach server");
                            } else {
                                showAlertDialog("Cannot reach server");
                            }
                        }
                    } else {
                        hideProgressDialog();
                        showAlertDialog("Cannot reach server");
                    }
                }

                @Override
                public void onFailure(Call<SavedCards> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        showAlertDialog("Please check internet connection");
                    } else {
                        showAlertDialog("Cannot reach server");
                    }

                    hideProgressDialog();
                }
            });
            return null;
        }

        private String prepareGetCardsJSON() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("userId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }
}
