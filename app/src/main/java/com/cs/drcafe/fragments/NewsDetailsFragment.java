package com.cs.drcafe.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

/**
 * Created by brahmam on 5/1/16.
 */
public class NewsDetailsFragment extends Fragment {

    private TextView newsHeader, newsDesc;
    private ImageView newsImage;
    private Button backBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_details, container,
                false);

        String header = getArguments().getString("news_header");
        String desc = getArguments().getString("news_desc");
        String image = getArguments().getString("news_image");
        newsHeader = (TextView) rootView.findViewById(R.id.news_header);
        newsDesc = (TextView) rootView.findViewById(R.id.news_desc);
        newsImage = (ImageView) rootView.findViewById(R.id.news_image);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);

        newsHeader.setText(header);
        newsDesc.setText(Html.fromHtml(desc));
        if(getActivity() != null){
            Glide.with(getActivity()).load("http://www.dr-cafe.com.sa/drcafesa/" + image).into(newsImage);
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
