package com.cs.drcafe.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;


/**
 * Created by brahmam on 4/12/15.
 */
public class MoreWebView extends Fragment {
    Button backBtn;
    TextView screenTitle;
    private ProgressBar mProgressBar;
    WebView wv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.more_webview, container,
                false);
        wv = (WebView) rootView.findViewById(R.id.webView);
        screenTitle = (TextView) rootView.findViewById(R.id.webview_title);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar1);
        mProgressBar.setVisibility(View.VISIBLE);
        screenTitle.setText(getArguments().getString("title"));
        wv.loadUrl(getArguments().getString("url"));
        wv.setWebViewClient(new MyWebViewClient());
        wv.getSettings().setLoadsImagesAutomatically(true);
        if(getArguments().getString("url").equals("http://www.snapchat.com/add/drcafeksa")) {
            wv.getSettings().setJavaScriptEnabled(false);
        }
        else{
            wv.getSettings().setJavaScriptEnabled(true);
        }
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return rootView;
    }

//    private class MyBrowser extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
//            return true;
//        }
//    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }


    @Override
    public void onPause() {
        super.onPause();
        wv.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        wv.onResume();
    }
}
