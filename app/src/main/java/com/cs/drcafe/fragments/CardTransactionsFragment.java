package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.CardTransactionsAdapter;
import com.cs.drcafe.adapter.CardTransactionsAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.Transactions;
import com.cs.drcafe.widget.CustomListView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by SKT on 26-12-2015.
 */
public class CardTransactionsFragment extends Fragment {

    private String SOAP_ACTION = "http://tempuri.org/getDCCardTransactions";
    private String METHOD_NAME = "getDCCardTransactions";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    private ArrayList<Transactions> transactionsList = new ArrayList<>();
    TextView cardBalance;
    private CustomListView transactionsListView;
    private CardTransactionsAdapter mAdapter;
    private CardTransactionsAdapterArabic mAdapterArabic;
    Button backBtn;
    View rootView;
    String language;
//    SharedPreferences cardPrefs;
//    String savedCards;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.card_transactions, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.card_transactions_arabic, container,
                    false);
        }

//        cardPrefs = getActivity().getSharedPreferences("CARD_PREFS", Context.MODE_PRIVATE);
//
//        savedCards = cardPrefs.getString("saved_cards", null);
        cardBalance = (TextView) rootView.findViewById(R.id.card_balance);
        transactionsListView = (CustomListView) rootView.findViewById(R.id.transaction_listview);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        if(language.equalsIgnoreCase("En")) {
            mAdapter = new CardTransactionsAdapter(getActivity(), transactionsList);
            transactionsListView.setAdapter(mAdapter);
        }else if(language.equalsIgnoreCase("Ar")){
            mAdapterArabic = new CardTransactionsAdapterArabic(getActivity(), transactionsList);
            transactionsListView.setAdapter(mAdapterArabic);
        }
        


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        cardBalance.setText(DrConstants.LATEST_BALANCE);
        new GetCardTransactions().execute();
        return rootView;
    }

    public class GetCardTransactions extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;

        @Override
        protected void onPreExecute() {

//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                request.addProperty("dcCardNo", DrConstants.LATEST_CARD);

                // request.addProperty("", ""); // incase you need to pass
                // parameters to the web-service

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                androidHttpTransport.call(SOAP_ACTION, envelope);
                result = (SoapObject) envelope.getResponse();
                response12 = result.toString();
                Log.v("TAG", response12);
                // SoapObject root = (SoapObject) result.getProperty(0);
                System.out.println("********Count : " + result.getPropertyCount());

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Responce", "" + response12);

            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (response12.equals("anyType{}")) {
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
            } else {

                try {

                    Object property = result.getProperty(1);
                    if (property instanceof SoapObject) {
                        try {
                            SoapObject category_list = (SoapObject) property;
                            SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                            for(int i=0; i<storeObject.getPropertyCount();i++) {
                                Transactions txn = new Transactions();
                                SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");
                                String storeName = sObj.getPropertyAsString("StoreName");
                                String transactionDate = sObj.getPropertyAsString("TransactionDate");
                                String checkTotal = sObj.getPropertyAsString("CheckTotal");

                                txn.setStoreName(storeName);
                                txn.setTransactionDate(transactionDate);
                                txn.setTotalBalance(checkTotal);

                                transactionsList.add(txn);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    }

                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }

                if(language.equalsIgnoreCase("En")) {
                    mAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
                    mAdapterArabic.notifyDataSetChanged();
                }

                super.onPostExecute(result1);
            }
        }
    }



    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
