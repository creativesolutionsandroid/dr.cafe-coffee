package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.SplashScreen;
import com.cs.drcafe.adapter.MyGiftsAdapter;
import com.cs.drcafe.adapter.OrderHistoryAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.MyGifts;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by brahmam on 19/2/16.
 */
public class MyGiftsFragment extends Fragment {

    private String SOAP_ACTION = "http://tempuri.org/GetUserGifts";
    private String METHOD_NAME = "GetUserGifts";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;

    private ArrayList<MyGifts> giftsList = new ArrayList<>();
    private ListView giftsListView;
    private MyGiftsAdapter mAdapter;
    private OrderHistoryAdapterArabic mAdapterArabic;
    Button backBtn;
    SharedPreferences userPrefs;
    String userId;
    TextView title;
    ProgressDialog dialog;
    String language;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        View rootView = inflater.inflate(R.layout.my_gifts, container,
                false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        giftsListView = (ListView) rootView.findViewById(R.id.myGiftsListView);
        title = (TextView) rootView.findViewById(R.id.title);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);

        if(language.equalsIgnoreCase("Ar")){
            title.setText("My Gifts");
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        new GetPromocodeResponse().execute();

        return rootView;
    }

    public class GetPromocodeResponse extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {

            try {

                if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                }else {
                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("Userid", userId);
                    request.addProperty("DeviceToken", SplashScreen.regid);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Responce", "" + response12);

            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(response12 == null){
                dialog.dismiss();

            }else if(response12.equalsIgnoreCase("no internet")){
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Communication Error! Please check the internet connection?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            } else {
                dialog.dismiss();
                try {

                    Object property = result.getProperty(1);
                    if (property instanceof SoapObject) {
                        try {
                            SoapObject category_list = (SoapObject) property;
                            if(category_list.getPropertyCount()>0) {
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    MyGifts myGifts = new MyGifts();
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                    String desc = sObj.getPropertyAsString("Desc_En");
                                    String descAr = sObj.getPropertyAsString("Desc_Ar");
                                    String remainingBonus = sObj.getPropertyAsString("RemainingBonus");
                                    String endDate = sObj.getPropertyAsString("PEndDate");
                                    String image = sObj.getPropertyAsString("Img");
                                    String promoType = sObj.getPropertyAsString("PromoType");

                                    myGifts.setDesc_En(desc);
                                    myGifts.setDesc_Ar(descAr);
                                    myGifts.setRemainingBonus(remainingBonus);
                                    myGifts.setEndDate(endDate);
                                    myGifts.setImage(image);
                                    myGifts.setPromoType(promoType);

                                    giftsList.add(myGifts);
                                }

                                mAdapter = new MyGiftsAdapter(getContext(), giftsList, language);
                                giftsListView.setAdapter(mAdapter);

                            }else{
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                                String msg;
                                if(language.equalsIgnoreCase("En")){
                                    msg = "There is no gifts for you";
                                }
                                else{
                                    msg = "لا توجد هدايا لك";
                                }
                                // set title
                                alertDialogBuilder.setTitle("dr.CAFE");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(msg)
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    }

                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }
            }

//            mPromoAdapter.notifyDataSetChanged();


            super.onPostExecute(result1);
        }
    }




    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

}
