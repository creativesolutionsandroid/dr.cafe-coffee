package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.MyAddressAdapter;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.MyAddress;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

public class MyAddressActivity extends Fragment implements View.OnClickListener{

    private ImageView buttonAddAddress;
    private ArrayList<MyAddress> myAddressArrayList = new ArrayList<>();
    private MyAddressAdapter mAddressAdapter;
    private SwipeMenuListView addressList;
    private TextView addressAlert;
    ProgressDialog dialog;
    Toolbar toolbar;
    String userId;
    SharedPreferences userPrefs;
    private static String TAG = "TAG";
    public static int ADD_ADDRESS_INTENT = 1;
    String language;
    View rootView;

    private String SOAP_ACTION_DELETE = "http://tempuri.org/DeleteUserAddress";
    private String METHOD_NAME_DELETE = "DeleteUserAddress";

    private String SOAP_ACTION = "http://tempuri.org/GetAllUserAddress";
    private String METHOD_NAME = "GetAllUserAddress";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;

    SharedPreferences storePrefs;
    SharedPreferences.Editor storePrefsEditor;
    Boolean storeDetails = false;
    ImageView backBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.activity_my_address, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.activity_my_address_ar, container,
                    false);
        }

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");

        try {
            storeDetails = getArguments().getBoolean("store_details");
        } catch (Exception e) {
            e.printStackTrace();
            storeDetails = false;
        }

        buttonAddAddress = (ImageView) rootView.findViewById(R.id.add_address);
        addressAlert = (TextView) rootView.findViewById(R.id.no_address_alert);
        addressList = (SwipeMenuListView) rootView.findViewById(R.id.address_list);
        backBtn = (ImageView) rootView.findViewById(R.id.back_btn);

        storePrefs = getContext().getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);
        storePrefsEditor = storePrefs.edit();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        addressList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (isDeliverable(myAddressArrayList.get(i).getLatitude(), myAddressArrayList.get(i).getLongitude())) {
                    DrConstants.addressId = myAddressArrayList.get(i).getAddressid();
                    DrConstants.addressHouseNo = myAddressArrayList.get(i).getHouseno();
                    DrConstants.address = myAddressArrayList.get(i).getAddress();
                    DrConstants.delLatitude = "" + myAddressArrayList.get(i).getLatitude();
                    DrConstants.delLongitude = "" + myAddressArrayList.get(i).getLatitude();

                    storePrefsEditor.putString("addressId", myAddressArrayList.get(i).getAddressid());
                    storePrefsEditor.putString("addressHouseNo", myAddressArrayList.get(i).getHouseno());
                    storePrefsEditor.putString("address", myAddressArrayList.get(i).getAddress());
                    storePrefsEditor.putString("delLatitude", "" + myAddressArrayList.get(i).getLatitude());
                    storePrefsEditor.putString("delLongitude", "" + myAddressArrayList.get(i).getLatitude());
                    storePrefsEditor.commit();
                    DrConstants.menuFlag = true;

                    Log.d(TAG, "onItemClick: "+storeDetails);
                    if (!storeDetails) {
                        Fragment newFragment = new MenuFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();

                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack
                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        transaction.add(R.id.realtabcontent, newFragment, "menu");
                        transaction.hide(MyAddressActivity.this);
                        transaction.addToBackStack(null);

                        // Commit the transaction
                        transaction.commit();
                    } else {
                        ((MainActivity) getActivity()).setCurrenTab(2);
                    }
                }
            }
        });

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {


                // create "edit" item
                SwipeMenuItem editItem = new SwipeMenuItem(
                        getContext());
                // set item background
                editItem.setBackground(new ColorDrawable(Color.rgb(0x25,
                        0xAE, 0x88)));
                // set item width
                editItem.setWidth(dp2px(90));

                editItem.setTitle("Edit");
                // set item title fontsize
                editItem.setTitleSize(18);
                // set item title font color
                editItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(editItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        addressList.setMenuCreator(creator);

        // step 2. listener item click event
        addressList.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        Intent intent = new Intent(getContext(), AddAddressActivity.class);
                        intent.putExtra("Address", myAddressArrayList.get(position).getAddress());
                        intent.putExtra("HouseNo", myAddressArrayList.get(position).getHouseno());
                        intent.putExtra("id", myAddressArrayList.get(position).getAddressid());
                        intent.putExtra("edit", true);
                        intent.putExtra("lat", myAddressArrayList.get(position).getLatitude());
                        intent.putExtra("lng", myAddressArrayList.get(position).getLongitude());
                        startActivity(intent);
                        break;
                    case 1:
                        new DeleteAddress().execute(myAddressArrayList.get(position).getAddressid());
                        break;
                }
                return false;
            }
        });

        buttonAddAddress.setOnClickListener(this);

        return rootView;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_address:
                Intent intent = new Intent(getContext(), AddAddressActivity.class);
                startActivityForResult(intent, ADD_ADDRESS_INTENT);
                break;
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public class GetAddressList extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        ProgressDialog dialog;
        private SoapObject result;
        private String response12 = null;
        @Override
        protected void onPreExecute() {
            myAddressArrayList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("UserId", userId);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    if(response12 != null) {
                        if (response12.equals("anyType{}")) {
                            dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                        } else {

                            try {

                                Object property = result.getProperty(1);
                                if (property instanceof SoapObject) {
                                    try {
                                        SoapObject category_list = (SoapObject) property;
                                        SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                        for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                            MyAddress addressDetails = new MyAddress();
                                            SoapObject sObj = (SoapObject) storeObject.getProperty(i);

//                                            if (!sObj.getPropertyAsString("itemTypeId").equals("0")) {
                                            String Id = sObj.getPropertyAsString("Id");
                                            String HouseNo = sObj.getPropertyAsString("HouseNo");
                                            String Address = sObj.getPropertyAsString("Address");
                                            String Latitude = sObj.getPropertyAsString("Latitude");
                                            String Longitude = sObj.getPropertyAsString("Longitude");

                                            addressDetails.setAddressid(Id);
                                            addressDetails.setHouseno(HouseNo);
                                            addressDetails.setAddress(Address);
                                            addressDetails.setLatitude(Double.parseDouble(Latitude));
                                            addressDetails.setLongitude(Double.parseDouble(Longitude));

                                            myAddressArrayList.add(addressDetails);
                                        }

                                        if(myAddressArrayList.size() > 0) {
                                            mAddressAdapter = new MyAddressAdapter(getContext(), myAddressArrayList, language);
                                            addressList.setAdapter(mAddressAdapter);
                                            addressList.setVisibility(View.VISIBLE);
                                            addressAlert.setVisibility(View.GONE);
                                        }
                                        else {
                                            addressList.setVisibility(View.GONE);
                                            addressAlert.setVisibility(View.VISIBLE);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                Log.d("", "Error while parsing the results!");
                                e.printStackTrace();
                            }

                            dialog.dismiss();
                            super.onPostExecute(result1);
                        }
                    }
                }
            }else{
                dialog.dismiss();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new GetAddressList().execute();
    }

    public class DeleteAddress extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus;
        ProgressDialog dialog;
        private SoapObject result;
        private String response12 = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = ProgressDialog.show(getContext(), "",
                    "Loading. Please Wait....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_DELETE);
                    request.addProperty("UserId", userId);
                    request.addProperty("Id", arg0[0]);

                    Log.d(TAG, "doInBackground: "+request);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_DELETE, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    dialog.dismiss();
                    Toast.makeText(getContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    if(response12 != null) {
                        if (response12.equals("anyType{}")) {
                            dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                        } else {

                            try {

                                Object property = result.getProperty(1);
                                if (property instanceof SoapObject) {
                                    try {
                                        SoapObject category_list = (SoapObject) property;
                                        SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                        for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                            SoapObject sObj = (SoapObject) storeObject.getProperty(i);

                                            new GetAddressList().execute();

                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                Log.d("", "Error while parsing the results!");
                                e.printStackTrace();
                            }

                            dialog.dismiss();
                            super.onPostExecute(result1);
                        }
                    }
                }
            }else{
                dialog.dismiss();
            }
        }
    }
    private boolean isDeliverable(double lat, double longi){
        double storeLat = getArguments().getDouble("lat");
        double storeLng = getArguments().getDouble("lng");
        float delDistance = getArguments().getFloat("delDistance");

        Location me   = new Location("");
        Location dest = new Location("");

        me.setLatitude(lat);
        me.setLongitude(longi);

        dest.setLatitude(storeLat);
        dest.setLongitude(storeLng);

        float dist = (me.distanceTo(dest))/1000;

//        Log.d(TAG, "dist: "+dist);
//        Log.d(TAG, "delDistance: "+delDistance);

        if (dist > delDistance) {
//        if (dist > 50000) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());

            if(language.equalsIgnoreCase("En")) {
                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Delivery location out of coverage area.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            }else if(language.equalsIgnoreCase("Ar")){
                // set title
                alertDialogBuilder.setTitle("د. كيف");

                // set dialog message
                alertDialogBuilder
                        .setMessage("العنوان المحدد خارج خدمة التوصيل")
                        .setCancelable(false)
                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            }

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
            return false;
        }
        else {
            return true;
        }
    }

}
