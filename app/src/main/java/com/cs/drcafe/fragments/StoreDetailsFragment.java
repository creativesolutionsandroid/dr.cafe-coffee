package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.JSONParser;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by BRAHMAM on 04-12-2015.
 */
public class StoreDetailsFragment extends Fragment implements OnMapReadyCallback{
    private GoogleMap map;
    MarkerOptions markerOptions;
    private String SOAP_ACTION = "http://tempuri.org/getStoreTimings";
    private String METHOD_NAME = "getStoreTimings";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;

    private String SOAP_ACTION_TIME = "http://tempuri.org/getCurrentTime";
    private String METHOD_NAME_TIME = "getCurrentTime";
    private SoapPrimitive timeResult;
    private String timeResponse = null;

    ProgressDialog dialog;
    AlertDialog alertDialog;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private String distanceResponse = null;

    private String storeId, storeName, storeAddress, storePhoneNumber, stTime, edTime;
    private Double latitude, longitude;
    private double lat, longi;
    private boolean is24x7, isOnline, isDeliverable;
    private TextView phoneNo, storeNameTextView, storeAddressTextView, storeHoursTextView,
            inviteFriends, rateTheStore, getDirectionsText, favoriteStoreText, travelTimeText, orderNowText;
    private Button backButton;
    private ImageView favImage;
    private boolean isFavoriteStore;
    private ArrayList<String> storeHours = new ArrayList<>();
    SharedPreferences favouritePrefs;
    SharedPreferences.Editor favEditor;
    ArrayList<String> favoriteStores = new ArrayList<>();
    String favStoreIds;
    String language;
    View rootView;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.store_details, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.store_details_arabic, container,
                    false);
        }

        storeId = getArguments().getString("storeId");
        storeName = getArguments().getString("storeName");
        storeAddress = getArguments().getString("storeAddress");
        latitude = getArguments().getDouble("latitude");
        longitude = getArguments().getDouble("longitude");
        lat = getArguments().getDouble("lat");
        longi = getArguments().getDouble("longi");
        stTime = getArguments().getString("stTime");
        edTime = getArguments().getString("edTime");
        is24x7 = getArguments().getBoolean("is24x7");
        isOnline = getArguments().getBoolean("is_online");
        isDeliverable = getArguments().getBoolean("isDeliverable");

        phoneNo = (TextView) rootView.findViewById(R.id.store_detail_call);
        storeNameTextView = (TextView) rootView.findViewById(R.id.store_detail_name);
        storeAddressTextView = (TextView) rootView.findViewById(R.id.store_detail_address);
        storeHoursTextView = (TextView) rootView.findViewById(R.id.store_detail_hours);
        inviteFriends = (TextView) rootView.findViewById(R.id.store_detail_invite);
        rateTheStore = (TextView) rootView.findViewById(R.id.store_detail_rate);
        getDirectionsText = (TextView) rootView.findViewById(R.id.store_detail_directions);
        favoriteStoreText = (TextView) rootView.findViewById(R.id.store_detail_favorite);
        travelTimeText = (TextView) rootView.findViewById(R.id.store_travel_time);
        orderNowText = (TextView) rootView.findViewById(R.id.ordernow_btn);
        favImage = (ImageView) rootView.findViewById(R.id.img15);
        backButton = (Button) rootView.findViewById(R.id.back_btn);

        favouritePrefs = getActivity().getSharedPreferences("FAV_PREFS", Context.MODE_PRIVATE);
        favEditor  = favouritePrefs.edit();

        favStoreIds = favouritePrefs.getString("favourite_store_ids",null);
        if(favStoreIds != null){
            String[] parts = favStoreIds.split(",");
            for(int i= 0;i < parts.length;i++){
                favoriteStores.add(parts[i]);
            }
        }

        if(favoriteStores.contains(storeId)){
            isFavoriteStore = true;
            if(language.equalsIgnoreCase("En")) {
                favoriteStoreText.setText("Remove from Favourite Store");
            }else if(language.equalsIgnoreCase("Ar")){
                favoriteStoreText.setText("حذف من قائمة الفروع المفضل");
            }

            favImage.setImageResource(R.drawable.store_details_fav_selected);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(StoreDetailsFragment.this);

//        FragmentManager fm = getChildFragmentManager();
//        Fragment fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
//        if (fragment == null) {
//            fragment = SupportMapFragment.newInstance();
//            fm.beginTransaction().replace(R.id.map, fragment).commit();
//        }
//        map = ((SupportMapFragment) fragment).getMap();
//        markerOptions = new MarkerOptions();
//        markerOptions.position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
//        map.addMarker(markerOptions);
//        map.animateCamera(CameraUpdateFactory.newLatLngZoom
//                (new LatLng(latitude, longitude), 7.0f));
        storeNameTextView.setText(storeName);
        storeAddressTextView.setText(storeAddress);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().onBackPressed();

//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//
//                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
//                transaction.addToBackStack(null);

                // Commit the transaction
//                transaction.commit();
            }
        });

        storeHoursTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = new StoreHoursFragment();
                Bundle mBundle = new Bundle();
                mBundle.putStringArrayList("open_hours", storeHours);
                newFragment.setArguments(mBundle);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.add(R.id.realtabcontent, newFragment);
                transaction.hide(StoreDetailsFragment.this);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });



        phoneNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                    startActivity(intent);
                }

            }
        });

        inviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "I would love to have a Cup of coffee with you at dr.CAFE Coffee In " + storeName +
                                ".\n http://maps.google.com/maps?saddr="+ lat + "," + longi+"&daddr=" + latitude + "," + longitude);
//                intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
//                        "");
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        rateTheStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = new RateStoreFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.add(R.id.realtabcontent, newFragment);
                transaction.hide(StoreDetailsFragment.this);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });

        getDirectionsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Uri gmmIntentUri = Uri.parse("google.navigation:q=17.4474905,78.3412107");
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                mapIntent.setPackage("com.google.android.apps.maps");
//                startActivity(mapIntent);


                Uri gmmIntentUri = Uri.parse("google.navigation:q="+ latitude + "," + longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);


//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+ lat + "," + longi+"&daddr=" + latitude + "," + longitude));
//                startActivity(intent);
            }
        });

        favoriteStoreText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFavoriteStore) {
                    favoriteStores.remove(storeId);
                    favStoreIds = null;
                    for (int i=0;i<favoriteStores.size();i++){
                        if(favStoreIds == null){
                            favStoreIds = favoriteStores.get(0);
                        }else{
                            favStoreIds = favStoreIds+","+favoriteStores.get(i);
                        }
                    }
                    Log.i("TAG", "" + favStoreIds);
                    favEditor.putString("favourite_store_ids", favStoreIds);
                    favEditor.commit();

                    isFavoriteStore = false;
                    if(language.equalsIgnoreCase("En")) {
                        favoriteStoreText.setText("Add as a Favourite Store");
                    }else if(language.equalsIgnoreCase("Ar")){
                        favoriteStoreText.setText("اضف كفرع مفضل");
                    }

                    favImage.setImageResource(R.drawable.store_details_fav_unselected);
                }else{
                    favStoreIds = null;
                    if(!favoriteStores.contains(storeId)){
                        favoriteStores.add(storeId);
                    }

                    for (int i=0;i<favoriteStores.size();i++){
                        if(favStoreIds == null){
                            favStoreIds = favoriteStores.get(0);
                        }else{
                            favStoreIds = favStoreIds+","+favoriteStores.get(i);
                        }
                    }
//                    Log.i("TAG",favStoreIds);

                    favEditor.putString("favourite_store_ids",favStoreIds);
                    favEditor.commit();
                    isFavoriteStore = true;
                    if(language.equalsIgnoreCase("En")) {
                        favoriteStoreText.setText("Remove from Favourite Store");
                    }else if(language.equalsIgnoreCase("Ar")){
                        favoriteStoreText.setText("حذف من قائمة الفروع المفضل");
                    }
                    favImage.setImageResource(R.drawable.store_details_fav_selected);
                }
            }
        });

        orderNowText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline) {
                    if (getArguments().getBoolean("ispickUp") || getArguments().getBoolean("isDineIn")
                            || getArguments().getBoolean("isDeliverable")) {
                        if (!is24x7) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.store_select_dialog, null);
                            dialogBuilder.setView(dialogView);

                            ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                            ImageView pickup = (ImageView) dialogView.findViewById(R.id.select_pickup);
                            ImageView dineIn = (ImageView) dialogView.findViewById(R.id.select_dinein);
                            ImageView pickupBig = (ImageView) dialogView.findViewById(R.id.select_pickup_big);
                            RelativeLayout pickupLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_layout);
                            RelativeLayout dineinLayout = (RelativeLayout) dialogView.findViewById(R.id.dinein_layout);
                            RelativeLayout pickupBigLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_big_layout);
                            RelativeLayout deliveryLayout = (RelativeLayout) dialogView.findViewById(R.id.delivery_layout);
                            TextView deliveryTv = (TextView) dialogView.findViewById(R.id.delivery_txt);
                            TextView storeMsg = (TextView) dialogView.findViewById(R.id.store_msg);
                            TextView pickupTv = (TextView) dialogView.findViewById(R.id.pickup_txt);
                            TextView dineInTv = (TextView) dialogView.findViewById(R.id.dinein_txt);
                            TextView pickupBigTv = (TextView) dialogView.findViewById(R.id.pickup_big_txt);
                            TextView heading = (TextView) dialogView.findViewById(R.id.order_type_heading);
                            pickupBigLayout.setVisibility(View.GONE);
                            if (language.equalsIgnoreCase("En")) {
                                pickupTv.setText("Pick Up");
                                dineInTv.setText("Dine In");
                                pickupBigTv.setText("Pick Up");
                                heading.setText("Order Type");
                                deliveryTv.setText(getArguments().getString("getDeliveryMessageEn"));
                                if (getArguments().getString("message") == null) {
                                    storeMsg.setVisibility(View.GONE);
                                } else {
                                    storeMsg.setVisibility(View.VISIBLE);
                                    storeMsg.setText(getArguments().getString("message"));
                                }
                            } else if (language.equalsIgnoreCase("Ar")) {
                                pickupTv.setText("الاستمتاع خارج الفرع");
                                dineInTv.setText("الاستمتاع في الفرع");
                                pickupBigTv.setText("الاستمتاع خارج الفرع");
                                heading.setText("نوع الطلب");
                                deliveryTv.setText(getArguments().getString("getDeliveryMessageAr"));
                                if (getArguments().getString("messageAr") == null) {
                                    storeMsg.setVisibility(View.GONE);
                                } else {
                                    storeMsg.setVisibility(View.VISIBLE);
                                    storeMsg.setText(getArguments().getString("messageAr"));
                                }
                            }

                            if (getArguments().getBoolean("isDineIn")) {

                            } else {
                                dineinLayout.setVisibility(View.GONE);
                                pickupLayout.setVisibility(View.GONE);
                                pickupBigLayout.setVisibility(View.VISIBLE);
                            }

//                        if (isDeliverable) {
//                            deliveryLayout.setVisibility(View.VISIBLE);
//                        }
//                        else {
//                            deliveryLayout.setVisibility(View.GONE);
//                        }

                            pickup.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    DrConstants.orderNow = true;
                                    DrConstants.storeName = storeName;
                                    DrConstants.storeAddress = storeAddress;
                                    DrConstants.storeId = storeId;
                                    DrConstants.latitude = latitude;
                                    DrConstants.longitude = longitude;
                                    DrConstants.fullHours = is24x7;
                                    DrConstants.startTime = stTime;
                                    DrConstants.endTime = edTime;
                                    DrConstants.orderType = "Pick Up";
                                    alertDialog.dismiss();
                                    DrConstants.menuFlag = true;
                                    ((MainActivity) getActivity()).setCurrenTab(2);
                                }
                            });

                            pickupBig.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    DrConstants.orderNow = true;
                                    DrConstants.storeName = storeName;
                                    DrConstants.storeAddress = storeAddress;
                                    DrConstants.storeId = storeId;
                                    DrConstants.latitude = latitude;
                                    DrConstants.longitude = longitude;
                                    DrConstants.fullHours = is24x7;
                                    DrConstants.startTime = stTime;
                                    DrConstants.endTime = edTime;
                                    DrConstants.orderType = "Pick Up";
                                    alertDialog.dismiss();
                                    DrConstants.menuFlag = true;
                                    ((MainActivity) getActivity()).setCurrenTab(2);
                                }
                            });

                            deliveryLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    DrConstants.orderNow = true;
                                    DrConstants.storeName = storeName;
                                    DrConstants.storeAddress = storeAddress;
                                    DrConstants.storeId = storeId;
                                    DrConstants.latitude = latitude;
                                    DrConstants.longitude = longitude;
                                    DrConstants.fullHours = is24x7;
                                    DrConstants.startTime = stTime;
                                    DrConstants.endTime = edTime;
                                    DrConstants.orderType = "Pick Up";
                                    DrConstants.MinOrderAmount = getArguments().getFloat("MinOrderAmount");
                                    DrConstants.DeliveryCharge = getArguments().getFloat("DeliveryCharge");
                                    DrConstants.FreeDeliveryAbove = getArguments().getFloat("FreeDeliveryAbove");
                                    alertDialog.dismiss();
                                    DrConstants.menuFlag = true;

                                    Bundle args = new Bundle();
                                    args.putBoolean("store_details", true);
                                    args.putDouble("lat", latitude);
                                    args.putDouble("lng", longitude);
                                    args.putFloat("delDistance", getArguments().getFloat("delDistance"));
                                    Fragment newFragment = new MyAddressActivity();
                                    newFragment.setArguments(args);
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack
                                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                    transaction.add(R.id.realtabcontent, newFragment, "address");
                                    transaction.hide(StoreDetailsFragment.this);
                                    transaction.addToBackStack(null);

                                    // Commit the transaction
                                    transaction.commit();
                                }
                            });

                            dineIn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    DrConstants.orderNow = true;
                                    DrConstants.storeName = storeName;
                                    DrConstants.storeAddress = storeAddress;
                                    DrConstants.storeId = storeId;
                                    DrConstants.latitude = latitude;
                                    DrConstants.longitude = longitude;
                                    DrConstants.fullHours = is24x7;
                                    DrConstants.startTime = stTime;
                                    DrConstants.endTime = edTime;
                                    DrConstants.orderType = "Dine In";
                                    alertDialog.dismiss();
                                    DrConstants.menuFlag = true;
                                    ((MainActivity) getActivity()).setCurrenTab(2);
                                }
                            });

                            cancelBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                }
                            });

                            alertDialog = dialogBuilder.create();
                            alertDialog.show();

                            //Grab the window of the dialog, and change the width
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = alertDialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);

                        } else {

                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.store_select_dialog, null);
                            dialogBuilder.setView(dialogView);

                            ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                            ImageView pickup = (ImageView) dialogView.findViewById(R.id.select_pickup);
                            ImageView dineIn = (ImageView) dialogView.findViewById(R.id.select_dinein);
                            ImageView pickupBig = (ImageView) dialogView.findViewById(R.id.select_pickup_big);
                            RelativeLayout pickupLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_layout);
                            RelativeLayout dineinLayout = (RelativeLayout) dialogView.findViewById(R.id.dinein_layout);
                            RelativeLayout pickupBigLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_big_layout);
                            RelativeLayout deliveryLayout = (RelativeLayout) dialogView.findViewById(R.id.delivery_layout);
                            TextView deliveryTv = (TextView) dialogView.findViewById(R.id.delivery_txt);
                            TextView storeMsg = (TextView) dialogView.findViewById(R.id.store_msg);
                            TextView pickupTv = (TextView) dialogView.findViewById(R.id.pickup_txt);
                            TextView dineInTv = (TextView) dialogView.findViewById(R.id.dinein_txt);
                            TextView pickupBigTv = (TextView) dialogView.findViewById(R.id.pickup_big_txt);
                            TextView heading = (TextView) dialogView.findViewById(R.id.order_type_heading);
                            pickupBigLayout.setVisibility(View.GONE);
                            if (language.equalsIgnoreCase("En")) {
                                pickupTv.setText("Pick Up");
                                dineInTv.setText("Dine In");
                                pickupBigTv.setText("Pick Up");
                                heading.setText("Order Type");
                                deliveryTv.setText(getArguments().getString("getDeliveryMessageEn"));
                                if (getArguments().getString("message") == null) {
                                    storeMsg.setVisibility(View.GONE);
                                } else {
                                    storeMsg.setVisibility(View.VISIBLE);
                                    storeMsg.setText(getArguments().getString("message"));
                                }
                            } else if (language.equalsIgnoreCase("Ar")) {
                                pickupTv.setText("الاستمتاع خارج الفرع");
                                dineInTv.setText("الاستمتاع في الفرع");
                                pickupBigTv.setText("الاستمتاع خارج الفرع");
                                heading.setText("نوع الطلب");
                                deliveryTv.setText(getArguments().getString("getDeliveryMessageAr"));
                                if (getArguments().getString("messageAr") == null) {
                                    storeMsg.setVisibility(View.GONE);
                                } else {
                                    storeMsg.setVisibility(View.VISIBLE);
                                    storeMsg.setText(getArguments().getString("messageAr"));
                                }
                            }

                            dineinLayout.setVisibility(View.GONE);
                            pickupLayout.setVisibility(View.GONE);
                            pickupBigLayout.setVisibility(View.VISIBLE);

//                        if (isDeliverable) {
//                            deliveryLayout.setVisibility(View.VISIBLE);
//                        }
//                        else {
//                            deliveryLayout.setVisibility(View.GONE);
//                        }

                            pickupBig.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    DrConstants.orderNow = true;
                                    DrConstants.storeName = storeName;
                                    DrConstants.storeAddress = storeAddress;
                                    DrConstants.storeId = storeId;
                                    DrConstants.latitude = latitude;
                                    DrConstants.longitude = longitude;
                                    DrConstants.fullHours = is24x7;
                                    DrConstants.startTime = stTime;
                                    DrConstants.endTime = edTime;
                                    DrConstants.orderType = "Pick Up";
                                    alertDialog.dismiss();
                                    DrConstants.menuFlag = true;
                                    ((MainActivity) getActivity()).setCurrenTab(2);
                                }
                            });

                            deliveryLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    DrConstants.orderNow = true;
                                    DrConstants.storeName = storeName;
                                    DrConstants.storeAddress = storeAddress;
                                    DrConstants.storeId = storeId;
                                    DrConstants.latitude = latitude;
                                    DrConstants.longitude = longitude;
                                    DrConstants.fullHours = is24x7;
                                    DrConstants.startTime = stTime;
                                    DrConstants.endTime = edTime;
                                    DrConstants.orderType = "Pick Up";
                                    DrConstants.MinOrderAmount = getArguments().getFloat("MinOrderAmount");
                                    DrConstants.DeliveryCharge = getArguments().getFloat("DeliveryCharge");
                                    DrConstants.FreeDeliveryAbove = getArguments().getFloat("FreeDeliveryAbove");
                                    alertDialog.dismiss();
                                    DrConstants.menuFlag = true;

                                    Bundle args = new Bundle();
                                    args.putBoolean("store_details", true);
                                    args.putDouble("lat", latitude);
                                    args.putDouble("lng", longitude);
                                    args.putFloat("delDistance", getArguments().getFloat("delDistance"));
                                    Fragment newFragment = new MyAddressActivity();
                                    newFragment.setArguments(args);
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack
                                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                    transaction.add(R.id.realtabcontent, newFragment, "address");
                                    transaction.hide(StoreDetailsFragment.this);
                                    transaction.addToBackStack(null);

                                    // Commit the transaction
                                    transaction.commit();
                                }
                            });

                            cancelBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                }
                            });


                            alertDialog = dialogBuilder.create();
                            alertDialog.show();

                            //Grab the window of the dialog, and change the width
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = alertDialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                        }
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                getActivity());

                        if (language.equalsIgnoreCase("En")) {
                            // set title
                            alertDialogBuilder.setTitle("dr.CAFE");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Right Now, This store can't take any orders.")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        } else if (language.equalsIgnoreCase("Ar")) {
                            // set title
                            alertDialogBuilder.setTitle("د. كيف");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("هذا الفرع لايستقبل الطلبات في الوقت الحالي.")
                                    .setCancelable(false)
                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        }

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                }
            }
        });

        new GetCurrentTime().execute();
        new GetStoreDetails().execute();
        new getTrafficTime().execute();
        return rootView;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
        map.addMarker(markerOptions);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom
                (new LatLng(latitude, longitude), 7.0f));
    }


    public class GetStoreDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("storeId", storeId);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("Responce", "store details" + response12);
            }else{
                response12 = "no internet";
            }
            return response12;

        }

        @Override
        protected void onPostExecute(String result1) {
            if(response12 == null){

            } else if(response12.equals("no internet")){
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                try {

                    Object property = result.getProperty(1);
                    if (property instanceof SoapObject) {
                        try {
                            SoapObject category_list = (SoapObject) property;
                            SoapObject storeObject = (SoapObject) category_list.getProperty(0);

                            SoapObject sObj = (SoapObject) storeObject.getProperty(0);
                            storePhoneNumber = sObj.getPropertyAsString("phone");
                            phoneNo.setText("Call  " + sObj.getPropertyAsString("phone"));
                            if (sObj.getPropertyAsString("isSunClose").equalsIgnoreCase("true")) {
                                storeHours.add("Closed");
                            } else {
                                if(sObj.getPropertyAsString("is24x7").equalsIgnoreCase("true")){
                                    storeHours.add("24/7 open");
                                }else {
                                    storeHours.add(sObj.getPropertyAsString("sunST") + " to " + sObj.getPropertyAsString("sunET"));
                                }
                            }

                            if (sObj.getPropertyAsString("isMonClose").equalsIgnoreCase("true")) {
                                storeHours.add("Closed");
                            } else {
                                if(sObj.getPropertyAsString("is24x7").equalsIgnoreCase("true")){
                                    storeHours.add("24/7 open");
                                }else {
                                    storeHours.add(sObj.getPropertyAsString("monST") + " to " + sObj.getPropertyAsString("monET"));
                                }
                            }

                            if (sObj.getPropertyAsString("isTueClose").equalsIgnoreCase("true")) {
                                storeHours.add("Closed");
                            } else {
                                if(sObj.getPropertyAsString("is24x7").equalsIgnoreCase("true")){
                                    storeHours.add("24/7 open");
                                }else {
                                    storeHours.add(sObj.getPropertyAsString("tueST") + " to " + sObj.getPropertyAsString("tueET"));
                                }
                            }

                            if (sObj.getPropertyAsString("isWedClose").equalsIgnoreCase("true")) {
                                storeHours.add("Closed");
                            } else {
                                if(sObj.getPropertyAsString("is24x7").equalsIgnoreCase("true")){
                                    storeHours.add("24/7 open");
                                }else {
                                    storeHours.add(sObj.getPropertyAsString("wedST") + " to " + sObj.getPropertyAsString("wedET"));
                                }
                            }

                            if (sObj.getPropertyAsString("isThuClose").equalsIgnoreCase("true")) {
                                storeHours.add("Closed");
                            } else {
                                if(sObj.getPropertyAsString("is24x7").equalsIgnoreCase("true")){
                                    storeHours.add("24/7 open");
                                }else {
                                    storeHours.add(sObj.getPropertyAsString("thuST") + " to " + sObj.getPropertyAsString("thuET"));
                                }
                            }

                            if (sObj.getPropertyAsString("isFriClose").equalsIgnoreCase("true")) {
                                storeHours.add("Closed");
                            } else {
                                if(sObj.getPropertyAsString("is24x7").equalsIgnoreCase("true")){
                                    storeHours.add("24/7 open");
                                }else {
                                    storeHours.add(sObj.getPropertyAsString("friST") + " to " + sObj.getPropertyAsString("friET"));
                                }
                            }

                            if (sObj.getPropertyAsString("isSatClose").equalsIgnoreCase("true")) {
                                storeHours.add("Closed");
                            } else {
                                if(sObj.getPropertyAsString("is24x7").equalsIgnoreCase("true")){
                                    storeHours.add("24/7 open");
                                }else {
                                    storeHours.add(sObj.getPropertyAsString("satST") + " to " + sObj.getPropertyAsString("satET"));
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    }

                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }

                if(dialog!=null){
                    dialog.dismiss();
                }

                //detailsAdapter.notifyDataSetChanged();
                super.onPostExecute(result1);
            }
        }
    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + lat +","+ longi +"&destinations="+ latitude +","+ longitude+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            }else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        if(language.equalsIgnoreCase("En")) {
                            travelTimeText.setText("Travel time  " + secs);
                        }else if(language.equalsIgnoreCase("Ar")){
                            travelTimeText.setText(secs+ "  وقت السفر" );
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }


    public boolean isStoreOpen(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

        String startTime = stTime;
        String endTime = edTime;
        if(is24x7){
            return true;
        }else if(startTime.equals("anyType{}") && endTime.equals("anyType{}")){
            return false;

        } else {

            if(endTime.equals("00:00AM")){
                return true;
            }else if(endTime.equals("12:00AM")){
                endTime = "11:59PM";
            }

            Calendar now = Calendar.getInstance();

            int hour = now.get(Calendar.HOUR_OF_DAY);
            int minute = now.get(Calendar.MINUTE);


            Date serverDate = null;
            Date end24Date = null;
            Date start24Date = null;
            Date current24Date = null;

            try {
                end24Date = timeFormat.parse(endTime);
                start24Date = timeFormat.parse(startTime);
                serverDate = dateFormat.parse(timeResponse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String current24 = timeFormat1.format(serverDate);
            String end24 =timeFormat1.format(end24Date);
            String start24 = timeFormat1.format(start24Date);

            try {
                end24Date = timeFormat1.parse(end24);
                start24Date = timeFormat1.parse(start24);
                current24Date = timeFormat1.parse(current24);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            String[] parts = end24.split(":");
            int endHour = Integer.parseInt(parts[0]);
            int endMinute = Integer.parseInt(parts[1]);

            String[] parts1 = current24.split(":");
            int currentHour = Integer.parseInt(parts1[0]);
            int currentMinute = Integer.parseInt(parts1[1]);


            Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");

            if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
                if(current24Date.after(start24Date) && current24Date.before(end24Date) ){
                    return true;
                }else{
                    return false;
                }

            }else{
                if (currentHour < endHour || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {

                    return true;
                }else{

                    return false;
                }
            }

        }
    }



    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy");

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_TIME);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_TIME, envelope);
                    timeResult = (SoapPrimitive) envelope.getResponse();
                    timeResponse = timeResult.toString();
                    Log.v("TAG", timeResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + timeResponse);
            }else{
                timeResponse = "no internet";
            }
            return timeResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(timeResponse == null){

            } else if(timeResponse.equals("no internet")){
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
//                if(isStoreOpen()){
//                    orderNowText.setVisibility(View.VISIBLE);
//                }else {
                    orderNowText.setVisibility(View.GONE);
//                }

            }


            super.onPostExecute(result1);
        }
    }


    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getActivity(), "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
