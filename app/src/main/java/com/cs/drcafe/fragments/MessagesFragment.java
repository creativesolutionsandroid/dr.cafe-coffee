package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.MessagesAdapter;
import com.cs.drcafe.adapter.MessagesAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.Messages;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by SKT on 22-02-2016.
 */
public class MessagesFragment extends Fragment {
    private String SOAP_ACTION = "http://tempuri.org/getPushNotificationMessages";
    private String METHOD_NAME = "getPushNotificationMessages";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;

    private String SOAP_ACTION_UPDATE = "http://tempuri.org/updatePushNotificationMessage";
    private String METHOD_NAME_UPDATE = "updatePushNotificationMessage";

    private String SOAP_ACTION_DELETE = "http://tempuri.org/deletePushNotificationMessage";
    private String METHOD_NAME_DELETE = "deletePushNotificationMessage";


    private ArrayList<Messages> messageList = new ArrayList<>();
    private SoapObject result;
    private String response12 = null;
    SwipeMenuListView messagesListView;
    TextView clearBtn, title;
    SharedPreferences userPrefs;
    String userId;
    MessagesAdapter mAdapter;
    MessagesAdapterArabic mAdapterArabic;
    ProgressDialog dialog;
    Button backBtn;
    String language;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        View rootView = inflater.inflate(R.layout.messages, container,
                false);
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        messagesListView = (SwipeMenuListView) rootView.findViewById(R.id.messages_listview);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        clearBtn = (TextView) rootView.findViewById(R.id.clear_button);
        title = (TextView) rootView.findViewById(R.id.title);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        if(language.equalsIgnoreCase("En")){
            title.setText("Messages");
            clearBtn.setText("Clear");
            mAdapter = new MessagesAdapter(getActivity(), messageList);
            messagesListView.setAdapter(mAdapter);
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("الرسائل");
            clearBtn.setText("واضح");
            mAdapterArabic = new MessagesAdapterArabic(getActivity(), messageList);
            messagesListView.setAdapter(mAdapterArabic);
        }

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                if(language.equalsIgnoreCase("En")){
                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Do you want to delete all messages")
                            .setCancelable(false)
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    for (Messages msg : messageList) {
                                        if (!msg.getPushNotificationType().equals("5")) {
                                            new UpdateNotification().execute(msg.getpId());
                                        }
                                    }

                                    new GetNotifications().execute();
                                }
                            });
                }else if(language.equalsIgnoreCase("Ar")){
                    // set dialog message
                    alertDialogBuilder
                            .setMessage("هل تحب حذف جميع الرسائل ؟")
                            .setCancelable(false)
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    for (Messages msg : messageList) {
                                        if (!msg.getPushNotificationType().equals("5")) {
                                            new UpdateNotification().execute(msg.getpId());
                                        }
                                    }

                                    new GetNotifications().execute();
                                }
                            });
                }



                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });

        messagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if ( messageList.get(position).equals("true")) {
                    if ( messageList.get(position).getPushNotificationType().equals("5")) {
                        if(DrConstants.messageCount>0){
                            DrConstants.messageCount = DrConstants.messageCount - 1;
                        }

                    } else {
                        new UpdateNotification().execute(messageList.get(position).getpId());
                    }
                }
                Fragment newFragment = new MessageDetails();
                Bundle mBundle = new Bundle();
                mBundle.putString("message", messageList.get(position).getPushMessage());
                mBundle.putString("date", messageList.get(position).getSentDate());
                mBundle.putString("status", messageList.get(position).getPushNotificationType());
                newFragment.setArguments(mBundle);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.add(R.id.realtabcontent, newFragment);
                transaction.hide(MessagesFragment.this);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });


        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity().getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        messagesListView.setMenuCreator(creator);

        // step 2. listener item click event
        messagesListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        new DeleteNotification().execute(messageList.get(position).getpId());
                        break;
                }
                return false;
            }
        });

        new GetNotifications().execute();

        return rootView;
    }


    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    public class GetNotifications extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("UserId", userId);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    if(response12 != null) {
                        if (response12.equals("anyType{}")) {
                            dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                        } else {

                            try {

                                Object property = result.getProperty(1);
                                if (property instanceof SoapObject) {
                                    try {
                                        SoapObject category_list = (SoapObject) property;
                                        SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                        for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                            Messages msg = new Messages();
                                            SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");

                                            String pId = sObj.getPropertyAsString("PID");
                                            String userId = sObj.getPropertyAsString("UserId");
                                            String deviceToken = sObj.getPropertyAsString("DeviceToken");
                                            String pushMessage = sObj.getPropertyAsString("PushMessage");
                                            String pushNotificationType = sObj.getPropertyAsString("PushNotificationType");
                                            String isRead = sObj.getPropertyAsString("isRead");
                                            String sentDate = sObj.getPropertyAsString("sentDate");


                                            if (isRead.equals("true")) {
                                                if (pushNotificationType.equals("5")) {
                                                    DrConstants.messageCount = DrConstants.messageCount + 1;
                                                } else {
                                                    new UpdateNotification().execute(pId);
                                                }
                                            }
                                            msg.setpId(pId);
                                            msg.setUserId(userId);
                                            msg.setDeviceToken(deviceToken);
                                            msg.setPushMessage(pushMessage);
                                            msg.setPushNotificationType(pushNotificationType);
                                            msg.setIsRead(isRead);
                                            msg.setSentDate(sentDate);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                            messageList.add(msg);

                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
//                    }

                            } catch (Exception e) {
                                Log.d("", "Error while parsing the results!");
                                e.printStackTrace();
                            }


                            dialog.dismiss();
                            if(language.equalsIgnoreCase("En")){
                                mAdapter.notifyDataSetChanged();
                            }else if(language.equalsIgnoreCase("Ar")){
                                mAdapterArabic.notifyDataSetChanged();
                            }

                            super.onPostExecute(result1);
                        }
                    }
                }
            }else{
                dialog.dismiss();
            }

        }
    }




    public class UpdateNotification extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;

        @Override
        protected void onPreExecute() {

//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_UPDATE);
                request.addProperty("PID", arg0[0]);
                request.addProperty("isRead", false);

                // request.addProperty("", ""); // incase you need to pass
                // parameters to the web-service

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                androidHttpTransport.call(SOAP_ACTION_UPDATE, envelope);
                result = (SoapObject) envelope.getResponse();


            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Responce", "" + response12);

            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (response12.equals("anyType{}")) {
//                dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
            }
                super.onPostExecute(result1);

        }
    }


    public class DeleteNotification extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_DELETE);
                    request.addProperty("PID", arg0[0]);
                    Log.i("TAG", ""+arg0[0]);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_DELETE, envelope);
//                    String deleteResult = envelope.bodyIn.toString();
//                    deleteResponse = deleteResult.toString();
                    Log.v("TAG", "" + envelope.bodyIn);
                    // SoapObject root = (SoapObject) result.getProperty(0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            dialog.dismiss();
            new GetNotifications().execute();
//            if(deleteResponse != null){
//
//            }else if(deleteResponse.equals("no internet")){
//                dialog.dismiss();
//                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
//            }
//            else if (deleteResponse.equals("anyType{}")) {
//                dialog.dismiss();
////                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
//            } else {
//
//
//            }
        }
    }



    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

}
