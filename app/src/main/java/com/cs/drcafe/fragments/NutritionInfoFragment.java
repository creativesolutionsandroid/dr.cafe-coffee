package com.cs.drcafe.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.ItemTypeAdapter;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.Nutrition;
import com.cs.drcafe.model.NutritionDetails;
import com.cs.drcafe.widget.CustomListView;

import org.apache.commons.lang3.text.WordUtils;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by SKT on 20-01-2016.
 */
public class NutritionInfoFragment extends Fragment{

    ImageView productDetailsBg, productIcon, dropdownArrow;
    RelativeLayout productDetailsLayout;
    TextView productTitle,productDesc, calories, saturatedFat, totalFat, transFat, cholesterol, sodium, totalCarboHydrates, dietaryFiber, sugar, protien, caffien;
    String title, description;
    Button backBtn;
    ScrollView scrollView;
    String language;
    View rootView;
    CustomListView typeListView;
    public static int selectedType = 0;
    TextView itemTypeText;
    LinearLayout nutritionsDetailsLayout, listViewLayout;
    ItemTypeAdapter mAdapter;
    ArrayList<String> itemTypesList = new ArrayList<>();
    ArrayList<NutritionDetails> nutritionInfoList = new ArrayList<>();

    private String SOAP_ACTION = "http://tempuri.org/getItemNutritions";
    private String METHOD_NAME = "getItemNutritions";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    ArrayList<Nutrition> nutritionInfo = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.nutrition_info, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.nutrition_info_arabic, container,
                    false);
        }
        selectedType = 0;

        nutritionInfo = (ArrayList<Nutrition>) getArguments().getSerializable("nutrition");
        title = getArguments().getString("title");
        description = getArguments().getString("description");
        getArguments().remove("nutrition");
        productDetailsBg = (ImageView) rootView.findViewById(R.id.product_details_bg);
        productDetailsLayout = (RelativeLayout) rootView.findViewById(R.id.product_details_layout);
        productIcon = (ImageView) rootView.findViewById(R.id.product_icon);
        productTitle = (TextView) rootView.findViewById(R.id.product_title);
        productDesc = (TextView) rootView.findViewById(R.id.product_descrition);
        calories = (TextView) rootView.findViewById(R.id.nt_calories);
        saturatedFat = (TextView) rootView.findViewById(R.id.nt_saturated_fat);
        totalFat = (TextView) rootView.findViewById(R.id.nt_totalfat);
        transFat = (TextView) rootView.findViewById(R.id.nt_trans_fat);
        cholesterol = (TextView) rootView.findViewById(R.id.nt_cholesterol);
        sodium = (TextView) rootView.findViewById(R.id.nt_sodium);
        totalCarboHydrates = (TextView) rootView.findViewById(R.id.nt_carbo_hydratess);
        dietaryFiber = (TextView) rootView.findViewById(R.id.nt_fiber);
        sugar = (TextView) rootView.findViewById(R.id.nt_sugar);
        protien = (TextView) rootView.findViewById(R.id.nt_protein);
        caffien = (TextView) rootView.findViewById(R.id.nt_caffien);
        itemTypeText = (TextView) rootView.findViewById(R.id.item_type_text);
        dropdownArrow = (ImageView) rootView.findViewById(R.id.img13);
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);
        typeListView = (CustomListView) rootView.findViewById(R.id.type_list);
        nutritionsDetailsLayout = (LinearLayout) rootView.findViewById(R.id.nutrition_details_layout);
        listViewLayout = (LinearLayout) rootView.findViewById(R.id.list_layout);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        itemTypeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(itemTypesList.size() > 0) {
                    nutritionsDetailsLayout.setVisibility(View.GONE);
                    listViewLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        dropdownArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nutritionInfoList.size() > 0) {
                    nutritionsDetailsLayout.setVisibility(View.VISIBLE);
                    listViewLayout.setVisibility(View.GONE);
                    displayNutritionInfo();
                    if(itemTypesList.size() > 0) {
                        itemTypeText.setText(itemTypesList.get(selectedType));
                    }
                    else {
                        itemTypeText.setText("");
                    }
                }
            }
        });


        ViewTreeObserver vto = productDetailsBg.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                productDetailsBg.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = productDetailsBg.getMeasuredHeight();
                LinearLayout.LayoutParams newsCountParam = new
                        LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                productDetailsLayout.setLayoutParams(newsCountParam);
                return true;
            }
        });

        new GetNutritionInfo().execute();

        Log.i("TAG","title is "+title);
        if(!title.contains("dr.CAFE")) {
            productTitle.setText(WordUtils.capitalizeFully(title).replace("Ml", "ML").replace("ml", "ML"));
        }
        else{
            productTitle.setText(WordUtils.capitalizeFully(title).replace("Dr.cafe", "dr.CAFE").replace("ml", "ML"));
        }
        productDesc.setText(Html.fromHtml(description));
        productDesc.setMovementMethod(ScrollingMovementMethod.getInstance());
//        displayNutritionInfo();

        try {
            // get input stream
            InputStream ims = getActivity().getAssets().open(nutritionInfo.get(0).getItemId()+".png");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            productIcon.setImageDrawable(d);
        }
        catch(IOException ex) {
        }

//        scrollView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                productDesc.getParent().requestDisallowInterceptTouchEvent(false);
//                return false;
//            }
//        });
//        productDesc.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                productDesc.getParent().requestDisallowInterceptTouchEvent(false);
//                return false;
//            }
//        });



        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public class GetNutritionInfo extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        ProgressDialog dialog;
        private SoapObject result;
        private String response12 = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("ItemId", nutritionInfo.get(0).getItemId());

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    if(response12 != null) {
                        if (response12.equals("anyType{}")) {
                            dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                        } else {

                            try {

                                Object property = result.getProperty(1);
                                if (property instanceof SoapObject) {
                                    try {
                                        SoapObject category_list = (SoapObject) property;
                                        SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                        for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                            NutritionDetails nutritionDetails = new NutritionDetails();
                                            SoapObject sObj = (SoapObject) storeObject.getProperty(i);

//                                            if (!sObj.getPropertyAsString("itemTypeId").equals("0")) {
                                                String nutritionId = sObj.getPropertyAsString("nutritionId");
                                                String categoryId = sObj.getPropertyAsString("categoryId");
                                                String subCategoryId = sObj.getPropertyAsString("subCategoryId");
                                                String itemId = sObj.getPropertyAsString("itemId");
                                                String itemTypeId = sObj.getPropertyAsString("itemTypeId");
                                                String calories = sObj.getPropertyAsString("calories");
                                                String saturatedFat = sObj.getPropertyAsString("saturatedFat");
                                                String totalFat = sObj.getPropertyAsString("totalFat");
                                                String transFat = sObj.getPropertyAsString("transFat");
                                                String cholestrol = sObj.getPropertyAsString("cholestrol");
                                                String sodium = sObj.getPropertyAsString("sodium");
                                                String totalCarboHydrate = sObj.getPropertyAsString("totalCarboHydrate");
                                                String dietaryFiber = sObj.getPropertyAsString("dietaryFiber");
                                                String sugar = sObj.getPropertyAsString("sugar");
                                                String protein = sObj.getPropertyAsString("protein");
                                                String caffien = sObj.getPropertyAsString("caffien");
                                                String Price = sObj.getPropertyAsString("Price");

                                                nutritionDetails.setNutritionId(Integer.parseInt(nutritionId));
                                                nutritionDetails.setCategoryId(Integer.parseInt(categoryId));
                                                nutritionDetails.setSubCategoryId(Integer.parseInt(subCategoryId));
                                                nutritionDetails.setItemId(Integer.parseInt(itemId));
                                                nutritionDetails.setItemTypeId(Integer.parseInt(itemTypeId));
                                                nutritionDetails.setCalories(Float.parseFloat(calories));
                                                nutritionDetails.setSaturatedFat(Float.parseFloat(saturatedFat));
                                                nutritionDetails.setTotalFat(Float.parseFloat(totalFat));
                                                nutritionDetails.setTransFat(Float.parseFloat(transFat));
                                                nutritionDetails.setCholestrol(Float.parseFloat(cholestrol));
                                                nutritionDetails.setSodium(Float.parseFloat(sodium));
                                                nutritionDetails.setTotalCarboHydrate(Float.parseFloat(totalCarboHydrate));
                                                nutritionDetails.setDietaryFiber(Float.parseFloat(dietaryFiber));
                                                nutritionDetails.setSugar(Float.parseFloat(sugar));
                                                nutritionDetails.setProtein(Float.parseFloat(protein));
                                                nutritionDetails.setCaffien(Float.parseFloat(caffien));
                                                nutritionDetails.setPrice(Float.parseFloat(Price));

                                                if (language.equalsIgnoreCase("En")) {
                                                    if (itemTypeId.equals("1")) {
                                                        itemTypesList.add("Short");
                                                    } else if (itemTypeId.equals("2")) {
                                                        itemTypesList.add("Grande");
                                                    } else if (itemTypeId.equals("3")) {
                                                        itemTypesList.add("Tall");
                                                    } else if (itemTypeId.equals("6")) {
                                                        itemTypesList.add("Jumbo");
                                                    } else if (itemTypeId.equals("7")) {
                                                        itemTypesList.add("Single");
                                                    } else if (itemTypeId.equals("8")) {
                                                        itemTypesList.add("Double");
                                                    } else if (itemTypeId.equals("9")) {
                                                        itemTypesList.add("Coffee Box");
                                                    } else if (itemTypeId.equals("10")) {
                                                        itemTypesList.add("Triple");
                                                    }
                                                } else if (language.equalsIgnoreCase("Ar")) {
                                                    if (itemTypeId.equals("1")) {
                                                        itemTypesList.add("صغير");
                                                    } else if (itemTypeId.equals("2")) {
                                                        itemTypesList.add("كبير");
                                                    } else if (itemTypeId.equals("3")) {
                                                        itemTypesList.add("عادي");
                                                    } else if (itemTypeId.equals("6")) {
                                                        itemTypesList.add("جامبو");
                                                    } else if (itemTypeId.equals("7")) {
                                                        itemTypesList.add("مفرد");
                                                    } else if (itemTypeId.equals("8")) {
                                                        itemTypesList.add("مضاعف");
                                                    } else if (itemTypeId.equals("9")) {
                                                        itemTypesList.add("صندوق القهوة");
                                                    } else if (itemTypeId.equals("10")) {
                                                        itemTypesList.add("ثلاثي");
                                                    }
                                                }

                                                nutritionInfoList.add(nutritionDetails);
                                            }
//                                        }

                                        if(nutritionInfoList.size() > 0) {
                                            displayNutritionInfo();
                                            if(itemTypesList.size() > 0) {
                                                mAdapter = new ItemTypeAdapter(getContext(), itemTypesList);
                                                typeListView.setAdapter(mAdapter);
                                                itemTypeText.setText(itemTypesList.get(selectedType));
                                            }
                                            else {
                                                itemTypeText.setText("");
                                            }
                                        }
                                        else {
                                            listViewLayout.setVisibility(View.VISIBLE);
                                            itemTypeText.setText("");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                Log.d("", "Error while parsing the results!");
                                e.printStackTrace();
                            }

                            dialog.dismiss();
                            super.onPostExecute(result1);
                        }
                    }
                }
            }else{
                dialog.dismiss();
            }
        }
    }

    private void displayNutritionInfo(){
        nutritionsDetailsLayout.setVisibility(View.VISIBLE);
        if(nutritionInfoList.size() > 0) {
            try {
                calories.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getCalories()));
            } catch (NumberFormatException nfe) {
                calories.setText("0");
            }

            saturatedFat.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getSaturatedFat()));
            totalFat.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getTotalFat()));
            transFat.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getTransFat()));
            cholesterol.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getCholestrol()));
            sodium.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getSodium()));
            totalCarboHydrates.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getTotalCarboHydrate()));
            dietaryFiber.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getDietaryFiber()));
            sugar.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getSugar()));
            protien.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getProtein()));
            caffien.setText(String.format("%.2f", nutritionInfoList.get(selectedType).getCaffien()));
        }
//        else {
//            try{
//                calories.setText(String.format("%.2f", Double.parseDouble(nutritionInfo.get(0).getCalories())));
//            }catch (NumberFormatException nfe){
//                calories.setText("0");
//            }
//
//            saturatedFat.setText(nutritionInfo.get(0).getSaturatedFat());
//            totalFat.setText(nutritionInfo.get(0).getTotalFat());
//            transFat.setText(nutritionInfo.get(0).getTransFat());
//            cholesterol.setText(nutritionInfo.get(0).getCholestrol());
//            sodium.setText(nutritionInfo.get(0).getSodium());
//            totalCarboHydrates.setText(nutritionInfo.get(0).getTotalCarboHydrates());
//            dietaryFiber.setText(nutritionInfo.get(0).getDiietaryFiber());
//            sugar.setText(nutritionInfo.get(0).getSugar());
//            protien.setText(nutritionInfo.get(0).getProtein());
//            caffien.setText(nutritionInfo.get(0).getCaffien());
//        }
    }
}
