package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

import java.util.ArrayList;

/**
 * Created by BRAHMAM on 05-12-2015.
 */
public class StoreHoursFragment extends Fragment {

    private ArrayList<String> storeHours = new ArrayList<>();
    TextView sundayHours, mondayHours, tuesdayHours, wednesdayHours, thursdayHours, fridayHours, saturdayHours;
    private Button backBtn;
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.store_timings, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.store_timings_arabic, container,
                    false);
        }

        sundayHours = (TextView) rootView.findViewById(R.id.store_time_sunday);
        mondayHours = (TextView) rootView.findViewById(R.id.store_time_monday);
        tuesdayHours = (TextView) rootView.findViewById(R.id.store_time_tuesday);
        wednesdayHours = (TextView) rootView.findViewById(R.id.store_time_wednesday);
        thursdayHours = (TextView) rootView.findViewById(R.id.store_time_thursday);
        fridayHours = (TextView) rootView.findViewById(R.id.store_time_friday);
        saturdayHours = (TextView) rootView.findViewById(R.id.store_time_saturday);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        storeHours = getArguments().getStringArrayList("open_hours");

        if(storeHours.size() > 0) {
            sundayHours.setText(storeHours.get(0));
            mondayHours.setText(storeHours.get(1));
            tuesdayHours.setText(storeHours.get(2));
            wednesdayHours.setText(storeHours.get(3));
            thursdayHours.setText(storeHours.get(4));
            fridayHours.setText(storeHours.get(5));
            saturdayHours.setText(storeHours.get(6));
        }else{
            sundayHours.setText("Closed");
            mondayHours.setText("Closed");
            tuesdayHours.setText("Closed");
            wednesdayHours.setText("Closed");
            thursdayHours.setText("Closed");
            fridayHours.setText("Closed");
            saturdayHours.setText("Closed");
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
