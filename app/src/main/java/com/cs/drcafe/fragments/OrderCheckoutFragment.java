package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.GPSTracker;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.SplashScreen;
import com.cs.drcafe.adapter.OrderInfoAdapter;
import com.cs.drcafe.adapter.OrderInfoAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.Item;
import com.cs.drcafe.model.Order;
import com.cs.drcafe.model.SectionItem;
import com.cs.drcafe.model.StoreInfo;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import static com.cs.drcafe.R.id.linearLayout;
import static com.cs.drcafe.fragments.MainFragment.currentVersion;

/**
 * Created by SKT on 14-02-2016.
 */
public class OrderCheckoutFragment extends Fragment implements View.OnClickListener{
    private String SOAP_ACTION_INSERT = "http://tempuri.org/insertOrder";
    private String METHOD_NAME_INSERT = "insertOrder";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapPrimitive insertResult;
    private String insertResponse = null;
    private static final String STORE_URL = "http://www.drcafeonline.net/dcservices.asmx";
//        private static final String STORE_URL = "http://csadms.com/dcservices/DCServices.asmx";
    String response12 = null;
    SoapObject result;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;
    GPSTracker gps;
    private double lat, longi;
    String mDistancePrefValue, mRadiusPrefValue, mDisplayPrefValue;

    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    private ArrayList<StoreInfo> favouriteStoresList = new ArrayList<>();
    private ArrayList<Item> totalStoresList = new ArrayList<>();
    private ArrayList<StoreInfo> tempStoresList = new ArrayList<>();
    private ArrayList<StoreInfo> filterStoresList = new ArrayList<>();
    ArrayList<String> favoriteStores = new ArrayList<>();


    private String SOAP_ACTION_FAVOURITE = "http://tempuri.org/InsertFavorieOrderName";
    private String METHOD_NAME_FAVOURITE = "InsertFavorieOrderName";
    private SoapPrimitive favouriteResult;
    private String favouriteResponse = null;
    private boolean isAddedtoList, isFilterOn;

    private String SOAP_ACTION_OFFER = "http://tempuri.org/ValidateRamadanPromo";
    private String METHOD_NAME_OFFER = "ValidateRamadanPromo";
    private SoapObject offerResult;
    //    private static final String URL_OFFER = "http://192.168.1.113/DcAppService/DCServices.asmx";
    private String offerResponse = null;

    private String METHOD_NAME_FREE_DRINK = "CheckFirstFreeDrinkOffer";
    private String SOAP_ACTION_FREE_DRINK = "http://tempuri.org/CheckFirstFreeDrinkOffer";

    private String SOAP_ACTION = "http://tempuri.org/getOnlineOrderStores";
    private String METHOD_NAME = "getOnlineOrderStores";

    private String SOAP_ACTION_TIME = "http://tempuri.org/getCurrentTime";
    private String METHOD_NAME_TIME = "getCurrentTime";
    private SoapPrimitive timeResult;
    private String timeResponse = null;

    public static String UpdatesAvailable = "0" , UpdateSeverity = "0";

    ListView listView;
    RelativeLayout checkoutLayout;
    LinearLayout checkoutItemsLayout;
    ImageView checkoutImage, closeUpdatePopup;
    //    ImageView bannerImage, offerCancel;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    OrderInfoAdapter orderInfoAdapter;
    OrderInfoAdapterArabic orderInfoAdapterArabic;
    TextView orderCancelBtn;
    //    public static RelativeLayout offerBanner;
    public static TextView orderPrice, orderQuantity, netTotal, vatAmount, roundUp, amount;
    LinearLayout orderNowLayout, addMoreLayout, recieptLayout, vatLayout;
    TextView vatPercent;
    ImageView receipt_close;
    Button backBtn;
    AlertDialog alertDialog;
    ProgressDialog dialog;
    String userId;
    SharedPreferences userPrefs;
    String favNameTxt;
    View rootView;
    String language;
    AlertDialog alertDialog2;
    float vat = 5;
    float tax = 0;

    private SoapObject freeResult;
    private String freeResponse12 = null;

    public static String itemId;
    LinearLayout popup;
    TextView popupText;
    SharedPreferences popupPref;
    SharedPreferences.Editor popupEditor;

    SharedPreferences favouritePrefs, locationPrefs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_checkout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.order_checkout_arabic, container,
                    false);
        }

        myDbHelper = new DataBaseHelper(getActivity());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        popupPref = getActivity().getApplicationContext().getSharedPreferences("Pref", Context.MODE_PRIVATE); // 0 - for private mode
        popupEditor = popupPref.edit();
//        popupEditor.putString("popupPref","yes");
//        popupEditor.commit();

        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            currentVersion = pInfo.versionName;
//                currentVersion = "1.4.6";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        orderList = myDbHelper.getOrderInfo();
        listView = (ListView) rootView.findViewById(R.id.order_info_list);
        checkoutImage = (ImageView) rootView.findViewById(R.id.checkout_image);
//        offerBanner = (RelativeLayout) rootView.findViewById(R.id.offer_banner);
        checkoutLayout = (RelativeLayout) rootView.findViewById(R.id.checkout_layout);
        checkoutItemsLayout = (LinearLayout) rootView.findViewById(linearLayout);
        addMoreLayout = (LinearLayout) rootView.findViewById(R.id.addmore_layout);
        orderNowLayout = (LinearLayout) rootView.findViewById(R.id.ordernow_layout);
        vatLayout = (LinearLayout) rootView.findViewById(R.id.vatLayout);
        orderPrice = (TextView) rootView.findViewById(R.id.order_price);
        orderQuantity = (TextView) rootView.findViewById(R.id.order_quantity);
        orderCancelBtn = (TextView) rootView.findViewById(R.id.cancel_order);
//        bannerImage = (ImageView) rootView.findViewById(R.id.banner_img);
//        offerCancel = (ImageView) rootView.findViewById(R.id.offer_cancel);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        popup = (LinearLayout) rootView.findViewById(R.id.popup);
        popupText = (TextView) rootView.findViewById(R.id.popup_text);
        closeUpdatePopup = (ImageView) rootView.findViewById(R.id.closeUpdatePopup);
        recieptLayout = (LinearLayout) rootView.findViewById(R.id.receipt_layout);
        receipt_close = (ImageView) rootView.findViewById(R.id.receipt_close);
        amount = (TextView) rootView.findViewById(R.id.amount);
        vatAmount = (TextView) rootView.findViewById(R.id.vatAmount);
        roundUp = (TextView) rootView.findViewById(R.id.round_up);
        vatPercent = (TextView) rootView.findViewById(R.id.vatPercent);
        netTotal = (TextView) rootView.findViewById(R.id.net_total);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);

        mDistancePrefValue = locationPrefs.getString("distance", "kilometer");
        mRadiusPrefValue = locationPrefs.getString("radius",DrConstants.storeDistance);
        mDisplayPrefValue = locationPrefs.getString("display","map");
        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);

        addMoreLayout.setOnClickListener(this);
        orderNowLayout.setOnClickListener(this);
        orderCancelBtn.setOnClickListener(this);
        closeUpdatePopup.setOnClickListener(this);
        vatLayout.setOnClickListener(this);
        receipt_close.setOnClickListener(this);
//        bannerImage.setOnClickListener(this);
//        offerCancel.setOnClickListener(this);

//        Log.i("TAG","firstOrderDone "+DrConstants.firstOrderDone);
//        Log.i("TAG","popupPref "+popupPref.getString("popupPref", ""));
//        if(!DrConstants.firstOrderDone) {
//            if (popupPref.getString("popupPref", "").equals("") || popupPref.getString("popupPref", "").equalsIgnoreCase("yes")) {
        new CheckFirstFreeDrinkOffer().execute();
//            }
//        }
//        else{
//            popup.setVisibility(View.GONE);
//        }

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }

        if(language.equalsIgnoreCase("En")) {
            orderInfoAdapter = new OrderInfoAdapter(getActivity(), orderList);
            listView.setAdapter(orderInfoAdapter);
        }else if(language.equalsIgnoreCase("Ar")){
            orderInfoAdapterArabic = new OrderInfoAdapterArabic(getActivity(), orderList);
            listView.setAdapter(orderInfoAdapterArabic);
        }

        ViewTreeObserver vto = checkoutImage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                checkoutImage.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = checkoutImage.getMeasuredHeight();
                Log.i("Height TAG", "" + finalHeight);
                LinearLayout.LayoutParams newsCountParam = new
                        LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                RelativeLayout.LayoutParams params = new
                        RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                checkoutLayout.setLayoutParams(newsCountParam);
                checkoutItemsLayout.setLayoutParams(params);
                return true;
            }
        });
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.receipt_close:
                recieptLayout.setVisibility(View.GONE);
                break;

            case R.id.vatLayout:
                recieptLayout.setVisibility(View.VISIBLE);
                final DecimalFormat priceFormat = new DecimalFormat("0.00");
                amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
                tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                float netAmount = (Float.parseFloat(priceFormat.format(myDbHelper.getTotalOrderPrice())) + (Float.parseFloat(priceFormat.format(tax))));
                float netAmountRounded = (float) Math.round(netAmount * 10)/10;
                final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
                roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(priceFormat.format(netAmount))));
                vatAmount.setText(""+priceFormat.format(tax));
                netTotal.setText(""+priceFormat.format((netAmountRounded)));
                orderPrice.setText("" + priceFormat.format((netAmountRounded)));
                break;

            case R.id.addmore_layout:
                ((MainActivity) getActivity()).setCurrenTab(0);
                ((MainActivity) getActivity()).setCurrenTab(2);
//                ((MainActivity) getActivity()).clearBackStack();
//                Fragment menuFragment = new MenuFragment();
//                // consider using Java coding conventions (upper first char class names!!!)
//                FragmentTransaction menuTransaction = getFragmentManager().beginTransaction();
//
//                // Replace whatever is in the fragment_container view with this fragment,
//                // and add the transaction to the back stack
//                menuTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                menuTransaction.add(R.id.realtabcontent, menuFragment);
//                menuTransaction.hide(OrderCheckoutFragment.this);
//                menuTransaction.addToBackStack(null);
//
//                // Commit the transaction
//                menuTransaction.commit();
                break;

            case R.id.closeUpdatePopup:
                popup.setVisibility(View.GONE);
                break;
            case R.id.ordernow_layout:
                if(UpdatesAvailable.equals("1") && UpdateSeverity.equals("1")){
                    showDialog2();
                }
                else {
                    if (myDbHelper.getTotalOrderQty() == 0) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                        if (language.equalsIgnoreCase("En")) {
                            // set title
                            alertDialogBuilder.setTitle("dr.CAFE");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("There is no items in your order? To proceed checkout please add the items")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        } else if (language.equalsIgnoreCase("Ar")) {
                            // set title
                            alertDialogBuilder.setTitle("د. كيف");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("لاتوجد منتجات في السلة ، لتفعيل التأكيد من فضلك اضف منتج")
                                    .setCancelable(false)
                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        }


                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                    else if (DrConstants.orderType.equalsIgnoreCase("Delivery") && (
                            (myDbHelper.getTotalOrderPrice()+tax) < DrConstants.MinOrderAmount)) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                        if (language.equalsIgnoreCase("En")) {
                            // set title
                            alertDialogBuilder.setTitle("dr.CAFE");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("For delivery, minimum order amount should be " + (int)DrConstants.MinOrderAmount +" SR.")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        } else if (language.equalsIgnoreCase("Ar")) {
                            // set title
                            alertDialogBuilder.setTitle("د. كيف");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("للتوصيل ، الحد الادنى للطلب يجيب الا يقل عن"+ + (int)DrConstants.MinOrderAmount + "ريال")
                                    .setCancelable(false)
                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        }


                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                    else {

                        new GetCurrentTime().execute();

//                        if (DrConstants.orderNow) {
//                            Fragment newFragment = new OrderConfimationFragment();
//                            Bundle mBundle = new Bundle();
//                            mBundle.putString("start_time", DrConstants.startTime);
//                            mBundle.putString("end_time", DrConstants.endTime);
//                            newFragment.setArguments(mBundle);
//                            // consider using Java coding conventions (upper first char class names!!!)
//                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
//
//                            // Replace whatever is in the fragment_container view with this fragment,
//                            // and add the transaction to the back stack
//                            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                            transaction.add(R.id.realtabcontent, newFragment, "store_detail");
//                            transaction.hide(OrderCheckoutFragment.this);
//                            transaction.addToBackStack(null);
//
//                            // Commit the transaction
//                            transaction.commit();
//                        } else {
//                            Fragment selectStoreFragment = new SelectStoreFragment();
//                            // consider using Java coding conventions (upper first char class names!!!)
//                            FragmentTransaction selectStoreTransaction = getFragmentManager().beginTransaction();
//
//                            // Replace whatever is in the fragment_container view with this fragment,
//                            // and add the transaction to the back stack
//                            selectStoreTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                            selectStoreTransaction.add(R.id.realtabcontent, selectStoreFragment);
//                            selectStoreTransaction.hide(OrderCheckoutFragment.this);
//                            selectStoreTransaction.addToBackStack(null);
//
//                            // Commit the transaction
//                            selectStoreTransaction.commit();
//                        }
                    }
                }
                break;

//            case R.id.banner_img:
//                boolean isAdded = false;
//                orderList = myDbHelper.getOrderInfo();
//                for(Order order : orderList){
//                    if(order.getItemId().equalsIgnoreCase(itemId)){
//                        isAdded = true;
//                    }
//                }
//                Log.d("TAG1", ""+myDbHelper.getTotalOrderQty());
//                if(!isClick){
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//
//                    if(language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("dr.CAFE");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("Sorry this item available to order from "+ sTime +" to " +eTime)
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        // set title
//                        alertDialogBuilder.setTitle("د. كيف");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage(" نأسف هذا المنتج متوفر للطلب من "+sTime+" الى "+eTime)
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();

//                }else if(!isAdded && isClick){
//                    String[] subCatId = myDbHelper.getSubCatId(itemId).split(",");
//                    HashMap<String, String> values = new HashMap<>();
//                    values.put("itemId", itemId);
//                    values.put("itemTypeId", "0");
//                    values.put("subCategoryId", subCatId[0]);
//                    values.put("additionals", "");
//                    values.put("qty", "1");
//                    values.put("price", "0");
//                    values.put("additionalsPrice", "");
//                    values.put("additionalsTypeId", "");
//                    values.put("totalAmount", "0");
//                    values.put("comment", "");
//                    values.put("status", "1");
//                    values.put("creationDate", "14/07/2015");
//                    values.put("modifiedDate", "14/07/2015");
//                    values.put("categoryId", subCatId[1]);
//                    myDbHelper.insertOrder(values);
//                    DrConstants.IFMAdded = true;
//
//                    orderList.clear();
//
//                    orderList = myDbHelper.getOrderInfo();
//                    Log.d("TAG2", ""+myDbHelper.getTotalOrderQty());
//                    if(language.equalsIgnoreCase("En")) {
//                        orderInfoAdapter = new OrderInfoAdapter(getActivity(), orderList);
//                        listView.setAdapter(orderInfoAdapter);
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        orderInfoAdapterArabic = new OrderInfoAdapterArabic(getActivity(), orderList);
//                        listView.setAdapter(orderInfoAdapterArabic);
//                    }
//
//                    try {
//                        MenuFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                        MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
//                        CategoriesListFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                        CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//                    OrderCheckoutFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                    OrderCheckoutFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
//
//                    Log.d("TAG3", ""+myDbHelper.getTotalOrderQty());
//
//                    offerBanner.setVisibility(View.GONE);
//                }
//                break;

//            case R.id.offer_cancel:
//                offerBanner.setVisibility(View.GONE);
//                break;

            case R.id.cancel_order:
                if (myDbHelper.getTotalOrderQty() == 0) {
                    DrConstants.menuFlag = true;
                    ((MainActivity) getActivity()).setCurrenTab(0);
                    ((MainActivity) getActivity()).setCurrenTab(2);
                }else{
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = null;
                    if(language.equalsIgnoreCase("En")){
                        dialogView = inflater.inflate(R.layout.cancel_order_dialog, null);
                    }else if(language.equalsIgnoreCase("Ar")){
                        dialogView = inflater.inflate(R.layout.cancel_order_dialog_arabic, null);
                    }
//                View dialogView = inflater.inflate(R.layout.cancel_order_dialog, null);
                    dialogBuilder.setView(dialogView);

                    final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                    TextView cancelBtn = (TextView) dialogView.findViewById(R.id.cancel);
                    TextView clearOrder = (TextView) dialogView.findViewById(R.id.order_clear);
                    TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                    TextView cancelAlertText = (TextView) dialogView.findViewById(R.id.cancel_alert_text);
                    if(language.equalsIgnoreCase("En")){
                        cancelAlertText.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }else if(language.equalsIgnoreCase("Ar")){
                        cancelAlertText.setText(" منتج في الحقيبة ، من خلال هذا الإجراء ستصبح حقيبتك خالية من المنتجا ت" +myDbHelper.getTotalOrderQty()+"  لديك  ");
                    }



                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    clearOrder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myDbHelper.deleteOrderTable();
                            try {
                                MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                                CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                                final DecimalFormat priceFormat = new DecimalFormat("0.00");
                                amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
                                float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                                float netAmount = (Float.parseFloat(priceFormat.format(myDbHelper.getTotalOrderPrice())) + (Float.parseFloat(priceFormat.format(tax))));
                                float netAmountRounded = (float) Math.round(netAmount * 10)/10 ;                                final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
                                roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(priceFormat.format(netAmount))));
                                vatAmount.setText(""+priceFormat.format(tax));
                                netTotal.setText(""+priceFormat.format((netAmountRounded)));
                                orderPrice.setText("" + priceFormat.format((netAmountRounded)));
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                            ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
                            ((MainActivity) getActivity()).setCurrenTab(0);
                            ((MainActivity) getActivity()).setCurrenTab(2);
                            alertDialog.dismiss();
                        }
                    });

                    saveOrder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            favNameTxt = favName.getText().toString();
                            if(favNameTxt.length()>0){
                                alertDialog.dismiss();
                                new InsertOrder().execute();
                            }else{
                                alertDialog.dismiss();
                                favNameTxt = "favourite1";
                                new InsertOrder().execute();
                            }

                        }
                    });


                    alertDialog = dialogBuilder.create();
                    alertDialog.show();

                    //Grab the window of the dialog, and change the width
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = alertDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                    break;
                }
        }

    }



    public class InsertOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        String currentTime;
        String estimatedTime, total_amt;
        String itemId = "", qty = "", comments = "", total_price = null, item_price = "", sizes = "", additionals = "", additionalsPices = "";

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please wait...");

            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());


            SimpleDateFormat df3 = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            currentTime = df3.format(c.getTime());
            estimatedTime = currentTime;
            total_amt = ""+myDbHelper.getTotalOrderPrice();
            for(Order order: orderList){
                if(itemId == ""){
                    itemId = order.getItemId();
                    qty = order.getQty();
                    comments = order.getComment();
                    item_price = order.getPrice();
                    sizes = order.getItemTypeId();
                    additionals = order.getAdditionalsTypeId();
                    additionalsPices = order.getAdditionalsPrice();
                }else{
                    itemId = itemId+"|"+order.getItemId();
                    qty = qty+"|"+order.getQty();
                    comments = comments+"|"+order.getComment();
                    item_price = item_price+"|"+order.getPrice();
                    sizes = sizes+"|"+order.getItemTypeId();
                    additionals = additionals+"|"+order.getAdditionalsTypeId();
                    additionalsPices = additionalsPices+"|"+order.getAdditionalsPrice();
                }
            }

        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_INSERT);
                    request.addProperty("userId", userId);
                    request.addProperty("storeId", "45");
                    request.addProperty("comments", "Save Order");
                    request.addProperty("date_time", currentTime);
                    request.addProperty("type", "Pick Up");
                    request.addProperty("expectedTime", "00:00 AM");
                    request.addProperty("status", "1");
                    request.addProperty("itemId", itemId);
                    request.addProperty("qty", qty);
                    request.addProperty("icomments", comments);
                    request.addProperty("OrderStatus", "Save");
                    request.addProperty("tot_price", total_amt);
                    request.addProperty("device_token", "-1");
                    request.addProperty("item_price", item_price);
                    request.addProperty("sizes", sizes);
                    request.addProperty("additionals", additionals);
                    request.addProperty("additionalsPrices", additionalsPices);
                    request.addProperty("isFavorite", "0");
                    request.addProperty("FavoriteName", "");
                    request.addProperty("promotionCode", "No");
                    request.addProperty("PaymentMode", "2");



                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_INSERT, envelope);
                    insertResult = (SoapPrimitive) envelope.getResponse();
                    insertResponse = insertResult.toString();
                    Log.v("TAG", insertResponse);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + insertResult);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + insertResponse);
            }else {
                return "no internet";
            }
            return insertResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    myDbHelper.deleteOrderTable();
                    try {final DecimalFormat priceFormat = new DecimalFormat("0.00");
                        amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
                        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                        float netAmount = (Float.parseFloat(priceFormat.format(myDbHelper.getTotalOrderPrice())) + (Float.parseFloat(priceFormat.format(tax))));
                        float netAmountRounded = (float) Math.round(netAmount * 10)/10 ;                        final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
                        roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(priceFormat.format(netAmount))));
                        vatAmount.setText(""+priceFormat.format(tax));
                        netTotal.setText(""+priceFormat.format((netAmountRounded)));
                        orderPrice.setText("" + priceFormat.format((netAmountRounded)));
                        OrderCheckoutFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                        MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                        CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                        CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
//                        amount.setText("" +myDbHelper.getTotalOrderPrice());
//                        tax = myDbHelper.getTotalOrderPrice()*(vat/100);
//                        vatAmount.setText(""+tax);
//                        netTotal.setText(""+(myDbHelper.getTotalOrderPrice()+tax));
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
                    dialog.dismiss();
                    ((MainActivity) getActivity()).setCurrenTab(2);
                    String[] orderNo = insertResponse.split("-");
                    String[] parts = insertResponse.split(",");
                    new GetCardBalance().execute(parts[0], favNameTxt);
//                    orderId = parts[0];
                }
            }

            super.onPostExecute(result1);
        }
    }



    public class GetCardBalance extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        }

        @Override
        protected String doInBackground(String... arg0) {

            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_FAVOURITE);
                    request.addProperty("orderID", arg0[0]);
                    request.addProperty("FavoriteName", arg0[1]);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_FAVOURITE, envelope);
                    favouriteResult = (SoapPrimitive) envelope.getResponse();
                    favouriteResponse = favouriteResult.toString();
                    Log.v("TAG", favouriteResponse);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + favouriteResult);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + favouriteResponse);
            }else{
                return "no internet";
            }
            return favouriteResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        Toast.makeText(getActivity(), "Favourite name saved", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            super.onPostExecute(result1);
        }
    }

    public class CheckFirstFreeDrinkOffer extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Please wait...");
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_FREE_DRINK);
                    request.addProperty("userId", userId);
                    request.addProperty("DeviceToken", SplashScreen.regid);
                    request.addProperty("UserVersion", currentVersion);
                    request.addProperty("AppType", "Andr");

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_FREE_DRINK, envelope);
                    freeResult = (SoapObject) envelope.getResponse();
                    freeResponse12 = freeResult.toString();
                    Log.v("TAG", freeResponse12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + freeResult.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + freeResponse12);
            }else {
                return "no internet";
            }
            return freeResponse12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
//                    dialog.dismiss();

                }else{
                    if (freeResponse12.equals("anyType{}")) {
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                    } else {

                        try {

                            Object property = freeResult.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject category_list = (SoapObject) property;
                                    SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                    for (int i = 0; i < storeObject.getPropertyCount(); i++) {

                                        SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                        String Foffer = sObj.getPropertyAsString("Foffer");
                                        String Desc_En = sObj.getPropertyAsString("Desc_En");
                                        String Desc_Ar = sObj.getPropertyAsString("Desc_Ar");
                                        UpdatesAvailable = sObj.getPropertyAsString("UpdatesAvailable");
                                        UpdateSeverity = sObj.getPropertyAsString("UpdateSeverity");
                                        DrConstants.vatString = sObj.getPropertyAsString("Vat");
                                        DrConstants.vat = Float.parseFloat(sObj.getPropertyAsString("Vat"));
                                        vat = DrConstants.vat;
                                        final DecimalFormat priceFormat = new DecimalFormat("0.00");
                                        vatPercent.setText("("+DrConstants.vatString+"%)");
                                        amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
                                        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                                        float netAmount = (Float.parseFloat(priceFormat.format(myDbHelper.getTotalOrderPrice())) + (Float.parseFloat(priceFormat.format(tax))));
                                        float netAmountRounded = (float) Math.round(netAmount * 10)/10 ;                                        final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
                                        roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(priceFormat.format(netAmount))));
                                        vatAmount.setText(""+priceFormat.format(tax));
                                        netTotal.setText(""+priceFormat.format((netAmountRounded)));
                                        orderPrice.setText("" + priceFormat.format((netAmountRounded)));
//                                        UpdatesAvailable = "1";
//                                        UpdateSeverity = "0";

                                        if(UpdatesAvailable.equals("1")){
                                            if(UpdateSeverity.equals("0")) {
                                                showDialog1();
                                            }
                                            else{
                                                showDialog2();
                                            }
                                        }

                                        if(Foffer.equalsIgnoreCase("yes")) {
                                            popupEditor.putString("popupPref",Foffer);
                                            popupEditor.commit();

                                            if (!DrConstants.firstOrderDone) {
                                                DrConstants.firstOrderDone = true;
                                                popup.setVisibility(View.VISIBLE);
                                                if (language.equalsIgnoreCase("En")) {
                                                    popupText.setText(Desc_En);
                                                } else {
                                                    popupText.setText(Desc_Ar);
                                                }
                                            }
                                            else{
                                                popup.setVisibility(View.GONE);
                                            }
                                        }
                                        else {
                                            popupEditor.putString("popupPref",Foffer);
                                            popupEditor.commit();
                                            popup.setVisibility(View.GONE);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
//                    }

                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }
                    }
                }
            }

            super.onPostExecute(result1);
        }
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public void showDialog1(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        int layout = R.layout.update_app;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView versionName = (TextView) dialogView.findViewById(R.id.version_name);
        TextView updateBtn = (TextView) dialogView.findViewById(R.id.update_now);
        TextView notNowBtn = (TextView) dialogView.findViewById(R.id.no_cancel);

        versionName.setText("v-"+currentVersion);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + getContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getContext().getPackageName())));
                }

                alertDialog2.dismiss();
            }
        });

        notNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog2.dismiss();
            }
        });

        alertDialog2 = dialogBuilder.create();
//        alertDialog2.getWindow().setGravity(Gravity.BOTTOM);
//        alertDialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        alertDialog2.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog2.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x; // int screenWidth = display.getWidth(); on API < 13
        int screenHeight = size.y;

        double d = screenWidth*0.9;
        double d1 = screenHeight*0.85;
        lp.width = (int) d;
        lp.height = (int) d1;
        window.setAttributes(lp);
    }

    public void showDialog2(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        int layout = R.layout.update_app;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView versionName = (TextView) dialogView.findViewById(R.id.version_name);
        TextView updateBtn = (TextView) dialogView.findViewById(R.id.update_now);
        TextView notNowBtn = (TextView) dialogView.findViewById(R.id.no_cancel);
        notNowBtn.setVisibility(View.GONE);

        versionName.setText("v-"+currentVersion);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + getContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getContext().getPackageName())));
                }

                alertDialog2.dismiss();
            }
        });

        notNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog2.dismiss();
            }
        });

        alertDialog2 = dialogBuilder.create();
//        alertDialog2.getWindow().setGravity(Gravity.BOTTOM);
//        alertDialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        alertDialog2.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog2.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x; // int screenWidth = display.getWidth(); on API < 13
        int screenHeight = size.y;

        double d = screenWidth*0.9;
        double d1 = screenHeight*0.85;
        lp.width = (int) d;
        lp.height = (int) d1;
        window.setAttributes(lp);
    }

    @Override
    public void onResume() {
        super.onResume();
        final DecimalFormat priceFormat = new DecimalFormat("0.00");
        amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
        float netAmount = (Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(myDbHelper.getTotalOrderPrice()))) + (Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(tax)))));
        float netAmountRounded = (float) Math.round(netAmount * 10)/10 ;
        final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
        roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(netAmount)))));
        vatAmount.setText(""+priceFormat.format(tax));
        netTotal.setText(""+priceFormat.format((netAmountRounded)));
        orderPrice.setText("" + priceFormat.format((netAmountRounded)));
    }

    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy");

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_TIME);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_TIME, envelope);
                    timeResult = (SoapPrimitive) envelope.getResponse();
                    timeResponse = timeResult.toString();
                    Log.v("TAG", timeResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + timeResponse);
            }else{
                timeResponse = "no internet";
            }
            return timeResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(timeResponse == null){
                if(dialog!=null) {
                    dialog.dismiss();
                }
            } else if(timeResponse.equals("no internet")){
                if(dialog!=null) {
                    dialog.dismiss();
                }
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                if(dialog!=null) {
                    dialog.dismiss();
                }
                new GetStoreInfo().execute();
            }
            super.onPostExecute(result1);
        }
    }

    public class GetStoreInfo extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus = "";
        String dayOfWeek;

        @Override
        protected void onPreExecute() {
            if(getActivity() != null) {
                networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            }
            try {
                dialog = ProgressDialog.show(getActivity(), "",
                        "Please wait...");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    SharedPreferences storePrefs = getContext().getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);

                    Gson gson = new Gson();
                    String jsonText = storePrefs.getString("store", null);
                    StoreInfo si = gson.fromJson(jsonText, StoreInfo.class);

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("day", dayOfWeek.toLowerCase());
                    request.addProperty("StoreId", si.getStoreId());

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(STORE_URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                Log.d("TAG", "onPostExecute: "+response12);
                if(result1.equalsIgnoreCase("no internet")){
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    storesList.clear();
                    filterStoresList.clear();
                    totalStoresList.clear();
                    favouriteStoresList.clear();
                    if(response12 == null){

                    }else if (response12.equals("anyType{}")) {
                        Toast.makeText(getActivity(), "Failed to get stores info", Toast.LENGTH_LONG).show();
                    } else {

                        try {
                            Object property = result.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject category_list = (SoapObject) property;
                                    SoapObject storeObject = (SoapObject) category_list.getProperty(0);
//                                    lat = 24.70321657;
//                                    longi = 46.68097073;
                                    for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                        isAddedtoList = false;
                                        SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                        StoreInfo si = new StoreInfo();

//                                    for (int j = 0; j < sObj.getPropertyCount(); j++) {
                                        si.setStoreId(sObj.getPropertyAsString(0));
                                        si.setStartTime(sObj.getPropertyAsString(1));
                                        si.setEndTime(sObj.getPropertyAsString(2));
                                        si.setStoreName(sObj.getPropertyAsString("StoreName"));
                                        si.setStoreAddress(sObj.getPropertyAsString("StoreAddress"));
                                        si.setLatitude(Double.valueOf(sObj.getPropertyAsString("Latitude")));
                                        si.setLongitude(Double.valueOf(sObj.getPropertyAsString("Longitude")));
                                        si.setCountryName(sObj.getPropertyAsString("CountryName"));
                                        si.setCityName(sObj.getPropertyAsString("CityName"));
                                        try {
                                            si.setImageURL(sObj.getPropertyAsString("imageURL"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        si.setFamilySection(Boolean.valueOf(sObj.getPropertyAsString("FamilySection")));
                                        si.setWifi(Boolean.valueOf(sObj.getPropertyAsString("Wifi")));
                                        si.setPatioSitting(Boolean.valueOf(sObj.getPropertyAsString("PatioSitting")));
                                        si.setV12(Boolean.valueOf(sObj.getPropertyAsString("V12")));
                                        si.setDriveThru(Boolean.valueOf(sObj.getPropertyAsString("DriveThru")));
                                        si.setMeetingSpace(Boolean.valueOf(sObj.getPropertyAsString("MeetingSpace")));
                                        si.setHospital(Boolean.valueOf(sObj.getPropertyAsString("Hospital")));
                                        si.setUniversity(Boolean.valueOf(sObj.getPropertyAsString("University")));
                                        si.setOffice(Boolean.valueOf(sObj.getPropertyAsString("Office")));
                                        si.setShoppingMall(Boolean.valueOf(sObj.getPropertyAsString("ShoppingMall")));
                                        try{
                                            si.setAirPort(Boolean.valueOf(sObj.getPropertyAsString("Airport")));
                                        }catch (Exception e){
                                            si.setAirPort(false);
                                        }
                                        try{
                                            si.setDineIn(Boolean.valueOf(sObj.getPropertyAsString("DineIn")));
                                        }catch (Exception e){
                                            si.setDineIn(false);
                                        }
                                        try{
                                            si.setLadies(Boolean.valueOf(sObj.getPropertyAsString("Ladies")));
                                        }catch (Exception e){
                                            si.setLadies(false);
                                        }
                                        si.setCashPayment(Boolean.valueOf(sObj.getPropertyAsString("CashPayment")));
                                        si.setOnlinePayment(Boolean.valueOf(sObj.getPropertyAsString("OnlinePayment")));
                                        si.setNeighborhood(Boolean.valueOf(sObj.getPropertyAsString("Neighborhood")));
                                        si.setIs24x7(Boolean.valueOf(sObj.getPropertyAsString("is24x7")));
                                        si.setStatus(Boolean.valueOf(sObj.getPropertyAsString("status")));
                                        si.setDcCountry(sObj.getPropertyAsString("DCCountry"));
                                        si.setDcCity(sObj.getPropertyAsString("DCCity"));
                                        si.setStoreName_ar(sObj.getPropertyAsString("StoreName_ar"));
                                        si.setStoreAddress_ar(sObj.getPropertyAsString("StoreAddress_ar"));
                                        si.setIemsJson(sObj.getPropertyAsString("ItemsJson"));
                                        try {
                                            si.setDeliveryItems(sObj.getPropertyAsString("DeliveryItems"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try{
                                            si.setMessage(sObj.getPropertyAsString("Message"));
                                        }catch (Exception e){
                                            si.setMessage("");
                                        }

                                        try{
                                            si.setMessage_ar(sObj.getPropertyAsString("Message_ar"));
                                        }catch (Exception e){
                                            si.setMessage_ar("");
                                        }

                                        Location me   = new Location("");
                                        Location dest = new Location("");

                                        me.setLatitude(lat);
                                        me.setLongitude(longi);

                                        dest.setLatitude(Double.valueOf(sObj.getPropertyAsString("Latitude")));
                                        dest.setLongitude(Double.valueOf(sObj.getPropertyAsString("Longitude")));

                                        float dist = (me.distanceTo(dest))/1000;
                                        float mile = (float) (dist * 0.621);
//                                        Log.i("storename", "" + sObj.getPropertyAsString(1) + "   " + sObj.getPropertyAsString(2));
                                        if(mDistancePrefValue.equalsIgnoreCase("kilometer")){
                                            si.setDistance(dist);
                                        }else if(mDistancePrefValue.equalsIgnoreCase("mile")){
                                            si.setDistance(mile);
                                        }
                                        if(dist<Integer.parseInt(mRadiusPrefValue)) {
//                                            storesList.add(si);
                                            if(favoriteStores.size()>0){
                                                if(favoriteStores.contains(sObj.getPropertyAsString(0))){
                                                    favouriteStoresList.add(si);
                                                }
                                            }
                                            else {
                                            }
                                            String startTime = sObj.getPropertyAsString(1);
                                            String endTime = sObj.getPropertyAsString(2);
                                            if(Boolean.valueOf(sObj.getPropertyAsString("is24x7"))){
                                                si.setOpenFlag(1);
                                                storesList.add(si);
                                            }else if(sObj.getPropertyAsString(1).equals("anyType{}") && sObj.getPropertyAsString(2).equals("anyType{}")){
                                                si.setOpenFlag(-1);
                                                storesList.add(si);
                                            }else{
                                                if(endTime.equals("00:00AM")){
                                                    si.setOpenFlag(1);
                                                    storesList.add(si);

                                                    continue;
                                                }else if(endTime.equals("12:00AM")){
                                                    endTime = "11:59PM";
                                                }

                                                Calendar now = Calendar.getInstance();

                                                int hour = now.get(Calendar.HOUR_OF_DAY);
                                                int minute = now.get(Calendar.MINUTE);

                                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
                                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                                Date serverDate = null;
                                                Date end24Date = null;
                                                Date start24Date = null;
                                                Date current24Date = null;
                                                Date dateToday = null;
                                                Calendar dateStoreClose = Calendar.getInstance();

                                                try {
                                                    end24Date = timeFormat.parse(endTime);
                                                    start24Date = timeFormat.parse(startTime);
                                                    serverDate = dateFormat.parse(timeResponse);
                                                    dateToday = dateFormat1.parse(timeResponse);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                dateStoreClose.setTime(dateToday);
                                                dateStoreClose.add(Calendar.DATE, 1);
                                                String current24 = timeFormat1.format(serverDate);
                                                String end24 =timeFormat1.format(end24Date);
                                                String start24 = timeFormat1.format(start24Date);
                                                String startDateString = dateFormat1.format(dateToday);
                                                String endDateString = dateFormat1.format(dateToday);
                                                String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                                                dateStoreClose.add(Calendar.DATE, -2);
                                                String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

                                                Date startDate = null;
                                                Date endDate = null;

                                                try {
                                                    end24Date = timeFormat1.parse(end24);
                                                    start24Date = timeFormat1.parse(start24);
                                                    current24Date = timeFormat1.parse(current24);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                                String[] parts2 = start24.split(":");
                                                int startHour = Integer.parseInt(parts2[0]);
                                                int startMinute = Integer.parseInt(parts2[1]);

                                                String[] parts = end24.split(":");
                                                int endHour = Integer.parseInt(parts[0]);
                                                int endMinute = Integer.parseInt(parts[1]);

                                                String[] parts1 = current24.split(":");
                                                int currentHour = Integer.parseInt(parts1[0]);
                                                int currentMinute = Integer.parseInt(parts1[1]);


                                                if(startTime.contains("AM") && endTime.contains("AM")){
                                                    if(startHour < endHour){
                                                        startDateString = startDateString+ " "+ startTime;
                                                        endDateString = endDateString+"  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }else if(startHour > endHour){
                                                        if(timeResponse.contains("AM")){
                                                            if(currentHour > endHour){
                                                                startDateString = startDateString + " " + startTime;
                                                                endDateString = endDateTomorrow + "  " + endTime;
                                                                try {
                                                                    startDate = dateFormat2.parse(startDateString);
                                                                    endDate = dateFormat2.parse(endDateString);
                                                                } catch (ParseException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            } else {
                                                                startDateString = endDateYesterday + " " + startTime;
                                                                endDateString = endDateString + "  " + endTime;
                                                                try {
                                                                    startDate = dateFormat2.parse(startDateString);
                                                                    endDate = dateFormat2.parse(endDateString);
                                                                } catch (ParseException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }else {
                                                            startDateString = startDateString + " " + startTime;
                                                            endDateString = endDateTomorrow + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                }else if(startTime.contains("AM") && endTime.contains("PM")){
                                                    startDateString = startDateString+ " "+ startTime;
                                                    endDateString = endDateString+"  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }else if(startTime.contains("PM") && endTime.contains("AM")){
                                                    if(timeResponse.contains("AM")){
                                                        if(currentHour <= endHour){
                                                            startDateString = endDateYesterday+ " "+ startTime;
                                                            endDateString = endDateString+"  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }else{
                                                            startDateString = startDateString+ " "+ startTime;
                                                            endDateString = endDateTomorrow+"  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }else{
                                                        startDateString = startDateString+ " "+ startTime;
                                                        endDateString = endDateTomorrow+"  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                }else if(startTime.contains("PM") && endTime.contains("PM")){
                                                    startDateString = startDateString+ " "+ startTime;
                                                    endDateString = endDateString+"  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                String serverDateString = dateFormat2.format(serverDate);

                                                try {
                                                    serverDate = dateFormat2.parse(serverDateString);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                                if(serverDate.after(startDate) && serverDate.before(endDate)){
//                                                Log.i("TAG Visible" , "true");
                                                    si.setOpenFlag(1);
                                                    storesList.add(si);
                                                    continue;
                                                }else{
                                                    si.setOpenFlag(0);
                                                    storesList.add(si);
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }

                        filterStoresList.addAll(storesList);

                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(favouriteStoresList, StoreInfo.storeDistance);
                        Collections.sort(storesList, StoreInfo.storeOpenSort);
                        Collections.sort(favouriteStoresList, StoreInfo.storeOpenSort);
                        if(favouriteStoresList.size() >0){
                            if(language.equalsIgnoreCase("En")) {
                                totalStoresList.add(new SectionItem("Favorite Stores"));
                            }else if(language.equalsIgnoreCase("Ar")){
                                totalStoresList.add(new SectionItem("  الفروع المفضلة"));
                            }
                            totalStoresList.addAll(favouriteStoresList);
                        }
                        if(language.equalsIgnoreCase("En")) {
                            totalStoresList.add((new SectionItem("Nearby Stores")));
                        }else if(language.equalsIgnoreCase("Ar")){
                            totalStoresList.add((new SectionItem(" الفروع القريبة")));
                        }
                        totalStoresList.addAll(storesList);
                        if(tempStoresList.size()==0){
                            tempStoresList.addAll(storesList);
                        }
                    }
                    checkStoreStatus();
                    if(dialog!=null) {
                        dialog.dismiss();
                    }
                    super.onPostExecute(result1);
                }
            }
        }
    }

    public void getGPSCoordinates(){
        if(getActivity()!=null) {
            gps = new GPSTracker(getActivity());
        }
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);

//                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }


    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void checkStoreStatus(){
        if(storesList.size() > 0) {
            final StoreInfo si = storesList.get(0);

            boolean BM = true;
            if (DrConstants.orderType.equalsIgnoreCase("Delivery")) {
                for (int i = 0; i < orderList.size(); i++) {
                    if (orderList.get(i).getCategoryId().equalsIgnoreCase("1") ||
                            orderList.get(i).getCategoryId().equalsIgnoreCase("2") ||
                            orderList.get(i).getCategoryId().equalsIgnoreCase("8") ||
                            orderList.get(i).getCategoryId().equalsIgnoreCase("9")) {
                        BM = false;
                    }
                }
            }
            Log.d("TAG", "checkStoreStatus si.is24x7(): "+si.is24x7());
            if (!si.is24x7() || !DrConstants.orderType.equalsIgnoreCase("Delivery")) {
                if (si.getOpenFlag() != 1) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            getActivity());

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Sorry Store is closed! You can't make the order from this store.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {

                    DrConstants.storeName = si.getStoreName();
                    DrConstants.storeAddress = si.getStoreAddress();
                    DrConstants.storeId = si.getStoreId();
                    DrConstants.latitude = si.getLatitude();
                    DrConstants.longitude = si.getLongitude();
                    DrConstants.fullHours = si.is24x7();
                    DrConstants.startTime = si.getStartTime();
                    DrConstants.endTime = si.getEndTime();
                    DrConstants.orderNow = true;
                    DrConstants.CashPayment = si.isCashPayment();
                    DrConstants.OnlinePayment = si.isOnlinePayment();

                    Fragment newFragment = new OrderConfimationFragment();
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    Bundle mBundle = new Bundle();
                    mBundle.putString("start_time", DrConstants.startTime);
                    mBundle.putString("end_time", DrConstants.endTime);
                    newFragment.setArguments(mBundle);
                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    transaction.add(R.id.realtabcontent, newFragment, "store_detail");
                    transaction.hide(OrderCheckoutFragment.this);
                    transaction.addToBackStack(null);

                    // Commit the transaction
                    transaction.commit();
                }
            } else if (si.is24x7() || (DrConstants.orderType.equalsIgnoreCase("Delivery") && BM)){
                DrConstants.storeName = si.getStoreName();
                DrConstants.storeAddress = si.getStoreAddress();
                DrConstants.storeId = si.getStoreId();
                DrConstants.latitude = si.getLatitude();
                DrConstants.longitude = si.getLongitude();
                DrConstants.fullHours = si.is24x7();
                DrConstants.startTime = si.getStartTime();
                DrConstants.endTime = si.getEndTime();
                DrConstants.orderNow = true;
                DrConstants.CashPayment = si.isCashPayment();
                DrConstants.OnlinePayment = si.isOnlinePayment();

                Fragment newFragment = new OrderConfimationFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Bundle mBundle = new Bundle();
                mBundle.putString("start_time", DrConstants.startTime);
                mBundle.putString("end_time", DrConstants.endTime);
                newFragment.setArguments(mBundle);
                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.add(R.id.realtabcontent, newFragment, "store_detail");
                transaction.hide(OrderCheckoutFragment.this);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
            else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        getActivity());

                if (language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Sorry store not available for Food and Beverage orders! At this time you can order for BEANS & MERCHANDISE only.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                } else if (language.equalsIgnoreCase("Ar")) {
                    // set title
                    alertDialogBuilder.setTitle("د. كيف");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("نأسف لكم طلبات الاطعمة والمشروبات غير متوفرة الان لدى فروعنا ! في الوقت الحالي يمكنكم فقط طلب حبوب القهوة و المنتجات العينية فقط .")
                            .setCancelable(false)
                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                }

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());

            if (language.equalsIgnoreCase("En")) {
                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Sorry Store is closed! You can't make the order from this store.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            } else if (language.equalsIgnoreCase("Ar")) {
                // set title
                alertDialogBuilder.setTitle("د. كيف");

                // set dialog message
                alertDialogBuilder
                        .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
                        .setCancelable(false)
                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            }

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }
}
