package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

import java.util.List;

/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class MoreFragment  extends Fragment implements View.OnClickListener {

    ImageView mFacebook, mTwitter, mForeSquare, mSnapchat, mInstagram, mYoutube,
            mAboutUs, mHistory, mPhilosophy, mMilestone, mPerfectShot,
            mLocation, mProfile, mLanguage, mContact, mNews, rateApp, shareApp;

    View rootView;
    String language;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.more_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.more_layout_arabic, container,
                    false);
        }

        mFacebook = (ImageView) rootView.findViewById(R.id.facebook);
        mTwitter = (ImageView) rootView.findViewById(R.id.twitter);
        mForeSquare = (ImageView) rootView.findViewById(R.id.foresquare);
        mSnapchat = (ImageView) rootView.findViewById(R.id.kik);
        mInstagram = (ImageView) rootView.findViewById(R.id.instagram);
        mYoutube = (ImageView) rootView.findViewById(R.id.youtube);
        mAboutUs = (ImageView) rootView.findViewById(R.id.aboutus);
        mHistory = (ImageView) rootView.findViewById(R.id.history);
        mPhilosophy = (ImageView) rootView.findViewById(R.id.philosophy);
        mMilestone = (ImageView) rootView.findViewById(R.id.milestone);
        mPerfectShot = (ImageView) rootView.findViewById(R.id.perfectshot);
        mLanguage = (ImageView) rootView.findViewById(R.id.language);
        mLocation = (ImageView) rootView.findViewById(R.id.location);
        mProfile = (ImageView) rootView.findViewById(R.id.profile);
        mContact = (ImageView) rootView.findViewById(R.id.contact);
        mNews = (ImageView) rootView.findViewById(R.id.newsroom);
        rateApp = (ImageView) rootView.findViewById(R.id.rate_app);
        shareApp = (ImageView) rootView.findViewById(R.id.share_app);
        mFacebook.setOnClickListener(this);
        mTwitter.setOnClickListener(this);
        mForeSquare.setOnClickListener(this);
        mSnapchat.setOnClickListener(this);
        mInstagram.setOnClickListener(this);
        mYoutube.setOnClickListener(this);
        mAboutUs.setOnClickListener(this);
        mHistory.setOnClickListener(this);
        mPhilosophy.setOnClickListener(this);
        mMilestone.setOnClickListener(this);
        mPerfectShot.setOnClickListener(this);
        mLocation.setOnClickListener(this);
        mLanguage.setOnClickListener(this);
        mProfile.setOnClickListener(this);
        mNews.setOnClickListener(this);
        mContact.setOnClickListener(this);
        rateApp.setOnClickListener(this);
        shareApp.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.facebook:
                Fragment newFragment = new MoreWebView();
                Bundle mBundle = new Bundle();
                mBundle.putString("title","Facebook");
                mBundle.putString("url","https://www.facebook.com/drcafeksa");
                newFragment.setArguments(mBundle);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.add(R.id.realtabcontent, newFragment);
                transaction.hide(MoreFragment.this);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();

                break;
            case R.id.twitter:
                Fragment newFragment1 = new MoreWebView();
                Bundle mBundle1 = new Bundle();
                mBundle1.putString("title","Twitter");
                mBundle1.putString("url", "https://twitter.com/drcafeksa");
                newFragment1.setArguments(mBundle1);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction1 = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction1.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction1.add(R.id.realtabcontent, newFragment1);
                transaction1.hide(MoreFragment.this);
                transaction1.addToBackStack(null);

                // Commit the transaction
                transaction1.commit();
                break;
            case R.id.foresquare:
                Fragment newFragment2 = new MoreWebView();
                Bundle mBundle2 = new Bundle();
                mBundle2.putString("title","Foresquare");
                mBundle2.putString("url", "https://foursquare.com/user/44581694");
                newFragment2.setArguments(mBundle2);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction2 = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction2.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction2.add(R.id.realtabcontent, newFragment2);
                transaction2.hide(MoreFragment.this);
                transaction2.addToBackStack(null);

                // Commit the transaction
                transaction2.commit();
                break;
            case R.id.kik:
                Fragment newFragment5 = new MoreWebView();
                Bundle mBundle5 = new Bundle();
                mBundle5.putString("title","Snapchat");
                mBundle5.putString("url", "http://www.snapchat.com/add/drcafeksa");
                newFragment5.setArguments(mBundle5);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction5 = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction5.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction5.add(R.id.realtabcontent, newFragment5);
                transaction5.hide(MoreFragment.this);
                transaction5.addToBackStack(null);

                // Commit the transaction
                transaction5.commit();
                break;
            case R.id.instagram:
                Fragment newFragment3 = new MoreWebView();
                Bundle mBundle3 = new Bundle();
                mBundle3.putString("title","Instagram");
                mBundle3.putString("url", "http://instagram.com/drcafeksa");
                newFragment3.setArguments(mBundle3);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction3 = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction3.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction3.add(R.id.realtabcontent, newFragment3);
                transaction3.hide(MoreFragment.this);
                transaction3.addToBackStack(null);

                // Commit the transaction
                transaction3.commit();
                break;
            case R.id.youtube:
                Fragment newFragment4 = new MoreWebView();
                Bundle mBundle4 = new Bundle();
                mBundle4.putString("title","Youtube");
                mBundle4.putString("url", "http://www.youtube.com/drcafeempire");
                newFragment4.setArguments(mBundle4);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction4 = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction4.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction4.add(R.id.realtabcontent, newFragment4);
                transaction4.hide(MoreFragment.this);
                transaction4.addToBackStack(null);

                // Commit the transaction
                transaction4.commit();
                break;

            case R.id.aboutus:
                Fragment aboutUsFragment = null;
                if(language.equalsIgnoreCase("En")){
                    aboutUsFragment = new MoreRemaining();
                }else if(language.equalsIgnoreCase("Ar")){
                    aboutUsFragment = new MoreAboutusArabic();
                }

                Bundle mBundle6 = new Bundle();
                mBundle6.putString("info","aboutUs");
                aboutUsFragment.setArguments(mBundle6);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction aboutUstransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                aboutUstransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                aboutUstransaction.add(R.id.realtabcontent, aboutUsFragment);
                aboutUstransaction.hide(MoreFragment.this);
                aboutUstransaction.addToBackStack(null);

                // Commit the transaction
                aboutUstransaction.commit();
                break;
            case R.id.history:
                Fragment historyFragment = null;
                if(language.equalsIgnoreCase("En")){
                    historyFragment = new MoreRemaining();
                }else if(language.equalsIgnoreCase("Ar")){
                    historyFragment = new MoreHistoryArabic();
                }
                Bundle mBundle7 = new Bundle();
                mBundle7.putString("info","history");
                historyFragment.setArguments(mBundle7);

                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction historyTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                historyTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                historyTransaction.add(R.id.realtabcontent, historyFragment);
                historyTransaction.hide(MoreFragment.this);
                historyTransaction.addToBackStack(null);

                // Commit the transaction
                historyTransaction.commit();
                break;
            case R.id.philosophy:
                Fragment philosophyFragment = null;
                if(language.equalsIgnoreCase("En")){
                    philosophyFragment = new MoreRemaining();
                }else if(language.equalsIgnoreCase("Ar")){
                    philosophyFragment = new MorePhilosophyArabic();
                }

                Bundle mBundle8 = new Bundle();
                mBundle8.putString("info","philosophy");
                philosophyFragment.setArguments(mBundle8);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction philosophyTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                philosophyTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                philosophyTransaction.add(R.id.realtabcontent, philosophyFragment);
                philosophyTransaction.hide(MoreFragment.this);
                philosophyTransaction.addToBackStack(null);

                // Commit the transaction
                philosophyTransaction.commit();
                break;
            case R.id.milestone:
                Fragment milestoneFragment = null;
                if(language.equalsIgnoreCase("En")){
                    milestoneFragment = new MoreRemaining();
                }else if(language.equalsIgnoreCase("Ar")){
                    milestoneFragment = new MoreMilestoneArabic();
                }

                Bundle mBundle9 = new Bundle();
                mBundle9.putString("info","mileStone");
                milestoneFragment.setArguments(mBundle9);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction milestoneTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                milestoneTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                milestoneTransaction.add(R.id.realtabcontent, milestoneFragment);
                milestoneTransaction.hide(MoreFragment.this);
                milestoneTransaction.addToBackStack(null);

                // Commit the transaction
                milestoneTransaction.commit();
                break;

            case R.id.perfectshot:
                Fragment perfectshotFragment = new PerfectShotFragment();
                Bundle mBundle10 = new Bundle();
                mBundle10.putString("info","mileStone");
                perfectshotFragment.setArguments(mBundle10);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction perfectshotTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                perfectshotTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                perfectshotTransaction.add(R.id.realtabcontent, perfectshotFragment);
                perfectshotTransaction.hide(MoreFragment.this);
                perfectshotTransaction.addToBackStack(null);

                // Commit the transaction
                perfectshotTransaction.commit();
                break;

            case R.id.location:
                Fragment locationFragment = new MoreLocation();
                Bundle mBundle11 = new Bundle();
                mBundle11.putString("info","mileStone");
                locationFragment.setArguments(mBundle11);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction locationTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                locationTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                locationTransaction.add(R.id.realtabcontent, locationFragment);
                locationTransaction.hide(MoreFragment.this);
                locationTransaction.addToBackStack(null);

                // Commit the transaction
                locationTransaction.commit();
                break;

            case R.id.language:
                Fragment languagetFragment = new LanguageFragment();
                Bundle mBundle12 = new Bundle();
                mBundle12.putString("info","mileStone");
                languagetFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction languagetTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                languagetTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                languagetTransaction.add(R.id.realtabcontent, languagetFragment);
                languagetTransaction.hide(MoreFragment.this);
                languagetTransaction.addToBackStack(null);

                // Commit the transaction
                languagetTransaction.commit();
                break;

            case R.id.profile:
                Fragment myProfileFragment = new MyProfileFragment();
                Bundle mBundle13 = new Bundle();
                mBundle13.putString("info","mileStone");
                myProfileFragment.setArguments(mBundle13);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction myProfileTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                myProfileTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                myProfileTransaction.add(R.id.realtabcontent, myProfileFragment);
                myProfileTransaction.hide(MoreFragment.this);
                myProfileTransaction.addToBackStack(null);

                // Commit the transaction
                myProfileTransaction.commit();
                break;

            case R.id.newsroom:
                Fragment newsFragment = new NewsFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction newsTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                newsTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                newsTransaction.add(R.id.realtabcontent, newsFragment);
                newsTransaction.hide(MoreFragment.this);
                newsTransaction.addToBackStack(null);

                // Commit the transaction
                newsTransaction.commit();
                break;

            case R.id.contact:
                String email = "info@dr-cafe.com";
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                final PackageManager pm = getContext().getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
                String className = null;
                for (final ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                        className = info.activityInfo.name;

                        if(className != null && !className.isEmpty()){
                            break;
                        }
                    }
                }
                intent.setClassName("com.google.android.gm", className);
                intent.putExtra(Intent.EXTRA_SUBJECT, "dr.CAFE Experiance");
                intent.putExtra(Intent.EXTRA_TITLE, "dr.CAFE Experiance");
                intent.putExtra(Intent.EXTRA_EMAIL,new String[] {email});
                getActivity().startActivity(intent);
                break;
            case R.id.rate_app:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.drcafe")));
                break;

            case R.id.share_app:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.cs.drcafe&hl=en");
                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                        "Check out dr.CAFE Coffee App !");
                startActivity(Intent.createChooser(shareIntent, "Share"));
                break;
        }

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}