package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by SKT on 21-02-2016.
 */
public class CommentFragment extends Fragment {

    EditText commentBox;
    TextView addButton, itemName;
    ImageView itemIcon;
    Button backBtn;
    String itemId, title;
    String language;
    View rootView;
    private DataBaseHelper myDbHelper;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.comment_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.comment_layout_arabic, container,
                    false);
        }

        myDbHelper = new DataBaseHelper(getActivity());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
        itemId = getArguments().getString("itemId");
        title = getArguments().getString("title");
        commentBox = (EditText) rootView.findViewById(R.id.comment_box);
        addButton = (TextView) rootView.findViewById(R.id.add_comment_button);
        itemName = (TextView) rootView.findViewById(R.id.item_name);
        itemIcon = (ImageView) rootView.findViewById(R.id.item_icon);


        try {
            // get input stream
            InputStream ims = getActivity().getAssets().open(itemId + ".png");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            itemIcon.setImageDrawable(d);
        } catch (IOException ex) {
        }
//        itemName.setText(WordUtils.capitalizeFully(title).replace("Ml", "ML").replace("ml", "ML"));

        if(!title.contains("dr.CAFE")) {
            itemName.setText(WordUtils.capitalizeFully(title).replace("Ml", "ML").replace("ml", "ML"));
        }
        else{
            itemName.setText(WordUtils.capitalizeFully(title).replace("Dr.cafe", "dr.CAFE").replace("ml", "ML"));
        }

        if(getArguments().getString("screen").equals("food")){
            commentBox.setText(getArguments().getString("comment"));
        }
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        if(DrConstants.comment.equals("")){

        }else{
            commentBox.setText(DrConstants.comment);
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = commentBox.getText().toString();
                if(getArguments().getString("screen").equals("additional")) {
                    DrConstants.comment = text;
                }else if(getArguments().getString("screen").equals("food")) {
                    myDbHelper.updateComment(getArguments().getString("orderId"), text);
                }
                getActivity().onBackPressed();
            }

        });

        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
