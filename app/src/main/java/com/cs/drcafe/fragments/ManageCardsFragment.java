package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

/**
 * Created by SKT on 21-02-2016.
 */
public class ManageCardsFragment extends Fragment {

    LinearLayout dcCardLayout, creditCardLayout;
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.manage_cards_new, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.manage_cards_new_ar, container,
                    false);
        }


        creditCardLayout = (LinearLayout) rootView.findViewById(R.id.credit_card_layout);
        dcCardLayout = (LinearLayout) rootView.findViewById(R.id.dc_card_layout);

        dcCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment giftsFragment = new CardFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, giftsFragment);
                nutritionTransaction.hide(ManageCardsFragment.this);
                nutritionTransaction.addToBackStack(null);

                // Commit the transaction
                nutritionTransaction.commit();
            }
        });

        creditCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment giftsFragment = new CreditCardsFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, giftsFragment);
                nutritionTransaction.hide(ManageCardsFragment.this);
                nutritionTransaction.addToBackStack(null);

                // Commit the transaction
                nutritionTransaction.commit();
            }
        });

        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
