package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.FreeGiftSelection;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.SplashScreen;
import com.cs.drcafe.adapter.MyGiftsAdapter;
import com.cs.drcafe.model.MyGifts;
import com.cs.drcafe.model.Tiers;
import com.cs.drcafe.model.WalletHistory;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by CS on 31-01-2018.
 */

public class WalletDashboardFragment extends Fragment {

    private String SOAP_ACTION = "http://tempuri.org/GetUserGifts";
    private String METHOD_NAME = "GetUserGifts";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;

    private String SOAP_ACTION_WALLET = "http://tempuri.org/GetWalletDashboard";
    private String METHOD_NAME_WALLET = "GetWalletDashboard";
    private SoapObject result_wallet;
    private String response12_wallet = null;

    private ArrayList<MyGifts> giftsList = new ArrayList<>();
    private ArrayList<WalletHistory> walletHistoryList = new ArrayList<>();
    private ListView giftsListView;
    private MyGiftsAdapter mAdapter;
    ProgressDialog dialog;
    String language;
    SharedPreferences userPrefs;
    String userId;
    TextView giftTextPrice, giftTextBadge, cartCount;
    int currentPoints = 0, minPoints = 0, maxPoints = 1500, progress;
    LinearLayout thumbText, walletHistoryLayout;
    private DataBaseHelper myDbHelper;
    Button backBtn;
    SeekBar seekBar;
    View rootView;
    TextView tvAvailablePoints, tvAvailableSR, tvCurrentPoints;
    String AvailablePoints, UsedPoints, AchievedPoints, pricePerPoint;
    TextView tvBeginnerTitle, tvBeginnerLimit, tvSilverTitle, tvSilverLimit, tvGoldTitle, tvGoldLimit, tvPlatinumTitle, tvPlatinumLimit;
    String boxMessage;
    ArrayList<Tiers> tiersList = new ArrayList<>();
    ScrollView sv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        rootView = inflater.inflate(R.layout.wallet_dashboard_new, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        myDbHelper = new DataBaseHelper(getActivity());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
        }
        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        giftTextPrice = (TextView) rootView.findViewById(R.id.giftText2);
        giftTextBadge = (TextView) rootView.findViewById(R.id.giftText5);
        tvAvailablePoints = (TextView) rootView.findViewById(R.id.availablePoints);
        tvAvailableSR = (TextView) rootView.findViewById(R.id.availablePointsAmount);
        tvCurrentPoints = (TextView) rootView.findViewById(R.id.currentPoints);
        tvSilverTitle = (TextView) rootView.findViewById(R.id.silverTitle);
        tvSilverLimit = (TextView) rootView.findViewById(R.id.silverTarget);
        tvGoldTitle = (TextView) rootView.findViewById(R.id.goldTitle);
        tvGoldLimit = (TextView) rootView.findViewById(R.id.goldTarget);
        tvPlatinumTitle = (TextView) rootView.findViewById(R.id.platinumTitle);
        tvPlatinumLimit = (TextView) rootView.findViewById(R.id.platinumTarget);
        tvBeginnerTitle = (TextView) rootView.findViewById(R.id.beginnerTitle);
        tvBeginnerLimit = (TextView) rootView.findViewById(R.id.beginnerTarget);
        cartCount = (TextView) rootView.findViewById(R.id.cart_count);

        seekBar = (SeekBar) rootView.findViewById(R.id.seekBar);

        thumbText = (LinearLayout) rootView.findViewById(R.id.thumbText);
        walletHistoryLayout = (LinearLayout) rootView.findViewById(R.id.walletHistory);

        giftsListView = (ListView) rootView.findViewById(R.id.myGiftsListView);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);

        sv = (ScrollView) rootView.findViewById(R.id.scrollView2);
        sv.smoothScrollTo(0,0);

        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });
        new GetWalletDashboard().execute();

        cartCount.setText("" + myDbHelper.getTotalOrderQty());

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        walletHistoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment giftsFragment = new WalletHistoryFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, giftsFragment);
                nutritionTransaction.hide(WalletDashboardFragment.this);
                nutritionTransaction.addToBackStack(null);
                // Commit the transaction
                nutritionTransaction.commit();
            }
        });

        giftsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment giftsFragment = new FreeGiftSelection();
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("array", giftsList);
                mBundle.putInt("position", position);
                giftsFragment.setArguments(mBundle);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, giftsFragment);
                nutritionTransaction.hide(WalletDashboardFragment.this);
                nutritionTransaction.addToBackStack(null);
                // Commit the transaction
                nutritionTransaction.commit();
            }
        });
        return rootView;
    }

    public void setAvailablePoints(){
        int available = Integer.parseInt(AvailablePoints);
        float price = Float.parseFloat(pricePerPoint);

        tvAvailablePoints.setText(AvailablePoints);
        String priceStr = String.format("%.2f", (available*price));
        tvAvailableSR.setText("("+priceStr+"SR)");
        setSeekBarProgress();
    }
    public void setSeekBarProgress(){
        float price = Float.parseFloat(pricePerPoint);
        int achieved = Integer.parseInt(AchievedPoints);
        int beginner = Integer.parseInt(tiersList.get(0).getTierlimit());
        int silver = Integer.parseInt(tiersList.get(1).getTierlimit());
        int gold = Integer.parseInt(tiersList.get(2).getTierlimit());
        int platinum = Integer.parseInt(tiersList.get(3).getTierlimit());
        maxPoints = platinum;
        currentPoints = achieved;

        tvBeginnerTitle.setText(tiersList.get(0).getTierName());
        tvSilverTitle.setText(tiersList.get(1).getTierName());
        tvGoldTitle.setText(tiersList.get(2).getTierName());
        tvPlatinumTitle.setText(tiersList.get(3).getTierName());
        tvBeginnerLimit.setText("("+beginner + " points/\n"+Math.round(beginner*price)+"SR)");
        tvSilverLimit.setText("("+silver + " points/\n"+Math.round(silver*price)+"SR)");
        tvGoldLimit.setText("("+gold + " points/\n"+Math.round(gold*price)+"SR)");
        tvPlatinumLimit.setText("("+platinum+ " points/\n"+Math.round(platinum*price)+"SR)");
        tvCurrentPoints.setText("("+achieved+ " points/"+Math.round(achieved*price)+"SR)");

        float finalp = (float) (maxPoints - currentPoints)/maxPoints;
        progress = 100 - Math.round(finalp*100);
        seekBar.setProgress(progress);

        ViewTreeObserver vto = thumbText.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int val = (progress * ((seekBar.getWidth()-275) - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                float xValue = seekBar.getX() + val + seekBar.getThumbOffset() / 2;
                xValue = xValue - 50;
                thumbText.setX(xValue);
            }
        });
    }

    public class GetWalletDashboard extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {
                if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                }else {
                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_WALLET);
                    request.addProperty("userId", userId);
                    request.addProperty("DeviceToken", SplashScreen.regid);
                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_WALLET, envelope);
                    result_wallet = (SoapObject) envelope.getResponse();
                    response12_wallet = result_wallet.toString();
                    Log.v("TAG","wallet response " + response12_wallet);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count wallet: " + result_wallet);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("TAG", "wallet response " + response12_wallet);

            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(response12_wallet == null){
                dialog.dismiss();

            }else if(response12_wallet.equalsIgnoreCase("no internet")){
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Communication Error! Please check the internet connection?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            } else {
                dialog.dismiss();
                try {

                    Object property = result_wallet.getProperty(1);
                    if (property instanceof SoapObject) {
                        try {
                            SoapObject category_list = (SoapObject) property;
                            if(category_list.getPropertyCount()>0) {
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    SoapObject summaryObject = (SoapObject) storeObject.getProperty(i);
                                    try {
                                        AvailablePoints = summaryObject.getPropertyAsString("AvailablePoints");
                                        UsedPoints = summaryObject.getPropertyAsString("UsedPoints");
                                        AchievedPoints = summaryObject.getPropertyAsString("AchievedPoints");
                                        pricePerPoint = summaryObject.getPropertyAsString("PointValue");
                                        boxMessage = summaryObject.getPropertyAsString("BoxMessage");
                                        tvAvailablePoints.setText(AvailablePoints);
                                    } catch (Exception e) {
//                                        SoapObject tiersObject = (SoapObject) storeObject.getProperty("TierMaster");

                                        SoapObject tiersObject = null;
                                        try {
                                            tiersObject = (SoapObject) storeObject.getProperty(i);
                                            Tiers tiers = new Tiers();
                                            tiers.setTierName(tiersObject.getPropertyAsString("Title"));
                                            tiers.setTierlimit(tiersObject.getPropertyAsString("PointsToReach"));
                                            tiersList.add(tiers);
                                        } catch (Exception e1) {
                                            e1.printStackTrace();

                                            try {
                                                MyGifts myGifts = new MyGifts();
                                                SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                                String desc = sObj.getPropertyAsString("Desc_En");
                                                String descAr = sObj.getPropertyAsString("Desc_Ar");
                                                String remainingBonus = sObj.getPropertyAsString("RemainingBonus");
                                                String endDate = sObj.getPropertyAsString("PEndDate");
                                                String image = sObj.getPropertyAsString("Img");
                                                String promoType = sObj.getPropertyAsString("PromoType");
                                                String CategoryId = sObj.getPropertyAsString("CategoryId");
                                                String SubCategoryId = sObj.getPropertyAsString("SubCategoryId");
                                                String ItemId = sObj.getPropertyAsString("ItemId");
                                                String Size = sObj.getPropertyAsString("Size");
                                                String Limit = sObj.getPropertyAsString("Limit");
                                                String MinSpend = sObj.getPropertyAsString("MinSpend");
                                                String ValidForDays = sObj.getPropertyAsString("ValidForDays");

                                                remainingBonus = Integer.toString(Math.round(Float.parseFloat(remainingBonus)));
                                                myGifts.setDesc_En(desc);
                                                myGifts.setDesc_Ar(descAr);
                                                myGifts.setRemainingBonus(remainingBonus);
                                                myGifts.setEndDate(endDate);
                                                myGifts.setImage(image);
                                                myGifts.setPromoType(promoType);
                                                myGifts.setCategoryId(CategoryId);
                                                myGifts.setSubCategoryId(SubCategoryId);
                                                myGifts.setItemId(ItemId);
                                                myGifts.setSize(Size);
                                                myGifts.setLimit(Limit);
                                                myGifts.setMinSpend(MinSpend);
                                                myGifts.setValidForDays(ValidForDays);

                                                giftsList.add(myGifts);
                                            } catch (Exception e2) {
                                                e2.printStackTrace();
                                            }
                                            Collections.reverse(giftsList);
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            setAvailablePoints();
                            if(giftsList.size()>0){
                                mAdapter = new MyGiftsAdapter(getContext(), giftsList, language);
                                giftsListView.setAdapter(mAdapter);
                                sv.smoothScrollTo(0,0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }
            }
            super.onPostExecute(result1);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        try {
            sv.smoothScrollTo(0,0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
