package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.WebViewActivity;
import com.cs.drcafe.adapter.CardsAdapter;
import com.cs.drcafe.adapter.CardsAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;


/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class CardFragment extends Fragment {
    private String SOAP_ACTION = "http://tempuri.org/getDCCardBalance";
    private String METHOD_NAME = "getDCCardBalance";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapPrimitive result;
    private String response12 = null;
    private ImageView addCard, transactions, menu;
    private LinearLayout cardListLayout;
    SwipeMenuListView cardsListView;
    AlertDialog alertDialog;
    SharedPreferences cardPrefs;
    SharedPreferences.Editor cardEditor;
    String savedCards;
    ArrayList<String> savedCardsList = new ArrayList<>();
    ArrayList<String> cardsList = new ArrayList<>();
    CardsAdapter mCardsAdapter;
    CardsAdapterArabic mCardsAdapterArabic;
    View rootView;
    ProgressDialog dialog;
    Button backBtn;
    String language;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.card_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.card_layout_arabic, container,
                    false);
        }
        cardPrefs = getActivity().getSharedPreferences("CARD_PREFS", Context.MODE_PRIVATE);
        cardEditor  = cardPrefs.edit();

        savedCards = cardPrefs.getString("saved_cards", null);

        addCard = (ImageView) rootView.findViewById(R.id.card);
        transactions = (ImageView) rootView.findViewById(R.id.transactions);
        menu = (ImageView) rootView.findViewById(R.id.card_menu);
        cardsListView = (SwipeMenuListView) rootView.findViewById(R.id.cardsListView);
        backBtn = (Button) rootView.findViewById(R.id.back_btn1);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        if(language.equalsIgnoreCase("En")){
            mCardsAdapter = new CardsAdapter(getActivity(), cardsList);
            cardsListView.setAdapter(mCardsAdapter);
        }else if(language.equalsIgnoreCase("Ar")){
            mCardsAdapterArabic = new CardsAdapterArabic(getActivity(), cardsList);
            cardsListView.setAdapter(mCardsAdapterArabic);
        }

//        cardListLayout = (LinearLayout) rootView.findViewById(R.id.card_list_layout);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });

        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getActivity().getLayoutInflater();
                if(language.equalsIgnoreCase("En")){
                    dialogView = inflater.inflate(R.layout.addcard_dialog, null);
                }else if(language.equalsIgnoreCase("Ar")){
                    dialogView = inflater.inflate(R.layout.addcard_dialog_arabic, null);
                }

                dialogBuilder.setView(dialogView);

                final EditText cardNumber = (EditText) dialogView.findViewById(R.id.card_addcardnumber);
                final EditText cardPassword = (EditText) dialogView.findViewById(R.id.card_password);
                ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                Button okBtn = (Button) dialogView.findViewById(R.id.ok_button);
                okBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String number = cardNumber.getText().toString();
                        String password = cardPassword.getText().toString();

                        Log.i("TAG",""+number.length()+" "+password.length());
                            if(number.length() ==0){
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        getActivity());

                                // set title
                                alertDialogBuilder.setTitle("dr.CAFE");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("Enter the dr.CAFE CARD Number")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                dialog.dismiss();
                                            }
                                        });

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }else if(password.length() == 0) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        getActivity());

                                // set title
                                alertDialogBuilder.setTitle("dr.CAFE");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("Enter 6 Digits Pin Number")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                dialog.dismiss();
                                            }
                                        });

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }else if(password.length() != 6){
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                            getActivity());

                                    // set title
                                    alertDialogBuilder.setTitle("dr.CAFE");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Password must be 6 digits")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,int id) {
                                                    dialog.dismiss();
                                                }
                                            });

                                    // create alert dialog
                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                    // show it
                                    alertDialog.show();


                            }else{

                                new GetCardBalance().execute(number,password);

                            }


                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

            }
        });

        transactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cardsList.size() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Enter the dr.CAFE CARD Number")
                            .setCancelable(false)
                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Fragment newFragment = new CardTransactionsFragment();
                    Bundle mBundle = new Bundle();
//                mBundle.putString("storeId", storesList.get(i).getStoreId());
//                mBundle.putString("storeName", storesList.get(i).getStoreName());
                    newFragment.setArguments(mBundle);
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    transaction.add(R.id.realtabcontent, newFragment, "card_transactions");
                    transaction.hide(CardFragment.this);
                    transaction.addToBackStack("card_transactions");

                    // Commit the transaction
                    transaction.commit();
                }
            }
        });



        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity().getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        cardsListView.setMenuCreator(creator);

        // step 2. listener item click event
        cardsListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        // delete
//					delete(item);
                        savedCards = null;
                        cardsList.remove(position);
                        savedCardsList.remove(position);
                        mCardsAdapter.notifyDataSetChanged();
                        for (int i = 0; i < savedCardsList.size(); i++) {
                            if (savedCards == null) {
                                savedCards = savedCardsList.get(0);
                            }else{
                                savedCards = savedCards+","+savedCardsList.get(i);
                            }
                        }

                        cardEditor.putString("saved_cards", savedCards);
                        cardEditor.commit();
                        break;
                }
                return false;
            }
        });

        cardsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[] cards = cardsList.get(position).split(":");
                DrConstants.LATEST_CARD = cards[1];
                DrConstants.LATEST_BALANCE = cards[0];
                mCardsAdapter.notifyDataSetChanged();
            }
        });

        Log.i("TAG", "" + savedCards);
        if(savedCards != null){
            String[] parts = savedCards.split(",");
            for(int i= 0;i < parts.length;i++){
                String[] card = parts[i].split(":");
                if(!savedCardsList.contains(parts[i])) {
                    savedCardsList.add(parts[i]);
                    new GetCardBalance().execute(card[0], card[1]);
                }
            }
        }else{

        }


        return rootView;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    public class GetCardBalance extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            cardNumber = arg0[0];
            password = arg0[1];
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("dcCardNo", arg0[0]);
                    request.addProperty("dcActivationCode", arg0[1]);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapPrimitive) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){

//                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    if(response12 == null){
//                        dialog.dismiss();
                    }else if (response12.equalsIgnoreCase("Invalid Card No. or Activation Code")) {
//                        dialog.dismiss();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                getActivity());

                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(response12)
                                .setCancelable(false)
                                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    } else {

                        response12 = response12.replace("{", "");
                        response12 = response12.replace("}", "");
                        response12 = response12.replace("Balance", "");
                        response12 = response12.replace("=", "");
                        response12 = response12.replaceAll(" ", "");
                        response12 = String.format("%.2f", Float.parseFloat(response12));


                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }

                        savedCards = null;
                        if (!savedCardsList.contains(cardNumber + ":" + password)) {
                            savedCardsList.add(cardNumber + ":" + password);

                        }

                        if (!cardsList.contains(response12 + ":" + cardNumber)) {
                            cardsList.add(response12 + ":" + cardNumber);
                        }
                        for (int i = 0; i < savedCardsList.size(); i++) {
                            if (savedCards == null) {
                                savedCards = savedCardsList.get(0);
                            } else {
                                savedCards = savedCards + "," + savedCardsList.get(i);
                            }
                        }
//            Log.i("TAG",favStoreIds);

                        DrConstants.LATEST_CARD = cardNumber;
                        DrConstants.LATEST_BALANCE = response12;
                        cardEditor.putString("saved_cards", savedCards);
                        cardEditor.commit();
//                        dialog.dismiss();
                        if (language.equalsIgnoreCase("En")) {
                            mCardsAdapter.notifyDataSetChanged();
                        }else if(language.equalsIgnoreCase("Ar")){
                            mCardsAdapterArabic.notifyDataSetChanged();
                        }
                    }
                }
            }
            dialog.dismiss();


            super.onPostExecute(result1);
        }
    }


    public void dialog() {

        final Dialog dialog2 = new Dialog(getActivity());

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if(language.equalsIgnoreCase("En")){
            dialog2.setContentView(R.layout.card_terms_dialog);
        }else if(language.equalsIgnoreCase("Ar")){
            dialog2.setContentView(R.layout.card_terms_dialog_arabic);
        }
        dialog2.setCanceledOnTouchOutside(true);
        TextView terms = (TextView) dialog2.findViewById(R.id.card_terms);
        TextView disclaimer = (TextView) dialog2.findViewById(R.id.disclaimer);
        TextView cancel = (TextView) dialog2.findViewById(R.id.cancel_button);

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                Intent loginIntent = new Intent(getActivity(), WebViewActivity.class);
                loginIntent.putExtra("webview_toshow", "card_terms");
                startActivity(loginIntent);
            }
        });

        disclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                Intent loginIntent = new Intent(getActivity(), WebViewActivity.class);
                loginIntent.putExtra("webview_toshow", "desclaimer");
                startActivity(loginIntent);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });




        dialog2.show();

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}