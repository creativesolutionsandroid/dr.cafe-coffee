package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.StoreInfo;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;

/**
 * Created by SKT on 01-01-2016.
 */
public class MenuFragment extends Fragment implements View.OnClickListener{

    RelativeLayout hotBeverages, checkoutLayout, pastry, sandwich, desert;
    View v12Line;
    ImageView checkoutImage, bannerImage;
    public static TextView orderPrice, orderQuantity, cartCount;
    LinearLayout checkoutOrder;
    LinearLayout coldBeverages, snacks, beans, merchandise, v12Layout;
    private DataBaseHelper myDbHelper;
    Button backBtn, back1;
    private View rootView;
    String language;
    private String SOAP_ACTION = "http://tempuri.org/GetSeasonalPromotions";
    private String METHOD_NAME = "GetSeasonalPromotions";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    RelativeLayout iftarMealLayout;
//    SharedPreferences languagePrefs;
//    SharedPreferences.Editor languageEditor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.menu_screen_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.menu_screen_layout_arabic, container,
                    false);
        }
        myDbHelper = new DataBaseHelper(getActivity());
        try {

            myDbHelper.createDataBase();

        } catch (IOException ioe) {

        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        bannerImage = (ImageView) rootView.findViewById(R.id.ramadan_banner);
        checkoutImage = (ImageView) rootView.findViewById(R.id.checkout_image);
        checkoutLayout = (RelativeLayout) rootView.findViewById(R.id.checkout_layout);
        hotBeverages = (RelativeLayout) rootView.findViewById(R.id.hot_beverages_layout);
        coldBeverages = (LinearLayout) rootView.findViewById(R.id.cold_beverages_layout);
        pastry = (RelativeLayout) rootView.findViewById(R.id.pastry_layout);
        sandwich = (RelativeLayout) rootView.findViewById(R.id.sandwich_layout);
        desert = (RelativeLayout) rootView.findViewById(R.id.desert_layout);
        snacks = (LinearLayout) rootView.findViewById(R.id.snacks_layout);
        beans = (LinearLayout) rootView.findViewById(R.id.beans_layout);
        v12Layout = (LinearLayout) rootView.findViewById(R.id.v12_layout);
        merchandise = (LinearLayout) rootView.findViewById(R.id.merchandise_layout);
        checkoutOrder = (LinearLayout) rootView.findViewById(R.id.checkout_order_layout);
        orderPrice = (TextView) rootView.findViewById(R.id.order_price);
        orderQuantity = (TextView) rootView.findViewById(R.id.order_quantity);
        cartCount = (TextView) rootView.findViewById(R.id.cart_count);
        iftarMealLayout = (RelativeLayout) rootView.findViewById(R.id.ramadan_meal_layout);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        back1 = (Button) rootView.findViewById(R.id.back_btn1);
        v12Line = (View) rootView.findViewById(R.id.view12);
        iftarMealLayout.setVisibility(View.GONE);

        if (DrConstants.orderType.equalsIgnoreCase("Delivery")) {
            setDeliveryCategoryItems();
        }
        else {
            setCategoryItems();
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myDbHelper.getTotalOrderQty()>0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    String title= "", msg= "", posBtn = "", negBtn = "";
                    if(language.equalsIgnoreCase("En")){
                        posBtn = "Yes";
                        negBtn = "No";
                        title = "dr.CAFE";
                        msg = "You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear. Do you want to continue?";
                    }else if(language.equalsIgnoreCase("Ar")){
                        posBtn = "نعم";
                        negBtn = "لا";
                        title = "د. كيف";
                        msg = "لديك " + myDbHelper.getTotalOrderQty()+ "  منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات . هل تود الاستمرار";
                    }
                    // set title
                    alertDialogBuilder.setTitle(title);

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    myDbHelper.deleteOrderTable();
                                    try {

                                        MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                        MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                        MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                                        CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                        CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                        CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                                        OrderCheckoutFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                        OrderCheckoutFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }

                                    ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
                                    FragmentManager manager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction trans = manager.beginTransaction();
                                    trans.remove(new MenuFragment());
                                    trans.commit();

                                    FragmentManager manager1 = getActivity().getSupportFragmentManager();
                                    FragmentTransaction trans1 = manager1.beginTransaction();
                                    trans1.remove(new OrderFragment());
                                    trans1.commit();

                                    manager1.popBackStack();
                                    manager.popBackStack();

                                    Fragment radiusFragment = new SelectStoreFragment();
                                    // consider using Java coding conventions (upper first char class names!!!)
                                    FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack
                                    radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                                    radiusTransaction.add(R.id.realtabcontent, radiusFragment, "selectstore");
                                    radiusTransaction.hide(MenuFragment.this);
                                    radiusTransaction.addToBackStack(null);

                                    // Commit the transaction
                                    radiusTransaction.commit();
                                }
                            }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }else {
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    trans.remove(new MenuFragment());
                    trans.commit();

                    FragmentManager manager1 = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans1 = manager1.beginTransaction();
                    trans1.remove(new OrderFragment());
                    trans1.commit();

                    manager1.popBackStack();
                    manager.popBackStack();

                    Fragment radiusFragment = new SelectStoreFragment();
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    radiusTransaction.add(R.id.realtabcontent, radiusFragment, "selectstore");
                    radiusTransaction.hide(MenuFragment.this);
                    radiusTransaction.addToBackStack(null);
                    radiusTransaction.commit();
//                    ((MainActivity) getActivity()).setCurrenTab(0);
//                    ((MainActivity) getActivity()).setCurrenTab(2);
                }
            }
        });
//        if(DrConstants.menuFlag){
//            if(myDbHelper.getTotalOrderQty()>0){
//
//            }else {
//                backBtn.setVisibility(View.GONE);
//                back1.setVisibility(View.GONE);
//                DrConstants.menuFlag = false;
//            }
//        }
        hotBeverages.setOnClickListener(this);
        coldBeverages.setOnClickListener(this);
        pastry.setOnClickListener(this);
        sandwich.setOnClickListener(this);
        desert.setOnClickListener(this);
        snacks.setOnClickListener(this);
        beans.setOnClickListener(this);
        merchandise.setOnClickListener(this);
        v12Layout.setOnClickListener(this);
        checkoutOrder.setOnClickListener(this);
        cartCount.setOnClickListener(this);
        iftarMealLayout.setOnClickListener(this);

        try {
            bannerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment pastryFragment1 = new CategoriesListFragment();
                    Bundle mBundle21 = new Bundle();
                    mBundle21.putInt("openfrom", 0);
                    mBundle21.putString("catId", "2");
                    pastryFragment1.setArguments(mBundle21);
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction pastryTransaction1 = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack

                    pastryTransaction1.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    pastryTransaction1.add(R.id.realtabcontent, pastryFragment1, "category");
                    pastryTransaction1.hide(MenuFragment.this);
                    pastryTransaction1.addToBackStack(null);

                    // Commit the transaction
                    pastryTransaction1.commit();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        ViewTreeObserver vto = checkoutImage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                checkoutImage.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = checkoutImage.getMeasuredHeight();
                Log.i("Height TAG", "" + finalHeight);
                LinearLayout.LayoutParams newsCountParam = new
                        LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                checkoutLayout.setLayoutParams(newsCountParam);
                return true;
            }
        });

//        try {
            orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
            orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        cartCount.setText("" + myDbHelper.getTotalOrderQty());
//        } catch (SQLiteException e) {
//            e.printStackTrace();
//        }

        // Commented on 20-11-2020
//        new GetRMTime().execute();

        return rootView;
    }

    private void setCategoryItems(){
        SharedPreferences storePrefs = getContext().getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String jsonText = storePrefs.getString("store", null);

        StoreInfo si = gson.fromJson(jsonText, StoreInfo.class);

        String itemsJson = si.getIemsJson();
        try {
            JSONObject mainObject = new JSONObject(itemsJson);
            JSONObject categoryObj = mainObject.getJSONObject("ICategories");
            Log.d("TAG", "ICategories: "+categoryObj.toString());
            if(!categoryObj.getBoolean("Hot Beverages")) {
                hotBeverages.setAlpha(0.5f);
                hotBeverages.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Cold Beverages")) {
                coldBeverages.setAlpha(0.5f);
                coldBeverages.setEnabled(false);
            }
            if(!categoryObj.getBoolean("V12 Coffee")) {
                v12Layout.setAlpha(0.5f);
                v12Layout.setEnabled(false);
                v12Layout.setVisibility(View.GONE);
                v12Line.setVisibility(View.GONE);
            }
            if(!categoryObj.getBoolean("Pastry")) {
                pastry.setAlpha(0.5f);
                pastry.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Sandwich")) {
                sandwich.setAlpha(0.5f);
                sandwich.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Dessert")) {
                desert.setAlpha(0.5f);
                desert.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Beans")) {
                beans.setAlpha(0.5f);
                beans.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Merchandise")) {
                merchandise.setAlpha(0.5f);
                merchandise.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Snacks")) {
                snacks.setAlpha(0.5f);
                snacks.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject mainObject = new JSONObject(itemsJson);
            JSONObject itemsObj = mainObject.getJSONObject("Items");
            String itemIds = itemsObj.getString("FOODS");
            if (itemIds.contains("327")) {
                bannerImage.setVisibility(View.VISIBLE);
                bannerImage.setImageDrawable(getResources().getDrawable(R.drawable.banner_327));
            }
            else if (itemIds.contains("475")) {
                bannerImage.setVisibility(View.VISIBLE);
                bannerImage.setImageDrawable(getResources().getDrawable(R.drawable.banner_475));
            }
            else if (itemIds.contains("501")) {
                bannerImage.setVisibility(View.VISIBLE);
                bannerImage.setImageDrawable(getResources().getDrawable(R.drawable.banner_501));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDeliveryCategoryItems(){
        SharedPreferences storePrefs = getContext().getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String jsonText = storePrefs.getString("store", null);
        StoreInfo si = gson.fromJson(jsonText, StoreInfo.class);

        String itemsJson = si.getDeliveryItems();
        try {
            JSONObject mainObject = new JSONObject(itemsJson);
            JSONObject categoryObj = mainObject.getJSONObject("ICategories");
            Log.d("TAG", "ICategories: "+categoryObj.toString());
            if(!categoryObj.getBoolean("Hot Beverages")) {
                hotBeverages.setAlpha(0.5f);
                hotBeverages.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Cold Beverages")) {
                coldBeverages.setAlpha(0.5f);
                coldBeverages.setEnabled(false);
            }
            if(!categoryObj.getBoolean("V12 Coffee")) {
                v12Layout.setAlpha(0.5f);
                v12Layout.setEnabled(false);
                v12Layout.setVisibility(View.GONE);
                v12Line.setVisibility(View.GONE);
            }
            if(!categoryObj.getBoolean("Pastry")) {
                pastry.setAlpha(0.5f);
                pastry.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Sandwich")) {
                sandwich.setAlpha(0.5f);
                sandwich.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Dessert")) {
                desert.setAlpha(0.5f);
                desert.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Beans")) {
                beans.setAlpha(0.5f);
                beans.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Merchandise")) {
                merchandise.setAlpha(0.5f);
                merchandise.setEnabled(false);
            }
            if(!categoryObj.getBoolean("Snacks")) {
                snacks.setAlpha(0.5f);
                snacks.setEnabled(false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject mainObject = new JSONObject(itemsJson);
            JSONObject itemsObj = mainObject.getJSONObject("Items");
            String itemIds = itemsObj.getString("FOODS");
            if (itemIds.contains("327")) {
                bannerImage.setVisibility(View.VISIBLE);
                bannerImage.setImageDrawable(getResources().getDrawable(R.drawable.banner_327));
            }
            else if (itemIds.contains("475")) {
                bannerImage.setVisibility(View.VISIBLE);
                bannerImage.setImageDrawable(getResources().getDrawable(R.drawable.banner_475));
            }
            else if (itemIds.contains("501")) {
                bannerImage.setVisibility(View.VISIBLE);
                bannerImage.setImageDrawable(getResources().getDrawable(R.drawable.banner_501));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.hot_beverages_layout:
                Fragment hotBeveragesFragment = new CategoriesListFragment();
                Bundle mBundle = new Bundle();
                mBundle.putInt("openfrom", 0);
                mBundle.putString("catId", "1");
                hotBeveragesFragment.setArguments(mBundle);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction hotBeveragesTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                hotBeveragesTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                hotBeveragesTransaction.add(R.id.realtabcontent, hotBeveragesFragment, "category");
                hotBeveragesTransaction.hide(MenuFragment.this);
                hotBeveragesTransaction.addToBackStack(null);

                // Commit the transaction
                hotBeveragesTransaction.commit();
                break;
            case R.id.cold_beverages_layout:
                Fragment coldBeveragesFragment = new CategoriesListFragment();
                Bundle mBundle1 = new Bundle();
                mBundle1.putInt("openfrom", 5);
                mBundle1.putString("catId", "1");
                coldBeveragesFragment.setArguments(mBundle1);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction coldBeveragesTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                coldBeveragesTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                coldBeveragesTransaction.add(R.id.realtabcontent, coldBeveragesFragment, "category");
                coldBeveragesTransaction.hide(MenuFragment.this);
                coldBeveragesTransaction.addToBackStack(null);

                // Commit the transaction
                coldBeveragesTransaction.commit();
                break;
            case R.id.v12_layout:
                Fragment v12Fragment = new CategoriesListFragment();
                Bundle mBundlev12 = new Bundle();
                mBundlev12.putInt("openfrom", 0);
                mBundlev12.putString("catId", "9");
                v12Fragment.setArguments(mBundlev12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction v12Transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                v12Transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                v12Transaction.add(R.id.realtabcontent, v12Fragment, "category");
                v12Transaction.hide(MenuFragment.this);
                v12Transaction.addToBackStack(null);

                // Commit the transaction
                v12Transaction.commit();
                break;
            case R.id.pastry_layout:
                Fragment pastryFragment = new CategoriesListFragment();
                Bundle mBundle2 = new Bundle();
                mBundle2.putInt("openfrom", 0);
                mBundle2.putString("catId", "2");
                pastryFragment.setArguments(mBundle2);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction pastryTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                pastryTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                pastryTransaction.add(R.id.realtabcontent, pastryFragment, "category");
                pastryTransaction.hide(MenuFragment.this);
                pastryTransaction.addToBackStack(null);

                // Commit the transaction
                pastryTransaction.commit();
                break;
            case R.id.sandwich_layout:
                Fragment sandwichFragment = new CategoriesListFragment();
                Bundle mBundle3 = new Bundle();
                mBundle3.putInt("openfrom", 12);
                mBundle3.putString("catId", "2");
                sandwichFragment.setArguments(mBundle3);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction sandwichTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                sandwichTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                sandwichTransaction.add(R.id.realtabcontent, sandwichFragment, "category");
                sandwichTransaction.hide(MenuFragment.this);
                sandwichTransaction.addToBackStack(null);

                // Commit the transaction
                sandwichTransaction.commit();
                break;
            case R.id.desert_layout:
                Fragment desertFragment = new CategoriesListFragment();
                Bundle mBundle4 = new Bundle();
                mBundle4.putInt("openfrom", 21);
                mBundle4.putString("catId", "2");
                desertFragment.setArguments(mBundle4);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction desertTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                desertTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                desertTransaction.add(R.id.realtabcontent, desertFragment, "category");
                desertTransaction.hide(MenuFragment.this);
                desertTransaction.addToBackStack(null);

                // Commit the transaction
                desertTransaction.commit();
                break;
            case R.id.snacks_layout:
                Fragment snacksFragment = new CategoriesListFragment();
                Bundle mBundle5 = new Bundle();
                mBundle5.putInt("openfrom", 0);
                mBundle5.putString("catId", "8");
                snacksFragment.setArguments(mBundle5);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction snacksTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                snacksTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                snacksTransaction.add(R.id.realtabcontent, snacksFragment, "category");
                snacksTransaction.hide(MenuFragment.this);
                snacksTransaction.addToBackStack(null);

                // Commit the transaction
                snacksTransaction.commit();
                break;

//            case R.id.ramadan_meal_layout:
//            case R.id.ramadan_banner:
//                Fragment pastryFragment1 = new CategoriesListFragment();
//                Bundle mBundle21 = new Bundle();
//                mBundle21.putInt("openfrom", 0);
//                mBundle21.putString("catId", "2");
//                pastryFragment1.setArguments(mBundle21);
//                // consider using Java coding conventions (upper first char class names!!!)
//                FragmentTransaction pastryTransaction1 = getFragmentManager().beginTransaction();
//
//                // Replace whatever is in the fragment_container view with this fragment,
//                // and add the transaction to the back stack
//
//                pastryTransaction1.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                pastryTransaction1.add(R.id.realtabcontent, pastryFragment1, "category");
//                pastryTransaction1.hide(MenuFragment.this);
//                pastryTransaction1.addToBackStack(null);
//
//                // Commit the transaction
//                pastryTransaction1.commit();
//                break;

            case R.id.beans_layout:
                Fragment beansFragment = new CategoriesListFragment();
                Bundle mBundle6 = new Bundle();
                mBundle6.putInt("openfrom", 0);
                mBundle6.putString("catId", "5");
                beansFragment.setArguments(mBundle6);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction beansTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                beansTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                beansTransaction.add(R.id.realtabcontent, beansFragment, "category");
                beansTransaction.hide(MenuFragment.this);
                beansTransaction.addToBackStack(null);

                // Commit the transaction
                beansTransaction.commit();
                break;
            case R.id.merchandise_layout:
                Fragment merchandiseFragment = new CategoriesListFragment();
                Bundle mBundle7 = new Bundle();
                mBundle7.putInt("openfrom", 0);
                mBundle7.putString("catId", "6");
                merchandiseFragment.setArguments(mBundle7);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction merchandiseTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                merchandiseTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction.add(R.id.realtabcontent, merchandiseFragment, "category");
                merchandiseTransaction.hide(MenuFragment.this);
                merchandiseTransaction.addToBackStack(null);

                // Commit the transaction
                merchandiseTransaction.commit();
                break;

            case R.id.checkout_order_layout:
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لاتوجد منتجات في السلة ، لتفعيل التأكيد من فضلك اضف منتج")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Fragment nutritionFragment = new OrderCheckoutFragment();
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                    nutritionTransaction.hide(MenuFragment.this);
                    nutritionTransaction.addToBackStack(null);

                    // Commit the transaction
                    nutritionTransaction.commit();
                }
                break;

            case R.id.cart_count:
                String text = cartCount.getText().toString();
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لاتوجد منتجات في السلة ، لتفعيل التأكيد من فضلك اضف منتج")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    Fragment checkoutFragment = new OrderCheckoutFragment();
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction checkoutTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    checkoutTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    checkoutTransaction.add(R.id.realtabcontent, checkoutFragment);
                    checkoutTransaction.hide(MenuFragment.this);
                    checkoutTransaction.addToBackStack(null);

                    // Commit the transaction
                    checkoutTransaction.commit();
                }
                break;
        }
    }



    public class GetRMTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());

        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){

                    iftarMealLayout.setVisibility(View.GONE);
                }else{
             try {

                        Object property = result.getProperty(1);
                        if (property instanceof SoapObject) {
                            try {
                                SoapObject category_list = (SoapObject) property;
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                if( storeObject.getPropertyCount() > 0) {
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(0);
                                    iftarMealLayout.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//                    }

                    } catch (Exception e) {
                        Log.d("", "Error while parsing the results!");
                        e.printStackTrace();
                    }


                }
            }else{
                iftarMealLayout.setVisibility(View.GONE);
            }


            super.onPostExecute(result1);
        }
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }


}
