package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

/**
 * Created by brahmam on 28/12/15.
 */
public class LanguageFragment extends Fragment {

    Button backBtn;
    TextView english, arabic;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.language_layout, container,
                false);
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        english = (TextView) rootView.findViewById(R.id.english);
        arabic = (TextView) rootView.findViewById(R.id.arabic);
        if(languagePrefs.getString("language","En").equalsIgnoreCase("En")){
            english.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
            arabic.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }else if(languagePrefs.getString("language","En").equalsIgnoreCase("Ar")){
            english.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            arabic.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagePrefsEditor.putString("language","En");
                languagePrefsEditor.commit();
                english.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                arabic.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                getActivity().recreate();

//                FragmentManager manager = getActivity().getSupportFragmentManager();
//                FragmentTransaction trans = manager.beginTransaction();
//                trans.remove(new LanguageFragment());
//                trans.commit();
//
//
//                Fragment engishFragment = new MoreFragment();
//                // consider using Java coding conventions (upper first char class names!!!)
//                FragmentTransaction englishTransaction = getFragmentManager().beginTransaction();
//
//                // Replace whatever is in the fragment_container view with this fragment,
//                // and add the transaction to the back stack
//                englishTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
//                englishTransaction.add(R.id.realtabcontent, engishFragment);
//                englishTransaction.hide(LanguageFragment.this);
//                englishTransaction.addToBackStack(null);
//
//                // Commit the transaction
//                englishTransaction.commit();
//
//                FragmentManager manager1 = getActivity().getSupportFragmentManager();
//                FragmentTransaction trans1 = manager1.beginTransaction();
//                trans1.remove(new MoreFragment());
//                trans1.commit();
//
//                manager1.popBackStack();
//                manager.popBackStack();


            }
        });

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagePrefsEditor.putString("language","Ar");
                languagePrefsEditor.commit();
                english.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                arabic.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);

                getActivity().recreate();

//                FragmentManager manager = getActivity().getSupportFragmentManager();
//                FragmentTransaction trans = manager.beginTransaction();
//                trans.remove(new LanguageFragment());
//                trans.commit();
//
//
//                manager.popBackStack();
//                Fragment arabicFragment = new MoreFragment();
//                // consider using Java coding conventions (upper first char class names!!!)
//                FragmentTransaction arabicTransaction = getFragmentManager().beginTransaction();
//
//                // Replace whatever is in the fragment_container view with this fragment,
//                // and add the transaction to the back stack
//                arabicTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
//                arabicTransaction.add(R.id.realtabcontent, arabicFragment);
//                arabicTransaction.hide(LanguageFragment.this);
//                arabicTransaction.addToBackStack(null);
//
//
//                // Commit the transaction
//                arabicTransaction.commit();
//                FragmentManager manager1 = getActivity().getSupportFragmentManager();
//                FragmentTransaction trans1 = manager1.beginTransaction();
//                trans1.remove(new MoreFragment());
//                trans1.commit();
//                manager1.popBackStack();
            }
        });
        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
