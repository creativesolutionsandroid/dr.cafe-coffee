package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.WalletHistoryAdapter;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.WalletHistory;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by brahmam on 19/2/16.
 */
public class WalletHistoryFragment extends Fragment {
    private String SOAP_ACTION_WALLET = "http://tempuri.org/GetWalletShortSummary";
    private String METHOD_NAME_WALLET = "GetWalletShortSummary";
    private SoapObject result_wallet;
    private String response12_wallet = null;
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;

    private ArrayList<WalletHistory> transactionsList = new ArrayList<>();
    private SwipeMenuListView orderHistoryListView;
    private WalletHistoryAdapter mAdapter;
//    private OrderHistoryAdapterArabic mAdapterArabic;
    Button backBtn;
    SharedPreferences userPrefs;
    String userId;
    TextView title;
    ProgressDialog dialog;
    String language;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        View rootView = inflater.inflate(R.layout.favourite_order, container,
                false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        orderHistoryListView = (SwipeMenuListView) rootView.findViewById(R.id.fav_order_listview);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        title = (TextView) rootView.findViewById(R.id.title);

        if(language.equalsIgnoreCase("En")){
            title.setText("Wallet History");
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("جميع الطلبات");
        }

        new GetWalletDashboard().execute();
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public class GetWalletDashboard extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {
                if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                }else {
                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_WALLET);
                    request.addProperty("userId", userId);
                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_WALLET, envelope);
                    result_wallet = (SoapObject) envelope.getResponse();
                    response12_wallet = result_wallet.toString();
                    Log.v("TAG","wallet response " + response12_wallet);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count wallet: " + result_wallet);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("TAG", "wallet response " + response12_wallet);

            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(response12_wallet == null){
                dialog.dismiss();

            }else if(response12_wallet.equalsIgnoreCase("no internet")){
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Communication Error! Please check the internet connection?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            } else {
                dialog.dismiss();
                try {

                    Object property = result_wallet.getProperty(1);
                    if (property instanceof SoapObject) {
                        try {
                            SoapObject category_list = (SoapObject) property;
                            if(category_list.getPropertyCount()>0) {
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    WalletHistory myGifts = new WalletHistory();
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                    String orderDate = sObj.getPropertyAsString("CreatedOn");
                                    String totalPrice = sObj.getPropertyAsString("Total_Price");
                                    String action_note = sObj.getPropertyAsString("TransactionMessage");
                                    String action = sObj.getPropertyAsString("Action");
                                    String invoiceNo = sObj.getPropertyAsString("InvoiceNo");

                                    myGifts.setOrderDate(orderDate);
                                    myGifts.setTotalPrice(totalPrice);
                                    myGifts.setAction_note(action_note);
                                    myGifts.setAction(action);
                                    myGifts.setInvoiceNo(invoiceNo);

                                    transactionsList.add(myGifts);
                                }

                                mAdapter = new WalletHistoryAdapter(getActivity(), transactionsList);
                                orderHistoryListView.setAdapter(mAdapter);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }
            }
            super.onPostExecute(result1);
        }
    }

}
