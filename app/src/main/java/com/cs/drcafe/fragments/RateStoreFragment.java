package com.cs.drcafe.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

/**
 * Created by BRAHMAM on 06-12-2015.
 */
public class RateStoreFragment extends Fragment {

    private TextView rateExcellent, rateGood, rateAverage, rateCall;
    private Button backBtn;
    String language;
    View rootView;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.rate_store, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.rate_store_arabic, container,
                    false);
        }
        rateCall = (TextView) rootView.findViewById(R.id.rate_store_call);
        rateExcellent = (TextView) rootView.findViewById(R.id.rate_excellent);
        rateGood = (TextView) rootView.findViewById(R.id.rate_good);
        rateAverage = (TextView) rootView.findViewById(R.id.rate_average);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);

        rateCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8001228222"));
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8001228222"));
                    startActivity(intent);
                }

            }
        });

        rateExcellent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(language.equalsIgnoreCase("En")) {
                    rateExcellent.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_mark, 0);
                    rateGood.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    rateAverage.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    rateExcellent.setCompoundDrawablesWithIntrinsicBounds(R.drawable.right_mark, 0, 0, 0);
                    rateGood.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    rateAverage.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }

            }
        });

        rateGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(language.equalsIgnoreCase("En")) {
                    rateExcellent.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    rateGood.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_mark, 0);
                    rateAverage.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    rateExcellent.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    rateGood.setCompoundDrawablesWithIntrinsicBounds(R.drawable.right_mark, 0, 0, 0);
                    rateAverage.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }

            }
        });

        rateAverage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(language.equalsIgnoreCase("En")) {
                    rateExcellent.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    rateGood.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    rateAverage.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_mark, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    rateExcellent.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    rateGood.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    rateAverage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.right_mark, 0, 0, 0);
                }

            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        return rootView;
    }


    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8001228222"));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getActivity(), "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
