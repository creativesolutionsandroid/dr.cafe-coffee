package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.GPSTracker;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.FavouriteOrderAdapter;
import com.cs.drcafe.adapter.FavouriteOrderAdapterArabic;
import com.cs.drcafe.model.Additionals;
import com.cs.drcafe.model.FavouriteOrder;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.Item;
import com.cs.drcafe.model.SectionItem;
import com.cs.drcafe.model.StoreInfo;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by SKT on 19-02-2016.
 */
public class FavouriteOrderFragment extends Fragment {

    private String SOAP_ACTION = "http://tempuri.org/getFavoriteOrderName";
    private String METHOD_NAME = "getFavoriteOrderName";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;


    private String SOAP_ACTION1 = "http://tempuri.org/getFavoriteOrder";
    private String METHOD_NAME1 = "getFavoriteOrder";
    private final String NAMESPACE1 = "http://tempuri.org/";
    private static final String URL1 = DrConstants.URL;
    private SoapObject result12;
    private String response123 = null;

    private String SOAP_ACTION_TIME = "http://tempuri.org/getCurrentTime";
    private String METHOD_NAME_TIME = "getCurrentTime";
    private SoapPrimitive timeResult;
    private String timeResponse = null;

    private String SOAP_ACTION_DELETE = "http://tempuri.org/deleteFavoriteOrder";
    private String METHOD_NAME_DELETE = "deleteFavoriteOrder";
    private SoapObject deleteResult;
    private String deleteResponse = null;

    private static final String STORE_URL = "http://www.drcafeonline.net/dcservices.asmx";

    private ArrayList<FavouriteOrder> transactionsList = new ArrayList<>();
    private SwipeMenuListView favouriteOrderListView;
    private FavouriteOrderAdapter mAdapter;
    private FavouriteOrderAdapterArabic mAdapterArabic;
    private DataBaseHelper myDbHelper;
    Button backBtn, back1;
    SharedPreferences userPrefs;
    String userId;
    ProgressDialog dialog;
    String language;
    SharedPreferences storePrefs;
    SharedPreferences.Editor storePrefsEditor;
    private boolean isAddedtoList, isFilterOn;

    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    private ArrayList<StoreInfo> favouriteStoresList = new ArrayList<>();
    private ArrayList<Item> totalStoresList = new ArrayList<>();
    private ArrayList<StoreInfo> tempStoresList = new ArrayList<>();
    private ArrayList<StoreInfo> filterStoresList = new ArrayList<>();
    ArrayList<String> favoriteStores = new ArrayList<>();

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;
    GPSTracker gps;
    private double lat, longi;
    String mDistancePrefValue, mRadiusPrefValue, mDisplayPrefValue;

    AlertDialog alertDialog;
    String storeId;
    SharedPreferences favouritePrefs, locationPrefs;
    String itemId, size, qty, comments, additionalids;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        View rootView = inflater.inflate(R.layout.favourite_order, container,
                false);

        myDbHelper = new DataBaseHelper(getActivity());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        Log.i("userid", "" + userId);
        TextView title = (TextView) rootView.findViewById(R.id.title);

        favouriteOrderListView = (SwipeMenuListView) rootView.findViewById(R.id.fav_order_listview);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        back1 = (Button) rootView.findViewById(R.id.back_btn1);

        if(language.equalsIgnoreCase("En")){
            title.setText("Favourite Order");
            mAdapter = new FavouriteOrderAdapter(getActivity(), transactionsList);
            favouriteOrderListView.setAdapter(mAdapter);
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("المفضلة");
            mAdapterArabic = new FavouriteOrderAdapterArabic(getActivity(), transactionsList);
            favouriteOrderListView.setAdapter(mAdapterArabic);
        }

        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);

        mDistancePrefValue = locationPrefs.getString("distance", "kilometer");
        mRadiusPrefValue = locationPrefs.getString("radius","50");
        mDisplayPrefValue = locationPrefs.getString("display","map");
        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        if(DrConstants.favFlag){
            backBtn.setVisibility(View.GONE);
            back1.setVisibility(View.GONE);
            DrConstants.favFlag = false;
        }

        new GetFavouriteOrders().execute();

        favouriteOrderListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                myDbHelper.deleteOrderTable();
                ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
                String orderId = transactionsList.get(position).getOrderId();
                new GetFavouriteOrderDetails().execute(orderId);

            }
        });


        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity().getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        favouriteOrderListView.setMenuCreator(creator);

        // step 2. listener item click event
        favouriteOrderListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                Log.i("TAG", "delete");
                switch (index) {
                    case 0:
                        Log.i("TAG", "case");
                        new DeleteFavouriteOrders().execute(transactionsList.get(position).getOrderId());
                        break;
                }
                return false;
            }
        });

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }

        return rootView;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    public class GetFavouriteOrders extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            transactionsList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("UserId", userId);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                response12 = "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (response12 != null){
                if (response12.equals("no internet")) {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                } else if (response12.equals("anyType{}")) {
                    dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                } else {

                    try {

                        Object property = result.getProperty(1);
                        if (property instanceof SoapObject) {
                            try {
                                SoapObject category_list = (SoapObject) property;
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    FavouriteOrder fo = new FavouriteOrder();
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");

                                    String orderId = sObj.getPropertyAsString("orderId");
                                    String favoriteName = sObj.getPropertyAsString("FavoriteName");
                                    String storeId = sObj.getPropertyAsString("storeId");
                                    String orderDate = sObj.getPropertyAsString("orderDate");
                                    String storeName = sObj.getPropertyAsString("StoreName");
                                    String storeName_ar = sObj.getPropertyAsString("StoreName_ar");
                                    String total_Price = sObj.getPropertyAsString("Total_Price");
                                    String orderDate1 = sObj.getPropertyAsString("OrderDate1");


                                    fo.setOrderId(orderId);
                                    fo.setFavoriteName(favoriteName);
                                    fo.setStoreId(storeId);
                                    fo.setOrderDate(orderDate);
                                    fo.setStoreName(storeName);
                                    fo.setStoreName_ar(storeName_ar);
                                    fo.setTotalPrice(total_Price);
                                    fo.setOrderDate1(orderDate1);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    transactionsList.add(fo);

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//                    }

                    } catch (Exception e) {
                        Log.d("", "Error while parsing the results!");
                        e.printStackTrace();
                    }


                    dialog.dismiss();
                    if (language.equalsIgnoreCase("En")) {
                        mAdapter.notifyDataSetChanged();
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mAdapterArabic.notifyDataSetChanged();
                    }

                    super.onPostExecute(result1);
                }
            }
        }
    }


    public class DeleteFavouriteOrders extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_DELETE);
                    request.addProperty("OrderId", arg0[0]);
                    Log.i("TAG", ""+arg0[0]);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_DELETE, envelope);
//                    String deleteResult = envelope.bodyIn.toString();
//                    deleteResponse = deleteResult.toString();
                    Log.v("TAG", ""+envelope.bodyIn);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + deleteResult);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + deleteResponse);
            }else{
                deleteResponse = "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            dialog.dismiss();
            new GetFavouriteOrders().execute();
//            if(deleteResponse != null){
//
//            }else if(deleteResponse.equals("no internet")){
//                dialog.dismiss();
//                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
//            }
//            else if (deleteResponse.equals("anyType{}")) {
//                dialog.dismiss();
////                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
//            } else {
//
//
//            }
        }
    }



    public class GetFavouriteOrderDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE1, METHOD_NAME1);
                    request.addProperty("UserId", userId);
                    request.addProperty("OrderID", arg0[0]);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL1);
                    androidHttpTransport.call(SOAP_ACTION1, envelope);
                    result12 = (SoapObject) envelope.getResponse();
                    response123 = result12.toString();
                    Log.v("TAG", response123);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response123);
            }else{
                response123 = "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(response123.equals("no internet")){
                dialog.dismiss();
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            }else if (response123.equals("anyType{}")) {
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                dialog.dismiss();

            } else {

                try {

                    Object property = result12.getProperty(1);
                    if (property instanceof SoapObject) {
                        try {
                            SoapObject category_list = (SoapObject) property;
                            SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                            for(int i=0; i<storeObject.getPropertyCount();i++) {
                                SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");

                                String orderId = sObj.getPropertyAsString("orderId");
                                String favoriteName = sObj.getPropertyAsString("FavoriteName");
                                String userId = sObj.getPropertyAsString("userId");
                                storeId = sObj.getPropertyAsString("storeId");
                                itemId = sObj.getPropertyAsString("itemId");
                                qty = sObj.getPropertyAsString("qty");
                                size = sObj.getPropertyAsString("Size");
                                comments = sObj.getPropertyAsString("Comments");
                                String orderItem = sObj.getPropertyAsString("orderItem");
                                additionalids = sObj.getPropertyAsString("Additionalids");

//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    }

                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }
                dialog.dismiss();
                new GetCurrentTime().execute();

//                Fragment nutritionFragment = new OrderCheckoutFragment();
//                // consider using Java coding conventions (upper first char class names!!!)
//                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();
//
//                // Replace whatever is in the fragment_container view with this fragment,
//                // and add the transaction to the back stack
//                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
//                nutritionTransaction.hide(FavouriteOrderFragment.this);
//                nutritionTransaction.addToBackStack(null);
//
//                // Commit the transaction
//                nutritionTransaction.commit();
//

                super.onPostExecute(result1);
            }
        }
    }

    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy");

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_TIME);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_TIME, envelope);
                    timeResult = (SoapPrimitive) envelope.getResponse();
                    timeResponse = timeResult.toString();
                    Log.v("TAG", timeResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + timeResponse);
            }else{
                timeResponse = "no internet";
            }
            return timeResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(timeResponse == null){
                if(dialog!=null) {
                    dialog.dismiss();
                }
            } else if(timeResponse.equals("no internet")){
                if(dialog!=null) {
                    dialog.dismiss();
                }
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                if(dialog!=null) {
                    dialog.dismiss();
                }
                new GetStoreInfo().execute();
            }
            super.onPostExecute(result1);
        }
    }

    public class GetStoreInfo extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus = "";
        String dayOfWeek;

        @Override
        protected void onPreExecute() {
            if(getActivity() != null) {
                networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            }
            try {
                dialog = ProgressDialog.show(getActivity(), "",
                        "Please wait...");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    SharedPreferences storePrefs = getContext().getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);

                    Log.d("TAG", "doInBackground: "+storeId);
                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("day", dayOfWeek.toLowerCase());
                    request.addProperty("StoreId", storeId);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(STORE_URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                Log.d("TAG", "onPostExecute: "+response12);
                if(result1.equalsIgnoreCase("no internet")){
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    storesList.clear();
                    filterStoresList.clear();
                    totalStoresList.clear();
                    favouriteStoresList.clear();
                    if(response12 == null){

                    }else if (response12.equals("anyType{}")) {
                        Toast.makeText(getActivity(), "Failed to get stores info", Toast.LENGTH_LONG).show();
                    } else {

                        try {
                            Object property = result.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject category_list = (SoapObject) property;
                                    SoapObject storeObject = (SoapObject) category_list.getProperty(0);
//                                    lat = 24.70321657;
//                                    longi = 46.68097073;
                                    for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                        isAddedtoList = false;
                                        SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                        StoreInfo si = new StoreInfo();

//                                    for (int j = 0; j < sObj.getPropertyCount(); j++) {
                                        si.setStoreId(sObj.getPropertyAsString(0));
                                        si.setStartTime(sObj.getPropertyAsString(1));
                                        si.setEndTime(sObj.getPropertyAsString(2));
                                        si.setStoreName(sObj.getPropertyAsString("StoreName"));
                                        si.setStoreAddress(sObj.getPropertyAsString("StoreAddress"));
                                        si.setLatitude(Double.valueOf(sObj.getPropertyAsString("Latitude")));
                                        si.setLongitude(Double.valueOf(sObj.getPropertyAsString("Longitude")));
                                        si.setCountryName(sObj.getPropertyAsString("CountryName"));
                                        si.setCityName(sObj.getPropertyAsString("CityName"));
                                        try {
                                            si.setImageURL(sObj.getPropertyAsString("imageURL"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        si.setFamilySection(Boolean.valueOf(sObj.getPropertyAsString("FamilySection")));
                                        si.setWifi(Boolean.valueOf(sObj.getPropertyAsString("Wifi")));
                                        si.setPatioSitting(Boolean.valueOf(sObj.getPropertyAsString("PatioSitting")));
                                        si.setV12(Boolean.valueOf(sObj.getPropertyAsString("V12")));
                                        si.setDriveThru(Boolean.valueOf(sObj.getPropertyAsString("DriveThru")));
                                        si.setMeetingSpace(Boolean.valueOf(sObj.getPropertyAsString("MeetingSpace")));
                                        si.setHospital(Boolean.valueOf(sObj.getPropertyAsString("Hospital")));
                                        si.setUniversity(Boolean.valueOf(sObj.getPropertyAsString("University")));
                                        si.setOffice(Boolean.valueOf(sObj.getPropertyAsString("Office")));
                                        si.setShoppingMall(Boolean.valueOf(sObj.getPropertyAsString("ShoppingMall")));
                                        try{
                                            si.setAirPort(Boolean.valueOf(sObj.getPropertyAsString("Airport")));
                                        }catch (Exception e){
                                            si.setAirPort(false);
                                        }
                                        try{
                                            si.setDineIn(Boolean.valueOf(sObj.getPropertyAsString("DineIn")));
                                        }catch (Exception e){
                                            si.setDineIn(false);
                                        }
                                        try{
                                            si.setLadies(Boolean.valueOf(sObj.getPropertyAsString("Ladies")));
                                        }catch (Exception e){
                                            si.setLadies(false);
                                        }
                                        si.setCashPayment(Boolean.valueOf(sObj.getPropertyAsString("CashPayment")));
                                        si.setOnlinePayment(Boolean.valueOf(sObj.getPropertyAsString("OnlinePayment")));
                                        si.setDeliverable(Boolean.valueOf(sObj.getPropertyAsString("Deliverable")));
                                        si.setDistanceForDelivery(Float.valueOf(sObj.getPropertyAsString("Distance")));
                                        si.setMinOrderAmount(Float.valueOf(sObj.getPropertyAsString("MinOrderAmount")));
                                        si.setDeliveryCharge(Float.valueOf(sObj.getPropertyAsString("DeliveryCharge")));
                                        si.setFreeDeliveryAbove(Float.valueOf(sObj.getPropertyAsString("FreeDeliveryAbove")));
                                        si.setDeliveryMessageEn(sObj.getPropertyAsString("DeliveryMessageEn"));
                                        si.setDeliveryMessageAr(sObj.getPropertyAsString("DeliveryMessageAr"));
                                        si.setNeighborhood(Boolean.valueOf(sObj.getPropertyAsString("Neighborhood")));
                                        si.setIs24x7(Boolean.valueOf(sObj.getPropertyAsString("is24x7")));
                                        si.setStatus(Boolean.valueOf(sObj.getPropertyAsString("status")));
                                        si.setDcCountry(sObj.getPropertyAsString("DCCountry"));
                                        si.setDcCity(sObj.getPropertyAsString("DCCity"));
                                        si.setStoreName_ar(sObj.getPropertyAsString("StoreName_ar"));
                                        si.setStoreAddress_ar(sObj.getPropertyAsString("StoreAddress_ar"));
                                        si.setIemsJson(sObj.getPropertyAsString("ItemsJson"));
                                        try {
                                            si.setDeliveryItems(sObj.getPropertyAsString("DeliveryItems"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try{
                                            si.setMessage(sObj.getPropertyAsString("Message"));
                                        }catch (Exception e){
                                            si.setMessage("");
                                        }

                                        try{
                                            si.setMessage_ar(sObj.getPropertyAsString("Message_ar"));
                                        }catch (Exception e){
                                            si.setMessage_ar("");
                                        }

                                        Location me   = new Location("");
                                        Location dest = new Location("");

                                        me.setLatitude(lat);
                                        me.setLongitude(longi);

                                        dest.setLatitude(Double.valueOf(sObj.getPropertyAsString("Latitude")));
                                        dest.setLongitude(Double.valueOf(sObj.getPropertyAsString("Longitude")));

                                        float dist = (me.distanceTo(dest))/1000;
                                        float mile = (float) (dist * 0.621);
//                                        Log.i("storename", "" + sObj.getPropertyAsString(1) + "   " + sObj.getPropertyAsString(2));
                                        if(mDistancePrefValue.equalsIgnoreCase("kilometer")){
                                            si.setDistance(dist);
                                        }else if(mDistancePrefValue.equalsIgnoreCase("mile")){
                                            si.setDistance(mile);
                                        }
                                        if(dist<Integer.parseInt(mRadiusPrefValue)) {
//                                            storesList.add(si);
                                            if(favoriteStores.size()>0){
                                                if(favoriteStores.contains(sObj.getPropertyAsString(0))){
                                                    favouriteStoresList.add(si);
                                                }
                                            }
                                            else {
                                            }
                                            String startTime = sObj.getPropertyAsString(1);
                                            String endTime = sObj.getPropertyAsString(2);
                                            if(Boolean.valueOf(sObj.getPropertyAsString("is24x7"))){
                                                si.setOpenFlag(1);
                                                storesList.add(si);
                                            }else if(sObj.getPropertyAsString(1).equals("anyType{}") && sObj.getPropertyAsString(2).equals("anyType{}")){
                                                si.setOpenFlag(-1);
                                                storesList.add(si);
                                            }else{
                                                if(endTime.equals("00:00AM")){
                                                    si.setOpenFlag(1);
                                                    storesList.add(si);

                                                    continue;
                                                }else if(endTime.equals("12:00AM")){
                                                    endTime = "11:59PM";
                                                }

                                                Calendar now = Calendar.getInstance();

                                                int hour = now.get(Calendar.HOUR_OF_DAY);
                                                int minute = now.get(Calendar.MINUTE);

                                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
                                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                                Date serverDate = null;
                                                Date end24Date = null;
                                                Date start24Date = null;
                                                Date current24Date = null;
                                                Date dateToday = null;
                                                Calendar dateStoreClose = Calendar.getInstance();

                                                try {
                                                    end24Date = timeFormat.parse(endTime);
                                                    start24Date = timeFormat.parse(startTime);
                                                    serverDate = dateFormat.parse(timeResponse);
                                                    dateToday = dateFormat1.parse(timeResponse);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                dateStoreClose.setTime(dateToday);
                                                dateStoreClose.add(Calendar.DATE, 1);
                                                String current24 = timeFormat1.format(serverDate);
                                                String end24 =timeFormat1.format(end24Date);
                                                String start24 = timeFormat1.format(start24Date);
                                                String startDateString = dateFormat1.format(dateToday);
                                                String endDateString = dateFormat1.format(dateToday);
                                                String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                                                dateStoreClose.add(Calendar.DATE, -2);
                                                String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

                                                Date startDate = null;
                                                Date endDate = null;

                                                try {
                                                    end24Date = timeFormat1.parse(end24);
                                                    start24Date = timeFormat1.parse(start24);
                                                    current24Date = timeFormat1.parse(current24);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                                String[] parts2 = start24.split(":");
                                                int startHour = Integer.parseInt(parts2[0]);
                                                int startMinute = Integer.parseInt(parts2[1]);

                                                String[] parts = end24.split(":");
                                                int endHour = Integer.parseInt(parts[0]);
                                                int endMinute = Integer.parseInt(parts[1]);

                                                String[] parts1 = current24.split(":");
                                                int currentHour = Integer.parseInt(parts1[0]);
                                                int currentMinute = Integer.parseInt(parts1[1]);


                                                if(startTime.contains("AM") && endTime.contains("AM")){
                                                    if(startHour < endHour){
                                                        startDateString = startDateString+ " "+ startTime;
                                                        endDateString = endDateString+"  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }else if(startHour > endHour){
                                                        if(timeResponse.contains("AM")){
                                                            if(currentHour > endHour){
                                                                startDateString = startDateString + " " + startTime;
                                                                endDateString = endDateTomorrow + "  " + endTime;
                                                                try {
                                                                    startDate = dateFormat2.parse(startDateString);
                                                                    endDate = dateFormat2.parse(endDateString);
                                                                } catch (ParseException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            } else {
                                                                startDateString = endDateYesterday + " " + startTime;
                                                                endDateString = endDateString + "  " + endTime;
                                                                try {
                                                                    startDate = dateFormat2.parse(startDateString);
                                                                    endDate = dateFormat2.parse(endDateString);
                                                                } catch (ParseException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }else {
                                                            startDateString = startDateString + " " + startTime;
                                                            endDateString = endDateTomorrow + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                }else if(startTime.contains("AM") && endTime.contains("PM")){
                                                    startDateString = startDateString+ " "+ startTime;
                                                    endDateString = endDateString+"  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }else if(startTime.contains("PM") && endTime.contains("AM")){
                                                    if(timeResponse.contains("AM")){
                                                        if(currentHour <= endHour){
                                                            startDateString = endDateYesterday+ " "+ startTime;
                                                            endDateString = endDateString+"  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }else{
                                                            startDateString = startDateString+ " "+ startTime;
                                                            endDateString = endDateTomorrow+"  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }else{
                                                        startDateString = startDateString+ " "+ startTime;
                                                        endDateString = endDateTomorrow+"  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                }else if(startTime.contains("PM") && endTime.contains("PM")){
                                                    startDateString = startDateString+ " "+ startTime;
                                                    endDateString = endDateString+"  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                String serverDateString = dateFormat2.format(serverDate);

                                                try {
                                                    serverDate = dateFormat2.parse(serverDateString);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                                if(serverDate.after(startDate) && serverDate.before(endDate)){
//                                                Log.i("TAG Visible" , "true");
                                                    si.setOpenFlag(1);
                                                    storesList.add(si);
                                                    continue;
                                                }else{
                                                    si.setOpenFlag(0);
                                                    storesList.add(si);
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }

                        filterStoresList.addAll(storesList);

                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(favouriteStoresList, StoreInfo.storeDistance);
                        Collections.sort(storesList, StoreInfo.storeOpenSort);
                        Collections.sort(favouriteStoresList, StoreInfo.storeOpenSort);
                        if(favouriteStoresList.size() >0){
                            if(language.equalsIgnoreCase("En")) {
                                totalStoresList.add(new SectionItem("Favorite Stores"));
                            }else if(language.equalsIgnoreCase("Ar")){
                                totalStoresList.add(new SectionItem("  الفروع المفضلة"));
                            }
                            totalStoresList.addAll(favouriteStoresList);
                        }
                        if(language.equalsIgnoreCase("En")) {
                            totalStoresList.add((new SectionItem("Nearby Stores")));
                        }else if(language.equalsIgnoreCase("Ar")){
                            totalStoresList.add((new SectionItem(" الفروع القريبة")));
                        }
                        totalStoresList.addAll(storesList);
                        if(tempStoresList.size()==0){
                            tempStoresList.addAll(storesList);
                        }
                    }
                    if (storesList.size() > 0) {
                        checkStoreStatus();
                    }
                    else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                getActivity());

                        if(language.equalsIgnoreCase("En")) {
                            // set title
                            alertDialogBuilder.setTitle("dr.CAFE");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Sorry Store is closed! You can't make the order from this store.")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        }else if(language.equalsIgnoreCase("Ar")){
                            // set title
                            alertDialogBuilder.setTitle("د. كيف");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
                                    .setCancelable(false)
                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        }

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                    if(dialog!=null) {
                        dialog.dismiss();
                    }
                    super.onPostExecute(result1);
                }
            }
        }
    }

    public void getGPSCoordinates(){
        if(getActivity()!=null) {
            gps = new GPSTracker(getActivity());
        }
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);

//                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }


    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void checkStoreStatus() {
        Log.d("TAG", "checkStoreStatus: " + storesList.size());
        final StoreInfo si = storesList.get(0);

        storePrefs = getContext().getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);
        storePrefsEditor = storePrefs.edit();
        storePrefsEditor.clear();
        storePrefsEditor.apply();

        Gson gson1 = new Gson();
        String jsonText1 = gson1.toJson(si);

        storePrefsEditor.putString("store", jsonText1);
        storePrefsEditor.putString("store", jsonText1);
        storePrefsEditor.apply();

//        if (si.isPickUp()) {
        if(si.isDineIn() || si.isPickUp() || si.isDeliverable()) {
            if (!si.is24x7()) {
                if (si.getOpenFlag() != 1) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            getActivity());

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Sorry Store is closed! You can't make the order from this store.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {

                    HashMap<String, String> values = new HashMap<>();
                    String ids = "0", additionalsStr = "", additionalsPrice = "";

                    String[] subCatId = myDbHelper.getSubCatId(itemId).split(",");
                    String price = myDbHelper.getPrice(itemId, size);
                    float subAmt = Float.parseFloat(price);
                    if (additionalids.length() == 1 && !additionalids.equals("0")) {
                        String[] parts = additionalids.split(",");
                        for (int j = 0; j < parts.length; j++) {
                            ArrayList<Additionals> additionalDetails = myDbHelper.getAdditionalDetails(parts[j]);

                            if (additionalDetails.size() > 0) {


                                for (Additionals sa : additionalDetails) {
                                    if (ids.equals("0")) {
                                        ids = sa.getAdditionalsId();
                                        additionalsStr = sa.getAdditionalName();
                                        additionalsPrice = sa.getAdditionalPrice();
                                    } else {
                                        ids = ids + "," + sa.getAdditionalsId();
                                        additionalsStr = additionalsStr + "," + sa.getAdditionalName();
                                        additionalsPrice = additionalsPrice + "," + sa.getAdditionalPrice();
                                    }
                                    subAmt = subAmt + Integer.parseInt(sa.getAdditionalPrice());

                                }
                            }
                        }
                    }

                    float totalAmt = subAmt * Integer.parseInt(qty);

                    values.put("itemId", itemId);
                    values.put("itemTypeId", size);
                    values.put("subCategoryId", subCatId[0]);
                    values.put("additionals", additionalsStr);
                    values.put("qty", qty);
                    values.put("price", Float.toString(subAmt));
                    values.put("additionalsPrice", additionalsPrice);
                    values.put("additionalsTypeId", ids);
                    values.put("totalAmount", Float.toString(totalAmt));
                    values.put("comment", comments);
                    values.put("status", "1");
                    values.put("creationDate", "14/07/2015");
                    values.put("modifiedDate", "14/07/2015");
                    values.put("categoryId", subCatId[1]);
                    myDbHelper.insertOrder(values);

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.store_select_dialog, null);
                    dialogBuilder.setView(dialogView);

                    ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                    ImageView pickup = (ImageView) dialogView.findViewById(R.id.select_pickup);
                    ImageView dineIn = (ImageView) dialogView.findViewById(R.id.select_dinein);
                    ImageView pickupBig = (ImageView) dialogView.findViewById(R.id.select_pickup_big);
                    RelativeLayout pickupLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_layout);
                    RelativeLayout dineinLayout = (RelativeLayout) dialogView.findViewById(R.id.dinein_layout);
                    RelativeLayout pickupBigLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_big_layout);
                    RelativeLayout deliveryLayout = (RelativeLayout) dialogView.findViewById(R.id.delivery_layout);
                    TextView deliveryTv = (TextView) dialogView.findViewById(R.id.delivery_txt);
                    TextView storeMsg = (TextView) dialogView.findViewById(R.id.store_msg);
                    TextView pickupTv = (TextView) dialogView.findViewById(R.id.pickup_txt);
                    TextView dineInTv = (TextView) dialogView.findViewById(R.id.dinein_txt);
                    TextView pickupBigTv = (TextView) dialogView.findViewById(R.id.pickup_big_txt);
                    TextView heading = (TextView) dialogView.findViewById(R.id.order_type_heading);
                    pickupBigLayout.setVisibility(View.GONE);
                    if (language.equalsIgnoreCase("En")) {
                        pickupTv.setText("Pick Up");
                        dineInTv.setText("Dine In");
                        pickupBigTv.setText("Pick Up");
                        heading.setText("Order Type");
                        if (si.getMessage().equalsIgnoreCase("")) {
                            storeMsg.setVisibility(View.GONE);
                        } else {
                            storeMsg.setVisibility(View.VISIBLE);
                            storeMsg.setText(si.getMessage());
                        }
                    } else if (language.equalsIgnoreCase("Ar")) {
                        pickupTv.setText("الاستمتاع خارج الفرع");
                        dineInTv.setText("الاستمتاع في الفرع");
                        pickupBigTv.setText("الاستمتاع خارج الفرع");
                        heading.setText("نوع الطلب");
                        if (si.getMessage_ar().equalsIgnoreCase("")) {
                            storeMsg.setVisibility(View.GONE);
                        } else {
                            storeMsg.setVisibility(View.VISIBLE);
                            storeMsg.setText(si.getMessage_ar());
                        }
                    }


                    if (si.isDineIn()) {

                    } else {
                        dineinLayout.setVisibility(View.GONE);
                        pickupLayout.setVisibility(View.GONE);
                        pickupBigLayout.setVisibility(View.VISIBLE);
                    }

//                if (si.isDeliverable()) {
//                    deliveryLayout.setVisibility(View.VISIBLE);
//                }
//                else {
//                    deliveryLayout.setVisibility(View.GONE);
//                }

                    DrConstants.storeName = si.getStoreName();
                    DrConstants.storeAddress = si.getStoreAddress();
                    DrConstants.storeId = si.getStoreId();
                    DrConstants.latitude = si.getLatitude();
                    DrConstants.longitude = si.getLongitude();
                    DrConstants.fullHours = si.is24x7();
                    DrConstants.startTime = si.getStartTime();
                    DrConstants.endTime = si.getEndTime();
                    DrConstants.CashPayment = si.isCashPayment();
                    DrConstants.OnlinePayment = si.isOnlinePayment();

                    Gson gson = new Gson();
                    String jsonText = gson.toJson(si);

                    storePrefsEditor.putString("store", jsonText);
                    storePrefsEditor.apply();

                    pickup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            DrConstants.orderNow = true;
                            DrConstants.orderType = "Pick Up";

                            storePrefsEditor.putString("orderType", DrConstants.orderType);
                            storePrefsEditor.apply();

                            alertDialog.dismiss();
                            DrConstants.menuFlag = true;

                            Fragment newFragment = new MenuFragment();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();

                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack
                            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                            transaction.add(R.id.realtabcontent, newFragment, "store_detail");
                            transaction.hide(FavouriteOrderFragment.this);
                            transaction.addToBackStack(null);

                            // Commit the transaction
                            transaction.commit();
                        }
                    });

                    pickupBig.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            DrConstants.orderNow = true;
                            DrConstants.orderType = "Pick Up";
                            storePrefsEditor.putString("orderType", DrConstants.orderType);
                            storePrefsEditor.commit();

                            alertDialog.dismiss();
                            DrConstants.menuFlag = true;

                            Fragment newFragment = new MenuFragment();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();

                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack
                            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                            transaction.add(R.id.realtabcontent, newFragment, "store_detail");
                            transaction.hide(FavouriteOrderFragment.this);
                            transaction.addToBackStack(null);

                            // Commit the transaction
                            transaction.commit();
                        }
                    });

                    deliveryLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            DrConstants.orderNow = true;
                            DrConstants.orderType = "Delivery";
                            DrConstants.MinOrderAmount = si.getMinOrderAmount();
                            DrConstants.DeliveryCharge = si.getDeliveryCharge();
                            DrConstants.FreeDeliveryAbove = si.getFreeDeliveryAbove();
                            storePrefsEditor.putFloat("MinOrderAmount", DrConstants.MinOrderAmount);
                            storePrefsEditor.putFloat("DeliveryCharge", DrConstants.DeliveryCharge);
                            storePrefsEditor.putFloat("FreeDeliveryAbove", DrConstants.FreeDeliveryAbove);
                            storePrefsEditor.putString("orderType", DrConstants.orderType);
                            storePrefsEditor.commit();

                            alertDialog.dismiss();
                            DrConstants.menuFlag = true;

                            Bundle args = new Bundle();
                            args.putBoolean("store_details", false);
                            args.putDouble("lat", si.getLatitude());
                            args.putDouble("lng", si.getLongitude());
                            args.putFloat("delDistance", si.getDistanceForDelivery());
                            Fragment newFragment = new MyAddressActivity();
                            newFragment.setArguments(args);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();

                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack
                            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                            transaction.add(R.id.realtabcontent, newFragment, "address");
                            transaction.hide(FavouriteOrderFragment.this);
                            transaction.addToBackStack(null);

                            // Commit the transaction
                            transaction.commit();
                        }
                    });

                    dineIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            DrConstants.orderNow = true;
                            DrConstants.orderType = "Dine In";

                            storePrefsEditor.putString("orderType", DrConstants.orderType);
                            storePrefsEditor.apply();

                            DrConstants.menuFlag = true;

                            Fragment newFragment = new MenuFragment();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();

                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack
                            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                            transaction.add(R.id.realtabcontent, newFragment, "store_detail");
                            transaction.hide(FavouriteOrderFragment.this);
                            transaction.addToBackStack(null);

                            // Commit the transaction
                            transaction.commit();
                        }
                    });


                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    alertDialog = dialogBuilder.create();
                    alertDialog.show();

                    //Grab the window of the dialog, and change the width
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = alertDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
            } else {

                HashMap<String, String> values = new HashMap<>();
                String ids = "0", additionalsStr = "", additionalsPrice = "";

                String[] subCatId = myDbHelper.getSubCatId(itemId).split(",");
                String price = myDbHelper.getPrice(itemId, size);
                float subAmt = Float.parseFloat(price);
                if (additionalids.length() == 1 && !additionalids.equals("0")) {
                    String[] parts = additionalids.split(",");
                    for (int j = 0; j < parts.length; j++) {
                        ArrayList<Additionals> additionalDetails = myDbHelper.getAdditionalDetails(parts[j]);

                        if (additionalDetails.size() > 0) {


                            for (Additionals sa : additionalDetails) {
                                if (ids.equals("0")) {
                                    ids = sa.getAdditionalsId();
                                    additionalsStr = sa.getAdditionalName();
                                    additionalsPrice = sa.getAdditionalPrice();
                                } else {
                                    ids = ids + "," + sa.getAdditionalsId();
                                    additionalsStr = additionalsStr + "," + sa.getAdditionalName();
                                    additionalsPrice = additionalsPrice + "," + sa.getAdditionalPrice();
                                }
                                subAmt = subAmt + Integer.parseInt(sa.getAdditionalPrice());

                            }
                        }
                    }
                }

                float totalAmt = subAmt * Integer.parseInt(qty);

                values.put("itemId", itemId);
                values.put("itemTypeId", size);
                values.put("subCategoryId", subCatId[0]);
                values.put("additionals", additionalsStr);
                values.put("qty", qty);
                values.put("price", Float.toString(subAmt));
                values.put("additionalsPrice", additionalsPrice);
                values.put("additionalsTypeId", ids);
                values.put("totalAmount", Float.toString(totalAmt));
                values.put("comment", comments);
                values.put("status", "1");
                values.put("creationDate", "14/07/2015");
                values.put("modifiedDate", "14/07/2015");
                values.put("categoryId", subCatId[1]);
                myDbHelper.insertOrder(values);

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.store_select_dialog, null);
                dialogBuilder.setView(dialogView);

                ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                ImageView pickup = (ImageView) dialogView.findViewById(R.id.select_pickup);
                ImageView dineIn = (ImageView) dialogView.findViewById(R.id.select_dinein);
                ImageView pickupBig = (ImageView) dialogView.findViewById(R.id.select_pickup_big);
                RelativeLayout pickupLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_layout);
                RelativeLayout dineinLayout = (RelativeLayout) dialogView.findViewById(R.id.dinein_layout);
                RelativeLayout pickupBigLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_big_layout);
                RelativeLayout deliveryLayout = (RelativeLayout) dialogView.findViewById(R.id.delivery_layout);
                TextView deliveryTv = (TextView) dialogView.findViewById(R.id.delivery_txt);
                TextView storeMsg = (TextView) dialogView.findViewById(R.id.store_msg);
                TextView pickupTv = (TextView) dialogView.findViewById(R.id.pickup_txt);
                TextView dineInTv = (TextView) dialogView.findViewById(R.id.dinein_txt);
                TextView pickupBigTv = (TextView) dialogView.findViewById(R.id.pickup_big_txt);
                TextView heading = (TextView) dialogView.findViewById(R.id.order_type_heading);
                pickupBigLayout.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    pickupTv.setText("Pick Up");
                    dineInTv.setText("Dine In");
                    pickupBigTv.setText("Pick Up");
                    heading.setText("Order Type");
                    deliveryTv.setText(si.getDeliveryMessageEn());
                    if (si.getMessage().equalsIgnoreCase("")) {
                        storeMsg.setVisibility(View.GONE);
                    } else {
                        storeMsg.setVisibility(View.VISIBLE);
                        storeMsg.setText(si.getMessage());
                    }
                } else if (language.equalsIgnoreCase("Ar")) {
                    pickupTv.setText("الاستمتاع خارج الفرع");
                    dineInTv.setText("الاستمتاع في الفرع");
                    pickupBigTv.setText("الاستمتاع خارج الفرع");
                    heading.setText("نوع الطلب");
                    deliveryTv.setText(si.getDeliveryMessageAr());
                    if (si.getMessage_ar().equalsIgnoreCase("")) {
                        storeMsg.setVisibility(View.GONE);
                    } else {
                        storeMsg.setVisibility(View.VISIBLE);
                        storeMsg.setText(si.getMessage_ar());
                    }
                }

                dineinLayout.setVisibility(View.GONE);
                pickupLayout.setVisibility(View.GONE);
                pickupBigLayout.setVisibility(View.VISIBLE);

                DrConstants.storeName = si.getStoreName();
                DrConstants.storeAddress = si.getStoreAddress();
                DrConstants.storeId = si.getStoreId();
                DrConstants.latitude = si.getLatitude();
                DrConstants.longitude = si.getLongitude();
                DrConstants.fullHours = si.is24x7();
                DrConstants.startTime = si.getStartTime();
                DrConstants.endTime = si.getEndTime();

                DrConstants.CashPayment = si.isCashPayment();
                DrConstants.OnlinePayment = si.isOnlinePayment();

                Gson gson = new Gson();
                String jsonText = gson.toJson(si);

                storePrefsEditor.putString("store", jsonText);
                storePrefsEditor.apply();

//            if (si.isDeliverable()) {
//                deliveryLayout.setVisibility(View.VISIBLE);
//            }
//            else {
//                deliveryLayout.setVisibility(View.GONE);
//            }

                pickupBig.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DrConstants.orderNow = true;
                        DrConstants.orderType = "Pick Up";
                        storePrefsEditor.putString("orderType", DrConstants.orderType);
                        storePrefsEditor.apply();

                        alertDialog.dismiss();
                        DrConstants.menuFlag = true;

                        Fragment newFragment = new MenuFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();

                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack
                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        transaction.add(R.id.realtabcontent, newFragment, "store_detail");
                        transaction.hide(FavouriteOrderFragment.this);
                        transaction.addToBackStack(null);

                        // Commit the transaction
                        transaction.commit();
                    }
                });

                deliveryLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DrConstants.orderNow = true;
                        DrConstants.orderType = "Delivery";
                        DrConstants.MinOrderAmount = si.getMinOrderAmount();
                        DrConstants.DeliveryCharge = si.getDeliveryCharge();
                        DrConstants.FreeDeliveryAbove = si.getFreeDeliveryAbove();
                        storePrefsEditor.putFloat("MinOrderAmount", DrConstants.MinOrderAmount);
                        storePrefsEditor.putFloat("DeliveryCharge", DrConstants.DeliveryCharge);
                        storePrefsEditor.putFloat("FreeDeliveryAbove", DrConstants.FreeDeliveryAbove);
                        storePrefsEditor.putString("orderType", DrConstants.orderType);
                        storePrefsEditor.commit();

                        alertDialog.dismiss();
                        DrConstants.menuFlag = true;

                        Bundle args = new Bundle();
                        args.putBoolean("store_details", false);
                        args.putDouble("lat", si.getLatitude());
                        args.putDouble("lng", si.getLongitude());
                        args.putFloat("delDistance", si.getDistanceForDelivery());
                        Fragment newFragment = new MyAddressActivity();
                        newFragment.setArguments(args);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();

                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack
                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        transaction.add(R.id.realtabcontent, newFragment, "address");
                        transaction.hide(FavouriteOrderFragment.this);
                        transaction.addToBackStack(null);

                        // Commit the transaction
                        transaction.commit();
                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        }else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());

            if(language.equalsIgnoreCase("En")) {
                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Right Now, This store can't take any orders.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            }else if(language.equalsIgnoreCase("Ar")){
                // set title
                alertDialogBuilder.setTitle("د. كيف");

                // set dialog message
                alertDialogBuilder
                        .setMessage("هذا الفرع لايستقبل الطلبات في الوقت الحالي.")
                        .setCancelable(false)
                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            }

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}

//SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
//Date dateObj=null;
//try {
//        dateObj = curFormater.parse(drydayList.get(position).getDate());
//        } catch (ParseException e) {
//        // TODO Auto-generated catch block
//        e.printStackTrace();
//        }
//        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy");
//        String date = postFormater.format(dateObj);