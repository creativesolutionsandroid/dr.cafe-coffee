package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.CountriesAdapter;
import com.cs.drcafe.adapter.ListViewCustomAdapter;
import com.cs.drcafe.model.Countries;
import com.cs.drcafe.model.FragmentUtils;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by BRAHMAM on 05-12-2015.
 */
public class PerfectShotFragment extends Fragment implements View.OnClickListener{
    private String SOAP_ACTION = "http://tempuri.org/insertPerfectShot";
    private String METHOD_NAME = "insertPerfectShot";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapPrimitive result;
    private String response12 = null;
    ProgressDialog dialog;
    ListView lview3;
    ListViewCustomAdapter adapter;
    private ArrayList<Countries> countriesList = new ArrayList<>();
    private Countries countries;
    private CountriesAdapter mCountriesAdapter;

    private LinearLayout mCountryPicker;
    private ImageView mCountryFlag;
    private EditText mCountryCode, mName, mProfession, mPhoneNumber, mEmail;
    private TextView mGenderMale, mGenderFemale, mVersionEnglish, mVersionArabic;
    String name, profession, phoneNumber, email, countryId = "190", countryCode = "+966", gender = "1", version = "2";
    Button backBtn, submit;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.perfect_shot, container,
                false);

        prepareArrayLits();
        mCountriesAdapter = new CountriesAdapter(getActivity(),countriesList);
        mCountryPicker = (LinearLayout) rootView.findViewById(R.id.country_picker);
        mCountryFlag = (ImageView) rootView.findViewById(R.id.country_flag);
        mCountryCode = (EditText) rootView.findViewById(R.id.country_code);
        mName = (EditText) rootView.findViewById(R.id.name);
        mProfession = (EditText) rootView.findViewById(R.id.profession);
        mPhoneNumber = (EditText) rootView.findViewById(R.id.mobile_number);
        mEmail = (EditText) rootView.findViewById(R.id.email);
        mGenderMale = (TextView) rootView.findViewById(R.id.gender_male);
        mGenderFemale = (TextView) rootView.findViewById(R.id.gender_female);
        mVersionEnglish = (TextView) rootView.findViewById(R.id.version_english);
        mVersionArabic = (TextView) rootView.findViewById(R.id.version_arabic);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        submit = (Button) rootView.findViewById(R.id.submit);



        backBtn.setOnClickListener(this);
        mCountryPicker.setOnClickListener(this);
        mGenderMale.setOnClickListener(this);
        mGenderFemale.setOnClickListener(this);
        mVersionEnglish.setOnClickListener(this);
        mVersionArabic.setOnClickListener(this);
        submit.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_btn:
                getActivity().onBackPressed();
                break;
            case R.id.country_picker:
                dialog();
                break;
            case R.id.gender_male:
                gender = "1";
                mGenderMale.setBackgroundColor(Color.parseColor("#00000000"));
                mGenderFemale.setBackgroundColor(Color.parseColor("#F1F1F1"));
                break;
            case R.id.gender_female:
                gender = "2";
                mGenderMale.setBackgroundColor(Color.parseColor("#F1F1F1"));
                mGenderFemale.setBackgroundColor(Color.parseColor("#00000000"));
                break;
            case R.id.version_english:
                version = "2";
                mVersionEnglish.setBackgroundColor(Color.parseColor("#00000000"));
                mVersionArabic.setBackgroundColor(Color.parseColor("#F1F1F1"));
                break;
            case R.id.version_arabic:
                version = "1";
                mVersionEnglish.setBackgroundColor(Color.parseColor("#F1F1F1"));
                mVersionArabic.setBackgroundColor(Color.parseColor("#00000000"));
                break;
            case R.id.submit:
                name = mName.getText().toString();
                profession = mProfession.getText().toString();
                phoneNumber = mPhoneNumber.getText().toString();
                email = mEmail.getText().toString();
                if(name.length() == 0){
                    mName.setError("Please enter First Name");
                }else if(profession.length() == 0){
                    mProfession.setError("Please enter Mobile Number");
                }else if(phoneNumber.length() != 10){
                    mPhoneNumber.setError("Please enter valid Mobile Number");
                }else if(email.length() == 0){
                    mEmail.setError("Please enter Email");
                }else if(!email.matches("[a-zA-Z0-9._-]+@[a-z-]+\\.+[a-z]+")){
                    mEmail.setError("Please use Email format (example - abc@abc.com");
                }else{
                    new UpdateUserProfile().execute();
                }
                break;
        }
    }



    public class UpdateUserProfile extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("fullname", name);
                    request.addProperty("genderId", gender);
                    request.addProperty("profession", profession);
                    request.addProperty("mobileno", countryCode+"-"+phoneNumber);
                    request.addProperty("email", email);
                    request.addProperty("versionId", version);
                    request.addProperty("countryId", countryId);



                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapPrimitive) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    if (response12.equals("user alredy exist.")) {
                        dialog.dismiss();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Email is already registered")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    } else if(response12.equals("success fully register.")){
                        dialog.dismiss();
                        Toast.makeText(getActivity(),"Successfully saved", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            super.onPostExecute(result1);
        }
    }



    /* Method used to prepare the ArrayList,
         * Same way, you can also do looping and adding object into the ArrayList.
         */
    public void prepareArrayLits()
    {

        AddObjectToList(R.drawable.bh, "Bahrain", "+973", "18");
        AddObjectToList(R.drawable.bd, "Bangladesh", "+880", "19");
        AddObjectToList(R.drawable.cn, "China", "+86", "45");
        AddObjectToList(R.drawable.cy, "Cyprus", "+537" ,"57");
        AddObjectToList(R.drawable.eg, "Egypt", "+20", "64");
        AddObjectToList(R.drawable.fr, "France", "+33", "74");
        AddObjectToList(R.drawable.de, "Germany", "+49", "81");
        AddObjectToList(R.drawable.in, "India", "+91", "101");
        AddObjectToList(R.drawable.id, "Indonesia", "+62", "102");

        AddObjectToList(R.drawable.ir, "Iran", "+98", "103");
        AddObjectToList(R.drawable.iq, "Iraq", "+964", "104");
        AddObjectToList(R.drawable.jo, "Jordan", "+962", "112");
        AddObjectToList(R.drawable.kw, "Kuwait", "+965", "118");
        AddObjectToList(R.drawable.lb, "Lebanon", "+961", "122");
        AddObjectToList(R.drawable.my, "Malaysia", "+60", "133");
        AddObjectToList(R.drawable.ma, "Morocco", "+212", "147");
        AddObjectToList(R.drawable.np, "Nepal", "+977", "152");
        AddObjectToList(R.drawable.om, "Oman", "+968", "164");

        AddObjectToList(R.drawable.pk, "Pakistan", "+92", "165");
        AddObjectToList(R.drawable.ps, "Palestinian Territories", "", "167");
        AddObjectToList(R.drawable.ph, "Philippines", "+63", "172");
        AddObjectToList(R.drawable.qa, "Qatar", "+974", "177");
        AddObjectToList(R.drawable.sa, "Saudi Arabia", "+966", "190");
        AddObjectToList(R.drawable.sg, "Singapore", "+65", "195");
        AddObjectToList(R.drawable.es, "Spain", "+34", "202");
        AddObjectToList(R.drawable.lk, "Sri Lanka", "+94", "203");
        AddObjectToList(R.drawable.sd, "Sudan", "+249", "204");

        AddObjectToList(R.drawable.tr, "Syria", "+963", "210");
        AddObjectToList(R.drawable.tr, "Turkey", "+90", "221");
        AddObjectToList(R.drawable.ae, "United Arab Emirates", "+971", "227");
        AddObjectToList(R.drawable.gb, "United Kingdom", "+44", "228");
        AddObjectToList(R.drawable.us, "United States", "+1", "229");
        AddObjectToList(R.drawable.ye, "Yemen", "+967", "240");
    }

    // Add one item into the Array List
    public void AddObjectToList(int image, String countryName, String countryCode, String countrrId)
    {
        countries = new Countries();
        countries.setCountryName(countryName);
        countries.setCountryFlag(image);
        countries.setCountryCode(countryCode);
        countries.setCountryID(countrrId);
        countriesList.add(countries);
    }

    public void dialog() {

        final Dialog dialog2 = new Dialog(getActivity());

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.setContentView(R.layout.countries_list);
        dialog2.setCanceledOnTouchOutside(true);
        ListView lv = (ListView) dialog2.findViewById(R.id.countries_list);
        lv.setAdapter(mCountriesAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                countryCode = countriesList.get(arg2).getCountryCode();
                countryId = countriesList.get(arg2).getCountryID();
                mCountryFlag.setImageResource(countriesList.get(arg2).getCountryFlag());
                mCountryCode.setText("" + countriesList.get(arg2).getCountryCode());
                dialog2.dismiss();

            }
        });


        dialog2.show();

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
