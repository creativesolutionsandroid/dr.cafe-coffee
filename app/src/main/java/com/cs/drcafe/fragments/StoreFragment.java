package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.GPSTracker;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.StoreListAdapter;
import com.cs.drcafe.adapter.StoreListAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.Item;
import com.cs.drcafe.model.SectionItem;
import com.cs.drcafe.model.StoreInfo;
import com.cs.drcafe.widget.FlipAnimation;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class StoreFragment extends Fragment implements View.OnClickListener, TextWatcher, OnMapReadyCallback {
    private GoogleMap map;
    private String SOAP_ACTION = "http://tempuri.org/getStoresInfo";
    private String METHOD_NAME = "getStoresInfo";
    private final String NAMESPACE = "http://tempuri.org/";

    private String SOAP_ACTION_TIME = "http://tempuri.org/getCurrentTime";
    private String METHOD_NAME_TIME = "getCurrentTime";
    private SoapPrimitive timeResult;
    private String timeResponse = null;
    private Timer timer = new Timer();
    boolean isFirstTime = true;

    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    private ArrayList<StoreInfo> favouriteStoresList = new ArrayList<>();
    private ArrayList<Item> totalStoresList = new ArrayList<>();
    private ArrayList<StoreInfo> tempStoresList = new ArrayList<>();
    private ArrayList<StoreInfo> filterStoresList = new ArrayList<>();
    private ListView mStoresListView;
    RelativeLayout buttonsLayout;
    private ImageView mStoreList, mStoreMap;
    private Button filterBtn;
    private TextView doneBtn, mapStandard, mapSatellite, mapHybrid;
    private LinearLayout mListviewLayout;
    private LinearLayout filterLayout, listLayout;
    private RelativeLayout storeLayout, mMapLayout;
    String response12 = null;
    SoapObject result;
    MarkerOptions markerOptions;
    StoreListAdapter mStoreListAdapter;
    StoreListAdapterArabic mStoreListAdapterArabic;
    private double lat, longi;
    private TextView wifiText, driveText, familyText, meetingText, patioText, v12Text, universityText, hospitalText, officeText, shoppingText, airportText, dineinText, ladiesText, neighborText;
    private EditText searchBar;
    public boolean isWifi, isDrive, isFamily, isMeeting, isPatio, isV12, isUniversity, isHospital, isOffice, isShopping, isAirport, isDineIn, isLadies, isNeighborhood;
    private boolean isAddedtoList, isFilterOn;
    ProgressDialog dialog;
    GPSTracker gps;
    public static Boolean isGPSRefreshed = false;

    private static final String URL = DrConstants.URL;
    SharedPreferences favouritePrefs, locationPrefs;
    SharedPreferences.Editor favEditor;
    ArrayList<String> favoriteStores = new ArrayList<>();
    String favStoreIds, previousStoreIds;
    String mDistancePrefValue, mRadiusPrefValue, mDisplayPrefValue;
    String language;
    View rootView;

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }

        try {
            SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");

            if (language.equalsIgnoreCase("En")) {
                rootView = inflater.inflate(R.layout.store_layout, container,
                        false);
            } else if (language.equalsIgnoreCase("Ar")) {
                rootView = inflater.inflate(R.layout.store_layout_arabic, container,
                        false);
            }
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }

        mStoresListView = (ListView) rootView.findViewById(R.id.store_listview);
//        mFavoriteListView = (ListView) rootView.findViewById(R.id.favourite_listview);

        mStoreList = (ImageView) rootView.findViewById(R.id.store_list);
        mStoreMap = (ImageView) rootView.findViewById(R.id.store_map);
        mMapLayout = (RelativeLayout) rootView.findViewById(R.id.map_layout);
        mListviewLayout = (LinearLayout) rootView.findViewById(R.id.listview_layout);
        buttonsLayout = (RelativeLayout) rootView.findViewById(R.id.relativeLayout);
        filterBtn = (Button) rootView.findViewById(R.id.filter_btn);
        doneBtn = (TextView) rootView.findViewById(R.id.done_btn);
        listLayout = (LinearLayout) rootView.findViewById(R.id.list_layout);
        filterLayout = (LinearLayout) rootView.findViewById(R.id.filter_layout);
        storeLayout = (RelativeLayout) rootView.findViewById(R.id.store_layout);

//        FragmentManager fm = getChildFragmentManager();
//        Fragment fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(StoreFragment.this);

//        if(language.equalsIgnoreCase("En")) {
//            mStoreListAdapter = new StoreListAdapter(getActivity(), totalStoresList);
//            mStoresListView.setAdapter(mStoreListAdapter);
//        }else if(language.equalsIgnoreCase("Ar")){
//            mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList);
//            mStoresListView.setAdapter(mStoreListAdapterArabic);
//        }

//        mFavouriteAdapter = new StoreListAdapter(getActivity(), favouriteStoresList);
//        mFavoriteListView.setAdapter(mFavouriteAdapter);
        wifiText = (TextView) rootView.findViewById(R.id.filter_hotspot);
        driveText = (TextView) rootView.findViewById(R.id.filter_drive);
        familyText = (TextView) rootView.findViewById(R.id.filter_family);
        meetingText = (TextView) rootView.findViewById(R.id.filter_meeting);
        patioText = (TextView) rootView.findViewById(R.id.filter_patio);
        v12Text = (TextView) rootView.findViewById(R.id.filter_v12);
        universityText = (TextView) rootView.findViewById(R.id.filter_university);
        hospitalText = (TextView) rootView.findViewById(R.id.filter_hospital);
        officeText = (TextView) rootView.findViewById(R.id.filter_office);
        shoppingText = (TextView) rootView.findViewById(R.id.filter_shopping);
        airportText = (TextView) rootView.findViewById(R.id.filter_airport);
        dineinText = (TextView) rootView.findViewById(R.id.filter_dinein);
        ladiesText = (TextView) rootView.findViewById(R.id.filter_ladies);
        neighborText = (TextView) rootView.findViewById(R.id.filter_neighbohood);
        searchBar = (EditText) rootView.findViewById(R.id.search_bar);
        mapStandard = (TextView) rootView.findViewById(R.id.map_standard);
        mapSatellite = (TextView) rootView.findViewById(R.id.map_satellite);
        mapHybrid = (TextView) rootView.findViewById(R.id.map_hybrid);

        wifiText.setOnClickListener(this);
        driveText.setOnClickListener(this);
        familyText.setOnClickListener(this);
        meetingText.setOnClickListener(this);
        patioText.setOnClickListener(this);
        v12Text.setOnClickListener(this);
        universityText.setOnClickListener(this);
        hospitalText.setOnClickListener(this);
        officeText.setOnClickListener(this);
        shoppingText.setOnClickListener(this);
        airportText.setOnClickListener(this);
        dineinText.setOnClickListener(this);
        ladiesText.setOnClickListener(this);
        neighborText.setOnClickListener(this);
        mapStandard.setOnClickListener(this);
        mapSatellite.setOnClickListener(this);
        mapHybrid.setOnClickListener(this);
        searchBar.addTextChangedListener(this);

        searchBar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    searchBar.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                } else {
                    searchBar.setGravity(Gravity.CENTER);
                }
            }
        });

        favouritePrefs = getActivity().getSharedPreferences("FAV_PREFS", Context.MODE_PRIVATE);
        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);
        favEditor = favouritePrefs.edit();

        mDistancePrefValue = locationPrefs.getString("distance", "kilometer");
        mRadiusPrefValue = locationPrefs.getString("radius", DrConstants.storeDistance);
        mDisplayPrefValue = locationPrefs.getString("display", "map");
        favStoreIds = favouritePrefs.getString("favourite_store_ids", null);
        previousStoreIds = favStoreIds;
        if (favStoreIds != null) {
            String[] parts = favStoreIds.split(",");
            for (int i = 0; i < parts.length; i++) {
                favoriteStores.add(parts[i]);
            }
        } else {
        }

        if (mDisplayPrefValue.equalsIgnoreCase("map")) {

        } else {
            mListviewLayout.setVisibility(View.VISIBLE);
            mMapLayout.setVisibility(View.GONE);
            mStoreList.setImageResource(R.drawable.store_list_selected);
            mStoreMap.setImageResource(R.drawable.store_map);
            buttonsLayout.removeView(mStoreMap);
            buttonsLayout.addView(mStoreMap);
        }


//        if (fragment == null) {
//            fragment = SupportMapFragment.newInstance();
//            fm.beginTransaction().replace(R.id.map, fragment).commit();
//        }
//        map = ((SupportMapFragment) fragment).getMap();


//        // Get LocationManager object from System Service LOCATION_SERVICE
//        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//
//        // Create a criteria object to retrieve provider
//        Criteria criteria = new Criteria();
//
//        // Get the name of the best provider
//        String provider = locationManager.getBestProvider(criteria, true);
//
//        // Get Current Location
//        Location myLocation = locationManager.getLastKnownLocation(provider);
//
//
//        // Get latitude of the current location
//        double latitude = myLocation.getLatitude();
//
//        // Get longitude of the current location
//        double longitude = myLocation.getLongitude();

        // check if GPS enabled




//        map.animateCamera(CameraUpdateFactory.zoomTo(12.0f));


        mStoreList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListviewLayout.setVisibility(View.VISIBLE);
                mMapLayout.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mStoreList.setImageResource(R.drawable.store_list_selected);
                    mStoreMap.setImageResource(R.drawable.store_map);
                } else if (language.equalsIgnoreCase("Ar")) {
                    mStoreList.setImageResource(R.drawable.store_list_selected_arabic);
                    mStoreMap.setImageResource(R.drawable.store_map_arabic);
                }

                buttonsLayout.removeView(mStoreMap);
                buttonsLayout.addView(mStoreMap);
            }
        });

        mStoreMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListviewLayout.setVisibility(View.GONE);
                mMapLayout.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    mStoreMap.setImageResource(R.drawable.store_map_selected);
                    mStoreList.setImageResource(R.drawable.store_list);
                } else if (language.equalsIgnoreCase("Ar")) {
                    mStoreMap.setImageResource(R.drawable.store_map_selected_arabic);
                    mStoreList.setImageResource(R.drawable.store_list_arabic);
                }
                buttonsLayout.removeView(mStoreList);
                buttonsLayout.addView(mStoreList);
            }
        });

        mStoresListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Item listitem = totalStoresList.get(i);
                StoreInfo si = (StoreInfo) listitem;
                Fragment newFragment = new StoreDetailsFragment();
                Bundle mBundle = new Bundle();
                if (language.equalsIgnoreCase("En")) {
                    mBundle.putString("storeName", si.getStoreName());
                    mBundle.putString("storeAddress", si.getStoreAddress());
                } else if (language.equalsIgnoreCase("Ar")) {
                    mBundle.putString("storeName", si.getStoreName_ar());
                    mBundle.putString("storeAddress", si.getStoreAddress_ar());
                }
                mBundle.putString("storeId", si.getStoreId());
                mBundle.putDouble("latitude", si.getLatitude());
                mBundle.putDouble("longitude", si.getLongitude());
                mBundle.putDouble("delDistance", si.getDistanceForDelivery());
                mBundle.putDouble("MinOrderAmount", si.getMinOrderAmount());
                mBundle.putDouble("DeliveryCharge", si.getDeliveryCharge());
                mBundle.putDouble("FreeDeliveryAbove", si.getFreeDeliveryAbove());
                mBundle.putString("getDeliveryMessageEn", si.getDeliveryMessageEn());
                mBundle.putString("getDeliveryMessageAr", si.getDeliveryMessageAr());
                mBundle.putString("stTime", si.getStartTime());
                mBundle.putBoolean("isDeliverable", si.isDeliverable());
                mBundle.putString("edTime", si.getEndTime());
                mBundle.putBoolean("is24x7", si.is24x7());
                mBundle.putBoolean("isDineIn", si.isDineIn());
                mBundle.putBoolean("ispickUp", si.isPickUp());
                mBundle.putDouble("lat", lat);
                mBundle.putDouble("longi", longi);
                mBundle.putString("message", si.getMessage());
                mBundle.putString("messageAr", si.getMessage_ar());
                mBundle.putBoolean("is_online", si.isOnlineOrderStatus());
                DrConstants.CashPayment = si.isCashPayment();
                DrConstants.OnlinePayment = si.isOnlinePayment();
                newFragment.setArguments(mBundle);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.add(R.id.realtabcontent, newFragment, "store_detail");
                transaction.hide(StoreFragment.this);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flipCard();
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                flipCard();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        filterData(response12);
                    }
                }, 500);
//                new FilterStoreInfo().execute();
            }
        });

        timer.schedule(new MyTimerTask(), 60000, 60000);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                gps = new GPSTracker(getActivity());
                try {
                    map.setMyLocationEnabled(true);
                }catch (NullPointerException npe){

                }
                getGPSCoordinates();
            }
        }else {
            gps = new GPSTracker(getActivity());
            try {
                map.setMyLocationEnabled(true);
            }catch (NullPointerException npe){

            }
            getGPSCoordinates();
        }

        LatLng latLng = new LatLng(lat, longi);

        // Show the current location in Google Map
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        // Zoom in the Google Map
        map.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
        markerOptions = new MarkerOptions();

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Fragment newFragment = new StoreDetailsFragment();

                Bundle mBundle = new Bundle();
                for (int i = 0; i < storesList.size(); i++) {
                    if (language.equalsIgnoreCase("En")) {
                        if (storesList.get(i) != null && storesList.get(i).getStoreName().equals(marker.getTitle())) {
                            mBundle.putString("storeId", storesList.get(i).getStoreId());
                            mBundle.putDouble("latitude", storesList.get(i).getLatitude());
                            mBundle.putDouble("longitude", storesList.get(i).getLongitude());
                            mBundle.putString("stTime", storesList.get(i).getStartTime());
                            mBundle.putString("edTime", storesList.get(i).getEndTime());
                            mBundle.putBoolean("is24x7", storesList.get(i).is24x7());
                            mBundle.putBoolean("isDineIn", storesList.get(i).isDineIn());
                            mBundle.putBoolean("isDeliverable", storesList.get(i).isDeliverable());
                            mBundle.putString("getDeliveryMessageEn", storesList.get(i).getDeliveryMessageEn());
                            mBundle.putString("getDeliveryMessageAr", storesList.get(i).getDeliveryMessageAr());
                            mBundle.putDouble("lat", lat);
                            mBundle.putDouble("longi", longi);
                            mBundle.putDouble("delDistance", storesList.get(i).getDistanceForDelivery());
                            mBundle.putDouble("MinOrderAmount", storesList.get(i).getMinOrderAmount());
                            mBundle.putDouble("DeliveryCharge", storesList.get(i).getDeliveryCharge());
                            mBundle.putDouble("FreeDeliveryAbove", storesList.get(i).getFreeDeliveryAbove());
                            mBundle.putString("message", storesList.get(i).getMessage());
                            mBundle.putString("messageAr", storesList.get(i).getMessage_ar());
                            DrConstants.CashPayment = storesList.get(i).isCashPayment();
                            DrConstants.OnlinePayment = storesList.get(i).isOnlinePayment();
                        }
                    } else if (language.equalsIgnoreCase("Ar")) {
                        if (storesList.get(i) != null && storesList.get(i).getStoreName_ar().equals(marker.getTitle())) {
                            mBundle.putString("storeId", storesList.get(i).getStoreId());
                            mBundle.putDouble("latitude", storesList.get(i).getLatitude());
                            mBundle.putDouble("longitude", storesList.get(i).getLongitude());
                            mBundle.putString("stTime", storesList.get(i).getStartTime());
                            mBundle.putString("edTime", storesList.get(i).getEndTime());
                            mBundle.putBoolean("is24x7", storesList.get(i).is24x7());
                            mBundle.putBoolean("isDineIn", storesList.get(i).isDineIn());
                            mBundle.putDouble("lat", lat);
                            mBundle.putDouble("longi", longi);
                            mBundle.putString("message", storesList.get(i).getMessage());
                            mBundle.putString("messageAr", storesList.get(i).getMessage_ar());
                            DrConstants.CashPayment = storesList.get(i).isCashPayment();
                            DrConstants.OnlinePayment = storesList.get(i).isOnlinePayment();
                        }
                    }

                }

                mBundle.putString("storeName", marker.getTitle());
                mBundle.putString("storeAddress", marker.getSnippet());
//                mBundle.putDouble("latitude", marker.getPosition().latitude);
//                mBundle.putDouble("longitude", marker.getPosition().longitude);
//                mBundle.putDouble("lat", lat);
//                mBundle.putDouble("longi", longi);
                newFragment.setArguments(mBundle);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.add(R.id.realtabcontent, newFragment, "store_detail");
                transaction.hide(StoreFragment.this);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });

    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if(getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        StoreFragment myFragment = (StoreFragment) getFragmentManager().findFragmentByTag("store");
                        if (myFragment != null && myFragment.isVisible()) {
//                            Log.i("TAG","fragment Visible ");
                            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                            if (currentapiVersion >= Build.VERSION_CODES.M) {
                                if (!canAccessLocation()) {
                                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                                } else {
                                    getGPSCoordinates();
                                }
                            }else {
                                getGPSCoordinates();
                            }
                        }
                        else {
//                            Log.i("TAG","fragment not visible ");
                        }
                    }
                });
            }
        }
    }

    private void flipCard() {
        FlipAnimation flipAnimation = new FlipAnimation(listLayout, filterLayout);

        if (listLayout.getVisibility() == View.GONE) {
            flipAnimation.reverse();
        }
        storeLayout.startAnimation(flipAnimation);
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }


    public void getGPSCoordinates(){
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
        if(gps != null){
            if (gps.canGetLocation()) {

                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                try {
                    LatLng latLng = new LatLng(lat, longi);

                    // Show the current location in Google Map
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                    // Zoom in the Google Map
                    map.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
                    markerOptions = new MarkerOptions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new GetCurrentTime().execute();
                Log.i("Location TAG", "outside" + lat + " " + longi);
                // \n is for new line
//            Toast.makeText(getActivity(), "Your Location is - \nLat: " + lat + "\nLong: " + longi, Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(getActivity());
                    try {
                        map.setMyLocationEnabled(true);
                    }catch (NullPointerException npe){

                    }
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
//        try {
//            FragmentManager fm = getChildFragmentManager();
//            Fragment f = (SupportMapFragment) fm.findFragmentById(R.id.map);
//            if (f != null)
//                getFragmentManager().beginTransaction().remove(f).commitAllowingStateLoss();
//            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        } catch (IllegalStateException ise) {
//            ise.printStackTrace();
//        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.filter_hotspot:
                if(isWifi){
                    isWifi= false;
                    wifiText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isWifi = true;
                    wifiText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_drive:
                if(isDrive){
                    isDrive = false;
                    driveText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isDrive = true;
                    driveText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_family:
                if(isFamily){
                    isFamily = false;
                    familyText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isFamily = true;
                    familyText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_meeting:
                if(isMeeting){
                    isMeeting = false;
                    meetingText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isMeeting = true;
                    meetingText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_patio:
                if(isPatio){
                    isPatio = false;
                    patioText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isPatio = true;
                    patioText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;

            case R.id.filter_v12:
                if(isV12){
                    isV12 = false;
                    v12Text.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isV12 = true;
                    v12Text.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;

            case R.id.filter_university:
                if(isUniversity){
                    isUniversity = false;
                    universityText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isUniversity = true;
                    universityText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_hospital:
                if(isHospital){
                    isHospital = false;
                    hospitalText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isHospital= true;
                    hospitalText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;

            case R.id.filter_office:
                if(isOffice){
                    isOffice = false;
                    officeText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isOffice = true;
                    officeText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_shopping:
                if(isShopping){
                    isShopping= false;
                    shoppingText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isShopping = true;
                    shoppingText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_airport:
                if(isAirport){
                    isAirport= false;
                    airportText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isAirport = true;
                    airportText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_dinein:
                if(isDineIn){
                    isDineIn= false;
                    dineinText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isDineIn = true;
                    dineinText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_ladies:
                if(isLadies){
                    isLadies= false;
                    ladiesText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isLadies = true;
                    ladiesText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_neighbohood:
                if (isNeighborhood) {
                    isNeighborhood = false;
                    neighborText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isNeighborhood = true;
                    neighborText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.map_standard:
                mapStandard.setBackgroundColor(Color.parseColor("#2196F3"));
                mapSatellite.setBackgroundColor(0);
                mapHybrid.setBackgroundColor(0);
                mapStandard.setTextColor(Color.parseColor("#FFFFFF"));
                mapSatellite.setTextColor(Color.parseColor("#2196F3"));
                mapHybrid.setTextColor(Color.parseColor("#2196F3"));
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.map_satellite:
                mapStandard.setBackgroundColor(0);
                mapSatellite.setBackgroundColor(Color.parseColor("#2196F3"));
                mapHybrid.setBackgroundColor(0);
                mapStandard.setTextColor(Color.parseColor("#2196F3"));
                mapSatellite.setTextColor(Color.parseColor("#FFFFFF"));
                mapHybrid.setTextColor(Color.parseColor("#2196F3"));
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.map_hybrid:
                mapStandard.setBackgroundColor(0);
                mapSatellite.setBackgroundColor(0);
                mapHybrid.setBackgroundColor(Color.parseColor("#2196F3"));
                mapStandard.setTextColor(Color.parseColor("#2196F3"));
                mapSatellite.setTextColor(Color.parseColor("#2196F3"));
                mapHybrid.setTextColor(Color.parseColor("#FFFFFF"));
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);


        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String text = searchBar.getText().toString().toLowerCase();
        mStoresListView.invalidate();
        totalStoresList.clear();
        if (text.length() == 0)
        {
            searchBar.setGravity(Gravity.CENTER);
            if(isFilterOn){
                if(favouriteStoresList.size()==0){
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add((new SectionItem("Nearby Stores")));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add((new SectionItem(" الفروع القريبة")));
                    }
                    totalStoresList.addAll(filterStoresList);
//            mFavouriteLayout.setVisibility(View.VISIBLE);
                }else{
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add(new SectionItem("Favorite Stores"));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add(new SectionItem("  الفروع المفضلة"));
                    }
                    totalStoresList.addAll(favouriteStoresList);
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add((new SectionItem("Nearby Stores")));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add((new SectionItem(" الفروع القريبة")));
                    }
                    totalStoresList.addAll(filterStoresList);
                }
            }else {
                System.out.println("if statement");
                if(favouriteStoresList.size()==0){
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add((new SectionItem("Nearby Stores")));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add((new SectionItem(" الفروع القريبة")));
                    }
                    totalStoresList.addAll(tempStoresList);
//            mFavouriteLayout.setVisibility(View.VISIBLE);
                }else{
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add(new SectionItem("Favorite Stores"));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add(new SectionItem("  الفروع المفضلة"));
                    }
                    totalStoresList.addAll(favouriteStoresList);
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add((new SectionItem("Nearby Stores")));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add((new SectionItem(" الفروع القريبة")));
                    }
                    totalStoresList.addAll(tempStoresList);
                }
            }
            // System.out.println("if stmt"+theatersList.size());
        }else{
            searchBar.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            if(isFilterOn){
                for(StoreInfo si: filterStoresList){
                    if (si.getStoreAddress().toLowerCase().contains(text) || si.getStoreName().toLowerCase().contains(text) || si.getCityName().toLowerCase().contains(text) || si.getStoreAddress_ar().contains(text) || si.getStoreName_ar().contains(text)) {
                        totalStoresList.add(si);
                    }
                }
            }
            else {
                System.out.println("else statement");
                for (StoreInfo si: tempStoresList) {
                    System.out.println("else for loop");
                    if (si.getStoreAddress().toLowerCase().contains(text) || si.getStoreName().toLowerCase().contains(text) || si.getCityName().toLowerCase().contains(text) || si.getStoreAddress_ar().contains(text) || si.getStoreName_ar().contains(text)) {
                        totalStoresList.add(si);
                    }
                }
            }
        }
        Collections.sort(storesList, StoreInfo.storeDistance);
        if(language.equalsIgnoreCase("En")) {
            if(mStoreListAdapter != null) {
                mStoreListAdapter.notifyDataSetChanged();
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if(mStoreListAdapterArabic != null) {
                mStoreListAdapterArabic.notifyDataSetChanged();
            }
        }
    }


    public class GetStoreInfo extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus;
        String dayOfWeek;
        @Override
        protected void onPreExecute() {
//            storesList.clear();
//            totalStoresList.clear();
//            tempStoresList.clear();
//            favouriteStoresList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if(isFirstTime) {
                dialog = ProgressDialog.show(getActivity(), "",
                        "Fetching stores...");
                isFirstTime = false;
            }
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("day", dayOfWeek);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;

        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    if(dialog!=null) {
                        dialog.dismiss();
                    }
                    Toast.makeText(getActivity(),"Please check internet connection",Toast.LENGTH_SHORT).show();
                }else{
                    storesList.clear();
                    filterStoresList.clear();
                    totalStoresList.clear();
                    favouriteStoresList.clear();
                    map.clear();
                    if(response12 == null){

                    }else if (response12.equals("anyType{}")) {
                        Toast.makeText(getActivity(), "Failed to get stores info", Toast.LENGTH_LONG).show();
                    } else {

                        try {
//                    Log.d("Responce", "" + result);
//                    for (int i = 0; i < result.getPropertyCount(); i++) {
                            Object property = result.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject category_list = (SoapObject) property;
                                    SoapObject storeObject = (SoapObject) category_list.getProperty(0);
//                                    lat = 24.70321657;
//                                    longi = 46.68097073;
                                    for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                        isAddedtoList = false;
                                        SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                        StoreInfo si = new StoreInfo();



//                                    for (int j = 0; j < sObj.getPropertyCount(); j++) {
                                        try {
                                            si.setStoreId(sObj.getPropertyAsString(0));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setStartTime(sObj.getPropertyAsString(1));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setEndTime(sObj.getPropertyAsString(2));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setStoreName(sObj.getPropertyAsString("StoreName"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setStoreAddress(sObj.getPropertyAsString("StoreAddress"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setLatitude(Double.valueOf(sObj.getPropertyAsString("Latitude")));
                                        } catch (NumberFormatException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setLongitude(Double.valueOf(sObj.getPropertyAsString("Longitude")));
                                        } catch (NumberFormatException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setCountryName(sObj.getPropertyAsString("CountryName"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setCityName(sObj.getPropertyAsString("CityName"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setImageURL(sObj.getPropertyAsString("imageURL"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setFamilySection(Boolean.valueOf(sObj.getPropertyAsString("FamilySection")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setWifi(Boolean.valueOf(sObj.getPropertyAsString("Wifi")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setPatioSitting(Boolean.valueOf(sObj.getPropertyAsString("PatioSitting")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setV12(Boolean.valueOf(sObj.getPropertyAsString("V12")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setDriveThru(Boolean.valueOf(sObj.getPropertyAsString("DriveThru")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setMeetingSpace(Boolean.valueOf(sObj.getPropertyAsString("MeetingSpace")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setHospital(Boolean.valueOf(sObj.getPropertyAsString("Hospital")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setUniversity(Boolean.valueOf(sObj.getPropertyAsString("University")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setOffice(Boolean.valueOf(sObj.getPropertyAsString("Office")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setShoppingMall(Boolean.valueOf(sObj.getPropertyAsString("ShoppingMall")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
//                                        try {
//                                            si.setIemsJson(sObj.getPropertyAsString("ItemsJson"));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }

//                                        try {
//                                            si.setDeliveryItems(sObj.getPropertyAsString("DeliveryItems"));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
                                        try{
                                            si.setAirPort(Boolean.valueOf(sObj.getPropertyAsString("Airport")));
                                        }catch (Exception e){
                                            si.setAirPort(false);
                                        }
                                        try{
                                            si.setDineIn(Boolean.valueOf(sObj.getPropertyAsString("DineIn")));
                                        }catch (Exception e){
                                            si.setDineIn(false);
                                        }
                                        try{
                                            si.setLadies(Boolean.valueOf(sObj.getPropertyAsString("Ladies")));
                                        }catch (Exception e){
                                            si.setLadies(false);
                                        }
//                                        try {
//                                            si.setDeliverable(Boolean.valueOf(sObj.getPropertyAsString("Deliverable")));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            si.setDistanceForDelivery(Float.valueOf(sObj.getPropertyAsString("Distance")));
//                                        } catch (NumberFormatException e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            si.setMinOrderAmount(Float.valueOf(sObj.getPropertyAsString("MinOrderAmount")));
//                                        } catch (NumberFormatException e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            si.setDeliveryCharge(Float.valueOf(sObj.getPropertyAsString("DeliveryCharge")));
//                                        } catch (NumberFormatException e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            si.setFreeDeliveryAbove(Float.valueOf(sObj.getPropertyAsString("FreeDeliveryAbove")));
//                                        } catch (NumberFormatException e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            si.setDeliveryMessageEn(sObj.getPropertyAsString("DeliveryMessageEn"));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            si.setDeliveryMessageAr(sObj.getPropertyAsString("DeliveryMessageAr"));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
                                        try {
                                            si.setNeighborhood(Boolean.valueOf(sObj.getPropertyAsString("Neighborhood")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setIs24x7(Boolean.valueOf(sObj.getPropertyAsString("is24x7")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setStatus(Boolean.valueOf(sObj.getPropertyAsString("status")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setDcCountry(sObj.getPropertyAsString("DCCountry"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setDcCity(sObj.getPropertyAsString("DCCity"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setStoreName_ar(sObj.getPropertyAsString("StoreName_ar"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            si.setStoreAddress_ar(sObj.getPropertyAsString("StoreAddress_ar"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
//                                        try {
//                                            si.setCashPayment(Boolean.valueOf(sObj.getPropertyAsString("CashPayment")));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            si.setOnlinePayment(Boolean.valueOf(sObj.getPropertyAsString("OnlinePayment")));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            si.setOnlineOrderStatus(Boolean.valueOf(sObj.getPropertyAsString("OnlineOrderStatus")));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
                                        if(language.equalsIgnoreCase("En")){
                                            markerOptions.position(new LatLng(Double.valueOf(sObj.getProperty("Latitude").toString()), Double.valueOf(sObj.getProperty("Longitude").toString())))
                                                    .title(sObj.getPropertyAsString(3))
                                                    .snippet(sObj.getPropertyAsString(4))
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
                                        }else if(language.equalsIgnoreCase("Ar")){
                                            markerOptions.position(new LatLng(Double.valueOf(sObj.getProperty("Latitude").toString()), Double.valueOf(sObj.getProperty("Longitude").toString())))
                                                    .title(sObj.getPropertyAsString("StoreName_ar"))
                                                    .snippet(sObj.getPropertyAsString("StoreAddress_ar"))
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
                                        }

                                        Location me   = new Location("");
                                        Location dest = new Location("");

                                        me.setLatitude(lat);
                                        me.setLongitude(longi);

                                        dest.setLatitude(Double.valueOf(sObj.getPropertyAsString(5)));
                                        dest.setLongitude(Double.valueOf(sObj.getPropertyAsString(6)));

                                        float dist = (me.distanceTo(dest))/1000;
                                        float mile = (float) (dist * 0.621);
                                        Log.i("storename", "" + sObj.getPropertyAsString(1) + "   " + sObj.getPropertyAsString(2));
                                        if(mDistancePrefValue.equalsIgnoreCase("kilometer")){
                                            si.setDistance(dist);
                                        }else if(mDistancePrefValue.equalsIgnoreCase("mile")){
                                            si.setDistance(mile);
                                        }

                                        if(dist<Integer.parseInt(mRadiusPrefValue)) {
//                                        if(dist<5000) {
//                                            else {
//                                    markerOptions.position(new LatLng(Double.valueOf(sObj.getProperty("Latitude").toString()), Double.valueOf(sObj.getProperty("Longitude").toString()))).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));

                                            map.addMarker(markerOptions);
                                            storesList.add(si);
//                                            }


                                            if(favoriteStores.size()>0){
                                                if(favoriteStores.contains(sObj.getPropertyAsString(0))){
                                                    favouriteStoresList.add(si);
                                                }
                                            }
                                            else {
                                            }

                                        }
//                                    }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
//                    }
//                            map.animateCamera(CameraUpdateFactory.newLatLngZoom
//                                    (new LatLng(lat, longi), 11.0f));
                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }



                        filterStoresList.addAll(storesList);


                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(favouriteStoresList, StoreInfo.storeDistance);
                        if(favouriteStoresList.size() >0){
                            if(language.equalsIgnoreCase("En")) {
                                totalStoresList.add(new SectionItem("Favorite Stores"));
                            }else if(language.equalsIgnoreCase("Ar")){
                                totalStoresList.add(new SectionItem("  الفروع المفضلة"));
                            }

                            totalStoresList.addAll(favouriteStoresList);
                        }
                        if(language.equalsIgnoreCase("En")) {
                            totalStoresList.add((new SectionItem("Nearby Stores")));
                        }else if(language.equalsIgnoreCase("Ar")){
                            totalStoresList.add((new SectionItem(" الفروع القريبة")));
                        }

                        totalStoresList.addAll(storesList);
                        if(tempStoresList.size()==0){
                            tempStoresList.addAll(storesList);
                        }
                        if(language.equalsIgnoreCase("En")) {
                            if(mStoreListAdapter != null) {
                                mStoreListAdapter.notifyDataSetChanged();
                            }
                        }else if(language.equalsIgnoreCase("Ar")){
                            if(mStoreListAdapterArabic != null) {
                                mStoreListAdapterArabic.notifyDataSetChanged();
                            }
                        }
                        mStoresListView.smoothScrollToPosition(10);
                        mStoresListView.smoothScrollToPosition(0);

//                        mFavouriteAdapter.notifyDataSetChanged();

                        //detailsAdapter.notifyDataSetChanged();
                    }
                    if(dialog!=null) {
                        dialog.dismiss();
                    }
                    super.onPostExecute(result1);
                }
            }

        }
    }


    public void filterData(String response12){
        storesList.clear();
        filterStoresList.clear();

        map.clear();
        for(StoreInfo si: tempStoresList) {
            isAddedtoList = false;
            if(language.equalsIgnoreCase("En")){
                markerOptions.position(new LatLng(Double.valueOf(si.getLatitude()), si.getLongitude()))
                        .title(si.getStoreName())
                        .snippet(si.getStoreAddress())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
            }else if(language.equalsIgnoreCase("Ar")){
                markerOptions.position(new LatLng(Double.valueOf(si.getLatitude()), si.getLongitude()))
                        .title(si.getStoreName_ar())
                        .snippet(si.getStoreAddress_ar())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
            }

            if (isWifi || isDrive || isFamily || isMeeting || isPatio || isV12 || isUniversity || isHospital || isOffice || isShopping || isAirport || isDineIn || isLadies || isNeighborhood) {
                isFilterOn = true;
                if (isWifi) {
                    if (si.isWifi()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }
                if (isDrive) {
                    if (si.isDriveThru()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }
                if (isFamily) {
                    if (si.isFamilySection()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }
                if (isMeeting) {
                    if (si.isMeetingSpace()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }
                if (isPatio) {
                    if (si.isPatioSitting()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isV12) {
                    if (si.isV12()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isUniversity) {
                    if (si.isUniversity()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isHospital) {
                    if (si.isHospital()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isOffice) {
                    if (si.isOffice()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isShopping) {
                    if (si.isShoppingMall()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isAirport) {
                    if (si.isAirPort()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isDineIn) {
                    if (si.isDineIn()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isLadies) {
                    if (si.isLadies()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                            map.addMarker(markerOptions);
                        }
                    }
                }

                if (isNeighborhood) {
                    if (si.isNeighborhood()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

            }else{
                storesList.add(si);
                map.addMarker(markerOptions);
            }
        }


        if(favouriteStoresList.size()==0){
            totalStoresList.clear();
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add((new SectionItem("Nearby Stores")));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add((new SectionItem(" الفروع القريبة")));
            }
            totalStoresList.addAll(storesList);
//            mFavouriteLayout.setVisibility(View.VISIBLE);
        }else{
            totalStoresList.clear();
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add(new SectionItem("Favorite Stores"));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add(new SectionItem("  الفروع المفضلة"));
            }
            totalStoresList.addAll(favouriteStoresList);
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add((new SectionItem("Nearby Stores")));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add((new SectionItem(" الفروع القريبة")));
            }
            totalStoresList.addAll(storesList);
        }
//        Collections.sort(storesList, StoreInfo.storeDistance);
        filterStoresList.addAll(storesList);
        if(language.equalsIgnoreCase("En")) {
            if(mStoreListAdapter != null) {
                mStoreListAdapter.notifyDataSetChanged();
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if(mStoreListAdapterArabic != null) {
                mStoreListAdapterArabic.notifyDataSetChanged();
            }
        }
    }


    public void setFavStores(){

        favouriteStoresList.clear();
        for(StoreInfo si:tempStoresList){
            if(favoriteStores.contains(si.getStoreId())){
                favouriteStoresList.add(si);
            }
        }
        if(favouriteStoresList.size()==0){
            totalStoresList.clear();
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add((new SectionItem("Nearby Stores")));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add((new SectionItem(" الفروع القريبة")));
            }
            totalStoresList.addAll(tempStoresList);
//            mFavouriteLayout.setVisibility(View.VISIBLE);
        }else{
            totalStoresList.clear();
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add(new SectionItem("Favorite Stores"));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add(new SectionItem("  الفروع المفضلة"));
            }
            totalStoresList.addAll(favouriteStoresList);
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add((new SectionItem("Nearby Stores")));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add((new SectionItem(" الفروع القريبة")));
            }
            totalStoresList.addAll(tempStoresList);
        }
        if(language.equalsIgnoreCase("En")) {
            if(mStoreListAdapter != null) {
                mStoreListAdapter.notifyDataSetChanged();
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if(mStoreListAdapterArabic != null) {
                mStoreListAdapterArabic.notifyDataSetChanged();
            }
        }
//        Collections.sort(favouriteStoresList, StoreInfo.storeDistance);
//        mFavouriteAdapter.notifyDataSetChanged();
    }

    public void refresh(){
        favoriteStores.clear();
        if(getActivity() != null) {
            favouritePrefs = getActivity().getSharedPreferences("FAV_PREFS", Context.MODE_PRIVATE);
            favEditor = favouritePrefs.edit();

            favStoreIds = favouritePrefs.getString("favourite_store_ids", null);
//            Log.i("TAG prefs", ""+favStoreIds);
            if (favStoreIds != null) {
//                Log.i("TAG prefs", favStoreIds);
                if(!favStoreIds.equalsIgnoreCase("null")) {
                    if (previousStoreIds != null) {
                        if (previousStoreIds.equalsIgnoreCase(favStoreIds)) {

                        } else {
                            previousStoreIds = favStoreIds;
                            String[] parts = favStoreIds.split(",");
                            for (int i = 0; i < parts.length; i++) {
                                if (!favoriteStores.contains(parts[i])) {
                                    favoriteStores.add(parts[i]);
                                }

                            }
                            setFavStores();
                        }
                    } else {
                        previousStoreIds = favStoreIds;
                        String[] parts = favStoreIds.split(",");
                        for (int i = 0; i < parts.length; i++) {
                            if (!favoriteStores.contains(parts[i])) {
                                favoriteStores.add(parts[i]);
                            }

                        }
                        setFavStores();
                    }
                }else{
                    setFavStores();
                }
            } else {
                setFavStores();
            }
        }
    }



    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy");

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if(isFirstTime) {
                dialog = ProgressDialog.show(getActivity(), "",
                        "Loading. Please Wait....");
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_TIME);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_TIME, envelope);
                    timeResult = (SoapPrimitive) envelope.getResponse();
                    timeResponse = timeResult.toString();
                    Log.v("TAG", timeResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + timeResponse);
            }else{
                timeResponse = "no internet";
            }
            return timeResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(timeResponse == null){
                if(dialog!=null) {
                    dialog.dismiss();
                }
            } else if(timeResponse.equals("no internet")){
                if(dialog!=null) {
                    dialog.dismiss();
                }
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                if(dialog!=null) {
                    dialog.dismiss();
                }
                new GetStoreInfo().execute();
                if(language.equalsIgnoreCase("En")) {
                    mStoreListAdapter = new StoreListAdapter(getActivity(), totalStoresList, timeResponse);
                    mStoresListView.setAdapter(mStoreListAdapter);
                }else if(language.equalsIgnoreCase("Ar")){
                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
                    mStoresListView.setAdapter(mStoreListAdapterArabic);
                }
            }
            super.onPostExecute(result1);
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if(!isGPSRefreshed) {
                Bundle b = intent.getBundleExtra("Location");
                Location location = (Location) b.getParcelable("Location");
                if (location != null) {
                    lat = location.getLatitude();
                    longi = location.getLongitude();

                    new GetStoreInfo().execute();

                    gps.stopUsingGPS();

                    isGPSRefreshed = true;
                }
            }
        }
    };
}