package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.GPSTracker;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.model.NutritionDetails;
import com.cs.drcafe.widget.FetchAddressIntentService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class AddAddressActivity extends AppCompatActivity implements OnMapReadyCallback {

    GPSTracker gps;
    Context context;
    Double lat, longi;
    private GoogleMap mMap;
    ImageView mSearchAddress;
    AlertDialog customDialog;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    private LatLng mCenterLatLong;
    private TextView mTitle;
    private ImageButton mSkipButton;
//    private RelativeLayout titleLayout;
    private ImageView backButton1;
    protected String mAddressOutput;
    private Button mConfirmLocation;
    private LinearLayout mExpandLayout;
    private static String TAG = "TAG";
    private boolean isExpanded = false;
    private GoogleApiClient mGoogleApiClient;
    EditText mLocationAddress;
    private AddressResultReceiver mResultReceiver;
    private ImageView homeImage, officeImage, otherImage;
    private RelativeLayout homeLayout, officeLayout, otherLayout;
    private String strAddress,  strHouseName;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    SupportMapFragment mapFragment;
    String language, userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    Boolean isEdit;

    private String SOAP_ACTION = "http://tempuri.org/ManageUserAddress";
    private String METHOD_NAME = "ManageUserAddress";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_add_address);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_add_address_ar);
        }
        context = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userId = userPrefs.getString("userId","0");

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mTitle = (TextView) findViewById(R.id.body_add_address);

        mLocationAddress = (EditText) findViewById(R.id.input_location);

        mConfirmLocation = (Button) findViewById(R.id.add_address);
        backButton1 = (ImageView) findViewById(R.id.back_btn);

        isEdit = getIntent().getBooleanExtra("edit", false);
        if(isEdit){
            mTitle.setText(getIntent().getStringExtra("Address"));
            mLocationAddress.setText(getIntent().getStringExtra("HouseNo"));
        }

        mapFragment.getMapAsync(this);
        mResultReceiver = new AddressResultReceiver(new Handler());

        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            if (!DrConstants.isLocationEnabled(context)) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddAddressActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                yes.setText("Open location settings");
                no.setText("Cancel");
                desc.setText("Location not enabled!");

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                        customDialog.dismiss();
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }

        } else {
            Toast.makeText(context, "Location not supported in this device", Toast.LENGTH_SHORT).show();
        }

    }


    public void addAddressClickEvents(View v) {
        switch (v.getId()){
            case R.id.back_btn:
                setResult(RESULT_CANCELED);
                finish();
                break;

            case R.id.add_address:
                if(mTitle.getText().toString().length()>0) {
                    if(validation()){
                        new AddAddress().execute();
                    }
                }
                else {
                    Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    private boolean validation(){
        strHouseName = mLocationAddress.getText().toString().trim();
        strAddress = mTitle.getText().toString();
        if(strHouseName.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                mLocationAddress.setError("Please enter Flat No. / House No. / Apt Name");
            }
            else {
                mLocationAddress.setError("من فضلك ادخل رقم الشقة / رقم المنزل / الاسم");
            }
            return false;
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15f));

        if (!isEdit) {
            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                if (!canAccessLocation()) {
                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                } else {
                    getGPSCoordinates();
                }
            } else {
                getGPSCoordinates();
            }
        }
        else {
            lat = getIntent().getDoubleExtra("lat", 0.0);
            longi = getIntent().getDoubleExtra("lng", 0.0);
            LatLng latLng = new LatLng(lat, longi);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15f));
        }

        View mapView = mapFragment.getView();
        View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent())
                .findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                mCenterLatLong = cameraPosition.target;
                try {
                    Location mLocation = new Location("");
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);

                    lat = mCenterLatLong.latitude;
                    longi = mCenterLatLong.longitude;

                    startIntentService(mLocation);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    public void getGPSCoordinates(){
        gps = new GPSTracker(AddAddressActivity.this);
        try {
            mMap.setMyLocationEnabled(true);
        }catch (Exception npe){
            npe.printStackTrace();
        }
        if(gps != null){
            if (gps.canGetLocation()) {
                if(!isEdit) {
                    lat = gps.getLatitude();
                    longi = gps.getLongitude();
                }
                else{
                    lat = getIntent().getDoubleExtra("Latitude",0);
                    longi = getIntent().getDoubleExtra("Longitude", 0);
                }
                LatLng latLng = new LatLng(lat, longi);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15f));
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(AddAddressActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(AddAddressActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //finish();
            }
            return false;
        }
        return true;
    }

    private void changeMap(Location location) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        // check if map is created successfully or not
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(false);
            LatLng latLong;

            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLong).zoom(15f).build();

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            lat = location.getLatitude();
            longi = location.getLongitude();

//            mLocationMarkerText.setText("Lat : " + location.getLatitude() + "," + "Long : " + location.getLongitude());
            startIntentService(location);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }
    }


    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(DrConstants.LocationConstants.RESULT_DATA_KEY);
            mAreaOutput = resultData.getString(DrConstants.LocationConstants.LOCATION_DATA_AREA);
            mCityOutput = resultData.getString(DrConstants.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(DrConstants.LocationConstants.LOCATION_DATA_STREET);

            Log.d(TAG, "onReceiveResult: "+mStateOutput);
            displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == DrConstants.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));

            }
        }
    }

    /**
     * Updates the address in the UI.
     */
    protected void displayAddressOutput() {
        try {
            if (mStateOutput != null)
            mTitle.setText(mStateOutput);
//            mLocationAddress.setSelection(mLocationAddress.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(DrConstants.LocationConstants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(DrConstants.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }

    private void openAutocompleteActivity() {
        Log.d(TAG, "openAutocompleteActivity: ");
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            e.printStackTrace();
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(context, data);

                // TODO call location based filter
                LatLng latLong;
                latLong = place.getLatLng();
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLong).zoom(15f).build();

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(context, data);
        } else if (resultCode == RESULT_CANCELED) {

        }
    }

    public class AddAddress extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus;
        ProgressDialog dialog;
        private SoapObject result;
        private String response12 = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(AddAddressActivity.this);
            dialog = ProgressDialog.show(AddAddressActivity.this, "",
                    "Loading. Please Wait....");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    if (!isEdit) {
                        request.addProperty("Flag", 1);
                        request.addProperty("Id", 0);
                    }
                    else {
                        request.addProperty("Flag", 2);
                        request.addProperty("Id", getIntent().getStringExtra("id"));
                    }
                    request.addProperty("UserId", userId);
                    request.addProperty("HouseNo", strHouseName);
                    request.addProperty("LandMark", " ");
                    request.addProperty("Lat", lat.toString());
                    request.addProperty("Lng", longi.toString());
                    request.addProperty("Address", strAddress);
                    request.addProperty("AddressType", "");

                    Log.d(TAG, "doInBackground: "+request);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    dialog.dismiss();
                    Toast.makeText(AddAddressActivity.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    if(response12 != null) {
                        if (response12.equals("anyType{}")) {
                            dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                        } else {

                            try {

                                Object property = result.getProperty(1);
                                if (property instanceof SoapObject) {
                                    try {
                                        SoapObject category_list = (SoapObject) property;
                                        SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                        for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                            NutritionDetails nutritionDetails = new NutritionDetails();
                                            SoapObject sObj = (SoapObject) storeObject.getProperty(i);

                                            finish();

                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                Log.d("", "Error while parsing the results!");
                                e.printStackTrace();
                            }

                            dialog.dismiss();
                            super.onPostExecute(result1);
                        }
                    }
                }
            }else{
                dialog.dismiss();
            }
        }
    }
}
