package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.AdditionalsListAdapter;
import com.cs.drcafe.adapter.AdditionalsListAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.Modifiers;
import com.cs.drcafe.model.Nutrition;
import com.cs.drcafe.model.SelectedAddtionals;
import com.cs.drcafe.widget.CustomExpandableListView;

import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class OrderPriceFragment extends Fragment implements View.OnClickListener {

    LinearLayout shortLayout, tallLayout, grandeLayout, jumboLayout, boxLayout, addItemLayout, dummyLayout;
    TextView shortPrice, tallPrice, grandePrice, jumboPrice, boxPrice, itemTitle;
    float shortFloat, tallFloat, grandeFloat, jumboFloat, boxFloat;
    TextView shortPriceTxt, tallPriceTxt, grandePriceTxt, jumboPriceTxt, itemQuantity;
    public static TextView itemPrice;
    ImageView itemIcon, downShort, downTall, downGrande, downJumbo, plusBtn, minusBtn;
    RelativeLayout commentLayout;
    String title, imageId, itemId;
    private CustomExpandableListView mExpListView;
    AdditionalsListAdapter mAdapter;
    AdditionalsListAdapterArabic mAdapterArabic;
    ArrayList<Modifiers> catList = new ArrayList<>();
    ArrayList<Nutrition> nutritionList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    RelativeLayout checkoutLayout;
    LinearLayout checkoutItemsLayout, additionalsLayout;
    ImageView checkoutImage;
    public static int quantity;
    public static float price, finalPrice;
    String modifiersId;
    private String itemTypeId;
    private int lastExpandedPosition = 0;
    Button backBtn;
    View rootView;
    ScrollView scrollView;
    String language;
    boolean singleSize = false, doubleSize = false, tripleSize = false;
    LinearLayout textLayout, sizeLayout, cappuccinoLayout;
    RelativeLayout standardLayout, wetLayout, dryLayout;
    RadioButton radio1, radio2;
    Boolean isRadio1Selected = false , isRadio2Selected = false, isCappuccinoSelected = false, isLatteSelected = false;
    String cappuccinoAdditional = "50", cappuccinoAdditionalName = "Standard", cappuccinoAdditionalNameAr = "أساسي" ;
    String latteAdditionalId = "", latteAdditionalName = "", latteAdditionalNameAr = "", latteid = "5" ;
    List<SelectedAddtionals> thingsToBeAdd = new ArrayList<>();
    public static int priceRatio = 1;
    float addlPrice = 0;
    public static boolean isFirstTime = false;
    private boolean isShortSizeAvailable = false;
    private LinearLayout latteArtLayout;
    private RelativeLayout heartLayout, swanLayout, rosettaLayout, tulipLayout;
    private ImageView heartCheckBox, swanCheckBox, rosettaCheckBox, tulipCheckBox;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_price, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.order_price_arabic, container,
                    false);
        }

        myDbHelper = new DataBaseHelper(getActivity());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        cappuccinoLayout = (LinearLayout) rootView.findViewById(R.id.cappuccino_layout);

        String additionals = getArguments().getString("additionals_type");
        String[] parts = additionals.split(",");
        for(int i = 0; i< parts.length; i++){
            if(parts[i].equals("12")){
                cappuccinoLayout.setVisibility(View.VISIBLE);
                isCappuccinoSelected = true;
            }
            else {
                Cursor cursor = myDbHelper.getModifiers(parts[i]);
                if (cursor.moveToFirst()) {
                    do {
                        Modifiers c = new Modifiers();
                        c.setModifierId(cursor.getString(0));
                        c.setModifierName(cursor.getString(1));
                        c.setModifierNameAr(cursor.getString(6));
                        c.setImages(cursor.getString(5));

                        c.setChildItems(myDbHelper.getAdditionalsList(cursor.getString(0)));

                        catList.add(c);
                    } while (cursor.moveToNext());
                }
            }
        }

        price = 0;
        finalPrice = 0;
        quantity = 0;

        title = getArguments().getString("title");
        itemId = getArguments().getString("item_id");
        imageId = getArguments().getString("image_id");
        modifiersId = getArguments().getString("modifiersId");
        DrConstants.comment = "";
        nutritionList = (ArrayList<Nutrition>) getArguments().getSerializable("nutrition");
        getArguments().remove("nutrition");

        rosettaLayout = (RelativeLayout) rootView.findViewById(R.id.rosetta_layout);
        tulipLayout = (RelativeLayout) rootView.findViewById(R.id.tulip_layout);
        heartLayout = (RelativeLayout) rootView.findViewById(R.id.heart_layout);
        swanLayout = (RelativeLayout) rootView.findViewById(R.id.swan_layout);

        heartCheckBox = (ImageView) rootView.findViewById(R.id.heart_radio);
        swanCheckBox = (ImageView) rootView.findViewById(R.id.swan_radio);
        rosettaCheckBox = (ImageView) rootView.findViewById(R.id.rosetta_radio);
        tulipCheckBox = (ImageView) rootView.findViewById(R.id.tulip_radio);


        shortLayout = (LinearLayout) rootView.findViewById(R.id.price_short_layout);
        tallLayout = (LinearLayout) rootView.findViewById(R.id.price_tall_layout);
        grandeLayout = (LinearLayout) rootView.findViewById(R.id.price_grande_layout);
        jumboLayout = (LinearLayout) rootView.findViewById(R.id.price_jumbo_layout);
        boxLayout = (LinearLayout) rootView.findViewById(R.id.price_box_layout);
        commentLayout = (RelativeLayout) rootView.findViewById(R.id.price_comment_layout);
        additionalsLayout = (LinearLayout) rootView.findViewById(R.id.additionals_layout);
        addItemLayout = (LinearLayout) rootView.findViewById(R.id.additem_layout);
        dummyLayout = (LinearLayout) rootView.findViewById(R.id.price_dummy_layout);
        textLayout = (LinearLayout) rootView.findViewById(R.id.text_layout);
        sizeLayout = (LinearLayout) rootView.findViewById(R.id.size_layout);
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);
        latteArtLayout = (LinearLayout) rootView.findViewById(R.id.latte_art_layout);

        radio1 = (RadioButton) rootView.findViewById(R.id.radio1);
        radio2 = (RadioButton) rootView.findViewById(R.id.radio2);

        shortPrice = (TextView) rootView.findViewById(R.id.price_short);
        tallPrice = (TextView) rootView.findViewById(R.id.price_tall);
        grandePrice = (TextView) rootView.findViewById(R.id.price_grande);
        jumboPrice = (TextView) rootView.findViewById(R.id.price_jumbo);
        boxPrice = (TextView) rootView.findViewById(R.id.price_box);
        shortPriceTxt = (TextView) rootView.findViewById(R.id.price_short_txt);
        tallPriceTxt = (TextView) rootView.findViewById(R.id.price_tall_txt);
        grandePriceTxt = (TextView) rootView.findViewById(R.id.price_grande_txt);
        itemTitle = (TextView) rootView.findViewById(R.id.item_name);
        itemIcon = (ImageView) rootView.findViewById(R.id.item_icon);
        downShort = (ImageView) rootView.findViewById(R.id.additionals_down_short);
        downTall = (ImageView) rootView.findViewById(R.id.additionals_down_tall);
        downGrande = (ImageView) rootView.findViewById(R.id.additionals_down_grande);
        downJumbo = (ImageView) rootView.findViewById(R.id.additionals_down_jumbo);
        plusBtn = (ImageView) rootView.findViewById(R.id.plus_btn);
        minusBtn = (ImageView) rootView.findViewById(R.id.minus_btn);
        itemPrice = (TextView) rootView.findViewById(R.id.item_price);
        itemQuantity = (TextView) rootView.findViewById(R.id.item_quantity);

        mExpListView = (CustomExpandableListView) rootView.findViewById(R.id.expandableListView);
        checkoutImage = (ImageView) rootView.findViewById(R.id.checkout_image);
        checkoutLayout = (RelativeLayout) rootView.findViewById(R.id.checkout_layout);
        checkoutItemsLayout = (LinearLayout) rootView.findViewById(R.id.linearLayout);

        standardLayout = (RelativeLayout) rootView.findViewById(R.id.standard_layout);
        wetLayout = (RelativeLayout) rootView.findViewById(R.id.wet_layout);
        dryLayout = (RelativeLayout) rootView.findViewById(R.id.dry_layout);

        checkoutLayout.setVisibility(View.GONE);
//        mExpListView.setIndicatorBounds(width-GetDipsFromPixel(35), width-GetDipsFromPixel(5));

        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

//        ViewTreeObserver vto = checkoutImage.getViewTreeObserver();
//        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            public boolean onPreDraw() {
//                checkoutImage.getViewTreeObserver().removeOnPreDrawListener(this);
//                int finalHeight = checkoutImage.getMeasuredHeight();
//                Log.i("Height TAG", "" + finalHeight);
//                LinearLayout.LayoutParams newsCountParam = new
//                        LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
//                RelativeLayout.LayoutParams params = new
//                        RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
//                checkoutLayout.setLayoutParams(newsCountParam);
//                checkoutItemsLayout.setLayoutParams(params);
//                return true;
//            }
//        });


        if(language.equalsIgnoreCase("En")) {
            itemTitle.setText(WordUtils.capitalizeFully(title).replace("Ml", "ML").replace("ml", "ML"));
            mAdapter = new AdditionalsListAdapter(getActivity(), catList, modifiersId,mExpListView);
            AdditionalsListAdapter.additionalsList.clear();
            AdditionalsListAdapter.additionalsIdList.clear();
            mExpListView.setAdapter(mAdapter);
        }else if(language.equalsIgnoreCase("Ar")){
            itemTitle.setText(title);
            mAdapterArabic = new AdditionalsListAdapterArabic(getActivity(), catList, modifiersId);
            AdditionalsListAdapterArabic.additionalsList.clear();
            AdditionalsListAdapterArabic.additionalsIdList.clear();
            mExpListView.setAdapter(mAdapterArabic);
        }

        mExpListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    mExpListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (modifiersId.equals("1") || modifiersId.equals("9") || modifiersId.equals("10") || modifiersId.equals("12")) {
                    if(language.equalsIgnoreCase("En")){
                        if(!AdditionalsListAdapter.isMilkSelected){
                            if(modifiersId.equals("1")){
                                milkDialog("Please select milk type");
                            }if(modifiersId.equals("9")){
                                milkDialog("Please select Coffee type");
                            }if(modifiersId.equals("10")){
                                milkDialog("Please select Flavored Tea");
                            }

                            return true;
                        }else {
                            return false;
                        }
                    }else if(language.equalsIgnoreCase("Ar")){
                        if(!AdditionalsListAdapterArabic.isMilkSelected){
                            if(modifiersId.equals("1")){
                                milkDialog("من فضلك حدد نوع الحليب");
                            }if(modifiersId.equals("9")){
                                milkDialog("من فضلك اختر نوع القهوة ");
                            }if(modifiersId.equals("10")){
                                milkDialog("من فضلك اختر نكهة الشاي ");
                            }
                            return true;
                        } else {
                            return false;
                        }
                    }
                    return false;
                }
                else {
                    return false;
                }
            }
        });

        if(modifiersId.equals("1") || modifiersId.equals("9") || modifiersId.equals("10") || modifiersId.equals("12")){
            mExpListView.expandGroup(0, true);
        }

        try {
            // get input stream
            InputStream ims = getActivity().getAssets().open(imageId+".png");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            itemIcon.setImageDrawable(d);
        }
        catch(IOException ex) {
        }

        for(Nutrition n: nutritionList){
            if(n.getItemTypeId().equals("1")){
                if(!n.getPrice().equals("-1")) {
                    isShortSizeAvailable = true;
//                    shortPrice.setText(n.getPrice() + " SR");
                    shortPrice.setText(DrConstants.format.format(Float.parseFloat(n.getPrice())));
                    shortFloat = Float.parseFloat(n.getPrice());
                    if(nutritionList.size()==1) {
                        tallLayout.setVisibility(View.INVISIBLE);
                        dummyLayout.setVisibility(View.INVISIBLE);
                        jumboLayout.setVisibility(View.INVISIBLE);
                        grandeLayout.setVisibility(View.INVISIBLE);
                    }
                }else {
                    shortLayout.setVisibility(View.GONE);
                    dummyLayout.setVisibility(View.INVISIBLE);
                }
            }else if(n.getItemTypeId().equals("3")){
                if(!n.getPrice().equals("-1")) {
                    tallPrice.setText(DrConstants.format.format(Float.parseFloat(n.getPrice())));
                    tallFloat = Float.parseFloat(n.getPrice());
//                    tallPrice.setText(n.getPrice() + " SR");
                    if(nutritionList.size()==1) {
                    shortLayout.setVisibility(View.GONE);
                    dummyLayout.setVisibility(View.INVISIBLE);
                    grandeLayout.setVisibility(View.INVISIBLE);
                    jumboLayout.setVisibility(View.INVISIBLE);
                    }
                }else {
                    tallLayout.setVisibility(View.INVISIBLE);
                }
            }else if(n.getItemTypeId().equals("2")){
                if(!n.getPrice().equals("-1")) {
//                    grandePrice.setText(n.getPrice() + " SR");
                    grandePrice.setText(DrConstants.format.format(Float.parseFloat(n.getPrice())));
                    grandeFloat = Float.parseFloat(n.getPrice());
                }else {
                    grandeLayout.setVisibility(View.INVISIBLE);
                }
            }else if(n.getItemTypeId().equals("6")){
                if(!n.getPrice().equals("-1")) {
//                    jumboPrice.setText(n.getPrice() + " SR");
                    jumboPrice.setText(DrConstants.format.format(Float.parseFloat(n.getPrice())));
                    jumboFloat = Float.parseFloat(n.getPrice());
                }else {
                    jumboLayout.setVisibility(View.INVISIBLE);
                }
            }
            else if(n.getItemTypeId().equals("9")){
                if(!n.getPrice().equals("-1")) {
                    boxLayout.setVisibility(View.VISIBLE);
//                    boxPrice.setText(n.getPrice() + " SR");
                    boxPrice.setText(DrConstants.format.format(Float.parseFloat(n.getPrice())));
                    boxFloat = Float.parseFloat(n.getPrice());
                }else {
                    boxLayout.setVisibility(View.INVISIBLE);
                }
            }
            else{
                Log.i("TAG","item id "+n.getItemTypeId());
                if(n.getItemTypeId().equals("7")){
                    singleSize = true;
                    isShortSizeAvailable = true;
//                    jumboLayout.setVisibility(View.INVISIBLE);
//                    grandeLayout.setVisibility(View.INVISIBLE);
//                    shortPrice.setText(n.getPrice() + " SR");
                    shortPrice.setText(DrConstants.format.format(Float.parseFloat(n.getPrice())));
                    shortFloat = Float.parseFloat(n.getPrice());
                    if(language.equalsIgnoreCase("En")) {
                        shortPriceTxt.setText("Single");
                    }else if(language.equalsIgnoreCase("Ar")){
                        shortPriceTxt.setText("مفرد");
                    }

                }else if(n.getItemTypeId().equals("8")){
                    doubleSize = true;
//                    tallPrice.setText(n.getPrice() + " SR");
                    tallPrice.setText(DrConstants.format.format(Float.parseFloat(n.getPrice())));
                    tallFloat = Float.parseFloat(n.getPrice());
                    if(language.equalsIgnoreCase("En")) {
                        tallPriceTxt.setText("Double");
                    }else if(language.equalsIgnoreCase("Ar")){
                        tallPriceTxt.setText("مضاعف");
                    }

                }else if(n.getItemTypeId().equals("10")){
                    tripleSize = true;
                    grandeLayout.setVisibility(View.VISIBLE);
//                    tallPrice.setText(n.getPrice() + " SR");
                    grandePrice.setText(DrConstants.format.format(Float.parseFloat(n.getPrice())));
                    grandeFloat = Float.parseFloat(n.getPrice());
                    if(language.equalsIgnoreCase("En")) {
                        grandePriceTxt.setText("Triple");
                    }else if(language.equalsIgnoreCase("Ar")){
                        grandePriceTxt.setText("ثلاثي");
                    }
                }
            }
        }

        Log.d("TAG", "onCreateView: "+isShortSizeAvailable);
        if(!isShortSizeAvailable){
            shortLayout.setVisibility(View.GONE);
            dummyLayout.setVisibility(View.INVISIBLE);
        }

        shortLayout.setOnClickListener(this);
        tallLayout.setOnClickListener(this);
        grandeLayout.setOnClickListener(this);
        jumboLayout.setOnClickListener(this);
        boxLayout.setOnClickListener(this);
        commentLayout.setOnClickListener(this);
        plusBtn.setOnClickListener(this);
        minusBtn.setOnClickListener(this);
        addItemLayout.setOnClickListener(this);
        standardLayout.setOnClickListener(this);
        wetLayout.setOnClickListener(this);
        dryLayout.setOnClickListener(this);
        heartLayout.setOnClickListener(this);
        rosettaLayout.setOnClickListener(this);
        swanLayout.setOnClickListener(this);
        tulipLayout.setOnClickListener(this);

        radio1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    isRadio1Selected = true;
                    isRadio2Selected = false;
                    radio2.setChecked(false);
                }
            }
        });

        radio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    isRadio1Selected = false;
                    isRadio2Selected = true;
                    radio1.setChecked(false);
                }
            }
        });

        return rootView;
    }

    public void milkDialog(String msg){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set title
        alertDialogBuilder.setTitle("dr.CAFE");

        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.price_short_layout:
                if(singleSize){
                    itemTypeId = "7";
                }else{
                    itemTypeId = "1";
                }

                quantity= 1;
                priceRatio = 1;
                addlPrice = 0;
                if(language.equalsIgnoreCase("En")) {
                    for (int i = 0; i < AdditionalsListAdapter.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapter.additionalsList.get(i).getPrice()));
                    }
                }
                else{
                    for (int i = 0; i < AdditionalsListAdapterArabic.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapterArabic.additionalsList.get(i).getPrice()));
                    }
                }

                try {
                    price = Float.parseFloat(shortPrice.getText().toString());
                } catch (Exception e) {
                    price = shortFloat;
                    e.printStackTrace();
                }
                price = price + addlPrice;
                finalPrice = price;
                itemPrice.setText(DrConstants.format.format(finalPrice) + " SR");
                itemQuantity.setText("" + quantity);
                additionalsLayout.setVisibility(View.VISIBLE);
                checkoutLayout.setVisibility(View.VISIBLE);
                scrollView.smoothScrollTo(0,0);
                textLayout.setVisibility(View.GONE);
                sizeLayout.setVisibility(View.GONE);
                shortLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price_selected));
                tallLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                grandeLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                jumboLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                boxLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                boxPrice.setTextColor(Color.parseColor("#000000"));
                shortPrice.setTextColor(Color.parseColor("#FFFFFF"));
                tallPrice.setTextColor(Color.parseColor("#000000"));
                grandePrice.setTextColor(Color.parseColor("#000000"));
                jumboPrice.setTextColor(Color.parseColor("#000000"));
                if(language.equalsIgnoreCase("En")) {
//                    AdditionalsListAdapter.additionalsList.clear();
//                    AdditionalsListAdapter.additionalsIdList.clear();
                    mAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
//                    AdditionalsListAdapterArabic.additionalsList.clear();
//                    AdditionalsListAdapterArabic.additionalsIdList.clear();
                    mAdapterArabic.notifyDataSetChanged();
                }

                if(itemId.equals("17") || itemId.equals("385")){
                    latteArtLayout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.price_tall_layout:
                if(doubleSize){
                    itemTypeId = "8";
                }else{
                    itemTypeId = "3";
                }
                quantity= 1;
                priceRatio = 1;
                addlPrice = 0;
                if(language.equalsIgnoreCase("En")) {
                    for (int i = 0; i < AdditionalsListAdapter.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapter.additionalsList.get(i).getPrice()));
                    }
                }
                else{
                    for (int i = 0; i < AdditionalsListAdapterArabic.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapterArabic.additionalsList.get(i).getPrice()));
                    }
                }

                try {
                    price = Float.parseFloat(tallPrice.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    price = tallFloat;
//                    String val = DrConstants.convertToArabic(tallPrice.getText().toString());
//                    Log.i("TAG","val "+val);
//                    price = Float.parseFloat(val);
                }
                price = price + addlPrice;
                finalPrice = price;
                itemPrice.setText(DrConstants.format.format(finalPrice) + " SR");
                itemQuantity.setText("" + quantity);
                additionalsLayout.setVisibility(View.VISIBLE);
                checkoutLayout.setVisibility(View.VISIBLE);
                scrollView.smoothScrollTo(0,0);
                textLayout.setVisibility(View.GONE);
                sizeLayout.setVisibility(View.GONE);
                shortLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                tallLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price_selected));
                grandeLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                jumboLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                boxLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                boxPrice.setTextColor(Color.parseColor("#000000"));
                shortPrice.setTextColor(Color.parseColor("#000000"));
                tallPrice.setTextColor(Color.parseColor("#FFFFFF"));
                grandePrice.setTextColor(Color.parseColor("#000000"));
                jumboPrice.setTextColor(Color.parseColor("#000000"));
                if(language.equalsIgnoreCase("En")) {
//                    AdditionalsListAdapter.additionalsList.clear();
//                    AdditionalsListAdapter.additionalsIdList.clear();
                    mAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
//                    AdditionalsListAdapterArabic.additionalsList.clear();
//                    AdditionalsListAdapterArabic.additionalsIdList.clear();
                    mAdapterArabic.notifyDataSetChanged();
                }

                if(itemId.equals("17") || itemId.equals("385")){
                    latteArtLayout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.price_grande_layout:
                if(tripleSize){
                    itemTypeId = "10";
                }else{
                    itemTypeId = "2";
                }
//                itemTypeId = "2";
                quantity= 1;
                priceRatio = 1;
                addlPrice = 0;
                if(language.equalsIgnoreCase("En")) {
                    for (int i = 0; i < AdditionalsListAdapter.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapter.additionalsList.get(i).getPrice()));
                    }
                }
                else{
                    for (int i = 0; i < AdditionalsListAdapterArabic.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapterArabic.additionalsList.get(i).getPrice()));
                    }
                }

                try {
                    price = Float.parseFloat(grandePrice.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    price = grandeFloat;
                }
                price = price + addlPrice;
                finalPrice = price;
                itemPrice.setText(DrConstants.format.format(finalPrice) + " SR");
                itemQuantity.setText("" + quantity);
                additionalsLayout.setVisibility(View.VISIBLE);
                checkoutLayout.setVisibility(View.VISIBLE);
                scrollView.smoothScrollTo(0,0);
                textLayout.setVisibility(View.GONE);
                sizeLayout.setVisibility(View.GONE);
                shortLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                tallLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                grandeLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price_selected));
                jumboLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                boxLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                boxPrice.setTextColor(Color.parseColor("#000000"));
                shortPrice.setTextColor(Color.parseColor("#000000"));
                tallPrice.setTextColor(Color.parseColor("#000000"));
                grandePrice.setTextColor(Color.parseColor("#FFFFFF"));
                jumboPrice.setTextColor(Color.parseColor("#000000"));
                if(language.equalsIgnoreCase("En")) {
//                    AdditionalsListAdapter.additionalsList.clear();
//                    AdditionalsListAdapter.additionalsIdList.clear();
                    mAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
//                    AdditionalsListAdapterArabic.additionalsList.clear();
//                    AdditionalsListAdapterArabic.additionalsIdList.clear();
                    mAdapterArabic.notifyDataSetChanged();
                }

                if(itemId.equals("17") || itemId.equals("385")){
                    latteArtLayout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.price_jumbo_layout:
                itemTypeId = "6";
                quantity= 1;
                priceRatio = 1;
                addlPrice = 0;
                if(language.equalsIgnoreCase("En")) {
                    for (int i = 0; i < AdditionalsListAdapter.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapter.additionalsList.get(i).getPrice()));
                    }
                }
                else{
                    for (int i = 0; i < AdditionalsListAdapterArabic.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapterArabic.additionalsList.get(i).getPrice()));
                    }
                }

                try {
                    price = Float.parseFloat(jumboPrice.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    price = jumboFloat;
                }
                price = price + addlPrice;
                finalPrice = price;
                itemPrice.setText(DrConstants.format.format(finalPrice) + " SR");
                itemQuantity.setText("" + quantity);
                additionalsLayout.setVisibility(View.VISIBLE);
                checkoutLayout.setVisibility(View.VISIBLE);
                scrollView.smoothScrollTo(0,0);
                textLayout.setVisibility(View.GONE);
                sizeLayout.setVisibility(View.GONE);
                shortLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                tallLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                grandeLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                jumboLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price_selected));
                boxLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                boxPrice.setTextColor(Color.parseColor("#000000"));
                shortPrice.setTextColor(Color.parseColor("#000000"));
                tallPrice.setTextColor(Color.parseColor("#000000"));
                grandePrice.setTextColor(Color.parseColor("#000000"));
                jumboPrice.setTextColor(Color.parseColor("#FFFFFF"));
                if(language.equalsIgnoreCase("En")) {
//                    AdditionalsListAdapter.additionalsList.clear();
//                    AdditionalsListAdapter.additionalsIdList.clear();
                    mAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
//                    AdditionalsListAdapterArabic.additionalsList.clear();
//                    AdditionalsListAdapterArabic.additionalsIdList.clear();
                    mAdapterArabic.notifyDataSetChanged();
                }

                if(itemId.equals("17") || itemId.equals("385")){
                    latteArtLayout.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.price_box_layout:
                itemTypeId = "9";
                quantity= 1;
                priceRatio = 12;
                addlPrice = 0;
                if(language.equalsIgnoreCase("En")) {
                    for (int i = 0; i < AdditionalsListAdapter.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapter.additionalsList.get(i).getPrice())*priceRatio);
                    }
                }
                else{
                    for (int i = 0; i < AdditionalsListAdapterArabic.additionalsList.size(); i++) {
                        addlPrice = addlPrice + (Float.parseFloat(AdditionalsListAdapterArabic.additionalsList.get(i).getPrice())*priceRatio);
                    }
                }

                try {
                    price = Float.parseFloat(boxPrice.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    price = boxFloat;
                }
                price = price + addlPrice;
                finalPrice = price;
                itemPrice.setText(DrConstants.format.format(finalPrice) + " SR");
                itemQuantity.setText("" + quantity);
                additionalsLayout.setVisibility(View.VISIBLE);
                checkoutLayout.setVisibility(View.VISIBLE);
                scrollView.smoothScrollTo(0,0);
                textLayout.setVisibility(View.VISIBLE);
                sizeLayout.setVisibility(View.VISIBLE);
                shortLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                tallLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                grandeLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                boxLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price_selected));
                jumboLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_price));
                jumboPrice.setTextColor(Color.parseColor("#000000"));
                shortPrice.setTextColor(Color.parseColor("#000000"));
                tallPrice.setTextColor(Color.parseColor("#000000"));
                grandePrice.setTextColor(Color.parseColor("#000000"));
                boxPrice.setTextColor(Color.parseColor("#FFFFFF"));
                if(language.equalsIgnoreCase("En")) {
//                    AdditionalsListAdapter.additionalsList.clear();
//                    AdditionalsListAdapter.additionalsIdList.clear();
                    mAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
//                    AdditionalsListAdapterArabic.additionalsList.clear();
//                    AdditionalsListAdapterArabic.additionalsIdList.clear();
                    mAdapterArabic.notifyDataSetChanged();
                }

                if(itemId.equals("17") || itemId.equals("385")){
                    latteArtLayout.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.price_comment_layout:
                Fragment nutritionFragment = new CommentFragment();
                Bundle mBundle11 = new Bundle();
                mBundle11.putString("title", title);
                mBundle11.putString("itemId", imageId);
                mBundle11.putString("screen", "additional");
                nutritionFragment.setArguments(mBundle11);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                nutritionTransaction.hide(OrderPriceFragment.this);
                nutritionTransaction.addToBackStack(null);

                // Commit the transaction
                nutritionTransaction.commit();
                break;
            case R.id.plus_btn:
                if(quantity>0) {
                    quantity = quantity + 1;
                    finalPrice = Float.parseFloat(DrConstants.convertToArabic(itemPrice.getText().toString().replace(" SR", "")));
                    finalPrice = finalPrice + price;
                    itemPrice.setText(DrConstants.format.format(finalPrice) + " SR");
                    itemQuantity.setText("" + quantity);
                }else{
                    showDialog();
                }
                break;

            case R.id.standard_layout:
                standardLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_selected_shape));
                wetLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_shape));
                dryLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_shape));
                cappuccinoAdditional = "50";
                cappuccinoAdditionalName = "Standard";
                cappuccinoAdditionalNameAr = "أساسي";
                break;

            case R.id.wet_layout:
                wetLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_selected_shape));
                standardLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_shape));
                dryLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_shape));
                cappuccinoAdditional = "51";
                cappuccinoAdditionalName = "Wet";
                cappuccinoAdditionalNameAr = "رطب";
                break;

            case R.id.dry_layout:
                dryLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_selected_shape));
                wetLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_shape));
                standardLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.cappuccino_shape));
                cappuccinoAdditional = "50";
                cappuccinoAdditionalName = "Dry";
                cappuccinoAdditionalNameAr = "جاف";
                break;

            case R.id.heart_layout:
                if(!latteAdditionalId.equals("53")) {
                    isLatteSelected = true;
                    heartCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_selected));
                    swanCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    rosettaCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    tulipCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    latteAdditionalId = "53";
                    latteAdditionalName = "Heart";
                    latteAdditionalNameAr = "قلب";
                }
                else{
                    isLatteSelected = false;
                    heartCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    latteAdditionalId = "";
                    latteAdditionalName = "";
                    latteAdditionalNameAr = "";
                }
                break;

            case R.id.swan_layout:
                if(!latteAdditionalId.equals("54")) {
                    isLatteSelected = true;
                    heartCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    swanCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_selected));
                    rosettaCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    tulipCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    latteAdditionalId = "54";
                    latteAdditionalName = "Swan";
                    latteAdditionalNameAr = "بجعة";
                }
                else{
                    isLatteSelected = false;
                    swanCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    latteAdditionalId = "";
                    latteAdditionalName = "";
                    latteAdditionalNameAr = "";
                }
                break;

            case R.id.rosetta_layout:
                if(!latteAdditionalId.equals("55")) {
                    isLatteSelected = true;
                    heartCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    swanCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    rosettaCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_selected));
                    tulipCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    latteAdditionalId = "55";
                    latteAdditionalName = "Roasetta";
                    latteAdditionalNameAr = "زهرة الروزيتا";
                }
                else{
                    isLatteSelected = false;
                    rosettaCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    latteAdditionalId = "";
                    latteAdditionalName = "";
                    latteAdditionalNameAr = "";
                }
                break;

            case R.id.tulip_layout:
                if(!latteAdditionalId.equals("56")) {
                    isLatteSelected = true;
                    heartCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    swanCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    rosettaCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    tulipCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_selected));
                    latteAdditionalId = "56";
                    latteAdditionalName = "Tulip";
                    latteAdditionalNameAr = "زهرة التيوليب";
                }
                else{
                    isLatteSelected = false;
                    tulipCheckBox.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.payment_normal));
                    latteAdditionalId = "";
                    latteAdditionalName = "";
                    latteAdditionalNameAr = "";
                }
                break;

            case R.id.minus_btn:
                if(quantity > 1) {
                    quantity = quantity - 1;
                    finalPrice = Float.parseFloat(DrConstants.convertToArabic(itemPrice.getText().toString().replace(" SR", "")));
                    finalPrice = finalPrice - price;
                    itemPrice.setText(DrConstants.format.format(finalPrice) + " SR");
                    itemQuantity.setText(""+quantity);
                }else if(quantity == 0) {
                    showDialog();
                }
                break;
            case  R.id.additem_layout:

                if(quantity > 0) {
                    if (modifiersId.equals("1") || modifiersId.equals("9") || modifiersId.equals("10") || modifiersId.equals("12")) {
                        if(language.equalsIgnoreCase("En")){
                            if(!AdditionalsListAdapter.isMilkSelected){
                                if(modifiersId.equals("1")){
                                    milkDialog("Please select milk type");
                                }if(modifiersId.equals("9")){
                                    milkDialog("Please select Coffee type");
                                }if(modifiersId.equals("10")){
                                    milkDialog("Please select Flavored Tea");
                                }
                            }else {
                                saveOrder();
                            }
                        }else if(language.equalsIgnoreCase("Ar")){
                            if(!AdditionalsListAdapterArabic.isMilkSelected){
                                if(modifiersId.equals("1")){
                                    milkDialog("من فضلك حدد نوع الحليب");
                                }if(modifiersId.equals("9")){
                                    milkDialog("من فضلك اختر نوع القهوة ");
                                }if(modifiersId.equals("10")){
                                    milkDialog("من فضلك اختر نكهة الشاي ");
                                }
                            } else {
                                saveOrder();
                            }
                        }
                    }
                    else if(modifiersId.equals("1,12")){
                        if(language.equalsIgnoreCase("En")){
                            if (!AdditionalsListAdapter.isCappuccinoSelected){
                                milkDialog("Please select type");
                            }
                            else if(!AdditionalsListAdapter.isMilkSelected){
                                milkDialog("Please select milk type");
                            }
                            else {
                                saveOrder();
                            }
                        }else if(language.equalsIgnoreCase("Ar")){
                            if (!AdditionalsListAdapterArabic.isCappuccinoSelected){
                                milkDialog("Please select type");
                            }
                            else if(!AdditionalsListAdapterArabic.isMilkSelected){
                                milkDialog("من فضلك حدد نوع الحليب");
                            }
                            else {
                                saveOrder();
                            }
                        }
                    }else {
                        saveOrder();
                    }
                }else{
                    showDialog();
                }

        }
    }


    public void saveOrder(){
        HashMap<String, String> values = new HashMap<>();

        if(isRadio1Selected) {
            SelectedAddtionals sa = new SelectedAddtionals();
            sa.setAdditionalId("48");
            sa.setModifierId("11");
            sa.setPrice("0");
            if(language.equalsIgnoreCase("En")) {
                sa.setAdditionalName("12 (Short/8oz) cups");
                AdditionalsListAdapter.additionalsList.add(sa);
            }
            else{
                sa.setAdditionalName("12 كوب (صغير/8 أونز)");
                AdditionalsListAdapterArabic.additionalsList.add(sa);
            }
        }
        else if(isRadio2Selected){
            SelectedAddtionals sa = new SelectedAddtionals();
            sa.setAdditionalId("49");
            sa.setModifierId("11");
            sa.setPrice("0");
            if(language.equalsIgnoreCase("En")) {
                sa.setAdditionalName("8 (Tall/12oz) cups");
                AdditionalsListAdapter.additionalsList.add(sa);
            }
            else{
                sa.setAdditionalName("8 كوب (عادي/12 أونز)");
                AdditionalsListAdapterArabic.additionalsList.add(sa);
            }
        }

        if(isLatteSelected){
            SelectedAddtionals sa = new SelectedAddtionals();
            sa.setAdditionalId(latteAdditionalId);
            sa.setModifierId("13");
            sa.setPrice("0");
            if(language.equalsIgnoreCase("En")) {
                sa.setAdditionalName(latteAdditionalName);
                AdditionalsListAdapter.additionalsList.add(sa);
            }
            else{
                sa.setAdditionalName(latteAdditionalNameAr);
                AdditionalsListAdapterArabic.additionalsList.add(sa);
            }
        }

        if(isCappuccinoSelected){
            SelectedAddtionals sa = new SelectedAddtionals();
            sa.setAdditionalId(cappuccinoAdditional);
            sa.setModifierId("12");
            sa.setPrice("0");
            if(language.equalsIgnoreCase("En")) {
                sa.setAdditionalName(cappuccinoAdditionalName);
                AdditionalsListAdapter.additionalsList.add(sa);
            }
            else{
                sa.setAdditionalName(cappuccinoAdditionalNameAr);
                AdditionalsListAdapterArabic.additionalsList.add(sa);
            }
//            Log.i("TAG",""+AdditionalsListAdapter.additionalsList.get(0).getAdditionalName());
        }

        String ids = "0", additionalsStr = "", additionalsPrice = "";
        if(language.equalsIgnoreCase("En")) {
            if (AdditionalsListAdapter.additionalsList.size() > 0) {

                for (SelectedAddtionals sa : AdditionalsListAdapter.additionalsList) {
                    if (ids.equals("0")) {
                        ids = sa.getAdditionalId();
                        additionalsStr = sa.getAdditionalName();
                        additionalsPrice = Float.toString(Float.parseFloat(sa.getPrice()) * priceRatio);
                    } else {
                        ids = ids + "," + sa.getAdditionalId();
                        additionalsStr = additionalsStr + "," + sa.getAdditionalName();
                        additionalsPrice = additionalsPrice + "," + Float.toString(Float.parseFloat(sa.getPrice()) * priceRatio);
                    }
                }
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if (AdditionalsListAdapterArabic.additionalsList.size() > 0) {

                for (SelectedAddtionals sa : AdditionalsListAdapterArabic.additionalsList) {
                    if (ids.equals("0")) {
                        ids = sa.getAdditionalId();
                        additionalsStr = sa.getAdditionalName();
                        additionalsPrice = Float.toString(Float.parseFloat(sa.getPrice()) * priceRatio);
                    } else {
                        ids = ids + "," + sa.getAdditionalId();
                        additionalsStr = additionalsStr + "," + sa.getAdditionalName();
                        additionalsPrice = additionalsPrice + "," + Float.toString(Float.parseFloat(sa.getPrice()) * priceRatio);
                    }
                }
            }
        }

        Log.i("TAG","additionals "+additionalsStr);

        DecimalFormat format = new DecimalFormat("0.00");
        values.put("itemId", nutritionList.get(0).getItemId());
        values.put("itemTypeId", itemTypeId);
        values.put("subCategoryId", nutritionList.get(0).getSubCatId());
        values.put("additionals", additionalsStr);
        values.put("qty", Integer.toString(quantity));
        values.put("price", Float.toString(price));
        values.put("additionalsPrice", (additionalsPrice));
        values.put("additionalsTypeId", ids);
        values.put("totalAmount", Float.toString(finalPrice));
        values.put("comment", DrConstants.comment);
        values.put("status", "1");
        values.put("creationDate", "14/07/2015");
        values.put("modifiedDate", "14/07/2015");
        values.put("categoryId", nutritionList.get(0).getCatId());
        myDbHelper.insertOrder(values);
//        if(language.equalsIgnoreCase("En")) {
//            AdditionalsListAdapter.additionalsList.clear();
//            AdditionalsListAdapter.additionalsIdList.clear();
//        }else if(language.equalsIgnoreCase("Ar")){
//            AdditionalsListAdapterArabic.additionalsList.clear();
//            AdditionalsListAdapterArabic.additionalsIdList.clear();
//        }
        MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
        MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
        CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
        CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());

        if(language.equalsIgnoreCase("En")) {
            CategoriesListFragment.mExpAdapter.notifyDataSetChanged();
        }else if(language.equalsIgnoreCase("Ar")){
            CategoriesListFragment.mExpAdapterArabic.notifyDataSetChanged();
        }
        getActivity().onBackPressed();
        ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
    }



    public void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set title
        alertDialogBuilder.setTitle("dr.CAFE");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please select an item to use this function")
                .setCancelable(false)
                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }


}
