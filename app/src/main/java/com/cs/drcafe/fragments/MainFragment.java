package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.drcafe.BannersAdapter;
import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.GPSTracker;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.SplashScreen;
import com.cs.drcafe.model.Banner;
import com.cs.drcafe.model.FragmentUtils;

import org.jsoup.Jsoup;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

import static com.cs.drcafe.MainActivity.bubbleLayout;


/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class MainFragment  extends Fragment {

    private String SOAP_ACTION_BANNER = "http://tempuri.org/DcBanner";
    private String METHOD_NAME_BANNER = "DcBanner";
//    private final String NAMESPACE = "http://tempuri.org/";
    private SoapObject bannerResult;
    private String bannerResponse = null;
    ProgressDialog dialog;
    AlertDialog alertDialog2;

    public static ArrayList<Banner> bannerList = new ArrayList<>();

    RelativeLayout findStore, messages, favourites;
    LinearLayout orderAndPickup, menu;
    TextView msgCount, changeLanguage;
    ImageView smilyBanner;
    public static LinearLayout trackBanner;
    AnimationDrawable animation;
    public static ImageView cupImg;
    SharedPreferences orderPrefs, userPrefs;
    String orderId, status;
    View rootView;
    String language;
    SharedPreferences.Editor languagePrefsEditor;
    String email, password, languageStr, userId;
    BannersAdapter defaultPagerAdapter;
    CircleIndicator defaultIndicator;
    ViewPager defaultViewpager;

    private String SOAP_ACTION = "http://tempuri.org/userLogin";
    private String METHOD_NAME = "userLogin";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;

    public static String latestVersion = "", currentVersion = "";
    private DataBaseHelper myDbHelper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        Log.d("lifecycle", "main fragment onCreateView: ");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.main_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.main_layout_arabic, container,
                    false);
        }

        myDbHelper = new DataBaseHelper(getActivity());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        orderPrefs = getActivity().getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderId = orderPrefs.getString("order_id", "");
        status = orderPrefs.getString("order_status","close");
        email = userPrefs.getString("user_email", null);
        password = userPrefs.getString("user_password", null);
        userId = userPrefs.getString("userId", null);
        defaultViewpager = (ViewPager) rootView.findViewById(R.id.viewPager);
        defaultIndicator = (CircleIndicator) rootView.findViewById(R.id.indicator_default);

        orderAndPickup = (LinearLayout) rootView.findViewById(R.id.order_pickup_layout);
        findStore = (RelativeLayout) rootView.findViewById(R.id.find_store_layout);
        messages = (RelativeLayout) rootView.findViewById(R.id.messsage_count_layout);
        menu = (LinearLayout) rootView.findViewById(R.id.menu_layout);
        favourites = (RelativeLayout) rootView.findViewById(R.id.favorite_layout);
        msgCount = (TextView) rootView.findViewById(R.id.messsage_count);
        changeLanguage = (TextView) rootView.findViewById(R.id.language_selection);
        smilyBanner = (ImageView) rootView.findViewById(R.id.smily_banner);
        trackBanner = (LinearLayout) rootView.findViewById(R.id.track_banner);
        cupImg = (ImageView) rootView.findViewById(R.id.cup_image);
        msgCount.setText("" + DrConstants.messageCount);

        if(status.equals("open")){
            trackBanner.setVisibility(View.VISIBLE);
            cupImg.setVisibility(View.VISIBLE);
            startAnimation();
        }else{
            trackBanner.setVisibility(View.GONE);
            cupImg.setVisibility(View.GONE);
        }

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }

        try {
            Bundle args = getArguments();
            if(args.getBoolean("push")){
                Fragment nutritionFragment = new MessagesFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                nutritionTransaction.hide(MainFragment.this);
                nutritionTransaction.addToBackStack(null);

                // Commit the transaction
                nutritionTransaction.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        cupImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bubbleLayout.setVisibility(View.GONE);
                Fragment merchandiseFragment = new TrackOrderFragment();
                Bundle mBundle7 = new Bundle();
                mBundle7.putString("orderId", orderId);
                mBundle7.putInt("flag", 2);
                merchandiseFragment.setArguments(mBundle7);

                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction merchandiseTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                merchandiseTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction.add(R.id.realtabcontent, merchandiseFragment);
                merchandiseTransaction.hide(MainFragment.this);
                merchandiseTransaction.addToBackStack(null);

                // Commit the transaction
                merchandiseTransaction.commit();
            }
        });

        messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bubbleLayout.setVisibility(View.GONE);
                Fragment nutritionFragment = new MessagesFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                nutritionTransaction.hide(MainFragment.this);
                nutritionTransaction.addToBackStack(null);

                // Commit the transaction
                nutritionTransaction.commit();
            }
        });

        orderAndPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bubbleLayout.setVisibility(View.GONE);
                DrConstants.storeFlag = false;
                DrConstants.menuFlag = false;
                ((MainActivity) getActivity()).setCurrenTab(2);
            }
        });

        findStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bubbleLayout.setVisibility(View.GONE);
                if(language.equalsIgnoreCase("En")) {
                    ((MainActivity) getActivity()).setCurrenTab(1);
                }else{
                    ((MainActivity) getActivity()).setCurrenTab(3);
                }
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bubbleLayout.setVisibility(View.GONE);
                if (myDbHelper.getTotalOrderQty() == 0) {
                    DrConstants.menuFlag = false;
                    DrConstants.storeFlag = true;
                }
                else {
                    DrConstants.menuFlag = true;
                    DrConstants.storeFlag = false;

                }
                        ((MainActivity) getActivity()).setCurrenTab(2);
            }
        });

        favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bubbleLayout.setVisibility(View.GONE);
//                DrConstants.giftsFlag = true;
//                Log.i("TAG","giftsFlag "+DrConstants.giftsFlag);
//                        ((MainActivity) getActivity()).setCurrenTab(0);

//                Fragment giftsFragment = new WalletDashboardFragment();
                Fragment giftsFragment = new MyGiftsFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, giftsFragment);
                nutritionTransaction.hide(MainFragment.this);
                nutritionTransaction.addToBackStack(null);

                // Commit the transaction
                nutritionTransaction.commit();
            }
        });

        smilyBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bubbleLayout.setVisibility(View.GONE);
                Fragment myProfileFragment = new MyProfileFragment();
                Bundle mBundle13 = new Bundle();
                mBundle13.putString("info","mileStone");
                myProfileFragment.setArguments(mBundle13);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction myProfileTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                myProfileTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                myProfileTransaction.add(R.id.realtabcontent, myProfileFragment);
                myProfileTransaction.hide(MainFragment.this);
                myProfileTransaction.addToBackStack(null);

                // Commit the transaction
                myProfileTransaction.commit();
            }
        });

        changeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language","Ar");
                    languagePrefsEditor.commit();
                    languageStr = "Ar";
                    new CheckLoginDetails().execute(email, password);
                    getActivity().recreate();
                }else if(language.equalsIgnoreCase("Ar")){
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    languageStr = "En";
                    new CheckLoginDetails().execute(email, password);
                    getActivity().recreate();
                }
            }
        });


//        defaultViewpager.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((MainActivity) getActivity()).setCurrenTab(2);
//                Fragment pastryFragment = new CategoriesListFragment();
//                Bundle mBundle2 = new Bundle();
//                mBundle2.putInt("openfrom", 0);
//                mBundle2.putString("catId", "2");
//                pastryFragment.setArguments(mBundle2);
//                // consider using Java coding conventions (upper first char class names!!!)
//                FragmentTransaction pastryTransaction = getFragmentManager().beginTransaction();
//
//                // Replace whatever is in the fragment_container view with this fragment,
//                // and add the transaction to the back stack
//
//                pastryTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                pastryTransaction.add(R.id.realtabcontent, pastryFragment, "category");
//                pastryTransaction.hide(MainFragment.this);
//                pastryTransaction.addToBackStack(null);
//
//                // Commit the transaction
//                pastryTransaction.commit();
//            }
//        });

        if(bannerList.size()>0){
            defaultPagerAdapter = new BannersAdapter(getActivity(), true, bannerList);
            defaultViewpager.setAdapter(defaultPagerAdapter);
            defaultIndicator.setViewPager(defaultViewpager);
        }else {
            defaultPagerAdapter = new BannersAdapter(getActivity(), false, bannerList);
            defaultViewpager.setAdapter(defaultPagerAdapter);
            defaultIndicator.setViewPager(defaultViewpager);
            new GetBannerImages().execute();
        }

        return rootView;
    }


    class Starter implements Runnable {
        public void run() {
            animation.start();
        }
    }

    private void startAnimation(){
        animation = new AnimationDrawable();

        animation.addFrame(getResources().getDrawable(R.drawable.track1), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track2), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track3), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track4), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track5), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track6), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track7), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track8), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track9), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track10), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track11), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track12), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.track13), 200);

        animation.setOneShot(false);

        cupImg.setImageDrawable(animation);

        cupImg.post(new Starter());
    }

    public class CheckLoginDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus, email, password;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Please wait...");
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("username", arg0[0]);
                    request.addProperty("password", arg0[1]);
                    request.addProperty("deviceToken", SplashScreen.regid);
                    request.addProperty("language", languageStr);
                    email = arg0[0];
                    password = arg0[1];

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
//                    dialog.dismiss();

                }else{
                    if (response12.equals("anyType{}")) {
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                    } else {

                        try {
                        }
                            catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }



                    }
                }
            }

            super.onPostExecute(result1);
        }
    }


    public class GetBannerImages extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            bannerList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_BANNER);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_BANNER, envelope);
                    bannerResult = (SoapObject) envelope.getResponse();
                    bannerResponse = bannerResult.toString();
                    Log.v("TAG", "bannerResponse "+bannerResponse);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + bannerResult);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + bannerResponse);
            }else{
                return "no internet";
            }
            return bannerResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    defaultPagerAdapter = new BannersAdapter(getActivity(), false, bannerList);
                    defaultViewpager.setAdapter(defaultPagerAdapter);
                    defaultIndicator.setViewPager(defaultViewpager);
                }else {
//                    dialog.dismiss();
                    try {

                        Object property = bannerResult.getProperty(1);
                        if (property instanceof SoapObject) {
                            try {
                                SoapObject category_list = (SoapObject) property;
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    Banner banner = new Banner();
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                    String bannerName = sObj.getPropertyAsString("bannername");
                                    try {
                                        boolean isclick = Boolean.valueOf(sObj.getPropertyAsString("isclick"));
                                        banner.setClickable(isclick);
                                    } catch (Exception e) {
                                        banner.setClickable(false);
                                        e.printStackTrace();
                                    }

                                    banner.setImage(bannerName);

                                    bannerList.add(banner);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//                    }

                    } catch (Exception e) {
                        Log.d("", "Error while parsing the results!");
                        e.printStackTrace();
                    }

                    if(bannerList.size()>0) {
                        defaultPagerAdapter = new BannersAdapter(getActivity(), true, bannerList);
                        defaultViewpager.setAdapter(defaultPagerAdapter);
                        defaultIndicator.setViewPager(defaultViewpager);
                    }else{
                        defaultPagerAdapter = new BannersAdapter(getActivity(), false, bannerList);
                        defaultViewpager.setAdapter(defaultPagerAdapter);
                        defaultIndicator.setViewPager(defaultViewpager);
                    }
                }
            }else{
                defaultPagerAdapter = new BannersAdapter(getActivity(), false, bannerList);
                defaultViewpager.setAdapter(defaultPagerAdapter);
                defaultIndicator.setViewPager(defaultViewpager);
            }

//            if(latestVersion .equals("")){
//                new VersionChecker().execute();
//            }

            super.onPostExecute(result1);
        }
    }




    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public class VersionChecker extends AsyncTask<String, String, String> {

        String newVersion;

        @Override
        protected String doInBackground(String... params) {

            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getContext().getApplicationContext().getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return newVersion;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            latestVersion = s;
            try {
                PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
                currentVersion = pInfo.versionName;
//                currentVersion = "1.4.6";
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (!DrConstants.updatePopupShown) {
                if(latestVersion == null || latestVersion.equals("null")){
                    latestVersion = currentVersion;
                }
                if (!currentVersion.equals(latestVersion)) {
                    DrConstants.updatePopupShown = true;
                    showDialog2();
                }
            }
        }
    }

    public void showDialog2(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        int layout = R.layout.update_app;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView versionName = (TextView) dialogView.findViewById(R.id.version_name);
        TextView updateBtn = (TextView) dialogView.findViewById(R.id.update_now);
        TextView notNowBtn = (TextView) dialogView.findViewById(R.id.no_cancel);

        versionName.setText("v-"+currentVersion);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + getContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getContext().getPackageName())));
                }

                alertDialog2.dismiss();
            }
        });

        notNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog2.dismiss();
            }
        });

        alertDialog2 = dialogBuilder.create();
//        alertDialog2.getWindow().setGravity(Gravity.BOTTOM);
//        alertDialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        alertDialog2.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog2.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x; // int screenWidth = display.getWidth(); on API < 13
        int screenHeight = size.y;

        double d = screenWidth*0.9;
        double d1 = screenHeight*0.85;
        lp.width = (int) d;
        lp.height = (int) d1;
        window.setAttributes(lp);
    }

    public void getGPSCoordinates(){
        GPSTracker gps = null;
        if(getActivity()!=null) {
            gps = new GPSTracker(getActivity());
        }
        if(gps != null){
            if (gps.canGetLocation()) {
                Double lat = gps.getLatitude();
                Double longi = gps.getLongitude();
                // Create a LatLng object for the current location


                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }
}