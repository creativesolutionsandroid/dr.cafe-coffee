package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.GPSTracker;
import com.cs.drcafe.JSONParser;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.SplashScreen;
import com.cs.drcafe.adapter.PromoAdapter;
import com.cs.drcafe.hyperpay.CheckoutBroadcastReceiver;
import com.cs.drcafe.hyperpay.CheckoutIdRequestAsyncTask;
import com.cs.drcafe.hyperpay.CheckoutIdRequestListener;
import com.cs.drcafe.hyperpay.PaymentStatusRequestAsyncTask;
import com.cs.drcafe.hyperpay.PaymentStatusRequestListener;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.MyCards;
import com.cs.drcafe.model.Order;
import com.cs.drcafe.model.Promos;
import com.cs.drcafe.widget.DateTimePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSkipCVVMode;
import com.oppwa.mobile.connect.checkout.meta.CheckoutStorePaymentDetailsMode;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.exception.PaymentException;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;
import com.oppwa.mobile.connect.service.ConnectService;
import com.oppwa.mobile.connect.service.IProviderBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by brahmam on 15/2/16.
 */
public class OrderConfimationFragment extends Fragment implements DateTimePicker.DateWatcher, OnMapReadyCallback,
        CheckoutIdRequestListener, PaymentStatusRequestListener {

    /**
     * Standard activity result: operation canceled.
     */
    public static final int RESULT_CANCELED = 0;
    /**
     * Standard activity result: operation succeeded.
     */
    public static final int RESULT_OK = -1;
    /**
     * Start of user-defined activity results.
     */
    public static final int RESULT_FIRST_USER = 1;
    static final int TIME_DIALOG_ID = 1111;
    private static final String URL = DrConstants.URL;
    private static final String[] PHONE_PERMS = {
            Manifest.permission.READ_PHONE_STATE
    };
    private static final int PHONE_REQUEST = 3;
    private static final String STATE_RESOURCE_PATH = "STATE_RESOURCE_PATH";
    public static int position = -1;
    public static int promoQty = 1;
    final DecimalFormat priceFormat = new DecimalFormat("0.00");
    private final String NAMESPACE = "http://tempuri.org/";
    protected IProviderBinder providerBinder;
    protected String resourcePath;
    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    String mobileNumberFormat = "";
    SharedPreferences userPrefs;
    String response;
    MarkerOptions markerOptions;
    ArrayList<Order> orderList = new ArrayList<>();
    GPSTracker gps;
    boolean promoFlag;
    int applyPromoQtyInt = 0;
    float remainingBonusInt;
    TextView userName, mobileNo, addressTyep, storeNameTextView, storeAddressTextView, totalItems, totalAmount, estTime;
    String subTotal, vatPrice;
    ImageView changeEstTime;
    TextView promocode;
    RelativeLayout promoLayout;
    LinearLayout recieptLayout, vatLayout;
    float totalAmt, subTotalAmt, walletAmtAvailable, walletAmtUsed, invoiceTotal, netAmt, pointValue;
    TextView netTotal, vatAmount, vatPercent, amount, roundUp, promotionAmount, subTotalAmount;
    ImageView receipt_close;
    ImageView promoCancel, promoArrow;
    RadioButton onlinePayment, cashPayment;
    int totalItemsCount;
    int totalAmountVal;
    int storeDistance;
    float distanceKilometersFloat;
    int distanceKilometers;
    List<Date> listTimes;
    Context ctx;
    boolean checkStatus;
    boolean killHandler = false;
    Date oldTime = null;
    Calendar c;
    String userId, userFullName, userEmail, userPhone;
    ProgressDialog dialog;
    Button confirmOrder, editOrder;
    int paymentMode = 3;
    float vat = 5;
    String promocodeStr = "No", promoIdStr = "", promoTypeStr = "", dupId = "0";
    String orderType;
    String pickerViewOpen, exp_time_to_dumy;
    String endTime, openTimeStr, todayDate, todayDate1, tomorrowDate;
    String changeTimeIsYes = "false", payerTimeIsYes = "false", No_DistanceStr = "true";
    String expTimeTo;
    Button backBtn;
    boolean flag = true, fullHours;
    int minuts, check_count, changeMints;
    String freeOrderId = "";
    String offerPrice = "";
    String secs;
    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefsEditor;
    int min = 0;
    String merchantId = "";
    String customerEmailId = "";
    RelativeLayout onlineLayout, cashLayout;
    String language;
    View rootView;
    String phone;
    private String SOAP_ACTION = "http://tempuri.org/GetUserGifts";
    private String METHOD_NAME = "GetUserGifts";
    private SoapObject result;
    private String response12 = null;
    private String SOAP_ACTION_INSERT = "http://tempuri.org/insertOrder";
    private String METHOD_NAME_INSERT = "insertOrder";
    private SoapPrimitive insertResult;
    private String insertResponse = null;
    private String SOAP_ACTION_TIME = "http://tempuri.org/getCurrentTime";
    private String METHOD_NAME_TIME = "getCurrentTime";
    private SoapPrimitive timeResult;
    private String timeResponse = null;
    private String SOAP_ACTION_SETCARD = "http://tempuri.org/InsertCreditCardDetails";
    private String METHOD_NAME_SETCARD = "InsertCreditCardDetails";
    private SoapPrimitive setCardResult;
    private String setCardResponse = null;
    private MyCards myRegistrationCardDetails = new MyCards();
    private String SOAP_ACTION_GETCARD = "http://tempuri.org/getRegisteredCards";
    private String METHOD_NAME_GETCARD = "getRegisteredCards";
    private SoapObject getCardResult;
    private String getCardResponse = null;
    private String regsiteredCardDetails = "";
    private String distanceResponse = null;
    private int trafficMins;
    private String storeId, storeName, storeAddress;
    private Double latitude, longitude;
    private GoogleMap map;
    private DataBaseHelper myDbHelper;
    private double lat, longi;
    private PromoAdapter mPromoAdapter;
    private ArrayList<Promos> promosList = new ArrayList<>();
    private int hour;
    private int minute;
    private Handler handler = new Handler();

    Location location;
    String addressLine = "";
    String locality = "";
    String postalCode = "";
    String countryName = "";
    String stateName = "";

    Runnable timedTask = new Runnable() {
        @Override
        public void run() {
            if (!killHandler) {
                Calendar cal = Calendar.getInstance();
                Date date = cal.getTime();
//                for(Date itemD : listTimes){
//                    Log.v("srinu", "item cal: " + getTimeCaluculation());
//                    int calC = getTimeCaluculation();
//                    if(calC<=20&&calC>0){
//                    }else{
//                        c.add(Calendar.MINUTE, 1);
//                        updateTime(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), 2);
//                        break;
//                    }
//                }
                handler.sendEmptyMessage(0);
                handler.postDelayed(timedTask, 1000 * 60);

            }
        }
    };
    private String deviceToken = "not found";
    private Timer timer = new Timer();
    private Timer timer1 = new Timer();
    private String expChangeStr = "0000";
    private SharedPreferences sharedSettings;
    private ProgressDialog progressDialog;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            OrderConfimationFragment.this.onServiceConnected(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            providerBinder = null;
        }
    };
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;
            updateTime(hour, minute, 1);

        }

    };

    public static float round(float d) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(2, BigDecimal.ROUND_UP);
        return bd.floatValue();
    }

    public static String[] findCommon(String[] arrayOne, String[] arrayTwo) {

        String[] arrayToHash;
        String[] arrayToSearch;

        if (arrayOne.length < arrayTwo.length) {
            arrayToHash = arrayOne;
            arrayToSearch = arrayTwo;
        } else {
            arrayToHash = arrayTwo;
            arrayToSearch = arrayOne;
        }

        HashSet<String> intersection = new HashSet<String>();

        HashSet<String> hashedArray = new HashSet<String>();
        for (String entry : arrayToHash) {
            hashedArray.add(entry);
        }

        for (String entry : arrayToSearch) {
            if (hashedArray.contains(entry)) {
                intersection.add(entry);
            }
        }

        return intersection.toArray(new String[0]);
    }

    private static String utilTime(int value) {

        if (value < 10)
            return "0" + String.valueOf(value);
        else
            return String.valueOf(value);
    }

    public static String getSourceCode(String requestURL) {
        String response = "";
        try {
            URL url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(15000);
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "UTF-8"));
                while ((line = br.readLine()) != null) {
                    response += line;
                }

            } else {
                response = "";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return response;
        }
        return response;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ctx = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_confirmation, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.order_confirmation_arabic, container,
                    false);
        }

        if (savedInstanceState != null) {
            resourcePath = savedInstanceState.getString(STATE_RESOURCE_PATH);
            Log.d("TAG", "onCreateView resourcePath: "+resourcePath);
        }

        vat = DrConstants.vat;
        listTimes = new ArrayList<>();
        myDbHelper = new DataBaseHelper(getActivity());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        orderPrefs = getActivity().getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor = orderPrefs.edit();
//        sharedSettings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        String response1 = userPrefs.getString("user_profile", null);
        try {
            JSONObject property = new JSONObject(response1);
            JSONObject userObjuect = property.getJSONObject("profile");

            userEmail = userObjuect.getString("username");
            userPhone = userObjuect.getString("phone");
            userFullName = userObjuect.getString("fullName");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        orderList = myDbHelper.getOrderInfo();  // saved orders list from DB
        position = -1;
        promoQty = 1;
        DrConstants.STOP_BACKPRESS = true;

        if (DrConstants.orderNow) {
            storeId = DrConstants.storeId;
            storeName = DrConstants.storeName;
            storeAddress = DrConstants.storeAddress;
            latitude = DrConstants.latitude;
            longitude = DrConstants.longitude;
            fullHours = DrConstants.fullHours;
            orderType = DrConstants.orderType;
        } else {
            storeId = getArguments().getString("storeId");
            storeName = getArguments().getString("storeName");
            storeAddress = getArguments().getString("storeAddress");
            latitude = getArguments().getDouble("latitude");
            longitude = getArguments().getDouble("longitude");
            fullHours = getArguments().getBoolean("full_hours");
            orderType = getArguments().getString("order_type");
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        response = userPrefs.getString("user_profile", null);

        userName = (TextView) rootView.findViewById(R.id.user_name);
        mobileNo = (TextView) rootView.findViewById(R.id.mobile_number);
        addressTyep = (TextView) rootView.findViewById(R.id.address_type);
        storeNameTextView = (TextView) rootView.findViewById(R.id.store_detail_name);
        storeAddressTextView = (TextView) rootView.findViewById(R.id.store_detail_address);
        onlinePayment = (RadioButton) rootView.findViewById(R.id.online_payment);
        cashPayment = (RadioButton) rootView.findViewById(R.id.cash_on_pickup);
        totalItems = (TextView) rootView.findViewById(R.id.totalItems);
        totalAmount = (TextView) rootView.findViewById(R.id.totalAmount);
        estTime = (TextView) rootView.findViewById(R.id.estTime);
        promocode = (TextView) rootView.findViewById(R.id.promo_code);
        changeEstTime = (ImageView) rootView.findViewById(R.id.change_esttime);
        confirmOrder = (Button) rootView.findViewById(R.id.confirm_order_btn);
        editOrder = (Button) rootView.findViewById(R.id.edit_order_btn);
        promoLayout = (RelativeLayout) rootView.findViewById(R.id.promo_layout);
        promoCancel = (ImageView) rootView.findViewById(R.id.promo_cancel);
        promoArrow = (ImageView) rootView.findViewById(R.id.promo_arrow);

        recieptLayout = (LinearLayout) rootView.findViewById(R.id.receipt_layout);
        receipt_close = (ImageView) rootView.findViewById(R.id.receipt_close);
        amount = (TextView) rootView.findViewById(R.id.amount);
        roundUp = (TextView) rootView.findViewById(R.id.round_up);
        vatAmount = (TextView) rootView.findViewById(R.id.vatAmount);
        vatPercent = (TextView) rootView.findViewById(R.id.vatPercent);
        netTotal = (TextView) rootView.findViewById(R.id.net_total);
        vatLayout = (LinearLayout) rootView.findViewById(R.id.vatLayout);
        promotionAmount = (TextView) rootView.findViewById(R.id.promo_amount);
        subTotalAmount = (TextView) rootView.findViewById(R.id.subAmount);
        onlineLayout = (RelativeLayout) rootView.findViewById(R.id.online_layout);
        cashLayout = (RelativeLayout) rootView.findViewById(R.id.cash_layout);

        mPromoAdapter = new PromoAdapter(getActivity(), promosList, getActivity());

        vatPercent.setText("(" + DrConstants.vatString + "%)");

        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrConstants.STOP_BACKPRESS = false;
//                if(DrConstants.orderNow){
//                    DrConstants.orderNow = false;
//                    FragmentManager manager = getActivity().getSupportFragmentManager();
//                    FragmentTransaction trans = manager.beginTransaction();
//                    trans.remove(new OrderConfimationFragment());
////                trans.remove(new MoreLocation());
//                    trans.commit();
//                    manager.popBackStack();
//
//                    Fragment radiusFragment = new SelectStoreFragment();
////                Bundle mBundle12 = new Bundle();
////                mBundle12.putString("info","mileStone");
////                radiusFragment.setArguments(mBundle12);
//                    // consider using Java coding conventions (upper first char class names!!!)
//                    FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();
//
//                    // Replace whatever is in the fragment_container view with this fragment,
//                    // and add the transaction to the back stack
//                    radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
//                    radiusTransaction.add(R.id.realtabcontent, radiusFragment);
//                    radiusTransaction.hide(OrderConfimationFragment.this);
//                    radiusTransaction.addToBackStack(null);
//
//                    // Commit the transaction
//                    radiusTransaction.commit();
//                }else{
                getActivity().onBackPressed();
//                }

            }
        });
        gps = new GPSTracker(getActivity());

//        deviceToken = Settings.Secure.getString(getActivity().getContentResolver(),
//                Settings.Secure.ANDROID_ID);

//        String devcieId;

//        int currentapiVersion = Build.VERSION.SDK_INT;
//        if (currentapiVersion >= Build.VERSION_CODES.M) {
//            if (!canAccessPhonecalls()) {
//                requestPermissions(PHONE_PERMS, PHONE_REQUEST);
//            } else {
//                TelephonyManager mTelephony = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
//                if (mTelephony.getDeviceId() != null){
//                    deviceToken = mTelephony.getDeviceId();
//                }else{
//
//                }
//            }
//        }else {
//            TelephonyManager mTelephony = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
//            if (mTelephony.getDeviceId() != null){
//                deviceToken = mTelephony.getDeviceId();
//            }else{
//
//            }
//        }

        totalItemsCount = myDbHelper.getTotalOrderQty();
        totalItems.setText("" + myDbHelper.getTotalOrderQty());
        amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
        float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
        float netAmount = (Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(myDbHelper.getTotalOrderPrice()))) + (Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(tax)))));
        float netAmountRounded = (float) Math.round(netAmount * 10)/10;
        final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
        roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(netAmount)))));
        vatAmount.setText("" + priceFormat.format(tax));
        netTotal.setText("" + priceFormat.format((netAmount)));
        totalAmount.setText("" + priceFormat.format((netAmount)));

        if (gps.canGetLocation()) {

            lat = gps.getLatitude();
            longi = gps.getLongitude();
            // \n is for new line
//            Toast.makeText(getActivity(), "Your Location is - \nLat: " + lat + "\nLong: " + longi, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

        Location me = new Location("");
        Location dest = new Location("");

        me.setLatitude(lat); // todo Replac with current location latitude 24.70321657
        me.setLongitude(longi); // todo Raplace with current location longitude 46.68097073

//        me.setLatitude(24.70321657);
//        me.setLongitude(46.68097073);


        dest.setLatitude(latitude);
        dest.setLongitude(longitude);

        distanceKilometersFloat = (me.distanceTo(dest)) / 1000f;
        distanceKilometers = (int) distanceKilometersFloat;

        if (response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                userName.setText(userObjuect.getString("fullName"));
                mobileNo.setText(userObjuect.getString("phone"));
                mobileNumberFormat = userObjuect.getString("phone");
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }

        if (!DrConstants.OnlinePayment) {
            onlineLayout.setVisibility(View.GONE);
            cashPayment.setChecked(true);
        } else if (!DrConstants.CashPayment) {
            cashLayout.setVisibility(View.GONE);
            onlinePayment.setChecked(true);
        }

        if (!DrConstants.orderType.equalsIgnoreCase("Delivery")) {
            storeNameTextView.setText(storeName);
            storeAddressTextView.setText(storeAddress);
        } else {
            storeNameTextView.setText(DrConstants.addressHouseNo);
            storeAddressTextView.setText(DrConstants.address);
            if (language.equalsIgnoreCase("En")) {
                addressTyep.setText("Delivery Address");
                cashPayment.setText("Cash on Delivery");
            } else {
                addressTyep.setText("عنوان التوصيل");
                cashPayment.setText("الدفع كاش عند التوصيل");
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(OrderConfimationFragment.this);

//        FragmentManager fm = getChildFragmentManager();
//        Fragment fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
//        if (fragment == null) {
//            fragment = SupportMapFragment.newInstance();
//            fm.beginTransaction().replace(R.id.map, fragment).commit();
//        }
//        map = ((SupportMapFragment) fragment).getMap();
//        MapsInitializer.initialize(getActivity());

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recieptLayout.setVisibility(View.GONE);
            }
        });

        vatLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recieptLayout.setVisibility(View.VISIBLE);
            }
        });

        onlinePayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!totalAmount.getText().toString().equalsIgnoreCase("0.00")) {
                    if (isChecked)
                        cashPayment.setChecked(false);
                    paymentMode = 3;
                } else {
                    onlinePayment.setChecked(false);
                }
            }
        });

        cashPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    onlinePayment.setChecked(false);
                paymentMode = 2;
            }
        });
        changeEstTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pickerViewOpen = "open";
                exp_time_to_dumy = "";
                changeExpTimeMethod();
                //showDialog(TIME_DIALOG_ID);
//                new TimePickerDialog(ctx, timePickerListener, hour, minute,
//                        false).show();
            }
        });

        promoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetPromocodeResponse().execute(userId);
//                }
            }
        });

        promoCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promoLayout.setClickable(true);
                promocode.setText("");
                promoArrow.setVisibility(View.VISIBLE);
                promoCancel.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    promocode.setHint("Apply Promotion");
                } else {
                    promocode.setHint("قدم للعرض");
                }
                amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                float netAmount = (Float.parseFloat(priceFormat.format(myDbHelper.getTotalOrderPrice())) + (Float.parseFloat(priceFormat.format(tax))));
                float netAmountRounded = (float) Math.round(netAmount * 10)/10;
                final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
                roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(priceFormat.format(netAmount))));
                vatAmount.setText("" + priceFormat.format(tax));
                netTotal.setText("" + priceFormat.format((netAmount)));
                totalAmount.setText("" + priceFormat.format((netAmount)));
                applyPromoQtyInt = 0;
                remainingBonusInt = 0;
                dupId = "0";
                if (!totalAmount.getText().toString().equalsIgnoreCase("0.00")) {
                    if (cashPayment.isChecked()) {
                        paymentMode = 2;
                    } else {
                        paymentMode = 3;
                    }
                }
                priceCalculation(myDbHelper.getTotalOrderPrice());
//                totalAmount.setText(""+myDbHelper.getTotalOrderPrice()+" SR");
            }
        });

        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrConstants.STOP_BACKPRESS = false;
                confirmOrder.setEnabled(false);
                Log.d("TAG", "paymentMode: " + paymentMode);

                if (paymentMode == 3) {
                    generateMerchantId();
//                    new GetStoredCards().execute();
                } else {
                    new InsertOrder().execute();
                }
            }
        });

        editOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrConstants.STOP_BACKPRESS = false;
                ((MainActivity) getActivity()).setCurrenTab(0);
                ((MainActivity) getActivity()).setCurrenTab(2);
            }
        });

//        if(DrConstants.IFMAdded){
//            promocode.setText("Free Iftar Meal");
//            promocode.setEnabled(false);
//        }else{
//            promocode.setEnabled(true);
//        }
        new getTrafficTime().execute();

        timer.schedule(new MyTimerTask(), 300000, 300000);
//        new GetTimes().execute();
        priceCalculation(myDbHelper.getTotalOrderPrice());
        return rootView;
    }

    public void priceCalculation(float orderAmount) {
        final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
        float tax = 0;
        float netAmountRounded = 0;
        totalAmt = orderAmount;
        tax = totalAmt * (vat / 100);
        subTotalAmt = (Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(totalAmt)))) + (Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(tax))));
        invoiceTotal = (Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(subTotalAmt)))) - (Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(remainingBonusInt))));
        if (invoiceTotal <= 0) {
//            subTotalAmt = 0;
            netAmt = 0;
            walletAmtUsed = 0;
            invoiceTotal = 0;
            paymentMode = 4;
        } else {
            netAmt = invoiceTotal;
            netAmountRounded = (float) Math.round(netAmt * 10)/10;
        }
        totalAmount.setText("" + priceFormat.format(netAmountRounded));
        amount.setText("" + myDbHelper.getTotalOrderPrice());
        promotionAmount.setText("- " + String.format("%.2f", remainingBonusInt));
        roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(DrConstants.convertToArabic(priceFormat.format(netAmt)))));
        subTotalAmount.setText("" + priceFormat.format(subTotalAmt));
        vatAmount.setText("+ " + priceFormat.format(tax));
        netTotal.setText("" + priceFormat.format(netAmountRounded));
    }

    public void dialog() {

        final Dialog dialog2 = new Dialog(getActivity());

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (language.equalsIgnoreCase("En")) {
            dialog2.setContentView(R.layout.promo_list);
        } else if (language.equalsIgnoreCase("Ar")) {
            dialog2.setContentView(R.layout.promo_list_arabic);
        }

        dialog2.setCanceledOnTouchOutside(true);
        TextView applyPromo = (TextView) dialog2.findViewById(R.id.applyPromo);
        ListView lv = (ListView) dialog2.findViewById(R.id.promos_list);
        ImageView cancel = (ImageView) dialog2.findViewById(R.id.cancel_button);
        lv.setAdapter(mPromoAdapter);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        applyPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position != -1) {

                    dialog2.dismiss();
                    boolean isStoreAvailable = false;
                    String[] stores = promosList.get(position).getStoreId().split(",");
                    if (promosList.get(position).getStoreId().equals("0")) {
                        isStoreAvailable = true;
                    } else {
                        for (int a = 0; a < stores.length; a++) {
                            if (stores[a].equals(storeId)) {
                                isStoreAvailable = true;
                                break;
                            }
                        }
                    }

                    Log.i("TAG", "isStoreAvailable " + isStoreAvailable);
                    Log.i("TAG", "getItemTypeId " + orderList.get(0).getItemTypeId());
                    Log.i("TAG", "getItemId " + promosList.get(position).getItemId());
                    if (isStoreAvailable) {
                        remainingBonusInt = 0;
                        // Specific sizes and NO additionals
                        if (promosList.get(position).getPromoType().equals("1") || promosList.get(position).getPromoType().equals("5")) {

                            float highestPrice = 0;
                            for (Order order : orderList) {
//                                Categories
                                String[] catids = promosList.get(position).getCategoryID().split(",");
                                for (int b = 0; b < catids.length; b++) {
                                    if (catids[b].equals(order.getCategoryId())) {
//                                        SubCategories
                                        String[] subCatids = promosList.get(position).getSubCategoryId().split(",");
                                        for (int c = 0; c < subCatids.length; c++) {
                                            if (subCatids[c].equals(order.getSubCatId())) {
//                                                Item Ids
                                                String[] itemIds = promosList.get(position).getItemId().split(",");
                                                for (int d = 0; d < itemIds.length; d++) {
                                                    if (itemIds[d].equals(order.getItemId())) {
//                                                        Size
                                                        String[] sizes = promosList.get(position).getSize().split(",");
                                                        for (int e = 0; e < sizes.length; e++) {
                                                            if (sizes[e].equals(order.getItemTypeId())) {
                                                                float tprice = Float.parseFloat(myDbHelper.getPrice(order.getItemId(), order.getItemTypeId()));

                                                                if (!order.getAdditionalsTypeId().equals("0")) {
                                                                    if (!promosList.get(position).getAdditionals().equals("0")) {
                                                                        String localAdditionals = order.getAdditionalsTypeId();
                                                                        String serverAdditionals = promosList.get(position).getAdditionals();
                                                                        String[] setC = localAdditionals.split(",");
                                                                        String[] setB = serverAdditionals.split(",");
                                                                        Set<String> distinct = new HashSet<String>();
                                                                        String result = Arrays.toString(findCommon(setC, setB)).replace("[", "").replace("]", "").replace(" ", "");
                                                                        ArrayList<Float> additionalPrice = myDbHelper.getAdditionalPrices(result);
                                                                        for (Float price : additionalPrice) {
                                                                            tprice = tprice + price;
                                                                        }
                                                                    }
                                                                }
//                                                            if (highestPrice < tprice) {
//                                                                highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
//                                                                freeOrderId = order.getOrderId();
//                                                            }

                                                                float tax = tprice * (vat / 100);
                                                                tprice = tprice + tax;
                                                                float limit = Integer.parseInt(promosList.get(position).getLimit());
                                                                if (tprice >= limit) {
                                                                    tprice = limit;
                                                                }

                                                                Log.d("TAG", "tprice: " + tprice);
                                                                Log.d("TAG", "highestPrice: " + highestPrice);
                                                                if (highestPrice < tprice) {
                                                                    highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (!(highestPrice == 0)) {
                                isHighestPriceYes(highestPrice, position);
                            } else {
                                Log.d("TAG", "highestPrice: " + highestPrice);
                                isHighestPriceNo();
                            }

                        } else if (promosList.get(position).getPromoType().equals("7")) {
                            float highestPrice = 0;
                            applyPromoQtyInt = 0;
                            for (Order order : orderList) {

                                if (applyPromoQtyInt == promoQty) {
                                    break;
                                }

//                            for (int f = 1; f <= promoQty; f++) {
//                                Categories
                                String[] catids = promosList.get(position).getCategoryID().split(",");
                                for (int b = 0; b < catids.length; b++) {
                                    if (catids[b].equals(order.getCategoryId())) {
//                                        SubCategories
                                        String[] subCatids = promosList.get(position).getSubCategoryId().split(",");
                                        for (int c = 0; c < subCatids.length; c++) {
                                            if (subCatids[c].equals(order.getSubCatId())) {
//                                                Item Ids
                                                String[] itemIds = promosList.get(position).getItemId().split(",");
                                                for (int d = 0; d < itemIds.length; d++) {
                                                    if (itemIds[d].equals(order.getItemId())) {
//                                                        Size
                                                        String[] sizes = promosList.get(position).getSize().split(",");
                                                        for (int e = 0; e < sizes.length; e++) {
                                                            if (sizes[e].equals(order.getItemTypeId())) {
                                                                float tprice = Float.parseFloat(myDbHelper.getPrice(order.getItemId(), order.getItemTypeId()));

                                                                if (!order.getAdditionalsTypeId().equals("0")) {
                                                                    if (!promosList.get(position).getAdditionals().equals("0")) {
                                                                        String localAdditionals = order.getAdditionalsTypeId();
                                                                        String serverAdditionals = promosList.get(position).getAdditionals();
                                                                        String[] setC = localAdditionals.split(",");
                                                                        String[] setB = serverAdditionals.split(",");
                                                                        Set<String> distinct = new HashSet<String>();
                                                                        String result = Arrays.toString(findCommon(setC, setB)).replace("[", "").replace("]", "").replace(" ", "");
                                                                        ArrayList<Float> additionalPrice = myDbHelper.getAdditionalPrices(result);
                                                                        for (Float price : additionalPrice) {
                                                                            tprice = tprice + price;
                                                                        }
                                                                    }
                                                                }
//                                                            if (highestPrice < tprice) {
//                                                                highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
//                                                                freeOrderId = order.getOrderId();
//                                                            }
                                                                float tax = tprice * (vat / 100);
                                                                tprice = tprice + tax;
                                                                float limit = Integer.parseInt(promosList.get(position).getLimit());
                                                                if (tprice >= limit) {
                                                                    tprice = limit;
                                                                }

                                                                int quanityIn = Integer.parseInt(order.getQty());
                                                                if (quanityIn > 1) {
                                                                    int remainingQtyInt = promoQty - applyPromoQtyInt;
                                                                    quanityIn = (quanityIn < remainingQtyInt) ? quanityIn : remainingQtyInt;
                                                                    tprice = tprice * quanityIn;
                                                                    highestPrice = highestPrice + tprice;
                                                                    applyPromoQtyInt = applyPromoQtyInt + quanityIn;

                                                                } else {
                                                                    highestPrice = highestPrice + tprice;
                                                                    applyPromoQtyInt = applyPromoQtyInt + 1;
                                                                }

//                                                                highestPrice = highestPrice + tprice;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
//                        }

                            if (!(highestPrice == 0)) {
                                isHighestPriceYes(highestPrice, position);
                            } else {
                                isHighestPriceNo();
                            }

                        } else if (promosList.get(position).getPromoType().equals("2")) {
                            if (!promosList.get(position).getRemainingBonus().equalsIgnoreCase("0")) {
                                float totalPriceInt = myDbHelper.getTotalOrderPrice();
                                float tax = totalPriceInt * (vat / 100);
                                totalPriceInt = totalPriceInt + tax;
                                float highestPrice = Float.parseFloat(promosList.get(position).getRemainingBonus());
                                highestPrice = totalPriceInt - highestPrice;
                                if (Float.parseFloat(promosList.get(position).getRemainingBonus()) >= totalPriceInt) {
                                    remainingBonusInt = totalPriceInt;
                                } else {
                                    remainingBonusInt = Float.parseFloat(promosList.get(position).getRemainingBonus());
                                }

                                if (highestPrice <= 0) {
//                                    totalAmount.setText("0.00");
//                                    amount.setText("0.00");
//                                    float tax = 0;
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText("0.00");
                                    priceCalculation(0);
                                    paymentMode = 4;
                                } else {
                                    priceCalculation(myDbHelper.getTotalOrderPrice());
//                                    amount.setText("" + priceFormat.format((float) highestPrice));
//                                    float tax = highestPrice*(vat/100);
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText(""+priceFormat.format((highestPrice+tax)));
//                                    totalAmount.setText("" + priceFormat.format((highestPrice+tax)));
                                }

                                promocodeStr = promosList.get(position).getPromoCode();

                                promoIdStr = promosList.get(position).getPromoID();
                                promoTypeStr = promosList.get(position).getPromoType();
                                dupId = promosList.get(position).getDupId();
//                            int totalPriceInt = myDbHelper.getTotalOrderPrice();
//                            highestPrice=totalPriceInt-highestPrice;
                                if (highestPrice <= 0) {
                                    if (language.equalsIgnoreCase("En")) {
                                        promocode.setText(promosList.get(position).getPromoTitle() + " " + DrConstants.format.format(remainingBonusInt) + " SR Redeem");
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        promocode.setText(promosList.get(position).getPromoTitleAr() + " " + DrConstants.format.format(remainingBonusInt) + " ريال مجانا ");
                                    }
//                                    totalAmount.setText("0.00");
//                                    amount.setText("0.00");
//                                    float tax = 0;
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText("0.00");
                                    priceCalculation(0);
                                    paymentMode = 4;
                                } else {
                                    if (language.equalsIgnoreCase("En")) {
                                        promocode.setText(DrConstants.format.format(remainingBonusInt) + " SR Redeem, " + promosList.get(position).getPromoTitle());
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        promocode.setText(promosList.get(position).getPromoTitleAr() + " " + DrConstants.format.format(remainingBonusInt) + " ريال مجانا ");
                                    }
                                    priceCalculation(myDbHelper.getTotalOrderPrice());
//                                    amount.setText("" + priceFormat.format((float) highestPrice));
//                                    float tax = highestPrice*(vat/100);
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText(""+priceFormat.format((highestPrice+tax)));
//                                    totalAmount.setText("" + priceFormat.format((highestPrice+tax)));
                                }
                                promoLayout.setClickable(false);
                                promoArrow.setVisibility(View.GONE);
                                promoCancel.setVisibility(View.VISIBLE);


                            } else {
                                promocodeStr = "";
                                if (language.equalsIgnoreCase("En")) {
                                    promocode.setHint("Apply Promotion");
//                                Toast.makeText(getActivity(), "Specified item not available in cart", Toast.LENGTH_SHORT).show();
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    promocode.setHint("قدم للعرض");
//                                Toast.makeText(getActivity(), "المنتجات المحددة غير موجودة في سلة الشراء", Toast.LENGTH_SHORT).show();
                                }
                                promoIdStr = "";
                                promoTypeStr = "";
                                dupId = "0";
//                            Toast.makeText(getActivity(), "Specified item not available in cart", Toast.LENGTH_SHORT).show();
//                                float totalPriceInt = myDbHelper.getTotalOrderPrice();
                                priceCalculation(myDbHelper.getTotalOrderPrice());
//                                amount.setText("" + priceFormat.format((float) totalPriceInt));
//                                float tax = totalPriceInt*(vat/100);
//                                vatAmount.setText(""+priceFormat.format(tax));
//                                netTotal.setText(""+priceFormat.format((totalPriceInt+tax)));
//                                totalAmount.setText("" + priceFormat.format((totalPriceInt+tax)));
//                            paymentMode = 3;
                            }
                        } else if (promosList.get(position).getPromoType().equals("3")) {
                            if (!promosList.get(position).getRemainingBonus().equalsIgnoreCase("0")) {
                                float totalPriceInt = myDbHelper.getTotalOrderPrice();
                                float tax = totalPriceInt * (vat / 100);
                                totalPriceInt = totalPriceInt + tax;
                                float highestPrice = Float.parseFloat(promosList.get(position).getRemainingBonus());
                                highestPrice = totalPriceInt - highestPrice;

                                if (Float.parseFloat(promosList.get(position).getRemainingBonus()) >= totalPriceInt) {
                                    remainingBonusInt = totalPriceInt;
                                } else {
                                    remainingBonusInt = Float.parseFloat(promosList.get(position).getRemainingBonus());
                                }

                                if (highestPrice <= 0) {
                                    priceCalculation(0);
//                                    totalAmount.setText("0.00");
//                                    amount.setText("0.00");
//                                    float tax = 0;
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText("0.00");
                                    paymentMode = 4;
                                } else {
                                    priceCalculation(myDbHelper.getTotalOrderPrice());
//                                    amount.setText("" + priceFormat.format((float) highestPrice));
//                                    float tax = highestPrice*(vat/100);
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText(""+priceFormat.format((highestPrice+tax)));
//                                    totalAmount.setText("" + priceFormat.format((highestPrice+tax)));
                                }

                                promocodeStr = promosList.get(position).getPromoCode();

                                promoIdStr = promosList.get(position).getPromoID();
                                promoTypeStr = promosList.get(position).getPromoType();
                                dupId = promosList.get(position).getDupId();
//                            int totalPriceInt = myDbHelper.getTotalOrderPrice();
//                            highestPrice=totalPriceInt-highestPrice;
                                if (highestPrice <= 0) {
                                    if (language.equalsIgnoreCase("En")) {
                                        promocode.setText(promosList.get(position).getPromoTitle() + " " + DrConstants.format.format(remainingBonusInt) + " SR Redeem");
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        promocode.setText(promosList.get(position).getPromoTitleAr() + " " + DrConstants.format.format(remainingBonusInt) + " ريال مجانا ");
                                    }
                                    priceCalculation(0);
//                                    totalAmount.setText("0.00");
//                                    amount.setText("0.00");
//                                    float tax = 0;
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText("0.00");
                                    paymentMode = 4;
                                } else {
                                    if (language.equalsIgnoreCase("En")) {
                                        promocode.setText(DrConstants.format.format(remainingBonusInt) + " SR Redeem, " + promosList.get(position).getPromoTitle());
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        promocode.setText(promosList.get(position).getPromoTitleAr() + " " + DrConstants.format.format(remainingBonusInt) + " ريال مجانا ");
                                    }
                                    priceCalculation(myDbHelper.getTotalOrderPrice());
//                                    amount.setText("" + priceFormat.format((float) highestPrice));
//                                    float tax = highestPrice*(vat/100);
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText(""+priceFormat.format((highestPrice+tax)));
//                                    totalAmount.setText("" + priceFormat.format((highestPrice+tax)));
                                }
                                promoLayout.setClickable(false);
                                promoArrow.setVisibility(View.GONE);
                                promoCancel.setVisibility(View.VISIBLE);


                            } else {
                                promocodeStr = "";
                                if (language.equalsIgnoreCase("En")) {
                                    promocode.setHint("Apply Promotion");
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    promocode.setHint("قدم للعرض");
                                }
                                promoIdStr = "";
                                promoTypeStr = "";
                                dupId = "0";
//                            Toast.makeText(getActivity(), "Specified item not available in cart", Toast.LENGTH_SHORT).show();
//                                float totalPriceInt = myDbHelper.getTotalOrderPrice();
                                priceCalculation(0);
//                                amount.setText("" + priceFormat.format((float) totalPriceInt));
//                                float tax = totalPriceInt*(vat/100);
//                                vatAmount.setText(""+priceFormat.format(tax));
//                                netTotal.setText(""+priceFormat.format((totalPriceInt+tax)));
//                                totalAmount.setText("" + priceFormat.format((totalPriceInt+tax)));
//                            paymentMode = 3;
                            }

                        } else if (promosList.get(position).getPromoType().equals("6")) {
                            if (!promosList.get(position).getRemainingBonus().equalsIgnoreCase("0")) {
                                float totalPriceInt = myDbHelper.getTotalOrderPrice();
                                float tax = totalPriceInt * (vat / 100);
                                totalPriceInt = totalPriceInt + tax;
                                float highestPrice = Float.parseFloat(promosList.get(position).getRemainingBonus());
                                highestPrice = totalPriceInt - highestPrice;

                                if (Float.parseFloat(promosList.get(position).getRemainingBonus()) >= totalPriceInt) {
                                    remainingBonusInt = totalPriceInt;
                                } else {
                                    remainingBonusInt = Float.parseFloat(promosList.get(position).getRemainingBonus());
                                }

                                if (highestPrice <= 0) {
                                    priceCalculation(0);
//                                    totalAmount.setText("0.00");
//                                    amount.setText("0.00");
//                                    float tax = 0;
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText("0.00");
                                    paymentMode = 4;
                                } else {
                                    priceCalculation(myDbHelper.getTotalOrderPrice());
//                                    amount.setText("" + priceFormat.format((float) highestPrice));
//                                    float tax = highestPrice*(vat/100);
//                                    vatAmount.setText(""+priceFormat.format(tax));
//                                    netTotal.setText(""+priceFormat.format((highestPrice+tax)));
//                                    totalAmount.setText("" + priceFormat.format((highestPrice+tax)));
                                }


                                promocodeStr = promosList.get(position).getPromoCode();


                                promoIdStr = promosList.get(position).getPromoID();
                                promoTypeStr = promosList.get(position).getPromoType();
                                dupId = promosList.get(position).getDupId();
//                            int totalPriceInt = myDbHelper.getTotalOrderPrice();
//                            highestPrice=totalPriceInt-highestPrice;
                                if (highestPrice <= 0) {
                                    if (language.equalsIgnoreCase("En")) {
                                        promocode.setText(promosList.get(position).getPromoTitle() + " " + DrConstants.format.format(remainingBonusInt) + " SR Redeem");
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        promocode.setText(promosList.get(position).getPromoTitleAr() + " " + DrConstants.format.format(remainingBonusInt) + " ريال مجانا ");
                                    }
                                } else {
                                    if (language.equalsIgnoreCase("En")) {
                                        promocode.setText(promosList.get(position).getPromoTitle() + " " + DrConstants.format.format(remainingBonusInt) + " SR Redeem");
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        promocode.setText(promosList.get(position).getPromoTitleAr() + " " + DrConstants.format.format(remainingBonusInt) + " ريال مجانا ");
                                    }
                                }
                                promoLayout.setClickable(false);
                                promoArrow.setVisibility(View.GONE);
                                promoCancel.setVisibility(View.VISIBLE);


                            } else {

                                float highestPrice = 0;
                                for (Order order : orderList) {
//                                Categories
                                    String[] catids = promosList.get(position).getCategoryID().split(",");
                                    for (int b = 0; b < catids.length; b++) {
                                        if (catids[b].equals(order.getCategoryId())) {
//                                        SubCategories
                                            String[] subCatids = promosList.get(position).getSubCategoryId().split(",");
                                            for (int c = 0; c < subCatids.length; c++) {
                                                if (subCatids[c].equals(order.getSubCatId())) {
//                                                Item Ids
                                                    String[] itemIds = promosList.get(position).getItemId().split(",");
                                                    for (int d = 0; d < itemIds.length; d++) {
                                                        if (itemIds[d].equals(order.getItemId())) {
//                                                        Size
                                                            String[] sizes = promosList.get(position).getSize().split(",");
                                                            for (int e = 0; e < sizes.length; e++) {
                                                                if (sizes[e].equals(order.getItemTypeId())) {
                                                                    Float tprice = Float.parseFloat(myDbHelper.getPrice(order.getItemId(), order.getItemTypeId()));

                                                                    if (!order.getAdditionalsTypeId().equals("0")) {
                                                                        if (!promosList.get(position).getAdditionals().equals("0")) {
                                                                            String localAdditionals = order.getAdditionalsTypeId();
                                                                            String serverAdditionals = promosList.get(position).getAdditionals();
                                                                            String[] setC = localAdditionals.split(",");
                                                                            String[] setB = serverAdditionals.split(",");
                                                                            Set<String> distinct = new HashSet<String>();
                                                                            String result = Arrays.toString(findCommon(setC, setB)).replace("[", "").replace("]", "").replace(" ", "");
                                                                            ArrayList<Float> additionalPrice = myDbHelper.getAdditionalPrices(result);
                                                                            for (Float price : additionalPrice) {
                                                                                tprice = tprice + price;
                                                                            }
                                                                        }
                                                                    }

                                                                    float tax = tprice * (vat / 100);
                                                                    tprice = tprice + tax;
                                                                    float limit = Integer.parseInt(promosList.get(position).getLimit());
                                                                    if (tprice >= limit) {
                                                                        tprice = limit;
                                                                    }

                                                                    if (highestPrice < tprice) {
                                                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!(highestPrice == 0)) {
                                    isHighestPriceYes(highestPrice, position);
                                } else {
                                    isHighestPriceNo();
                                }
                            }
                        }
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                getActivity());

                        if (language.equalsIgnoreCase("En")) {
                            // set title
                            alertDialogBuilder.setTitle("dr.CAFE");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage(promosList.get(position).getMsg_En())
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        } else if (language.equalsIgnoreCase("Ar")) {
                            // set title
                            alertDialogBuilder.setTitle("د. كيف");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage(promosList.get(position).getMsg_Ar())
                                    .setCancelable(false)
                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                        }
                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Please select any one promotion")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });

        dialog2.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog2.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

    }


   /* @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:

                // set time picker as current time
                return new TimePickerDialog(ctx, timePickerListener, hour, minute,
                        false);

        }
        return null;
    }*/

    @Override
    public void onDateChanged(Calendar c) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        try {
            markerOptions = new MarkerOptions();
            markerOptions.position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
            map.addMarker(markerOptions);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom
                    (new LatLng(latitude, longitude), 7.0f));
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DrConstants.STOP_BACKPRESS = false;
        timer.cancel();
    }

    public void changeExpTimeMethod() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH/mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd/MM/yyyy/HH/mm", Locale.US);
        payerTimeIsYes = "false";
        String st_time;
        String ed_time;
        final String st_time1, ed_time1;
        boolean BM = true;
        if (DrConstants.orderType.equalsIgnoreCase("Delivery")) {
            for (int i = 0; i < orderList.size(); i++) {
                if (orderList.get(i).getCategoryId().equalsIgnoreCase("1") ||
                        orderList.get(i).getCategoryId().equalsIgnoreCase("2") ||
                        orderList.get(i).getCategoryId().equalsIgnoreCase("8") ||
                        orderList.get(i).getCategoryId().equalsIgnoreCase("9")) {
                    BM = false;
                }
            }
        }
        if (fullHours || (DrConstants.orderType.equalsIgnoreCase("Delivery") && BM)) {
            Date current24Date = null, currentServerDate = null, currentServerDate1;
            try {
                current24Date = timeFormat.parse(timeResponse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String currentTime = timeFormat1.format(current24Date);
            try {
                currentServerDate = timeFormat1.parse(currentTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar now = Calendar.getInstance();
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, changeMints);
            currentServerDate = now.getTime();

            Calendar now1 = Calendar.getInstance();
            now1.setTime(currentServerDate);
            now1.add(Calendar.MINUTE, changeMints - 1);
            currentServerDate1 = now1.getTime();

            st_time = timeFormat1.format(currentServerDate);
            ed_time = timeFormat1.format(currentServerDate1);
            st_time = todayDate + "/" + st_time;
            ed_time = tomorrowDate + "/" + ed_time;
        } else {
            endTime = getArguments().getString("end_time");
            endTime = endTime.replace(" ", "");
            if (endTime.equals("12:00AM")) {
                endTime = "11:59PM";
            }
            Date date21 = null, date1 = null, date2 = null;
            try {
//            date21 = timeFormat3.parse(endTime);
                date1 = timeFormat3.parse(openTimeStr);
                date2 = timeFormat3.parse(endTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date current24Date = null, currentServerDate = null, currentServerDate1;
            try {
                current24Date = timeFormat.parse(timeResponse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String currentTime = timeFormat1.format(current24Date);
            try {
                currentServerDate = timeFormat1.parse(currentTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar now = Calendar.getInstance();
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, changeMints);
            currentServerDate = now.getTime();

            st_time = timeFormat1.format(currentServerDate);
            ed_time = timeFormat1.format(date2);
            Date st_time2Date = null;
            try {
                st_time2Date = timeFormat1.parse(st_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String ETimeString = timeFormat2.format(date2);
            String[] parts = ETimeString.split(":");
            int endHour = Integer.parseInt(parts[0]);
            int endMinute = Integer.parseInt(parts[1]);

            String st_time2str = timeFormat2.format(st_time2Date);
            String[] parts1 = st_time2str.split(":");
            int startHour = Integer.parseInt(parts1[0]);
            int startMinute = Integer.parseInt(parts1[1]);

            String CTimeString = timeFormat2.format(current24Date);
            String[] parts2 = CTimeString.split(":");
            int currentHour = Integer.parseInt(parts2[0]);
            int currentMinute = Integer.parseInt(parts2[1]);

            if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
                if (startHour < 0 || (startHour > 5 || (startHour == 5 && (startMinute > 59 || 0 > 59)))) {
                    st_time = todayDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                } else {
                    st_time = tomorrowDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                }
            } else {
                if (currentHour < 0 || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
                    st_time = todayDate + "/" + st_time;
                    ed_time = tomorrowDate + "/" + ed_time;
                } else {
                    st_time = todayDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                }
            }

        }
        st_time1 = st_time;
        ed_time1 = ed_time;

        Date st_time2Date = null;
        try {
            st_time2Date = timeFormat4.parse(st_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String st_time2str = timeFormat2.format(st_time2Date);
        String[] parts1 = st_time2str.split(":");
        int startHour = Integer.parseInt(parts1[0]);
        int startMinute = Integer.parseInt(parts1[1]);
//        new TimePickerDialog(ctx, timePickerListener, startHour, startMinute,
//                false).show();


        final Dialog mDateTimeDialog = new Dialog(getActivity());
        // Inflate the root layout
        final RelativeLayout mDateTimeDialogView = (RelativeLayout) getActivity().getLayoutInflater().inflate(R.layout.date_time_picker, null);
        // Grab widget instance
        final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView.findViewById(R.id.DateTimePicker);
        mDateTimePicker.initData(st_time2Date);
        mDateTimePicker.setDateChangedListener(this);

        // Update demo edittext when the "OK" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime)).setOnClickListener(new View.OnClickListener
                () {
            public void onClick(View v) {
                SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MMM/yyyy/HH/mm", Locale.US);
                SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy/HH/mm", Locale.US);
                SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
                SimpleDateFormat timeFormat3 = new SimpleDateFormat("HH:mm", Locale.US);
                mDateTimePicker.clearFocus();
                String result_string = mDateTimePicker.getDay() + "/" + String.valueOf(mDateTimePicker.getMonth()) + "/" + String.valueOf(mDateTimePicker.getYear())
                        + "/" + String.valueOf(mDateTimePicker.getHour()) + "/" + String.valueOf(mDateTimePicker.getMinute());
//                edit_text.setText(result_string);
                Date pickerDate = null, pickerDate1, stDate = null, edDate = null;
                try {
                    pickerDate = timeFormat.parse(result_string);
                    stDate = timeFormat1.parse(st_time1);
                    edDate = timeFormat1.parse(ed_time1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (pickerDate.after(stDate) && pickerDate.before(edDate)) {
                    for (Date d : listTimes) {
                        Log.i("CHANGE TIME", "FOR");
                        Date prayerDate = d;
                        Date prayerEndTime;
                        Calendar prayerEnd = Calendar.getInstance();
                        prayerEnd.setTime(d);
                        prayerEnd.add(Calendar.MINUTE, 20);
                        prayerEndTime = prayerEnd.getTime();
                        String payerString = timeFormat3.format(prayerDate);
                        String payerEndString = timeFormat3.format(prayerEndTime);
                        String CTimeString = timeFormat3.format(pickerDate);
                        String[] startParts = payerString.split(":");
                        String[] endParts = payerEndString.split(":");
                        String[] currentParts = CTimeString.split(":");
                        int startHourInteger = Integer.parseInt(startParts[0]);
                        int startMintInteger = Integer.parseInt(startParts[1]);
                        int endHourInteger = Integer.parseInt(endParts[0]);
                        int endMintInteger = Integer.parseInt(endParts[1]);
                        int currentHour = Integer.parseInt(currentParts[0]);
                        int currentMinute = Integer.parseInt(currentParts[1]);
                        int c = (int) (currentHour * 60) + (int) currentMinute;
                        int p = (int) (startHourInteger * 60) + (int) startMintInteger - (int) 5;
                        int f = (int) (endHourInteger * 60) + (int) endMintInteger;
                        if (c > p && c < f) {
                            Log.i("CHANGE TIME", "C>P");
                            expTimeTo = timeFormat2.format(prayerEndTime);
                            estTime.setText(expTimeTo);
                            payerTimeIsYes = "true";
                            expChangeStr = expTimeTo;
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    getActivity());

                            if (language.equalsIgnoreCase("En")) {
                                // set title
                                alertDialogBuilder.setTitle("dr.CAFE");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("It's prayer time from " + payerString + " to " + payerEndString + " we are closed. Please select any other time (or) your order will be considered after prayer time")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                            } else if (language.equalsIgnoreCase("Ar")) {
                                alertDialogBuilder.setTitle("د. كيف");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("اوقات الصلاة من %@ الى %@ الفرع مغلق الان . من فضلك حدد اي وقت اخر (أو) سيتم تنفيذ طلبك" + payerEndString + " د ا " + payerString + " ء الصلاة مباشرة")
                                        .setCancelable(false)
                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                            }


                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        }
                    }

                    if (payerTimeIsYes.equals("false")) {
                        expTimeTo = timeFormat2.format(pickerDate);
                        expChangeStr = expTimeTo;
                        estTime.setText(expTimeTo);
                        No_DistanceStr = "false";
                        payerTimeIsYes = "false";
                        changeTimeIsYes = "true";
                    }

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Order can't be processed for selected time, Please select different time.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }


                mDateTimeDialog.dismiss();
            }
        });

        // Cancel the dialog when the "Cancel" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDateTimeDialog.cancel();
            }
        });

        // Reset Date and Time pickers when the "Reset" button is clicked

//        ((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime)).setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                mDateTimePicker.reset();
//            }
//        });

        // Setup TimePicker
        // No title on the dialog window
        mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Set the dialog content view
        mDateTimeDialog.setContentView(mDateTimeDialogView);
        // Display the dialog
        mDateTimeDialog.show();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        killHandler = true;
        handler.removeCallbacks(timedTask);
    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTime(int hours, int mins, int atTime) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        Date parseD = doParse(aTime);
        if (hours < 6 && timeSet.equals("AM") || hours == 12 && timeSet.equals("AM")) {
            Toast.makeText(ctx, "At this time not available estimaiton time and order requests", Toast.LENGTH_SHORT).show();
            return;
        }
        if (atTime == 1)
//            if(oldTime!=null){
////                int diff = getTimeCaluculation();
//                Log.v("srinu", "old time not null: "+diff);
//                if(diff<0){
//                    Toast.makeText(ctx,"Please select future time",Toast.LENGTH_SHORT).show();
//                    return;
//                }
//            }
        /*if(atTime == 1||atTime == 2){
            oldTime = parseD;


            Log.v("srinu", "old time: ");
        }else{
            Log.v("srinu", "old time nnnn: ");
        }*/
            oldTime = parseD;
        checkStatus = false;

        Calendar cal = Calendar.getInstance();
        cal.setTime(parseD);
//        for(Date itemD : listTimes){
//            Log.v("srinu", "item cal: " + getTimeCaluculation());
//            int calC = getTimeCaluculation();
//            if(calC<=20&&calC>0){
//                checkStatus = true;
//                if(atTime == 1||atTime == 2)
//                    cal.add(Calendar.MINUTE,20);
//                //showAlert("Prayer time");
//            }
//        }
        if (atTime != 0) {
            if (!checkStatus) {
                //estTime.setText(getTimeStr(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
                //showAlert("There are no prayers found at this time");
            }
        }

        estTime.setText(getTimeStr(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
        /*if(timeSet.equals("AM")){
            Toast.makeText(getApplicationContext(),"Please select time between 12:00 PM to 11:59 PM",Toast.LENGTH_SHORT).show();
        }
        else{
            btnClick.setText(aTime);
        }*/
    }

    public void getTimeCaluculation(int addOneMin) {
        Log.d("sudheer", "getTimeCaluculation: " + addOneMin);
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        Date current24Date = null, currentServerDate = null;
        try {
            current24Date = timeFormat.parse(timeResponse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String currentTime = timeFormat1.format(current24Date);
        try {
            currentServerDate = timeFormat1.parse(currentTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (changeTimeIsYes.equals("false")) {
            Log.i("CHANGE TIME", "YES");
            minuts = 0;
            check_count = 0;
            changeMints = 0;
            check_count = myDbHelper.getTotalOrderQty();
            if (No_DistanceStr.equals("true")) {
//                int dist = distanceKilometers + 1;
//                minuts = minuts + dist;
                minuts = trafficMins;
            }
            check_count = (check_count * 90 / 60) + 1;
            if (DrConstants.orderType.equalsIgnoreCase("Delivery")) {
                changeMints = minuts + check_count;
                minuts = minuts + check_count;
            } else {
                changeMints = check_count;
                if (minuts < check_count) {
                    minuts = check_count;
                }
            }
            if (minuts < 5) {
                minuts = 5;
            }
            Calendar now = Calendar.getInstance();
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, minuts);
            now.add(Calendar.MINUTE, addOneMin);
            currentServerDate = now.getTime();
            for (Date d : listTimes) {
                Log.i("CHANGE TIME", "FOR");
                Date prayerDate = d;
                Date prayerEndTime;
                Calendar prayerEnd = Calendar.getInstance();
                prayerEnd.setTime(d);
                prayerEnd.add(Calendar.MINUTE, 20);
                prayerEndTime = prayerEnd.getTime();
                String payerString = timeFormat1.format(prayerDate);
                String payerEndString = timeFormat1.format(prayerEndTime);
                String CTimeString = timeFormat1.format(currentServerDate);
                String[] startParts = payerString.split(":");
                String[] endParts = payerEndString.split(":");
                String[] currentParts = CTimeString.split(":");
                int startHourInteger = Integer.parseInt(startParts[0]);
                int startMintInteger = Integer.parseInt(startParts[1]);
                int endHourInteger = Integer.parseInt(endParts[0]);
                int endMintInteger = Integer.parseInt(endParts[1]);
                int CTHourInteger = Integer.parseInt(currentParts[0]);
                int CTMintInteger = Integer.parseInt(currentParts[1]);
                int c = (int) (CTHourInteger * 60) + (int) CTMintInteger;
                int p = (int) (startHourInteger * 60) + (int) startMintInteger - (int) 5;
                int f = (int) (endHourInteger * 60) + (int) endMintInteger;
                if (c > p && c < f) {
                    Log.i("CHANGE TIME", "C>P");
                    expTimeTo = timeFormat2.format(prayerEndTime);
                    estTime.setText(expTimeTo);
                    payerTimeIsYes = "true";
                }
            }
            if (payerTimeIsYes.equals("false")) {
                expTimeTo = timeFormat2.format(currentServerDate);
                estTime.setText(expTimeTo);
            }
        } else {
            Log.i("CHANGE TIME", "NO");
            Date prayerDate = null;
            try {
                prayerDate = timeFormat2.parse(expTimeTo);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String payerString = timeFormat1.format(prayerDate);
            String CTimeString = timeFormat1.format(currentServerDate);
            String[] prayerParts = payerString.split(":");
            String[] currentParts = CTimeString.split(":");
            int startHourInteger = Integer.parseInt(prayerParts[0]);
            int startMintInteger = Integer.parseInt(prayerParts[1]);
            int CTHourInteger = Integer.parseInt(currentParts[0]);
            int CTMintInteger = Integer.parseInt(currentParts[1]);
            int c = (int) (CTHourInteger * 60) + (int) CTMintInteger + minuts;
            int p = (int) (startHourInteger * 60) + (int) startMintInteger;
            if (c >= p) {
                changeTimeIsYes = "false";
                getTimeCaluculation(0);
            }
        }

        String startTime = expTimeTo.replace(" ", "");
        if (fullHours) {
            return;
        }

        openTimeStr = getArguments().getString("start_time");
        endTime = getArguments().getString("end_time");
        endTime = endTime.replace(" ", "");
        Log.i("start TIME", "" + openTimeStr);
        Log.i("end TIME", "" + endTime);

        if (endTime.equals("12:00AM")) {
            endTime = "11:59PM";
        }
        Date date21 = null, date1 = null, date2 = null;
        try {
//            date21 = timeFormat3.parse(endTime);
            date1 = timeFormat2.parse(expTimeTo);
            date2 = timeFormat3.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String st_time = timeFormat1.format(date1);
        String ed_time = timeFormat1.format(date2);
        String currentServerDate1 = timeFormat1.format(current24Date);

        String[] parts = ed_time.split(":");
        int endHour = Integer.parseInt(parts[0]);
        int endMinute = Integer.parseInt(parts[1]);

        String[] parts1 = st_time.split(":");
        int startHour = Integer.parseInt(parts1[0]);
        int startMinute = Integer.parseInt(parts1[1]);

        String[] parts2 = currentServerDate1.split(":");
        int currentHour = Integer.parseInt(parts2[0]);
        int currentMinute = Integer.parseInt(parts2[1]);

        if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
            if (startHour < 0 || (startHour > 5 || (startHour == 5 && (startMinute > 59 || 0 > 59)))) {
                st_time = todayDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            } else {
                st_time = tomorrowDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            }
        } else {
            if (currentHour < 0 || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
                st_time = todayDate + " " + st_time;
                ed_time = tomorrowDate + " " + ed_time;
            } else {
                st_time = todayDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            }
        }

        Log.i("DATE TAG ddd", st_time + "  " + ed_time);

        Date date3 = null, date4 = null;
        try {
//            date21 = timeFormat3.parse(endTime);
            date3 = timeFormat4.parse(st_time);
            date4 = timeFormat4.parse(ed_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date3.before(date4)) {
            return;
        } else {
            if (getActivity() != null) {
                if (DrConstants.orderNow) {
                    DrConstants.orderNow = false;
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    trans.remove(new OrderConfimationFragment());
//                trans.remove(new MoreLocation());
                    trans.commit();

                    manager.popBackStack();

                    Fragment radiusFragment = new SelectStoreFragment();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                    radiusTransaction.hide(OrderConfimationFragment.this);
                    radiusTransaction.addToBackStack(null);

                    // Commit the transaction
                    radiusTransaction.commit();
                } else {
                    getActivity().onBackPressed();
                }
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        getActivity());

                if (language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Sorry Store is closed! You can't make the order from this store.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                } else if (language.equalsIgnoreCase("Ar")) {
                    // set title
                    alertDialogBuilder.setTitle("د. كيف");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
                            .setCancelable(false)
                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                }
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        }
    }

    String getTimeStr(int hours, int mins) {
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        return aTime;
    }

    Date doParse(String timeStr) {
        try {

            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US); //if 24 hour format
            return format.parse(timeStr);
        } catch (Exception e) {

            Log.e("Exception is ", e.toString());
        }
        return null;
    }

    void showAlert(String messageStr) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        dialog.setTitle("");
        dialog.setMessage(messageStr);
        dialog.setNeutralButton("OK", null);
        dialog.create().show();
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.READ_PHONE_STATE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    TelephonyManager mTelephony = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                    if (mTelephony.getDeviceId() != null) {
                        deviceToken = mTelephony.getDeviceId();
                    } else {

                    }
                } else {
                    Toast.makeText(getActivity(), "Read Phone permission denied, Unable to get device identity", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {
            };
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public void isHighestPriceYes(float highestPrice, int position) {
        promocodeStr = promosList.get(position).getPromoCode();
//        int limit= Integer.parseInt(promosList.get(position).getLimit());

        promoIdStr = promosList.get(position).getPromoID();
        promoTypeStr = promosList.get(position).getPromoType();
        dupId = promosList.get(position).getDupId();
        float totalPriceInt = myDbHelper.getTotalOrderPrice();

//        int offer=highestPrice-limit;
//        if (offer<=0) {
//            offerPrice = "Free";
//        }else{
//            offerPrice = Integer.toString(offer);
//        }
//
//        highestPrice=(highestPrice <limit)?highestPrice:limit;

        remainingBonusInt = highestPrice;

        if (language.equalsIgnoreCase("En")) {
            promocode.setText(promosList.get(position).getPromoTitle() + " " + DrConstants.format.format(highestPrice) + " SR Redeem");
        } else if (language.equalsIgnoreCase("Ar")) {
            promocode.setText(promosList.get(position).getPromoTitleAr() + " " + DrConstants.format.format(highestPrice) + " ريال مجانا ");
        }

        highestPrice = totalPriceInt - highestPrice;
        if (highestPrice == 0) {
            priceCalculation(0);
//            totalAmount.setText("0.00");
//            amount.setText("0.00");
//            float tax = highestPrice*(vat/100);
//            vatAmount.setText("0.00");
//            netTotal.setText("0.00");
            paymentMode = 4;
        } else {
            priceCalculation(myDbHelper.getTotalOrderPrice());
//            amount.setText("" + priceFormat.format((float) highestPrice));
//            float tax = highestPrice*(vat/100);
//            vatAmount.setText(""+priceFormat.format(tax));
//            netTotal.setText(""+priceFormat.format((highestPrice+tax)));
//            totalAmount.setText("" + priceFormat.format((highestPrice+tax)));
        }
        promoLayout.setClickable(false);
        promoArrow.setVisibility(View.GONE);
        promoCancel.setVisibility(View.VISIBLE);
    }

    public void isHighestPriceNo() {
        promocodeStr = "";
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        if (language.equalsIgnoreCase("En")) {
            promocode.setHint("Apply Promotion");
            // set title
            alertDialogBuilder.setTitle("dr.CAFE");

            // set dialog message
            alertDialogBuilder
                    .setMessage(promosList.get(position).getMsg_En())
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
        } else if (language.equalsIgnoreCase("Ar")) {
            // set title
            promocode.setHint("قدم للعرض");
            alertDialogBuilder.setTitle("د. كيف");

            // set dialog message
            alertDialogBuilder
                    .setMessage(promosList.get(position).getMsg_Ar())
                    .setCancelable(false)
                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
        }
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        promoIdStr = "";
        promoTypeStr = "";
        dupId = "0";
//        float totalPriceInt = myDbHelper.getTotalOrderPrice();
        priceCalculation(myDbHelper.getTotalOrderPrice());
//        amount.setText("" + priceFormat.format((float) totalPriceInt));
//        float tax = totalPriceInt*(vat/100);
//        vatAmount.setText(""+priceFormat.format(tax));
//        netTotal.setText(""+priceFormat.format((totalPriceInt+tax)));
//        totalAmount.setText("" + priceFormat.format((totalPriceInt+tax)));
//                                paymentMode = 3;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CheckoutActivity.CHECKOUT_ACTIVITY) {
            switch (resultCode) {
                case CheckoutActivity.RESULT_OK:
                    /* Transaction completed. */
                    Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);

                    resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);
                    Log.d("TAG", "onActivityResult: "+resourcePath);

                    /* Check the transaction type. */
                    if (transaction.getTransactionType() == TransactionType.SYNC) {
                        /* Check the status of synchronous transaction. */
                        requestPaymentStatus(resourcePath);
                    } else {
                        /* The on onNewIntent method may be called before onActivityResult
                           if activity was destroyed in the background, so check
                           if the intent already has the callback scheme */

                            if (resourcePath != null) {
                                requestPaymentStatus(resourcePath);
                            } else {
                                /* The on onNewIntent method wasn't called yet,
                                   wait for the callback. */
                                showProgressDialog(R.string.progress_message_please_wait);
                            }
//                            if (hasCallbackScheme(getActivity().getIntent())) {
//                                requestPaymentStatus(resourcePath);
//                            } else {
//                                /* The on onNewIntent method wasn't called yet,
//                                   wait for the callback. */
//                                showProgressDialog(R.string.progress_message_please_wait);
//                            }
                    }

                    break;
                case CheckoutActivity.RESULT_CANCELED:
                    hideProgressDialog();
                    confirmOrder.setEnabled(true);

                    break;
                case CheckoutActivity.RESULT_ERROR:
                    showAlertDialog(R.string.error_message);
                    PaymentError error = data.getParcelableExtra(
                            CheckoutActivity.CHECKOUT_RESULT_ERROR);
                    Log.d("TAG", "onActivityResult: error " + error.getErrorInfo());
                    Log.d("TAG", "onActivityResult: error " + error.getErrorMessage());
                    Log.d("TAG", "onActivityResult: error " + error.getErrorCode());
                    confirmOrder.setEnabled(true);
            }
        }
    }

    protected boolean hasCallbackScheme(Intent intent) {
        String scheme = intent.getScheme();

        Log.d("TAG", "hasCallbackScheme: "+scheme);
        return "com.creativesols.drcafe.payments".equals(scheme + "://result");
    }

    @Override
    public void onStart() {
        super.onStart();

        Intent intent = new Intent(getContext(), ConnectService.class);

        getContext().startService(intent);
        getContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();

        getContext().unbindService(serviceConnection);
        getContext().stopService(new Intent(getContext(), ConnectService.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putString(STATE_RESOURCE_PATH, resourcePath);
    }

    protected void onServiceConnected(IBinder service) {
        providerBinder = (IProviderBinder) service;

        /* We have a connection to the service. */
        try {
            /* Initialize Payment Provider with test mode. */
            providerBinder.initializeProvider(Connect.ProviderMode.LIVE);
        } catch (PaymentException e) {
            showAlertDialog(R.string.error_message);
        }
    }

    protected void requestCheckoutId() {
        showProgressDialog(R.string.progress_message_checkout_id);
        String total_amt = DrConstants.convertToArabic(totalAmount.getText().toString().replace(" SR", ""));

        String address = getAddress();

        new CheckoutIdRequestAsyncTask(this)
                .execute(DrConstants.format.format(Float.parseFloat(total_amt)), DrConstants.SHOPPER_RESULT_URL, "false",
                        merchantId, userEmail, userId, userPhone, userFullName, addressLine, locality, stateName, countryName, postalCode);
    }

    @Override
    public void onCheckoutIdReceived(String checkoutId) {
        hideProgressDialog();

        if (checkoutId == null) {
            showAlertDialog(R.string.error_message);
            confirmOrder.setEnabled(true);
        }

        if (checkoutId != null) {
            openCheckoutUI(checkoutId);
        }
    }

    private void openCheckoutUI(String checkoutId) {
        CheckoutSettings checkoutSettings = createCheckoutSettings(checkoutId);

        ComponentName componentName = new ComponentName(
                getContext().getPackageName(), CheckoutBroadcastReceiver.class.getName());

        /* Set up the Intent and start the checkout activity. */

        Intent intent = checkoutSettings.createCheckoutActivityIntent(getContext(), componentName);

        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);
    }

    @Override
    public void onErrorOccurred() {
        hideProgressDialog();
        showAlertDialog(R.string.error_message);
    }

    @Override
    public void onPaymentStatusReceived(MyCards paymentStatus) {
        hideProgressDialog();
        myRegistrationCardDetails = paymentStatus;
        if ("OK".equals(paymentStatus.getData().get(0).getCustomPayment())) {
            new InsertOrder().execute();
            return;
        } else {
            showAlertDialog(R.string.message_unsuccessful_payment);
            confirmOrder.setEnabled(true);
        }
    }

    protected void requestPaymentStatus(String resourcePath) {
        showProgressDialog(R.string.progress_message_payment_status);
        Log.d("TAG", "requestPaymentStatus: " + resourcePath);
        new PaymentStatusRequestAsyncTask(this).execute(resourcePath);
    }

    /**
     * Creates the new instance of {@link CheckoutSettings}
     * to instantiate the {@link CheckoutActivity}.
     *
     * @param checkoutId the received checkout id
     * @return the new instance of {@link CheckoutSettings}
     */
    protected CheckoutSettings createCheckoutSettings(String checkoutId) {
        return new CheckoutSettings(checkoutId, DrConstants.Config.PAYMENT_BRANDS,
                Connect.ProviderMode.LIVE)
                .setStorePaymentDetailsMode(CheckoutStorePaymentDetailsMode.PROMPT)
                .setSkipCVVMode(CheckoutSkipCVVMode.FOR_STORED_CARDS)
                .setWebViewEnabledFor3DSecure(true)
                .setWindowSecurityEnabled(false)
                .setShopperResultUrl(DrConstants.SHOPPER_RESULT_URL);
    }

    protected void showProgressDialog(int messageId) {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setCancelable(false);
        }

        progressDialog.setMessage(getString(messageId));
        progressDialog.show();
    }

    protected void hideProgressDialog() {
        if (progressDialog == null) {
            return;
        }

        progressDialog.dismiss();
    }

    protected void showAlertDialog(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, null)
                .setCancelable(false)
                .show();
    }

    protected void showAlertDialog(int messageId) {
        showAlertDialog(getString(messageId));
    }

    public void generateMerchantId() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("ddMMyyyy", Locale.US);
        SimpleDateFormat timeFormater = new SimpleDateFormat("HHmm", Locale.US);
        SimpleDateFormat timeFormater1 = new SimpleDateFormat("hh:mm a", Locale.US);

        String estimatedTime = estTime.getText().toString();
        Date estimatedT = null;
        try {
            estimatedT = timeFormater1.parse(estimatedTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String timeStr = timeFormater.format(estimatedT);
//        String dateStr = dateFormater.format(todayDate1);

        merchantId = storeId + todayDate1 + userId + timeStr;
        Log.d("TAG", "generateMerchantId: " + merchantId);

        String response = userPrefs.getString("user_profile", null);
        if (response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                String parts[] = userObjuect.getString("phone").split("-");

                String countryCode = parts[0].replace("+", "");
                String mobile = parts[1];

                customerEmailId = countryCode + mobile + "@drcafe.com";

                Log.d("TAG", "generateMerchantId: " + customerEmailId);


            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }

        try {
            requestCheckoutId();
        } catch (Exception e) {
            Log.e("connect", "error creating the payment page", e);
        }
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        new GetCurrentTime().execute();
                        new GetTimes().execute();
                    }
                });
            }
        }
    }

    private class MyTimerTask1 extends TimerTask {
        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (min < 4) {
                            min = min + 1;
                            getTimeCaluculation(min);
                        } else {
                            min = 0;
                        }
                    }
                });
            }
        }
    }

    public class GetTimes extends AsyncTask<Void, String, String> {
        ProgressDialog pd;
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("ddMMyyyy", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        String networkStatus;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ctx);
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            if (flag) {
                pd.show();
            }
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                String res = getSourceCode("http://csadms.com/UtilityServices/api/CommonServices/GetPrayerTimings?CityId=1");
                Log.d("Responce", "" + res);
                return res;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s == null || s.equals("")) {
                if (flag) {
                    pd.dismiss();
                }
                new GetTimes().execute();
            } else {
                if (s.equalsIgnoreCase("no internet")) {
                    if (flag) {
                        pd.dismiss();
                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    listTimes.clear();
                    try {
                        JSONObject resObj = new JSONObject(s);
                        JSONArray array = resObj.getJSONArray("Data");
//                    String fajr = array.getJSONObject(0).getString("Fajr");
//                    String Dhuhr = array.getJSONObject(0).getString("Dhuhr");
//                    String Asr = array.getJSONObject(0).getString("Asr");
//                    String Maghrib = array.getJSONObject(0).getString("Maghrib");
//                    String Isha = array.getJSONObject(0).getString("Isha");
//
//                    try {
//                        Date d1 = dateFormat1.parse(fajr);
//                        Date d2 = dateFormat1.parse(Dhuhr);
//                        Date d3 = dateFormat1.parse(Asr);
//                        Date d4 = dateFormat1.parse(Maghrib);
//                        Date d5 = dateFormat1.parse(Isha);
//
//                        fajr = dateFormat.format(d1);
//                        Dhuhr = dateFormat.format(d2);
//                        Asr = dateFormat.format(d3);
//                        Maghrib = dateFormat.format(d4);
//                        Isha = dateFormat.format(d5);
//                        Log.d("sudheer", "onPostExecute: "+fajr);
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }

//                        listTimes.add(doParse(array.getJSONObject(0).getString("Fajr")));
//                        listTimes.add(doParse(array.getJSONObject(0).getString("Dhuhr")));
//                        listTimes.add(doParse(array.getJSONObject(0).getString("Asr")));
//                        listTimes.add(doParse(array.getJSONObject(0).getString("Maghrib")));
//                        listTimes.add(doParse(array.getJSONObject(0).getString("Isha")));
                        timeResponse = array.getJSONObject(0).getString("KsaTime");

                        String[] resp = timeResponse.split("T");
//                    String[] time = resp[1].split(".");
//                    Log.d("sudheer", "resp: "+time[0]);
                        resp[0] = resp[0].replace("-", "/");
                        timeResponse = resp[0] + " " + resp[1];

                        Date d1 = null;
                        Date d2 = null;
                        try {
                            d1 = timeFormat.parse(timeResponse);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        todayDate = timeFormat1.format(d1);
                        todayDate1 = timeFormat2.format(d1);
                        Calendar prayerEnd = Calendar.getInstance();
                        prayerEnd.setTime(d1);
                        prayerEnd.add(Calendar.DATE, 1);
                        d2 = prayerEnd.getTime();
                        tomorrowDate = timeFormat1.format(d2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    min = 0;
                    getTimeCaluculation(0);

                    if (flag) {
                        timer1.schedule(new MyTimerTask1(), 60000, 60000);
                        pd.dismiss();
                        flag = false;
                    }
                }

                super.onPostExecute(s);
            }
        }
    }

    public class GetPromocodeResponse extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            promosList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {

            try {

                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                } else {
                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("Userid", arg0[0]);
                    request.addProperty("DeviceToken", SplashScreen.regid);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result);
                }
            } catch (Exception e) {
                e.printStackTrace();
//                Toast.makeText(getActivity(), "Cannot reach server!", Toast.LENGTH_SHORT).show();
            }
            Log.d("Responce", "" + response12);

            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            promoFlag = false;
            if (response12 == null) {
                dialog.dismiss();

            } else if (response12.equalsIgnoreCase("no internet")) {
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Communication Error! Please check the internet connection?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            } else {
                dialog.dismiss();
                try {

                    Object property = result.getProperty(1);
                    if (property instanceof SoapObject) {
                        try {
                            SoapObject category_list = (SoapObject) property;
                            if (category_list.getPropertyCount() > 0) {
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    Promos prom = new Promos();
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                    String promoId = sObj.getPropertyAsString("PromoID");
                                    String promoCode = sObj.getPropertyAsString("PromoCode");
                                    String percentage = sObj.getPropertyAsString("Percentage");
                                    String desc = sObj.getPropertyAsString("Desc_En");
                                    String descAr = sObj.getPropertyAsString("Desc_Ar");
                                    String Msg_En = sObj.getPropertyAsString("Msg_En");
                                    String Msg_Ar = sObj.getPropertyAsString("Msg_Ar");
                                    String promoTitleAr = sObj.getPropertyAsString("PromoTitle_Ar");
                                    String promoTitle = sObj.getPropertyAsString("PromoTitle_En");
                                    String CategoryId = sObj.getPropertyAsString("CategoryId");
                                    String SubCategoryId = sObj.getPropertyAsString("SubCategoryId");
                                    String itemId = sObj.getPropertyAsString("ItemId");
                                    String storeId = sObj.getPropertyAsString("StoreId");
                                    String additonals = sObj.getPropertyAsString("Additonals");
                                    String size = sObj.getPropertyAsString("Size");
                                    String limit = sObj.getPropertyAsString("Limit");
                                    String promoType = sObj.getPropertyAsString("PromoType");
                                    String remainingBonus = sObj.getPropertyAsString("RemainingBonus");
                                    String PEndDate = sObj.getPropertyAsString("PEndDate");
                                    String DailyOnce = sObj.getPropertyAsString("DailyOnce");
                                    String ValidDays = sObj.getPropertyAsString("ValidDays");
                                    String ValidForDays = sObj.getPropertyAsString("ValidForDays");
                                    String dupId = sObj.getPropertyAsString("dupId");

                                    prom.setPromoID(promoId);
                                    prom.setPromoCode(promoCode);
                                    prom.setPercentage(percentage);
                                    prom.setDescription(desc);
                                    prom.setDescriptionAr(descAr);
                                    prom.setMsg_En(Msg_En);
                                    prom.setMsg_Ar(Msg_Ar);
                                    prom.setPromoID(promoId);
                                    prom.setPromoCode(promoCode);
                                    prom.setPromoTitle(promoTitle);
                                    prom.setPromoTitleAr(promoTitleAr);
                                    prom.setCategoryID(CategoryId);
                                    prom.setSubCategoryId(SubCategoryId);
                                    prom.setItemId(itemId);
                                    prom.setStoreId(storeId);
                                    prom.setAdditionals(additonals);
                                    prom.setSize(size);
                                    prom.setLimit(limit);
                                    prom.setPromoType(promoType);
                                    prom.setRemainingBonus(remainingBonus);
                                    prom.setEndDate(PEndDate);
                                    prom.setDailyOnce(DailyOnce);
                                    prom.setValidDays(ValidDays);
                                    prom.setValidForDays(ValidForDays);
                                    prom.setDupId(dupId);

                                    promosList.add(prom);
                                }
                                dialog();
                            } else {
                                Toast.makeText(getActivity(), "Sorry! No active promotions", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    }

                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }
            }

            mPromoAdapter.notifyDataSetChanged();


            super.onPostExecute(result1);
        }
    }

    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Calculating time...");
//            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                //24.70321657, 46.68097073
//                distanceResponse = jParser
//                        .getJSONFromUrl(URL_DISTANCE + "24.70321657,46.68097073&destinations="+ latitude +","+ longitude+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + lat + "," + longi + "&destinations=" + latitude + "," + longitude + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        secs = jo3.getString("value");
                        trafficMins = Integer.parseInt(secs) / 60;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dialog.dismiss();
                    new GetTimes().execute();
                }
            } else {
                dialog.dismiss();
                new GetTimes().execute();
            }
//            new GetCurrentTime().execute();
            super.onPostExecute(result);
        }
    }

    public class InsertOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        String currentTime;
        String estimatedTime, total_amt;

        String itemId = "", qty = "", comments = "", total_price = null, item_price = "", sizes = "", additionals = "", additionalsPices = "";

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please wait...");
//            promocodeStr = promocode.getText().toString();
//            if(promocodeStr.length()!=5){
//                promocodeStr = "No";
//            }

            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());

            SimpleDateFormat df3 = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
            currentTime = df3.format(c.getTime());
            estimatedTime = estTime.getText().toString().replace("am", "AM").replace("pm", "PM");
            total_amt = DrConstants.convertToArabic(totalAmount.getText().toString().replace(" SR", ""));
            vatPrice = DrConstants.convertToArabic(vatAmount.getText().toString().replace("+ ", ""));
            subTotal = DrConstants.convertToArabic(amount.getText().toString());

            if (total_amt.equalsIgnoreCase("0")) {
                paymentMode = 4;
            }

            for (Order order : orderList) {
                if (itemId == "") {
                    itemId = order.getItemId();
                    qty = order.getQty();
                    comments = order.getComment();
                    item_price = DrConstants.format.format(Float.parseFloat(DrConstants.convertToArabic(myDbHelper.getPrice(order.getItemId(), order.getItemTypeId()))));
                    ;
                    sizes = order.getItemTypeId();
                    additionals = order.getAdditionalsTypeId();
                    String[] addls = order.getAdditionalsPrice().split(",");
                    String newSrt = "";
                    for (int i = 0; i < addls.length; i++) {
                        if (i == 0) {
                            newSrt = DrConstants.convertToArabic(addls[i]);
                        } else {
                            newSrt = newSrt + "," + DrConstants.convertToArabic(addls[i]);
                        }
                    }
                    additionalsPices = newSrt;
                } else {
                    itemId = itemId + "|" + order.getItemId();
                    qty = qty + "|" + order.getQty();
                    comments = comments + "|" + order.getComment();
                    item_price = item_price + "|" + DrConstants.format.format(Float.parseFloat(DrConstants.convertToArabic(myDbHelper.getPrice(order.getItemId(), order.getItemTypeId()))));
                    sizes = sizes + "|" + order.getItemTypeId();
                    additionals = additionals + "|" + order.getAdditionalsTypeId();
                    String[] addls = order.getAdditionalsPrice().split(",");
                    String newSrt = "";
                    for (int i = 0; i < addls.length; i++) {
                        if (i == 0) {
                            newSrt = DrConstants.convertToArabic(addls[i]);
                        } else {
                            newSrt = newSrt + "," + DrConstants.convertToArabic(addls[i]);
                        }
                    }
                    additionalsPices = additionalsPices + "|" + newSrt;
                }
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    String version = "";
                    try {
                        PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
                        version = pInfo.versionName;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    if(SplashScreen.regid == null){
//                        SplashScreen.regid = "-1";
//                    }

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_INSERT);
                    request.addProperty("userId", userId);
                    request.addProperty("storeId", storeId);
                    request.addProperty("comments", "Android v" + version);
                    request.addProperty("date_time", currentTime);
                    request.addProperty("type", orderType);
                    request.addProperty("expectedTime", estimatedTime);
                    request.addProperty("status", "1");
                    request.addProperty("itemId", itemId);
                    request.addProperty("qty", qty);
                    request.addProperty("icomments", comments);
                    request.addProperty("OrderStatus", "New");
                    request.addProperty("tot_price", total_amt);
                    request.addProperty("device_token", SplashScreen.regid);
                    request.addProperty("item_price", DrConstants.convertToArabic(item_price));
                    request.addProperty("sizes", sizes);
                    request.addProperty("additionals", additionals);
                    request.addProperty("additionalsPrices", additionalsPices);
                    request.addProperty("isFavorite", "0");
                    request.addProperty("FavoriteName", "");
                    request.addProperty("promotionCode", promocodeStr);
                    request.addProperty("PaymentMode", paymentMode);
                    request.addProperty("BonusRemain", DrConstants.convertToArabic(DrConstants.format.format(remainingBonusInt)));
                    request.addProperty("PromoId", promoIdStr);
                    request.addProperty("PromoType", promoTypeStr);
                    request.addProperty("dupId", dupId);
//                    request.addProperty("WalletCharges", "0");
                    request.addProperty("CouponQty", Integer.toString(applyPromoQtyInt));
                    request.addProperty("subTotal", subTotal);
                    request.addProperty("vat", vatPrice);
                    request.addProperty("vatPercentage", DrConstants.vatString);
                    request.addProperty("DeliveryCharges", "0.00");
                    if (DrConstants.orderType.equalsIgnoreCase("Delivery")) {
                        request.addProperty("addressId", DrConstants.addressId);
                    } else {
                        request.addProperty("addressId", 0);
                    }

                    Log.i("TAG", "request " + request.toString());
                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_INSERT, envelope);
                    insertResult = (SoapPrimitive) envelope.getResponse();
                    insertResponse = insertResult.toString();
                    Log.i("TAG", "insertResponse " + insertResponse);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + insertResult);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("TAG", "Response: " + insertResponse);
            } else {
                return "no internet";
            }
            return insertResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    if (insertResponse.equalsIgnoreCase("failed Order")) {
                        dialog.dismiss();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Promotion Already Used")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    } else if (insertResponse.equalsIgnoreCase("user blocked")) {
                        dialog.dismiss();
                        String msg;
                        if (language.equalsIgnoreCase("En")) {
                            msg = "User Account Blocked, Please Contact Customer Care (800-122-8222).";
                        } else {
                            msg = "حساب المستخدم تم ايقافه ، من فضلك تواصل مع خدمة العملاء (8222 122-800).";
                        }
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(msg)
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    } else if (insertResponse == null) {
                        dialog.dismiss();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Communication Error! Please check the internet connection?")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    } else {
                        DrConstants.orderNow = false;
                        myDbHelper.deleteOrderTable();
                        try {
                            OrderCheckoutFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice());
                            OrderCheckoutFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                            MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                            CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                            CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        try {
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    orderPrefsEditor.putString("order_status", "close");
                                    orderPrefsEditor.commit();
                                    // This method will be executed once the timer is over
                                    MainFragment.trackBanner.setVisibility(View.GONE);
                                    MainFragment.cupImg.setVisibility(View.GONE);
                                }
                            }, Integer.parseInt(secs));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        DrConstants.IFMAdded = false;
                        ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
                        dialog.dismiss();

                        Fragment menuFragment = new OrderDetails();
                        // consider using Java coding conventions (upper first char class names!!!)
                        FragmentTransaction menuTransaction = getFragmentManager().beginTransaction();
                        Bundle mBundle = new Bundle();
                        mBundle.putString("storeId", storeId);
                        if (orderType.equalsIgnoreCase("Delivery")) {
                            mBundle.putString("storeName", DrConstants.addressHouseNo);
                            mBundle.putString("storeAddress", DrConstants.address);
                        } else {
                            mBundle.putString("storeName", storeName);
                            mBundle.putString("storeAddress", storeAddress);
                        }
                        mBundle.putDouble("latitude", latitude);
                        mBundle.putDouble("longitude", longitude);
                        mBundle.putString("total_amt", totalAmount.getText().toString());
                        mBundle.putString("sub_total", amount.getText().toString());
                        mBundle.putString("vat", vatAmount.getText().toString());
                        mBundle.putString("promoAmt", promotionAmount.getText().toString().replace("- ", ""));
                        mBundle.putString("subTotalAmount", subTotalAmount.getText().toString());
                        mBundle.putString("total_items", totalItems.getText().toString());
                        mBundle.putString("expected_time", estTime.getText().toString());
                        mBundle.putString("payment_mode", Integer.toString(paymentMode));
                        mBundle.putString("order_type", orderType);
                        mBundle.putString("order_number", insertResponse);
                        menuFragment.setArguments(mBundle);

                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack

                        menuTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        menuTransaction.add(R.id.realtabcontent, menuFragment, "order_details");
                        menuTransaction.hide(OrderConfimationFragment.this);
                        menuTransaction.addToBackStack(null);

                        // Commit the transaction
                        menuTransaction.commit();
                    }
                }
            } else {
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Cannot reach server")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
            super.onPostExecute(result1);
        }
    }

    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("ddMMyyyy", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if (flag) {
                dialog = ProgressDialog.show(getActivity(), "",
                        "Please Wait....");
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_TIME);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_TIME, envelope);
                    timeResult = (SoapPrimitive) envelope.getResponse();
                    timeResponse = timeResult.toString();
                    Log.v("TAG", timeResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + timeResponse);
            } else {
                timeResponse = "no internet";
            }
            return timeResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (timeResponse == null) {
                if (flag) {
                    dialog.dismiss();
                }
            } else if (timeResponse.equals("no internet")) {
                if (flag) {
                    dialog.dismiss();
                }
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();

            } else {

                Log.i("TIME IF TAG", "" + flag);
                if (flag) {
                    dialog.dismiss();
                    Log.i("TIME TAG", "" + flag);
                    Date d1 = null;
                    Date d2 = null;
                    try {
                        d1 = timeFormat.parse(timeResponse);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    todayDate = timeFormat1.format(d1);
                    todayDate1 = timeFormat2.format(d1);
                    Calendar prayerEnd = Calendar.getInstance();
                    prayerEnd.setTime(d1);
                    prayerEnd.add(Calendar.DATE, 1);
                    d2 = prayerEnd.getTime();
                    tomorrowDate = timeFormat1.format(d2);
                    Log.i("DATE TAG TTTT", todayDate + "  " + tomorrowDate);

                    new GetTimes().execute();
                    flag = false;
                    Log.i("TIME IFINSIDE TAG", "" + flag);
                } else {
                    getTimeCaluculation(0);
                }
            }


            super.onPostExecute(result1);
        }
    }

    public class GetStoredCards extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_GETCARD);
                    request.addProperty("userId", userId);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_GETCARD, envelope);
                    setCardResult = (SoapPrimitive) envelope.getResponse();
                    getCardResponse = setCardResult.toString();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("TAGD", "" + getCardResponse);
            } else {
                getCardResponse = "no internet";
            }
            return getCardResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (dialog != null) {
                dialog.dismiss();
            }
            if (getCardResponse == null) {

            } else if (getCardResponse.equals("no internet")) {
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                regsiteredCardDetails = getCardResponse;
                super.onPostExecute(result1);
            }

            try {
                requestCheckoutId();
            } catch (Exception e) {
                Log.e("connect", "error creating the payment page", e);
            }
        }
    }

    public String getAddress() {
        List<Address> addresses = getGeocoderAddress(getContext());
        if (addresses != null && addresses.size() > 0) {
            Address address = addresses.get(0);
            addressLine = address.getSubLocality();
            locality = address.getLocality();
            postalCode = address.getPostalCode();
            countryName = address.getCountryName();
            stateName = address.getSubAdminArea();
            return addressLine;
        }
        else {
            return "";
        }
    }

    /**
     * Get list of address by latitude and longitude
     *
     * @return null or List<Address>
     */
    public List<Address> getGeocoderAddress(Context context) {
//        if (location != null) {
            Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
            try {
                List<Address> addresses = geocoder.getFromLocation(latitude,
                        longitude, 1);
                return addresses;
            } catch (IOException e) {
                 e.printStackTrace();
            }
//        }
        return null;
    }
}
