package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.IntroActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.SplashScreen;
import com.cs.drcafe.VerifyRandomNumber;
import com.cs.drcafe.adapter.CountriesAdapter;
import com.cs.drcafe.adapter.ListViewCustomAdapter;
import com.cs.drcafe.model.Countries;
import com.cs.drcafe.model.FragmentUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by SKT on 29-12-2015.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener{
    private String SOAP_ACTION = "http://tempuri.org/UpdateUserInfo";
    private String METHOD_NAME = "UpdateUserInfo";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    ProgressDialog dialog;

    ListView lview3;
    ListViewCustomAdapter adapter;
    private ArrayList<Countries> countriesList = new ArrayList<>();
    private Countries countries;
    private CountriesAdapter mCountriesAdapter;

    private LinearLayout mCountryPicker;
    private ImageView mCountryFlag;
    private TextView mGenderMale, mGenderFemale, mLogout;
    private EditText mCountryCode, mFirstName, mFamilyName, mNickName, mEmail, mMobileNumber;
    private String firstName, familyName,nickName, countryCode = "+966", phoneNumber, email, gender = "Male", userId;
    private int phoneNumberLength;
    int countryFlag;

    Button backBtn, submit, changePassword;
    SharedPreferences userPrefs;
    SharedPreferences.Editor  userPrefEditor;
    String response;
    SoapObject obj;
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.myprofile_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.myprofile_layout_arabic, container,
                    false);
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        response = userPrefs.getString("user_profile", null);

        Log.i("TAG", ""+response);

        prepareArrayLits();
        mCountriesAdapter = new CountriesAdapter(getActivity(),countriesList);
        mCountryPicker = (LinearLayout) rootView.findViewById(R.id.country_picker);
        mCountryFlag = (ImageView) rootView.findViewById(R.id.country_flag);
        mCountryCode = (EditText) rootView.findViewById(R.id.country_code);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        mFirstName = (EditText) rootView.findViewById(R.id.first_name);
        mFamilyName = (EditText) rootView.findViewById(R.id.family_name);
        mNickName = (EditText) rootView.findViewById(R.id.nick_name);
        mEmail = (EditText) rootView.findViewById(R.id.email_id);
        mMobileNumber = (EditText) rootView.findViewById(R.id.mobile_number);
        mGenderMale = (TextView) rootView.findViewById(R.id.gender_male);
        mGenderFemale = (TextView) rootView.findViewById(R.id.gender_female);
        mLogout = (TextView) rootView.findViewById(R.id.logout);
        submit = (Button) rootView.findViewById(R.id.submit);
        changePassword = (Button) rootView.findViewById(R.id.change_password);

//        mFirstName = (EditText) rootView.findViewById(R.id.first_name);



        if(response != null) {
            try {


                JSONObject  property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                String parts[] = userObjuect.getString("phone").split("-");

                userId = userObjuect.getString("userId");
                mFirstName.setText(userObjuect.getString("fullName"));
                mFamilyName.setText(userObjuect.getString("FamilyName"));
                mNickName.setText(userObjuect.getString("NickName"));
                mEmail.setText(userObjuect.getString("username"));

                mCountryCode.setText(parts[0]);
                mMobileNumber.setText(parts[1]);
                countryFlag = userObjuect.getInt("country_flag");
                Log.i("TEST","flag is "+countryFlag);
                mCountryFlag.setImageResource(countryFlag);


            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
        if(countryCode.equalsIgnoreCase("+966")){
            int maxLength = 9;
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(maxLength);
            mMobileNumber.setFilters(fArray);
            phoneNumberLength = 9;
        }

        mMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = mMobileNumber.getText().toString();
                if(!mMobileNumber.hasWindowFocus() || mMobileNumber.hasFocus() || s == null){
                    if(countryCode.equalsIgnoreCase("+966")){
                        if(text.startsWith("0")){
                            mMobileNumber.setText("");
                            return;
                        }
                    }
                    return;
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mLogout.setOnClickListener(this);
        backBtn.setOnClickListener(this);
//        mCountryPicker.setOnClickListener(this);
        mGenderMale.setOnClickListener(this);
        mGenderFemale.setOnClickListener(this);
        submit.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                getActivity().onBackPressed();
                break;
            case R.id.country_picker:
                dialog();
                break;
            case R.id.gender_male:
                gender = "Male";
                mGenderMale.setBackgroundColor(Color.parseColor("#00000000"));
                mGenderFemale.setBackgroundColor(Color.parseColor("#F1F1F1"));
                break;
            case R.id.gender_female:
                gender = "Female";
                mGenderMale.setBackgroundColor(Color.parseColor("#F1F1F1"));
                mGenderFemale.setBackgroundColor(Color.parseColor("#00000000"));
                break;
            case R.id.change_password:
                Fragment menuFragment = new ChangePasswordFragment();
//                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction menuTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                menuTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                menuTransaction.add(R.id.realtabcontent, menuFragment);
                menuTransaction.hide(MyProfileFragment.this);
                menuTransaction.addToBackStack(null);

                // Commit the transaction
                menuTransaction.commit();
                break;
            case R.id.logout:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());





                if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Do you want to logout from this account?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    userPrefEditor.clear();
                                    userPrefEditor.commit();
                                    Intent introIntent = new Intent(getActivity(), IntroActivity.class);
                                    getActivity().startActivity(introIntent);
                                    getActivity().finish();
                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }else if(language.equalsIgnoreCase("Ar")){
                    // set title
                    alertDialogBuilder.setTitle("د. كيف");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("هل تود الخروج من هذا الحساب")
                            .setCancelable(false)
                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    userPrefEditor.clear();
                                    userPrefEditor.commit();
                                    Intent introIntent = new Intent(getActivity(), IntroActivity.class);
                                    getActivity().startActivity(introIntent);
                                    getActivity().finish();
                                }
                            }).setNegativeButton("لا", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }


                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

                break;
            case R.id.submit:
                Log.i("TAG",""+phoneNumberLength);
                firstName = mFirstName.getText().toString();
                familyName = mFamilyName.getText().toString();
                nickName = mNickName.getText().toString();
                phoneNumber = mMobileNumber.getText().toString();
                email = mEmail.getText().toString();
                if(firstName.length() == 0){
                    mFirstName.setError("Please enter First Name");
                }else if(phoneNumber.length() == 0){
                    mMobileNumber.setError("Please enter Mobile Number");
                }else if(phoneNumber.length() != phoneNumberLength){
                    mMobileNumber.setError("Please enter valid Mobile Number");
                }else if(email.length() == 0){
                    mEmail.setError("Please enter Email");
                }else if(!email.matches("[a-zA-Z0-9._-]+@[a-z-]+\\.+[a-z]+")){
                    mEmail.setError("Please use Email format (example - abc@abc.com");
                }else{
                    new UpdateUserProfile().execute();
                }


        }
    }



    public class UpdateUserProfile extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("UserId", userId);
                    request.addProperty("countryId", 190);
                    request.addProperty("fullName", firstName);
                    request.addProperty("fullAddress", "riydh");
                    request.addProperty("email", email);
                    request.addProperty("phone", countryCode+"-"+phoneNumber);
                    request.addProperty("mobile", countryCode+"-"+phoneNumber);
                    request.addProperty("NickName", nickName);
                    request.addProperty("Gender", gender);
                    request.addProperty("DeviceToken", SplashScreen.regid);
                    request.addProperty("FamilyName", familyName);



                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    if (response12.equals("anyType{}")) {
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                    } else {

                        try {

                            Object property = result.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject loginObject = (SoapObject) property;
                                    SoapObject userObject = (SoapObject) loginObject.getProperty(0);
                                    SoapObject userObject1 = (SoapObject) userObject.getProperty(0);
                                    String userId = userObject1.getPropertyAsString("userId");
                                    Log.i("TAG", "" + userId);
                                    if (userId.equalsIgnoreCase("0")) {
                                        dialog.dismiss();
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                                        // set title
                                        alertDialogBuilder.setTitle("dr.CAFE");

                                        // set dialog message
                                        alertDialogBuilder
                                                .setMessage("Invalid email or password")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                });

                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();

                                        // show it
                                        alertDialog.show();
                                    } else {

                                        String fullName = userObject1.getPropertyAsString("fullName");
                                        String fullAddress = userObject1.getPropertyAsString("fullAddress");
                                        String phone = userObject1.getPropertyAsString("phone");
                                        String countryId = userObject1.getPropertyAsString("countryId");
                                        String country = userObject1.getPropertyAsString("country");
                                        String username = userObject1.getPropertyAsString("username");
                                        String mobile = userObject1.getPropertyAsString("mobile");
                                        String NickName = userObject1.getPropertyAsString("NickName");
                                        if(NickName.equalsIgnoreCase("anytype{}")){
                                            NickName = "";
                                        }
                                        String Gender = userObject1.getPropertyAsString("Gender");
                                        String FamilyName = userObject1.getPropertyAsString("FamilyName");
                                        if(FamilyName.equalsIgnoreCase("anytype{}")){
                                            FamilyName = "";
                                        }
                                        boolean isVerified = Boolean.valueOf(userObject1.getPropertyAsString("isVerified"));

                                        try {
                                            JSONObject parent = new JSONObject();
                                            JSONObject jsonObject = new JSONObject();
                                            JSONArray jsonArray = new JSONArray();
                                            jsonArray.put("lv1");
                                            jsonArray.put("lv2");

                                            jsonObject.put("userId", userId);
                                            jsonObject.put("fullName", fullName);
                                            jsonObject.put("phone", phone);
                                            jsonObject.put("mobile", mobile);
                                            jsonObject.put("FamilyName", FamilyName);
                                            jsonObject.put("Gender", Gender);
                                            jsonObject.put("country", country);
                                            jsonObject.put("NickName", NickName);
                                            jsonObject.put("username", username);
                                            jsonObject.put("country_flag", countryFlag);
                                            jsonObject.put("user_details", jsonArray);

                                            parent.put("profile", jsonObject);
                                            Log.d("output", parent.toString());
                                            userPrefEditor.putString("user_profile", parent.toString());

                                            userPrefEditor.commit();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        dialog.dismiss();


                                        if(isVerified) {
                                            Toast.makeText(getActivity(),"Profile updated successfully",Toast.LENGTH_SHORT).show();

                                        }else{
                                            userPrefEditor.putString("login_status","not verified");
                                            userPrefEditor.commit();
                                            Intent loginIntent = new Intent(getActivity(), VerifyRandomNumber.class);
                                            loginIntent.putExtra("phone_number", mobile);
                                            loginIntent.putExtra("country_flag", countryFlag);
                                            loginIntent.putExtra("userId", userId);
                                            startActivity(loginIntent);
                                            getActivity().finish();
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }



                    }
                }
            }

            super.onPostExecute(result1);
        }
    }



    /* Method used to prepare the ArrayList,
     * Same way, you can also do looping and adding object into the ArrayList.
     */
    public void prepareArrayLits()
    {

        AddObjectToList(R.drawable.bh, "Bahrain", "+973", "18");
        AddObjectToList(R.drawable.bd, "Bangladesh", "+880", "19");
        AddObjectToList(R.drawable.cn, "China", "+86", "45");
        AddObjectToList(R.drawable.cy, "Cyprus", "+537" ,"57");
        AddObjectToList(R.drawable.eg, "Egypt", "+20", "64");
        AddObjectToList(R.drawable.fr, "France", "+33", "74");
        AddObjectToList(R.drawable.de, "Germany", "+49", "81");
        AddObjectToList(R.drawable.in, "India", "+91", "101");
        AddObjectToList(R.drawable.id, "Indonesia", "+62", "102");

        AddObjectToList(R.drawable.ir, "Iran", "+98", "103");
        AddObjectToList(R.drawable.iq, "Iraq", "+964", "104");
        AddObjectToList(R.drawable.jo, "Jordan", "+962", "112");
        AddObjectToList(R.drawable.kw, "Kuwait", "+965", "118");
        AddObjectToList(R.drawable.lb, "Lebanon", "+961", "122");
        AddObjectToList(R.drawable.my, "Malaysia", "+60", "133");
        AddObjectToList(R.drawable.ma, "Morocco", "+212", "147");
        AddObjectToList(R.drawable.np, "Nepal", "+977", "152");
        AddObjectToList(R.drawable.om, "Oman", "+968", "164");

        AddObjectToList(R.drawable.pk, "Pakistan", "+92", "165");
        AddObjectToList(R.drawable.ps, "Palestinian Territories", "", "167");
        AddObjectToList(R.drawable.ph, "Philippines", "+63", "172");
        AddObjectToList(R.drawable.qa, "Qatar", "+974", "177");
        AddObjectToList(R.drawable.sa, "Saudi Arabia", "+966", "190");
        AddObjectToList(R.drawable.sg, "Singapore", "+65", "195");
        AddObjectToList(R.drawable.es, "Spain", "+34", "202");
        AddObjectToList(R.drawable.lk, "Sri Lanka", "+94", "203");
        AddObjectToList(R.drawable.sd, "Sudan", "+249", "204");

        AddObjectToList(R.drawable.tr, "Syria", "+963", "210");
        AddObjectToList(R.drawable.tr, "Turkey", "+90", "221");
        AddObjectToList(R.drawable.ae, "United Arab Emirates", "+971", "227");
        AddObjectToList(R.drawable.gb, "United Kingdom", "+44", "228");
        AddObjectToList(R.drawable.us, "United States", "+1", "229");
        AddObjectToList(R.drawable.ye, "Yemen", "+967", "240");
    }

    // Add one item into the Array List
    public void AddObjectToList(int image, String countryName, String countryCode, String countrrId)
    {
        countries = new Countries();
        countries.setCountryName(countryName);
        countries.setCountryFlag(image);
        countries.setCountryCode(countryCode);
        countries.setCountryID(countrrId);
        countriesList.add(countries);
    }

    public void dialog() {

        final Dialog dialog2 = new Dialog(getActivity());

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.setContentView(R.layout.countries_list);
        dialog2.setCanceledOnTouchOutside(true);
        ListView lv = (ListView) dialog2.findViewById(R.id.countries_list);
        lv.setAdapter(mCountriesAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                countryCode = countriesList.get(arg2).getCountryCode();
                countryFlag = countriesList.get(arg2).getCountryFlag();

                if(countryCode.equalsIgnoreCase("+966")){
                    int maxLength = 9;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mMobileNumber.setFilters(fArray);
                    phoneNumberLength = 9;
                }else{
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mMobileNumber.setFilters(fArray);
                    phoneNumberLength = 10;
                }

                mCountryFlag.setImageResource(countriesList.get(arg2).getCountryFlag());
                mCountryCode.setText("" + countriesList.get(arg2).getCountryCode());
                dialog2.dismiss();

            }
        });


        dialog2.show();

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
