package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.ExpandListAdapter;
import com.cs.drcafe.adapter.ExpandListAdapterArabic;
import com.cs.drcafe.model.Categories;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.StoreInfo;
import com.cs.drcafe.model.SubCategories;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by SKT on 11-01-2016.
 */
public class CategoriesListFragment extends Fragment implements View.OnClickListener{

    private String SOAP_ACTION = "http://tempuri.org/GetSeasonalPromotions";
    private String METHOD_NAME = "GetSeasonalPromotions";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    ProgressDialog dialog;

    public static boolean isValid;
    boolean isTimetoShow;
    public static String sTime, eTime;
    ExpandableListView mExpListView;
    ArrayList<Categories> catList = new ArrayList<>();
    public static ExpandListAdapter mExpAdapter;
    public static ExpandListAdapterArabic mExpAdapterArabic;
    private TextView beveragesTv, v12tv, foodTv, snacksTv, beansTv, marchandiseTv;
    public static TextView orderPrice, orderQuantity, cartCount;
    String catId = "1";
    private DataBaseHelper myDbHelper;
    private int lastExpandedPosition = 0;
    RelativeLayout checkoutLayout;
    LinearLayout checkoutItemsLayout;
    ImageView checkoutImage;
    private LinearLayout addMoreLayout, orderNowLayout;
    int expandGrp;
    Button backBtn;
    View rootView;
    String language;
    String BEVERAGES, FOODS, BEANS, SNACKS, MERCHANDISE, V12_Coffee;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.categories_liist, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.categories_list_arabic, container,
                    false);
        }
        myDbHelper = new DataBaseHelper(getActivity());
        try {

            myDbHelper.createDataBase();

        } catch (IOException ioe) {


        }

        try {

            myDbHelper.openDataBase();

        } catch (SQLException sqle) {

            throw sqle;

        }

//        Cursor cursor = myDbHelper.getCategoriesList(catId);
//        if (cursor.moveToFirst()) {
//            do {
//                Categories c = new Categories();
//                c.setSubCatId(cursor.getString(0));
//                c.setCatId(cursor.getString(1));
//                c.setSubCat(cursor.getString(2));
//                c.setDescription(cursor.getString(3));
//                c.setImages(cursor.getString(4));
//                c.setStatus(cursor.getString(5));
//                c.setCreateDate(cursor.getString(6));
//                c.setModifyDate(cursor.getString(7));
//                c.setSequenceId(cursor.getString(8));
//                c.setSubCatAr(cursor.getString(9));
//                c.setDescriptionAr(cursor.getString(10));
//
//                c.setChildItems(myDbHelper.getSubCategoriesList(cursor.getString(1), cursor.getString(0)));
//
//                catList.add(c);
//            } while (cursor.moveToNext());
//        }

        if(DrConstants.offerFlag){
            catId = "2";
        }else{
            catId = getArguments().getString("catId", "1");
            expandGrp = getArguments().getInt("openfrom", 0);
        }

        mExpListView = (ExpandableListView) rootView.findViewById(R.id.expandableListView);
        beveragesTv = (TextView) rootView.findViewById(R.id.beverages_text);
        v12tv = (TextView) rootView.findViewById(R.id.v12_text);
        foodTv = (TextView) rootView.findViewById(R.id.food_text);
        snacksTv = (TextView) rootView.findViewById(R.id.snacks_text);
        beansTv = (TextView) rootView.findViewById(R.id.beans_text);
        marchandiseTv = (TextView) rootView.findViewById(R.id.marchandise_text);
        addMoreLayout = (LinearLayout) rootView.findViewById(R.id.addmore_layout);
        orderNowLayout = (LinearLayout) rootView.findViewById(R.id.ordernow_layout);
        orderPrice = (TextView) rootView.findViewById(R.id.order_price);
        orderQuantity = (TextView) rootView.findViewById(R.id.order_quantity);
        cartCount = (TextView) rootView.findViewById(R.id.cart_count);

        checkoutImage = (ImageView) rootView.findViewById(R.id.checkout_image);
        checkoutLayout = (RelativeLayout) rootView.findViewById(R.id.checkout_layout);
        checkoutItemsLayout = (LinearLayout) rootView.findViewById(R.id.linearLayout);
//        mExpListView.setIndicatorBounds(width-GetDipsFromPixel(35), width-GetDipsFromPixel(5));

        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DrConstants.offerFlag){
                    DrConstants.offerFlag = false;
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    trans.remove(new CategoriesListFragment());
                    trans.commit();

                    FragmentManager manager1 = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans1 = manager1.beginTransaction();
                    trans1.remove(new MenuFragment());
                    trans1.commit();

                    manager1.popBackStack();
                    manager.popBackStack();

                    Fragment radiusFragment = new MenuFragment();
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                    radiusTransaction.hide(CategoriesListFragment.this);
                    radiusTransaction.addToBackStack(null);

                    // Commit the transaction
                    radiusTransaction.commit();
                }else {
                    getActivity().onBackPressed();
                }
            }
        });


        ViewTreeObserver vto = checkoutImage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                checkoutImage.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = checkoutImage.getMeasuredHeight();
                Log.i("Height TAG", "" + finalHeight);
                LinearLayout.LayoutParams newsCountParam = new
                        LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                RelativeLayout.LayoutParams params = new
                        RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                checkoutLayout.setLayoutParams(newsCountParam);
                checkoutItemsLayout.setLayoutParams(params);
                return true;
            }
        });

        disableSubCat();

        beveragesTv.setOnClickListener(this);
        foodTv.setOnClickListener(this);
        snacksTv.setOnClickListener(this);
        beansTv.setOnClickListener(this);
        v12tv.setOnClickListener(this);
        marchandiseTv.setOnClickListener(this);
        orderNowLayout.setOnClickListener(this);
        addMoreLayout.setOnClickListener(this);
        cartCount.setOnClickListener(this);

        if(language.equalsIgnoreCase("En")) {
            mExpAdapter = new ExpandListAdapter(getActivity(), catList);
            mExpListView.setAdapter(mExpAdapter);
        }else if(language.equalsIgnoreCase("Ar")){
            mExpAdapterArabic = new ExpandListAdapterArabic(getActivity(), catList);
            mExpListView.setAdapter(mExpAdapterArabic);
        }

//        mExpListView.expandGroup(0, true);
        mExpListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    mExpListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
//                mExpListView.setSelectionFromTop(groupPosition, 0);
            }
        });

        orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        cartCount.setText("" + myDbHelper.getTotalOrderQty());

        if(catId.equals("2")){
            new GetRMTime().execute();
            beveragesTv.setTextColor(Color.parseColor("#FFFFFF"));
            foodTv.setTextColor(Color.parseColor("#917101"));
            snacksTv.setTextColor(Color.parseColor("#FFFFFF"));
            beansTv.setTextColor(Color.parseColor("#FFFFFF"));
            marchandiseTv.setTextColor(Color.parseColor("#FFFFFF"));
            catId = "2";
            getCatData(FOODS);
            if(language.equalsIgnoreCase("En")) {
                mExpAdapter.notifyDataSetChanged();
            }else if(language.equalsIgnoreCase("Ar")){
                mExpAdapterArabic.notifyDataSetChanged();
            }
            mExpListView.invalidate();
            getExpandGroup();
            mExpListView.expandGroup(expandGrp, true);
        }else if(catId.equals("8")){
            snacksTv.performClick();
            expandGrp = 0;
        }else if(catId.equals("5")){
            beansTv.performClick();
            expandGrp = 0;
        }else if(catId.equals("6")){
            marchandiseTv.performClick();
            expandGrp = 0;
        }
        else if(catId.equals("9")){
            v12tv.performClick();
            expandGrp = 0;
        }else {
            beveragesTv.performClick();
            expandGrp = 0;
        }

        return rootView;
    }


    public void refreshList(){
        if(language.equalsIgnoreCase("En")) {
            mExpAdapter.notifyDataSetChanged();
        }else if(language.equalsIgnoreCase("Ar")){
            mExpAdapterArabic.notifyDataSetChanged();
        }
    }

    public int GetDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.beverages_text:
                beveragesTv.setTextColor(Color.parseColor("#917101"));
                v12tv.setTextColor(Color.parseColor("#FFFFFF"));
                foodTv.setTextColor(Color.parseColor("#FFFFFF"));
                snacksTv.setTextColor(Color.parseColor("#FFFFFF"));
                beansTv.setTextColor(Color.parseColor("#FFFFFF"));
                marchandiseTv.setTextColor(Color.parseColor("#FFFFFF"));
                catId = "1";
                getCatData(BEVERAGES);

                if(language.equalsIgnoreCase("En")) {
                    mExpAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
                    mExpAdapterArabic.notifyDataSetChanged();
                }
                mExpListView.invalidate();
                getExpandGroup();
                mExpListView.expandGroup(expandGrp, true);
                expandGrp = 0;
                break;
            case R.id.v12_text:
                v12tv.setTextColor(Color.parseColor("#917101"));
                beveragesTv.setTextColor(Color.parseColor("#FFFFFF"));
                foodTv.setTextColor(Color.parseColor("#FFFFFF"));
                snacksTv.setTextColor(Color.parseColor("#FFFFFF"));
                beansTv.setTextColor(Color.parseColor("#FFFFFF"));
                marchandiseTv.setTextColor(Color.parseColor("#FFFFFF"));
                catId = "9";
                getCatData(V12_Coffee);

                if(language.equalsIgnoreCase("En")) {
                    mExpAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
                    mExpAdapterArabic.notifyDataSetChanged();
                }
                mExpListView.invalidate();
                mExpListView.expandGroup(0, true);
                break;
            case R.id.food_text:
                expandGrp = 0;
                new GetRMTime().execute();
                beveragesTv.setTextColor(Color.parseColor("#FFFFFF"));
                v12tv.setTextColor(Color.parseColor("#FFFFFF"));
                foodTv.setTextColor(Color.parseColor("#917101"));
                snacksTv.setTextColor(Color.parseColor("#FFFFFF"));
                beansTv.setTextColor(Color.parseColor("#FFFFFF"));
                marchandiseTv.setTextColor(Color.parseColor("#FFFFFF"));
                catId = "2";
                getCatData(FOODS);
                if(language.equalsIgnoreCase("En")) {
                    mExpAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
                    mExpAdapterArabic.notifyDataSetChanged();
                }
                mExpListView.invalidate();
                getExpandGroup();
                mExpListView.expandGroup(expandGrp, true);
                expandGrp = 0;
                break;
            case R.id.snacks_text:
                beveragesTv.setTextColor(Color.parseColor("#FFFFFF"));
                v12tv.setTextColor(Color.parseColor("#FFFFFF"));
                foodTv.setTextColor(Color.parseColor("#FFFFFF"));
                snacksTv.setTextColor(Color.parseColor("#917101"));
                beansTv.setTextColor(Color.parseColor("#FFFFFF"));
                marchandiseTv.setTextColor(Color.parseColor("#FFFFFF"));
                catId = "8";
                getCatData(SNACKS);
                if(language.equalsIgnoreCase("En")) {
                    mExpAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
                    mExpAdapterArabic.notifyDataSetChanged();
                }
                mExpListView.invalidate();
                getExpandGroup();
                mExpListView.expandGroup(expandGrp, true);
                expandGrp = 0;
                break;
            case R.id.beans_text:
                beveragesTv.setTextColor(Color.parseColor("#FFFFFF"));
                v12tv.setTextColor(Color.parseColor("#FFFFFF"));
                foodTv.setTextColor(Color.parseColor("#FFFFFF"));
                snacksTv.setTextColor(Color.parseColor("#FFFFFF"));
                beansTv.setTextColor(Color.parseColor("#917101"));
                marchandiseTv.setTextColor(Color.parseColor("#FFFFFF"));
                catId = "5";
                getCatData(BEANS);
                if(language.equalsIgnoreCase("En")) {
                    mExpAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
                    mExpAdapterArabic.notifyDataSetChanged();
                }
                mExpListView.invalidate();
                getExpandGroup();
                mExpListView.expandGroup(expandGrp, true);
                expandGrp = 0;
                break;
            case R.id.marchandise_text:
                beveragesTv.setTextColor(Color.parseColor("#FFFFFF"));
                v12tv.setTextColor(Color.parseColor("#FFFFFF"));
                foodTv.setTextColor(Color.parseColor("#FFFFFF"));
                snacksTv.setTextColor(Color.parseColor("#FFFFFF"));
                beansTv.setTextColor(Color.parseColor("#FFFFFF"));
                marchandiseTv.setTextColor(Color.parseColor("#917101"));
                catId = "6";
                getCatData(MERCHANDISE);
                if(language.equalsIgnoreCase("En")) {
                    mExpAdapter.notifyDataSetChanged();
                }else if(language.equalsIgnoreCase("Ar")){
                    mExpAdapterArabic.notifyDataSetChanged();
                }
                mExpListView.invalidate();
                getExpandGroup();
                mExpListView.expandGroup(expandGrp, true);
                expandGrp = 0;
                break;

            case R.id.addmore_layout:
                if(DrConstants.offerFlag){
                    DrConstants.offerFlag = false;
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    trans.remove(new CategoriesListFragment());
                    trans.commit();

                    FragmentManager manager1 = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans1 = manager1.beginTransaction();
                    trans1.remove(new MenuFragment());
                    trans1.commit();

                    manager1.popBackStack();
                    manager.popBackStack();

                    Fragment radiusFragment = new MenuFragment();
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                    radiusTransaction.hide(CategoriesListFragment.this);
                    radiusTransaction.addToBackStack(null);

                    // Commit the transaction
                    radiusTransaction.commit();
                }else {
                    getActivity().onBackPressed();
                }
                break;

            case R.id.ordernow_layout:
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لاتوجد منتجات في السلة ، لتفعيل التأكيد من فضلك اضف منتج")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Fragment nutritionFragment = new OrderCheckoutFragment();
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                    nutritionTransaction.hide(CategoriesListFragment.this);
                    nutritionTransaction.addToBackStack(null);

                    // Commit the transaction
                    nutritionTransaction.commit();
                }
                break;

            case R.id.cart_count:
                String text = cartCount.getText().toString();
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لاتوجد منتجات في السلة ، لتفعيل التأكيد من فضلك اضف منتج")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    Fragment checkoutFragment = new OrderCheckoutFragment();
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction checkoutTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    checkoutTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    checkoutTransaction.add(R.id.realtabcontent, checkoutFragment);
                    checkoutTransaction.hide(CategoriesListFragment.this);
                    checkoutTransaction.addToBackStack(null);

                    // Commit the transaction
                    checkoutTransaction.commit();
                }
                break;
        }
    }


    public void getCatData(String categoryId) {
        Log.d("TAG", "getCatData: "+categoryId);
        catList.clear();
        Cursor cursor = null;
        if (isTimetoShow) {
            cursor = myDbHelper.getRMCategoriesList(catId);
        } else {
            if (categoryId.equals("0")) {
                cursor = myDbHelper.getCategoriesList(categoryId);
            }
            else if(categoryId.equals("-1")) {
                Toast.makeText(getContext(), "Items not available", Toast.LENGTH_SHORT).show();
            }
            else {
                cursor = myDbHelper.getCategoriesListUsingItemIds(categoryId);
            }
        }

        int i = 0;
        if(cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Categories c = new Categories();
                    c.setSubCatId(cursor.getString(0));
                    c.setCatId(cursor.getString(1));
                    c.setSubCat(cursor.getString(2));
                    c.setDescription(cursor.getString(3));
                    c.setImages(cursor.getString(4));
                    c.setStatus(cursor.getString(5));
                    c.setCreateDate(cursor.getString(6));
                    c.setModifyDate(cursor.getString(7));
                    c.setSequenceId(cursor.getString(8));
                    c.setSubCatAr(cursor.getString(9));
                    c.setDescriptionAr(cursor.getString(10));
                    Log.d("TAG", "getCatData: "+cursor.getString(0) + "," + cursor.getString(2));

                    if (categoryId.equals("0")) {
                        c.setChildItems(myDbHelper.getSubCategoriesList(cursor.getString(1), cursor.getString(0)));
                    }
                    else {
                        ArrayList<SubCategories> subCatList = new ArrayList<>();
                        subCatList = myDbHelper.getSubCategoriesList(cursor.getString(1), cursor.getString(0));
                        String[] itemIds = categoryId.split(",");
                        for (int j = 0; j < subCatList.size(); j++) {
                            Log.d("TAG", "subCatList.get(j).getItemId(): "+subCatList.get(j).getItemId());
                            boolean isItemAvailable = false;
                            for (int k = 0; k < itemIds.length; k++) {
                                if (itemIds[k].equals(subCatList.get(j).getItemId())) {
                                    isItemAvailable = true;
                                    break;
                                }
                            }
                            if (!isItemAvailable) {
                                subCatList.remove(j);
                                j = j - 1;
                            }
                        }
                        c.setChildItems(subCatList);
                    }
                    catList.add(c);
                    for (int j = 0; j < catList.size(); j++) {
                        for (int k = 0; k < catList.get(j).getChildItems().size(); k++) {
                            Log.d("TAG", "catList: " + catList.get(j).getChildItems().get(k).getItemName());
                        }
                    }
                    i++;
                } while (cursor.moveToNext());
            }
        }
    }



    public class GetRMTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){

                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                    isValid = false;
                    catId = "2";
                    getCatData(FOODS);
                    if(language.equalsIgnoreCase("En")) {
                        mExpAdapter.notifyDataSetChanged();
                    }else if(language.equalsIgnoreCase("Ar")){
                        mExpAdapterArabic.notifyDataSetChanged();
                    }
                    mExpListView.invalidate();
                    getExpandGroup();
                    mExpListView.expandGroup(expandGrp, true);
                    expandGrp = 0;
                }else{
                    dialog.dismiss();
//                    if(response12 == null){
//                        dialog.dismiss();
//                    }else if (response12.equalsIgnoreCase("true")) {
//                        dialog.dismiss();
//                        catId = "2";
//                        getCatData();
//                        if(language.equalsIgnoreCase("En")) {
//                            mExpAdapter.notifyDataSetChanged();
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            mExpAdapterArabic.notifyDataSetChanged();
//                        }
//                        mExpListView.invalidate();
//                        mExpListView.expandGroup(expandGrp, true);
//                        expandGrp = 0;
//                    } else {
//                        dialog.dismiss();
//                        catId = "2";
//                        getCatData();
//                        if(language.equalsIgnoreCase("En")) {
//                            mExpAdapter.notifyDataSetChanged();
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            mExpAdapterArabic.notifyDataSetChanged();
//                        }
//                        mExpListView.invalidate();
//                        mExpListView.expandGroup(expandGrp, true);
//                        expandGrp = 0;
//                    }


                    try {

                        Object property = result.getProperty(1);
                        if (property instanceof SoapObject) {
                            try {
                                SoapObject category_list = (SoapObject) property;
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                if( storeObject.getPropertyCount() > 0) {
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(0);
                                    isValid = Boolean.valueOf(sObj.getPropertyAsString("isValid"));
                                    sTime = sObj.getPropertyAsString("sTime");
                                    eTime = sObj.getPropertyAsString("eTime");

                                    isTimetoShow = true;
                                    catId = "2";
                                    getCatData(FOODS);
                                    if (language.equalsIgnoreCase("En")) {
                                        mExpAdapter.notifyDataSetChanged();
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        mExpAdapterArabic.notifyDataSetChanged();
                                    }
                                    mExpListView.invalidate();
                                    getExpandGroup();
                                    mExpListView.expandGroup(expandGrp, true);
                                    expandGrp = 0;


                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//                    }

                    } catch (Exception e) {
                        Log.d("", "Error while parsing the results!");
                        e.printStackTrace();
                    }
                }
            }else{
                dialog.dismiss();
            }

            super.onPostExecute(result1);
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    private void disableSubCat(){
        SharedPreferences storePrefs = getContext().getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String jsonText = storePrefs.getString("store", null);
        StoreInfo si = gson.fromJson(jsonText, StoreInfo.class);

        String itemsJson = si.getIemsJson();

        try {
            JSONObject mainObject = new JSONObject(itemsJson);
            JSONObject categoryObj = mainObject.getJSONObject("Items");

            BEVERAGES = categoryObj.getString("BEVERAGES");
            FOODS = categoryObj.getString("FOODS");
            BEANS = categoryObj.getString("BEANS");
            SNACKS = categoryObj.getString("SNACKS");
            MERCHANDISE = categoryObj.getString("MERCHANDISE");
            V12_Coffee = categoryObj.getString("V12 Coffee");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(BEVERAGES.equals("-1")) {
            beveragesTv.setVisibility(View.GONE);
            foodTv.performClick();
        }
        if(FOODS.equals("-1")) {
            foodTv.setVisibility(View.GONE);
        }
        if(SNACKS.equals("-1")) {
            snacksTv.setVisibility(View.GONE);
        }
        if(BEANS.equals("-1")){
            beansTv.setVisibility(View.GONE);
        }
        if(MERCHANDISE.equals("-1")) {
            marchandiseTv.setVisibility(View.GONE);
        }
        if(V12_Coffee.equals("-1")) {
            v12tv.setVisibility(View.GONE);
        }
    }

    private void getExpandGroup(){
        ArrayList<String> subCats = new ArrayList<>();
        for (int i = 0; i < catList.size(); i++) {
            subCats.add(catList.get(i).getSubCat());
        }

        if(catId.equals("1") && expandGrp == 5) { // cold beverages
            if (subCats.contains("FRAPPE")) {
                expandGrp = subCats.indexOf("FRAPPE");
            } else if (subCats.contains("FRANILLA")) {
                expandGrp = subCats.indexOf("FRANILLA");
            }else if (subCats.contains("ICED Beverages")) {
                expandGrp = subCats.indexOf("ICED Beverages");
            } else if (subCats.contains("MOCKTAILS")) {
                expandGrp = subCats.indexOf("MOCKTAILS");
            } else if (subCats.contains("100% NATURAL JUICES")) {
                expandGrp = subCats.indexOf("100% NATURAL JUICES");
            } else if (subCats.contains("Greek Yoghurt")) {
                expandGrp = subCats.indexOf("Greek Yoghurt");
            } else if (subCats.contains("FRESHLY SQUEEZED")) {
                expandGrp = subCats.indexOf("FRESHLY SQUEEZED");
            } else if (subCats.contains("KIDS COLD BEVERAGES")) {
                expandGrp = subCats.indexOf("KIDS COLD BEVERAGES");
            } else if (subCats.contains("PURE WATER")) {
                expandGrp = subCats.indexOf("PURE WATER");
            }
        }
        else if(catId.equals("2") && expandGrp == 12) { // Sandwich
            if (subCats.contains("E- Breakfast")) {
                expandGrp = subCats.indexOf("E- Breakfast");
            } else if (subCats.contains("Wrap Sandwiches")) {
                expandGrp = subCats.indexOf("Wrap Sandwiches");
            } else if (subCats.contains("Panini Sandwiches")) {
                expandGrp = subCats.indexOf("Panini Sandwiches");
            } else if (subCats.contains("Baguette Sandwiches")) {
                expandGrp = subCats.indexOf("Baguette Sandwiches");
            }
            else if (subCats.contains("Low Fat Baguette Sandwiches")) {
                expandGrp = subCats.indexOf("Low Fat Baguette Sandwiches");
            }
            else if (subCats.contains("French Bread")) {
                expandGrp = subCats.indexOf("French Bread");
            }
            else if (subCats.contains("Multi Grain Sandwiches")) {
                expandGrp = subCats.indexOf("Multi Grain Sandwiches");
            }
            else if (subCats.contains("Bagels")) {
                expandGrp = subCats.indexOf("Bagels");
            }
        }
        else if(catId.equals("2") && expandGrp == 21) { // Dessert
            if (subCats.contains("Cakes")) {
                expandGrp = subCats.indexOf("Cakes");
            } else if (subCats.contains("Regional Cakes")) {
                expandGrp = subCats.indexOf("Regional Cakes");
            } else if (subCats.contains("Cheese Cakes")) {
                expandGrp = subCats.indexOf("Cheese Cakes");
            } else if (subCats.contains("Friend's Cake")) {
                expandGrp = subCats.indexOf("Friend's Cake");
            }
        }

        if (expandGrp > catList.size()) {
            expandGrp = 0;
        }
    }
}
