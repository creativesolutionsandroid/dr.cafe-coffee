package com.cs.drcafe.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cs.drcafe.R;

/**
 * Created by BRAHMAM on 19-03-2016.
 */
public class MorePhilosophyArabic extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.more_our_pholosophy_arabic, container,
                false);
        ImageView img = (ImageView) rootView.findViewById(R.id.aboutus_banner);
//        try {
//            // get input stream
//            InputStream ims = getActivity().getAssets().open("our_philosophy.jpg");
//            // load image as Drawable
//            Drawable d = Drawable.createFromStream(ims, null);
//            // set image to ImageView
//            img.setImageDrawable(d);
//        } catch (IOException ex) {
//        }
        return rootView;
    }
}
