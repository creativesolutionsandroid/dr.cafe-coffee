package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.GPSTracker;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.TrackOrderAdapter;
import com.cs.drcafe.adapter.TrackOrderAdapterArabic;
import com.cs.drcafe.model.Additionals;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.OrderDetailsTrack;
import com.cs.drcafe.widget.Utility;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by SKT on 20-02-2016.
 */
public class TrackOrderFragment extends Fragment {
    private String SOAP_ACTION = "http://tempuri.org/getFavoriteOrder";
    private String METHOD_NAME = "getFavoriteOrder";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;

    private String mSOAP_ACTION = "http://tempuri.org/getTrackOrder";
    private String mMETHOD_NAME = "getTrackOrder";
    private SoapObject mResult;
    private String mResponse12 = null;

    private String SOAP_ACTION_CANCEL_ORDER = "http://tempuri.org/CancleOrder";
    private String METHOD_NAME_CANCEL_ORDER = "CancleOrder";
    private SoapPrimitive cancelOrderResult;
    private String cancelOrderResponse = null;

    ArrayList<OrderDetailsTrack> orderDetailList = new ArrayList<>();

    TextView orderNo, pickupTime, branch, addressType, paymentMode, reOrder;
    TextView totalQty, totalPrice, bonusPrice, netPrice, vatAmount, subTotal, roundUp, vatTitle;
    String subAmount, vatPrice, vatpercent, netAmount;
    LinearLayout promoAmtLayout;
    ImageView call, share, location;
    RelativeLayout cancelOrder;
    private DataBaseHelper myDbHelper;
    String orderId;
    ListView listView;
    TrackOrderAdapter mTrackOrderAdapter;
    TrackOrderAdapterArabic mTrackOrderAdapterArabic;
    SharedPreferences userPrefs;
    ImageView statusImage, loadingImage;
    String userId;
    Button backBtn;
    int flag, reorderFlag = 0;
    ProgressDialog dialog;
    Animation animRotate;
    private Timer timer = new Timer();
    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefsEditor;
    View rootView;
    String language;
    public static int totalAmount = 0;
    boolean isFirstTime = true;
    final DecimalFormat priceFormat = new DecimalFormat("0.00");

    GPSTracker gps;
    private double lat, longi;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    String phone;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.track_order, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.track_order_arabic, container,
                    false);
        }

        myDbHelper = new DataBaseHelper(getActivity());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        orderId = getArguments().getString("orderId");
        flag = getArguments().getInt("flag");
        orderPrefs = getActivity().getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor  = orderPrefs.edit();
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        orderNo = (TextView) rootView.findViewById(R.id.order_number);
        pickupTime = (TextView) rootView.findViewById(R.id.order_time);
        branch = (TextView) rootView.findViewById(R.id.order_branch);
        addressType = (TextView) rootView.findViewById(R.id.address_type);
        paymentMode = (TextView) rootView.findViewById(R.id.order_payment_mode);
        totalQty = (TextView) rootView.findViewById(R.id.totalQty);
        totalPrice = (TextView) rootView.findViewById(R.id.totalAmount);
        bonusPrice = (TextView) rootView.findViewById(R.id.promoAmount);
        netPrice = (TextView) rootView.findViewById(R.id.netAmount);
        vatAmount = (TextView) rootView.findViewById(R.id.vatAmount);
        vatTitle = (TextView) rootView.findViewById(R.id.vatTitle);
        subTotal = (TextView) rootView.findViewById(R.id.subTotal);
        roundUp = (TextView) rootView.findViewById(R.id.round_up);
        promoAmtLayout = (LinearLayout) rootView.findViewById(R.id.promo_amt_layout);
        listView = (ListView) rootView.findViewById(R.id.order_listview);
        statusImage = (ImageView) rootView.findViewById(R.id.status_icon);
        loadingImage = (ImageView) rootView.findViewById(R.id.loading);
        call = (ImageView) rootView.findViewById(R.id.track_call);
        share = (ImageView) rootView.findViewById(R.id.track_share);
        location = (ImageView) rootView.findViewById(R.id.track_location);
        reOrder = (TextView) rootView.findViewById(R.id.reorder_button);
        cancelOrder = (RelativeLayout) rootView.findViewById(R.id.cancel_order);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        cancelOrder.setVisibility(View.INVISIBLE);
        animRotate = AnimationUtils.loadAnimation(getActivity(),
                R.anim.rotate_centre);
        loadingImage.startAnimation(animRotate);
        loadingImage.setAlpha(0.05f);

        gps = new GPSTracker(getActivity());

        if(gps.canGetLocation()){

            lat = gps.getLatitude();
            longi = gps.getLongitude();
            // \n is for new line
//            Toast.makeText(getActivity(), "Your Location is - \nLat: " + lat + "\nLong: " + longi, Toast.LENGTH_LONG).show();
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

        String response = userPrefs.getString("user_profile", null);
        String mFirstName = "Guest", mFamilyName = "", email = "", mCountryCode = "", mobile = "";
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");
                mFirstName = userObjuect.getString("fullName");
                mFamilyName = userObjuect.getString("FamilyName");
                email = userObjuect.getString("username");

                String[] mobileStr = userObjuect.getString("phone").split("-");
                mCountryCode = mobileStr[0];
                mobile = mobileStr[0]+mobileStr[1];
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }

        FreshchatConfig freshchatConfig = new FreshchatConfig("c2362c09-e69d-469b-9d43-fa61be9e42da", "8cc1c2f6-aec7-4d6d-9193-577acfd6e263");
        freshchatConfig.setCameraCaptureEnabled(true);
        freshchatConfig.setGallerySelectionEnabled(true);
        Freshchat.getInstance(getContext()).init(freshchatConfig);

        FreshchatUser freshUser = Freshchat.getInstance(getContext()).getUser();
        freshUser.setFirstName(mFirstName);
        freshUser.setLastName(mFamilyName);
        freshUser.setEmail(email);
        freshUser.setPhone(mCountryCode, mobile);
        Freshchat.getInstance(getContext()).setUser(freshUser);

        totalAmount = 0;

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag == 1){
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    trans.remove(new TrackOrderFragment());
//                trans.remove(new MoreLocation());
                    trans.commit();
                    FragmentManager manager1 = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans1 = manager1.beginTransaction();
                    trans1.remove(new OrderHistoryFragment());
                    trans1.commit();

                    manager1.popBackStack();
                    manager.popBackStack();

                    Fragment radiusFragment = new OrderHistoryFragment();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                    radiusTransaction.hide(TrackOrderFragment.this);
                    radiusTransaction.addToBackStack(null);

                    // Commit the transaction
                    radiusTransaction.commit();
                }else if(flag == 0){
                    getActivity().onBackPressed();
                }
            }
        });

        if(language.equalsIgnoreCase("En")){
            if (flag == 1) {
                reorderFlag = 1;
                reOrder.setText("Help");
            } else if (flag == 0) {
                reorderFlag = 0;
                reOrder.setText("Help");
            } else {
//                reOrder.setVisibility(View.GONE);
                reOrder.setText("Help");
            }

        }else if(language.equalsIgnoreCase("Ar")) {
            if (flag == 1) {
                reorderFlag = 1;
                reOrder.setText("للمساعدة");
            } else if (flag == 0) {
                reorderFlag = 0;
                reOrder.setText("للمساعدة");
            } else {
//                reOrder.setVisibility(View.GONE);
                reOrder.setText("للمساعدة");
            }
        }

        if(language.equalsIgnoreCase("En")) {
            mTrackOrderAdapter = new TrackOrderAdapter(getActivity(), orderDetailList);
            listView.setAdapter(mTrackOrderAdapter);
            Utility.setListViewHeightBasedOnChildren(listView);
        }else if(language.equalsIgnoreCase("Ar")){
            mTrackOrderAdapterArabic = new TrackOrderAdapterArabic(getActivity(), orderDetailList);
            listView.setAdapter(mTrackOrderAdapterArabic);
            Utility.setListViewHeightBasedOnChildren(listView);
        }

        listView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    return true; // Indicates that this has been handled by you and will not be forwarded further.
                }
                return false;
            }
        });


        cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                String title= "", msg= "", posBtn = "", negBtn = "";
                if(language.equalsIgnoreCase("En")){
                    posBtn = "Yes";
                    negBtn = "No";
                    title = "dr.CAFE";
                    msg = "Do you want to cancel the order?";
                }else if(language.equalsIgnoreCase("Ar")){
                    posBtn = "نعم";
                    negBtn = "لا";
                    title = "د. كيف";
                    msg = "هل ترغب في إلغاء الطلب ؟";
                }
                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();

                                orderPrefsEditor.putString("order_status", "close");
                                orderPrefsEditor.commit();
                                MainFragment.cupImg.setVisibility(View.GONE);
                                MainFragment.trackBanner.setVisibility(View.GONE);

                                new GetCancelOrderResponse().execute();
                            }
                        }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


//        listView.setScrollContainer(false);

        new GetTrackOrderDetails().execute(orderId);

        timer.schedule(new MyTimerTask(), 18000, 18000);
        return rootView;
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if(getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isFirstTime = false;
                        new GetTrackOrderDetails().execute(orderId);
                    }
                });
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        timer.cancel();
        if(new GetTrackOrderDetails() != null) {
            new GetTrackOrderDetails().cancel(true);
        }
        if(new GetOrderDetails() != null) {
            new GetOrderDetails().cancel(true);
        }
    }

    public class GetOrderDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("UserId", userId);
                    request.addProperty("OrderID", arg0[0]);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                response12 = "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            String bonusAmt = "0";
            if (response12 != null){
                if (response12.equals("no internet")) {
//                dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                } else if (response12.equals("anyType{}")) {

//                dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                } else {

                    try {
                        int total_qty = 0;
                        float total_price = 0;

                        Object property = result.getProperty(1);
                        if (property instanceof SoapObject) {
                            try {
                                SoapObject category_list = (SoapObject) property;
                                final SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");

                                    OrderDetailsTrack ort = new OrderDetailsTrack();

                                    String orderId = sObj.getPropertyAsString("orderId");
                                    String favoriteName = sObj.getPropertyAsString("FavoriteName");
                                    String userId = sObj.getPropertyAsString("userId");
                                    String storeId = sObj.getPropertyAsString("storeId");
                                    final String itemId = sObj.getPropertyAsString("itemId");
                                    final String qty = sObj.getPropertyAsString("qty");
                                    final String size = sObj.getPropertyAsString("Size");
                                    final String comments = sObj.getPropertyAsString("Comments");
                                    String orderItem = sObj.getPropertyAsString("orderItem");
                                    bonusAmt = sObj.getPropertyAsString("bonusamt");
                                    final String additionalids = sObj.getPropertyAsString("Additionalids");

                                    Cursor c = myDbHelper.getNonShopItem(itemId);
                                    String itemName = "", itemNameAr = "";
                                    if (c.moveToFirst()) {
                                        do {
                                            itemName = c.getString(4);
                                            itemNameAr = c.getString(13);
                                        } while (c.moveToNext());
                                    }
                                    String price = myDbHelper.getPrice(itemId, size);
                                    float subAmt = Float.parseFloat(price);
                                    String additionalsStr = "", additionalsStrArabic = "", additionalsPrice = "";
                                    if (additionalids.length() >= 1 && !additionalids.equals("0")) {
                                        String[] parts = additionalids.split(",");
                                        for (int j = 0; j < parts.length; j++) {
                                            ArrayList<Additionals> additionalDetails = myDbHelper.getAdditionalDetails(parts[j]);

                                            if (additionalDetails.size() > 0) {


                                                for (Additionals sa : additionalDetails) {
                                                    float addlPrice = 0;
                                                    if (size.equals("9")){
                                                        addlPrice = (Float.parseFloat(sa.getAdditionalPrice()) * 12);
                                                    }
                                                    else{
                                                        addlPrice = (Float.parseFloat(sa.getAdditionalPrice()));
                                                    }
                                                    if (additionalsPrice.equals("")) {
                                                        additionalsStr = sa.getAdditionalName();
                                                        additionalsStrArabic = sa.getAdditionalNameAr();
                                                        additionalsPrice = Float.toString(addlPrice);
                                                    } else {
                                                        additionalsStr = additionalsStr+","+sa.getAdditionalName();
                                                        additionalsStrArabic = additionalsStrArabic+","+sa.getAdditionalNameAr();
                                                        additionalsPrice = additionalsPrice + "," + Float.toString(addlPrice);
                                                    }
//                                                    additionalsStr = additionalsStr+","+sa.getAdditionalName();
//                                                    additionalsStrArabic = sa.getAdditionalNameAr();
                                                    subAmt = subAmt + addlPrice;

                                                }
                                                ort.setAdditionalList(additionalDetails);
                                            }
                                        }
                                    }

                                    float totalAmt = subAmt * Integer.parseInt(qty);


                                    total_qty = total_qty + Integer.parseInt(qty);
                                    total_price = total_price + totalAmt;

                                    ort.setItemName(itemName);
                                    ort.setItemNameAr(itemNameAr);
                                    ort.setAdditionalName(additionalsStr);
                                    ort.setAdditionalNameAr(additionalsStrArabic);
                                    ort.setPrice(price);
                                    ort.setAdditionalPrice(additionalsPrice);
                                    ort.setTottalAmount("" + totalAmt);
                                    ort.setItemTypeId(size);
                                    ort.setQty(qty);

                                    orderDetailList.add(ort);
                                }

                                reOrder.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        Freshchat.showConversations(getContext());


////                                        String text = reOrder.getText().toString();
////                                        if (reorderFlag == 1) {
////                                            myDbHelper.deleteOrderTable();
////                                            ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
////                                            HashMap<String, String> values = new HashMap<>();
////                                            for (int i = 0; i < storeObject.getPropertyCount(); i++) {
////                                                SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//////                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");
////
////                                                OrderDetailsTrack ort = new OrderDetailsTrack();
////
////                                                String orderId = sObj.getPropertyAsString("orderId");
////                                                String favoriteName = sObj.getPropertyAsString("FavoriteName");
////                                                String userId = sObj.getPropertyAsString("userId");
////                                                String storeId = sObj.getPropertyAsString("storeId");
////                                                final String itemId = sObj.getPropertyAsString("itemId");
////                                                final String qty = sObj.getPropertyAsString("qty");
////                                                final String size = sObj.getPropertyAsString("Size");
////                                                String comments = sObj.getPropertyAsString("Comments");
////                                                String orderItem = sObj.getPropertyAsString("orderItem");
////                                                final String additionalids = sObj.getPropertyAsString("Additionalids");
////
////                                                Cursor c = myDbHelper.getNonShopItem(itemId);
////                                                String itemName = "";
////                                                if (c.moveToFirst()) {
////                                                    do {
////                                                        itemName = c.getString(4);
////                                                    } while (c.moveToNext());
////                                                }
////                                                String price = myDbHelper.getPrice(itemId, size);
////                                                float subAmt = Float.parseFloat(price);
////                                                String additionalsStr = "", additionalsPrice = "", ids = "0";
////                                                if (additionalids.length() >= 1 && !additionalids.equals("0")) {
////                                                    String[] parts = additionalids.split(",");
////                                                    for (int j = 0; j < parts.length; j++) {
////                                                        ArrayList<Additionals> additionalDetails = myDbHelper.getAdditionalDetails(parts[j]);
////
////                                                        if (additionalDetails.size() > 0) {
////
////
////                                                            for (Additionals sa : additionalDetails) {
////                                                                float addlPrice = 0;
////                                                                if (size.equals("9")){
////                                                                    addlPrice = (Float.parseFloat(sa.getAdditionalPrice()) * 12);
////                                                                }
////                                                                else{
////                                                                    addlPrice = (Float.parseFloat(sa.getAdditionalPrice()));
////                                                                }
////                                                                if (additionalsPrice.equals("")) {
////                                                                    additionalsStr = sa.getAdditionalName();
////                                                                    ids = sa.getAdditionalsId();
////                                                                    additionalsPrice = Float.toString(addlPrice);
////                                                                } else {
////                                                                    additionalsStr = sa.getAdditionalName();
////                                                                    ids = sa.getAdditionalsId();
////                                                                    additionalsPrice = additionalsPrice + "," + Float.toString(addlPrice);
////                                                                }
////                                                                additionalsStr = sa.getAdditionalName();
////                                                                subAmt = subAmt + addlPrice;
////
////                                                            }
////                                                            ort.setAdditionalList(additionalDetails);
////                                                        }
////                                                    }
////                                                }
////
////
////                                                String[] subCatId = myDbHelper.getSubCatId(itemId).split(",");
////
////
////                                                if(comments.contains("anyType{}")){
////                                                    comments = "";
////                                                }
////
////                                                float totalAmt = subAmt * Integer.parseInt(qty);
////
////                                                values.put("itemId", itemId);
////                                                values.put("itemTypeId", size);
////                                                values.put("subCategoryId", subCatId[0]);
////                                                values.put("additionals", additionalsStr);
////                                                values.put("qty", qty);
////                                                values.put("price", Float.toString(subAmt));
////                                                values.put("additionalsPrice", additionalsPrice);
////                                                values.put("additionalsTypeId", ids);
////                                                values.put("totalAmount", Float.toString(totalAmt));
////                                                values.put("comment", comments);
////                                                values.put("status", "1");
////                                                values.put("creationDate", "14/07/2015");
////                                                values.put("modifiedDate", "14/07/2015");
////                                                values.put("categoryId", subCatId[1]);
////                                                myDbHelper.insertOrder(values);
////                                                try {
////                                                    MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
////                                                    MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
////                                                    MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
////                                                    CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
////                                                    CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
////                                                    CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
////                                                } catch (NullPointerException e) {
////                                                    e.printStackTrace();
////                                                }
////                                                ((MainActivity) getActivity()).setBadge("" + myDbHelper.getTotalOrderQty());
////                                            }
////                                            Fragment nutritionFragment = new OrderCheckoutFragment();
////                                            // consider using Java coding conventions (upper first char class names!!!)
////                                            FragmentTransaction nutritionTransaction = getFragmentManager().beginTransaction();
////
////                                            // Replace whatever is in the fragment_container view with this fragment,
////                                            // and add the transaction to the back stack
////                                            nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
////                                            nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
////                                            nutritionTransaction.hide(TrackOrderFragment.this);
////                                            nutritionTransaction.addToBackStack(null);
////
////                                            // Commit the transaction
////                                            nutritionTransaction.commit();
//                                        } else if (reorderFlag == 0) {
//                                            ((MainActivity) getActivity()).setCurrenTab(0);
//                                            ((MainActivity) getActivity()).setCurrenTab(2);
//                                        }
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//                    }
                        final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");

                        totalQty.setText("" + total_qty);
                        totalPrice.setText("" + priceFormat.format(total_price));
                        float bonus = total_price + Float.parseFloat(vatPrice);
                        float net = (Float.parseFloat(priceFormat.format(bonus))) - (Float.parseFloat(bonusAmt));
                        float netAmountRounded = (float) Math.round(net * 10)/10;
                        bonusPrice.setText("- " +priceFormat.format(Float.parseFloat(bonusAmt)));
                        vatAmount.setText("+ "+vatPrice);
                        subTotal.setText(""+priceFormat.format(bonus));
                        netPrice.setText(""+priceFormat.format(netAmountRounded));
                        roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(priceFormat.format(net))));
                        if(language.equalsIgnoreCase("En")){
                            vatTitle.setText("Vat ("+vatpercent+"%) :");
                        }
                        else{
                            vatTitle.setText("ضريبة القيمة المضافة (%"+vatpercent+") :");
                        }

                    } catch (Exception e) {
                        Log.d("", "Error while parsing the results!");
                        e.printStackTrace();
                    }

                    isFirstTime = false;
                    dialog.dismiss();
                    if (language.equalsIgnoreCase("En")) {
                        mTrackOrderAdapter.notifyDataSetChanged();
                        Utility.setListViewHeightBasedOnChildren(listView);
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mTrackOrderAdapterArabic.notifyDataSetChanged();
                        Utility.setListViewHeightBasedOnChildren(listView);
                    }
                    super.onPostExecute(result1);
                }
            }
        }
    }

    public class GetTrackOrderDetails extends AsyncTask<String, String, String> {
        //        java.net.URL url = null;
//        String responce = null;
//        double lat, longi;
        String networkStatus;
        String pmtMode = "";

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if(isFirstTime) {
                dialog = ProgressDialog.show(getActivity(), "",
                        "Loading. Please Wait....");
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, mMETHOD_NAME);
                    request.addProperty("UserId", userId);
                    request.addProperty("OrderId", orderId);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(mSOAP_ACTION, envelope);
                    mResult = (SoapObject) envelope.getResponse();
                    mResponse12 = mResult.toString();
                    Log.v("TAG", mResponse12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + mResult.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + mResponse12);
            }else{
                mResponse12 = "no internet";
            }
            return mResponse12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (mResponse12 != null){
                if (mResponse12.equals("no internet")) {
//                dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                } else if (mResponse12.equals("anyType{}")) {
//                dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                } else {

                    try {

                        Object property = mResult.getProperty(1);
                        if (property instanceof SoapObject) {
                            try {
                                SoapObject category_list = (SoapObject) property;
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");

                                    OrderDetailsTrack ort = new OrderDetailsTrack();
                                    String address = "";
                                    final String expectedTime = sObj.getPropertyAsString("ExpectedTime");
                                    final String storeName = sObj.getPropertyAsString("StoreName");
                                    final String storeName_ar = sObj.getPropertyAsString("StoreName_ar");
                                    String orderStatus = sObj.getPropertyAsString("OrderStatus");
                                    final String latitude = sObj.getPropertyAsString("Latitude");
                                    final String longitude = sObj.getPropertyAsString("Longitude");
                                    phone = sObj.getPropertyAsString("phone");
                                    final String paymentmode = sObj.getPropertyAsString("PaymentMode");
                                    String orderDate = sObj.getPropertyAsString("orderDate");
                                    String invoiceNo = sObj.getPropertyAsString("InvoiceNo");
                                    subAmount = sObj.getPropertyAsString("SubTotal");
                                    vatPrice = sObj.getPropertyAsString("Vat");
                                    vatpercent = sObj.getPropertyAsString("VatPercentage");
                                    netAmount = sObj.getPropertyAsString("Total_Price");
                                    String addressId = sObj.getPropertyAsString("AddressId");
                                    address = sObj.getPropertyAsString("HouseNo") + "," +
                                            sObj.getPropertyAsString("CAddress");

                                    Log.d("TAG", "addressId: "+addressId);
                                    final String[] order_no = invoiceNo.split("-");
                                    if (addressId == null || addressId.equalsIgnoreCase("0")) {
                                        if (language.equalsIgnoreCase("En")) {
                                            if (orderStatus.equals("New")) {
                                                cancelOrder.setVisibility(View.VISIBLE);
                                                statusImage.setImageResource(R.drawable.track_status_order);
                                            } else if (orderStatus.equals("Pending")) {
                                                cancelOrder.setVisibility(View.VISIBLE);
                                                statusImage.setImageResource(R.drawable.track_status_accept);
                                            } else if (orderStatus.equals("Ready")) {
                                                cancelOrder.setVisibility(View.INVISIBLE);
                                                statusImage.setImageResource(R.drawable.track_status_ready);
                                            } else if (orderStatus.equals("Close")) {
                                                cancelOrder.setVisibility(View.INVISIBLE);
                                                if (orderPrefs.getString("order_id", "").equals(orderId)) {
                                                    orderPrefsEditor.putString("order_status", "close");
                                                    orderPrefsEditor.commit();
                                                }
                                                statusImage.setImageResource(R.drawable.track_status_served);
                                            } else if (orderStatus.equals("Rejected")) {
                                                cancelOrder.setVisibility(View.INVISIBLE);
                                                if (orderPrefs.getString("order_id", "").equals(orderId)) {
                                                    orderPrefsEditor.putString("order_status", "close");
                                                    orderPrefsEditor.commit();
                                                }
                                                statusImage.setImageResource(R.drawable.track_status_rejected);
                                            }


                                            orderNo.setText(order_no[1]);
                                            pickupTime.setText(expectedTime);
                                            branch.setText(storeName);

                                            if (paymentmode.equals("2")) {
                                                pmtMode = "Cash on Pick Up";
                                                paymentMode.setText("Cash on Pick Up");
                                            } else if (paymentmode.equals("3")) {
                                                pmtMode = "Online Payment";
                                                paymentMode.setText("Online Payment");
                                            } else if (paymentmode.equals("4")) {
                                                pmtMode = "Free Drink";
                                                paymentMode.setText("Free Drink");
                                            }

                                        } else if (language.equalsIgnoreCase("Ar")) {
                                            if (orderStatus.equals("New")) {
                                                cancelOrder.setVisibility(View.VISIBLE);
                                                statusImage.setImageResource(R.drawable.track_status_order_arabic);
                                            } else if (orderStatus.equals("Pending")) {
                                                cancelOrder.setVisibility(View.VISIBLE);
                                                statusImage.setImageResource(R.drawable.track_status_accept_arabic);
                                            } else if (orderStatus.equals("Ready")) {
                                                cancelOrder.setVisibility(View.INVISIBLE);
                                                statusImage.setImageResource(R.drawable.track_status_ready_arabic);
                                            } else if (orderStatus.equals("Close")) {
                                                cancelOrder.setVisibility(View.INVISIBLE);
                                                if (orderPrefs.getString("order_id", "").equals(orderId)) {
                                                    orderPrefsEditor.putString("order_status", "close");
                                                    orderPrefsEditor.commit();
                                                }
                                                statusImage.setImageResource(R.drawable.track_status_served_arabic);
                                            } else if (orderStatus.equals("Rejected")) {
                                                cancelOrder.setVisibility(View.INVISIBLE);
                                                if (orderPrefs.getString("order_id", "").equals(orderId)) {
                                                    orderPrefsEditor.putString("order_status", "close");
                                                    orderPrefsEditor.commit();
                                                }
                                                statusImage.setImageResource(R.drawable.track_status_rejected_arabic);
                                            }

//                                    final String[] order_no = invoiceNo.split("-");
                                            orderNo.setText(order_no[1]);
                                            pickupTime.setText(expectedTime);
                                            branch.setText(storeName_ar);

                                            if (paymentmode.equals("2")) {
                                                pmtMode = "Cash on Pick Up";
                                                paymentMode.setText("نقدي عند استلام الطلب");
                                            } else if (paymentmode.equals("3")) {
                                                pmtMode = "Online Payment";
                                                paymentMode.setText("سداد أونلاين");
                                            } else if (paymentmode.equals("4")) {
                                                pmtMode = "Free Drink";
                                                paymentMode.setText("مجاني");
                                            }
                                        }
                                    }
                                        else {
                                            if (language.equalsIgnoreCase("En")) {
                                                if (orderStatus.equals("New")) {
                                                    cancelOrder.setVisibility(View.VISIBLE);
                                                    statusImage.setImageResource(R.drawable.track_status_order_delivery);
                                                } else if (orderStatus.equals("Pending")) {
                                                    cancelOrder.setVisibility(View.VISIBLE);
                                                    statusImage.setImageResource(R.drawable.track_status_accept_delivery);
                                                } else if (orderStatus.equals("Ready")) {
                                                    cancelOrder.setVisibility(View.INVISIBLE);
                                                    statusImage.setImageResource(R.drawable.track_status_ready_delivery);
                                                } else if (orderStatus.equals("Close")) {
                                                    cancelOrder.setVisibility(View.INVISIBLE);
                                                    if (orderPrefs.getString("order_id", "").equals(orderId)) {
                                                        orderPrefsEditor.putString("order_status", "close");
                                                        orderPrefsEditor.commit();
                                                    }
                                                    statusImage.setImageResource(R.drawable.track_status_served_delivery);
                                                } else if (orderStatus.equals("Rejected")) {
                                                    cancelOrder.setVisibility(View.INVISIBLE);
                                                    if (orderPrefs.getString("order_id", "").equals(orderId)) {
                                                        orderPrefsEditor.putString("order_status", "close");
                                                        orderPrefsEditor.commit();
                                                    }
                                                    statusImage.setImageResource(R.drawable.track_status_rejected_delivery);
                                                }


                                                orderNo.setText(order_no[1]);
                                                pickupTime.setText(expectedTime);
                                                addressType.setText("Delivery : ");
                                                branch.setText(address);

                                                if (paymentmode.equals("2")) {
                                                    pmtMode = "Cash on Pick Up";
                                                    paymentMode.setText("Cash on Delivery");
                                                } else if (paymentmode.equals("3")) {
                                                    pmtMode = "Online Payment";
                                                    paymentMode.setText("Online Payment");
                                                } else if (paymentmode.equals("4")) {
                                                    pmtMode = "Free Drink";
                                                    paymentMode.setText("Free Drink");
                                                }

                                            } else if (language.equalsIgnoreCase("Ar")) {
                                                if (orderStatus.equals("New")) {
                                                    cancelOrder.setVisibility(View.VISIBLE);
                                                    statusImage.setImageResource(R.drawable.track_status_order_delivery_ar);
                                                } else if (orderStatus.equals("Pending")) {
                                                    cancelOrder.setVisibility(View.VISIBLE);
                                                    statusImage.setImageResource(R.drawable.track_status_accept_delivery_ar);
                                                } else if (orderStatus.equals("Ready")) {
                                                    cancelOrder.setVisibility(View.INVISIBLE);
                                                    statusImage.setImageResource(R.drawable.track_status_ready_delivery_ar);
                                                } else if (orderStatus.equals("Close")) {
                                                    cancelOrder.setVisibility(View.INVISIBLE);
                                                    if (orderPrefs.getString("order_id", "").equals(orderId)) {
                                                        orderPrefsEditor.putString("order_status", "close");
                                                        orderPrefsEditor.commit();
                                                    }
                                                    statusImage.setImageResource(R.drawable.track_status_served_delivery_ar);
                                                } else if (orderStatus.equals("Rejected")) {
                                                    cancelOrder.setVisibility(View.INVISIBLE);
                                                    if (orderPrefs.getString("order_id", "").equals(orderId)) {
                                                        orderPrefsEditor.putString("order_status", "close");
                                                        orderPrefsEditor.commit();
                                                    }
                                                    statusImage.setImageResource(R.drawable.track_status_rejected_delivery_ar);
                                                }

//                                    final String[] order_no = invoiceNo.split("-");
                                                orderNo.setText(order_no[1]);
                                                pickupTime.setText(expectedTime);
                                                addressType.setText("التوصيل : ");
                                                branch.setText(address);

                                                if (paymentmode.equals("2")) {
                                                    pmtMode = "Cash on Pick Up";
                                                    paymentMode.setText("الدفع كاش عند التوصيل");
                                                } else if (paymentmode.equals("3")) {
                                                    pmtMode = "Online Payment";
                                                    paymentMode.setText("سداد أونلاين");
                                                } else if (paymentmode.equals("4")) {
                                                    pmtMode = "Free Drink";
                                                    paymentMode.setText("مجاني");
                                                }
                                            }
                                        }
                                        call.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                                                if (currentapiVersion >= Build.VERSION_CODES.M) {
                                                    if (!canAccessPhonecalls()) {
                                                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                                                    } else {
                                                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                                                        startActivity(intent);
                                                    }
                                                }else {
                                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                                                    startActivity(intent);
                                                }




                                            }
                                        });

                                        share.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intent = new Intent(Intent.ACTION_SEND);
                                                intent.setType("text/plain");
                                                intent.putExtra(Intent.EXTRA_TEXT,
                                                        "dr.CAFE Order Details \n Order No: " + order_no[1] + "\n Order Expected Pickup Time: " + expectedTime + "\n Branch: " + storeName + "\nPayment Mode: " + pmtMode + "\nhttp://maps.google.com/maps?saddr="+lat+","+longi+"&daddr=" + latitude + "," + longitude);
                                                ;

                                                startActivity(Intent.createChooser(intent, "Share"));


                                            }
                                        });

                                        location.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Uri gmmIntentUri = Uri.parse("google.navigation:q="+ latitude + "," + longitude);
                                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                                mapIntent.setPackage("com.google.android.apps.maps");
                                                startActivity(mapIntent);
                                            }
                                        });

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
//                    }

                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }

                        if(dialog!=null) {
                            dialog.dismiss();
                        }
                        if (language.equalsIgnoreCase("En")) {
                            mTrackOrderAdapter.notifyDataSetChanged();
                            Utility.setListViewHeightBasedOnChildren(listView);
                        } else if (language.equalsIgnoreCase("Ar")) {
                            mTrackOrderAdapterArabic.notifyDataSetChanged();
                            Utility.setListViewHeightBasedOnChildren(listView);
                        }
                        if(isFirstTime) {
                            new GetOrderDetails().execute(orderId);
                        }
                        super.onPostExecute(result1);
                    }
                }
            }
        }


        public class GetCancelOrderResponse extends AsyncTask<String, String, String> {
            java.net.URL url = null;
            double lat, longi;
            String  networkStatus;
            @Override
            protected void onPreExecute() {
                networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                dialog = ProgressDialog.show(getActivity(), "",
                        "Loading. Please Wait....");
            }

            @Override
            protected String doInBackground(String... arg0) {
                if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    try {

                        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_CANCEL_ORDER);
                        request.addProperty("userId", userId);
                        request.addProperty("orderId", orderId);
                        // request.addProperty("", ""); // incase you need to pass
                        // parameters to the web-service

                        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                                SoapEnvelope.VER11);
                        envelope.dotNet = true;
                        envelope.setOutputSoapObject(request);

                        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                        androidHttpTransport.call(SOAP_ACTION_CANCEL_ORDER, envelope);
                        cancelOrderResult = (SoapPrimitive) envelope.getResponse();
                        cancelOrderResponse = cancelOrderResult.toString();
                        // SoapObject root = (SoapObject) result.getProperty(0);
                        System.out.println("********Count : " + cancelOrderResult);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d("Responce", "" + cancelOrderResponse);
                }else{
                    return "no internet";
                }
                return cancelOrderResponse;
            }

            @Override
            protected void onPostExecute(String result1) {
                if(result1 != null ){
                    if(result1.equalsIgnoreCase("no internet")){
                        dialog.dismiss();
                    }else {
                        dialog.dismiss();
                        if(cancelOrderResponse.equalsIgnoreCase("1")){
                            if(language.equalsIgnoreCase("En")){
                                Toast.makeText(getActivity(),"Your order successfully canceled",Toast.LENGTH_LONG);
                            }else if(language.equalsIgnoreCase("Ar")){
                                Toast.makeText(getActivity(),"طلبك تم إلغاءه بنجاح",Toast.LENGTH_LONG);
                            }
                            new GetTrackOrderDetails().execute();
                        }else{
                            if(language.equalsIgnoreCase("En")){
                                Toast.makeText(getActivity(),"Your order already started processing",Toast.LENGTH_LONG);
                            }else if(language.equalsIgnoreCase("Ar")){
                                Toast.makeText(getActivity(),"طلبك يتم تحضيره حاليا",Toast.LENGTH_LONG);
                            }
                        }
                    }
                }else {
                    dialog.dismiss();
                }


                super.onPostExecute(result1);
            }
        }


        private boolean canAccessPhonecalls() {
            return (hasPermission(Manifest.permission.CALL_PHONE));
        }

        private boolean hasPermission(String perm) {
            return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

            switch (requestCode) {


                case PHONE_REQUEST:
                    if (canAccessPhonecalls()) {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(getActivity(), "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }

        @Override
        public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
            if (FragmentUtils.sDisableFragmentAnimations) {
                Animation a = new Animation() {};
                a.setDuration(0);
                return a;
            }
            return super.onCreateAnimation(transit, enter, nextAnim);
        }

    }
