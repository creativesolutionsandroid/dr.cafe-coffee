package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

/**
 * Created by SKT on 27-12-2015.
 */
public class LocationDisplayFragment extends Fragment {

    Button backBtn;
    TextView map, list;
    SharedPreferences locationPrefs;
    SharedPreferences.Editor locationPrefsEditor;

    String savedValue;
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.map_disply_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.map_display_layout_arabic, container,
                    false);
        }

        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);
        locationPrefsEditor  = locationPrefs.edit();
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        map = (TextView) rootView.findViewById(R.id.map);
        list = (TextView) rootView.findViewById(R.id.list);

        savedValue = locationPrefs.getString("display", "map");

        if(savedValue.equalsIgnoreCase("map")){
            if(language.equalsIgnoreCase("En")) {
                map.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                list.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }else if(language.equalsIgnoreCase("Ar")){
                map.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                list.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

        }else if(savedValue.equalsIgnoreCase("list")){
            if(language.equalsIgnoreCase("En")) {
                list.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                map.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }else if(language.equalsIgnoreCase("Ar")){
                list.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                map.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPrefsEditor.putString("display", "map");
                locationPrefsEditor.commit();
                if(language.equalsIgnoreCase("En")) {
                    map.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                    list.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    map.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                    list.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(new LocationDisplayFragment());
//                trans.remove(new MoreLocation());
                trans.commit();
                FragmentManager manager1 = getActivity().getSupportFragmentManager();
                FragmentTransaction trans1 = manager1.beginTransaction();
                trans1.remove(new MoreLocation());
                trans1.commit();

                manager1.popBackStack();
                manager.popBackStack();

                Fragment mapFragment = new MoreLocation();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction mapTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                mapTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                mapTransaction.add(R.id.realtabcontent, mapFragment);
                mapTransaction.hide(LocationDisplayFragment.this);
                mapTransaction.addToBackStack(null);

                // Commit the transaction
                mapTransaction.commit();

            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPrefsEditor.putString("display", "list");
                locationPrefsEditor.commit();
                if(language.equalsIgnoreCase("En")) {
                    list.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                    map.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }else if(language.equalsIgnoreCase("Ar")){
                    list.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark, 0, 0, 0);
                    map.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }

                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(new LocationDisplayFragment());
//                trans.remove(new MoreLocation());
                trans.commit();
                FragmentManager manager1 = getActivity().getSupportFragmentManager();
                FragmentTransaction trans1 = manager1.beginTransaction();
                trans1.remove(new MoreLocation());
                trans1.commit();

                manager1.popBackStack();
                manager.popBackStack();

                Fragment radiusFragment = new MoreLocation();
//                Bundle mBundle12 = new Bundle();
//                mBundle12.putString("info","mileStone");
//                radiusFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                radiusTransaction.hide(LocationDisplayFragment.this);
                radiusTransaction.addToBackStack(null);

                // Commit the transaction
                radiusTransaction.commit();

            }
        });

        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
