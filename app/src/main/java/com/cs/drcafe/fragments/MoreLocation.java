package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;

/**
 * Created by SKT on 27-12-2015.
 */
public class MoreLocation extends Fragment implements View.OnClickListener{

    Button backBtn;
    RelativeLayout distanceLayout, radiusLayout, mapDisplayLayout;
    TextView distance, radius, mapDisplay;
    SharedPreferences locationPrefs;
    String distancePref, radiusPref, displayPref;
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.more_location, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.more_location_arabic, container,
                    false);
        }

        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);
        distanceLayout = (RelativeLayout) rootView.findViewById(R.id.distance_layout);
        radiusLayout = (RelativeLayout) rootView.findViewById(R.id.radiius_layout);
        mapDisplayLayout = (RelativeLayout) rootView.findViewById(R.id.map_disply_layout);

        distance = (TextView) rootView.findViewById(R.id.distance);
        radius = (TextView) rootView.findViewById(R.id.radius);
        mapDisplay = (TextView) rootView.findViewById(R.id.map_display);

        backBtn = (Button) rootView.findViewById(R.id.back_btn);

        distancePref = locationPrefs.getString("distance","kilometer");
        radiusPref = locationPrefs.getString("radius","50");
        displayPref = locationPrefs.getString("display","map");

        if(distancePref.equalsIgnoreCase("kilometer")){
            if(language.equalsIgnoreCase("En")) {
                distance.setText("Kilometer");
                if(radiusPref.equalsIgnoreCase("5")){
                    radius.setText("5 KM");
                }else if(radiusPref.equalsIgnoreCase("10")){
                    radius.setText("10 KM");
                }else if(radiusPref.equalsIgnoreCase("20")){
                    radius.setText("20 KM");
                }else if(radiusPref.equalsIgnoreCase("40")){
                    radius.setText("40 KM");
                }else if(radiusPref.equalsIgnoreCase("50")){
                    radius.setText("50 KM");
                }
            }else if(language.equalsIgnoreCase("Ar")){
                distance.setText("كيلو متر");
                if(radiusPref.equalsIgnoreCase("5")){
                    radius.setText("5كيلو");
                }else if(radiusPref.equalsIgnoreCase("10")){
                    radius.setText("10كيلو");
                }else if(radiusPref.equalsIgnoreCase("20")){
                    radius.setText("20كيلو");
                }else if(radiusPref.equalsIgnoreCase("40")){
                    radius.setText("40كيلو");
                }else if(radiusPref.equalsIgnoreCase("50")){
                    radius.setText("50كيلو");
                }
            }

        }else if(distancePref.equalsIgnoreCase("mile")){
            if(language.equalsIgnoreCase("En")) {
                distance.setText("Mile");
            }else if(language.equalsIgnoreCase("Ar")){
                distance.setText("ميل");
            }

            if(radiusPref.equalsIgnoreCase("5")){
                radius.setText("5 Mi");
            }else if(radiusPref.equalsIgnoreCase("10")){
                radius.setText("10 Mi");
            }else if(radiusPref.equalsIgnoreCase("20")){
                radius.setText("20 Mi");
            }else if(radiusPref.equalsIgnoreCase("40")){
                radius.setText("40 Mi");
            }else if(radiusPref.equalsIgnoreCase("50")){
                radius.setText("50 Mi");
            }
        }

        if(displayPref.equalsIgnoreCase("map")){
            if(language.equalsIgnoreCase("En")) {
                mapDisplay.setText("Map");
            }else if(language.equalsIgnoreCase("Ar")){
                mapDisplay.setText("الخريطة");
            }

        }else if(displayPref.equalsIgnoreCase("list")){
            if(language.equalsIgnoreCase("En")) {
                mapDisplay.setText("List");
            }else if(language.equalsIgnoreCase("Ar")){
                mapDisplay.setText("قائمة");
            }
        }

        backBtn.setOnClickListener(this);
        distanceLayout.setOnClickListener(this);
        radiusLayout.setOnClickListener(this);
        mapDisplayLayout.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.back_btn:
                getActivity().onBackPressed();
                break;

            case R.id.distance_layout:
                Fragment distanceFragment = new DistanceFragment();
                Bundle mBundle11 = new Bundle();
                mBundle11.putString("info","mileStone");
                distanceFragment.setArguments(mBundle11);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction distanceTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                distanceTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                distanceTransaction.add(R.id.realtabcontent, distanceFragment);
                distanceTransaction.hide(MoreLocation.this);
                distanceTransaction.addToBackStack(null);

                // Commit the transaction
                distanceTransaction.commit();
                break;

            case R.id.radiius_layout:
                Fragment radiusFragment = new RadiusFragment();
                Bundle mBundle12 = new Bundle();
                mBundle12.putString("info","mileStone");
                radiusFragment.setArguments(mBundle12);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction radiusTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                radiusTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                radiusTransaction.add(R.id.realtabcontent, radiusFragment);
                radiusTransaction.hide(MoreLocation.this);
                radiusTransaction.addToBackStack(null);

                // Commit the transaction
                radiusTransaction.commit();
                break;

            case R.id.map_disply_layout:
                Fragment locationDisplayFragment = new LocationDisplayFragment();
                Bundle mBundle13 = new Bundle();
                mBundle13.putString("info","mileStone");
                locationDisplayFragment.setArguments(mBundle13);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction locationDisplayTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                locationDisplayTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                locationDisplayTransaction.add(R.id.realtabcontent, locationDisplayFragment);
                locationDisplayTransaction.hide(MoreLocation.this);
                locationDisplayTransaction.addToBackStack(null);

                // Commit the transaction
                locationDisplayTransaction.commit();
                break;
        }

    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
