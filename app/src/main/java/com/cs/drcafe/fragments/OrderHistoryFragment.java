package com.cs.drcafe.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.OrderHistoryAdapter;
import com.cs.drcafe.adapter.OrderHistoryAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.OrderHistory;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by brahmam on 19/2/16.
 */
public class OrderHistoryFragment extends Fragment {
    private String SOAP_ACTION = "http://tempuri.org/getOrderHistory";
    private String METHOD_NAME = "getOrderHistory";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;

    private String SOAP_ACTION_DELETE = "http://tempuri.org/deleteOrderHistory";
    private String METHOD_NAME_DELETE = "deleteOrderHistory";


    private ArrayList<OrderHistory> transactionsList = new ArrayList<>();
    private SwipeMenuListView orderHistoryListView;
    private OrderHistoryAdapter mAdapter;
    private OrderHistoryAdapterArabic mAdapterArabic;
    Button backBtn;
    SharedPreferences userPrefs;
    String userId;
    TextView title;
    ProgressDialog dialog;
    String language;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        View rootView = inflater.inflate(R.layout.favourite_order, container,
                false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        orderHistoryListView = (SwipeMenuListView) rootView.findViewById(R.id.fav_order_listview);
        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        title = (TextView) rootView.findViewById(R.id.title);

        if(language.equalsIgnoreCase("En")){
            title.setText("Order History");
            mAdapter = new OrderHistoryAdapter(getActivity(), transactionsList);
            orderHistoryListView.setAdapter(mAdapter);
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("جميع الطلبات");
            mAdapterArabic = new OrderHistoryAdapterArabic(getActivity(), transactionsList);
            orderHistoryListView.setAdapter(mAdapterArabic);
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        new GetOrderHistory().execute();

        orderHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment merchandiseFragment = new TrackOrderFragment();
                Bundle mBundle7 = new Bundle();
                mBundle7.putString("orderId", transactionsList.get(position).getOrderId());
                mBundle7.putInt("flag", 1);
                merchandiseFragment.setArguments(mBundle7);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction merchandiseTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                merchandiseTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction.add(R.id.realtabcontent, merchandiseFragment);
                merchandiseTransaction.hide(OrderHistoryFragment.this);
                merchandiseTransaction.addToBackStack(null);

                // Commit the transaction
                merchandiseTransaction.commit();
            }
        });


        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity().getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        orderHistoryListView.setMenuCreator(creator);

        // step 2. listener item click event
        orderHistoryListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        if(transactionsList.get(position).getOrderStatus().equals("Close") || transactionsList.get(position).getOrderStatus().equals("Rejected")){
                            new DeleteOrderHistory().execute(transactionsList.get(position).getOrderId());
                        }else{
                            Toast.makeText(getActivity(), "Order not completed", Toast.LENGTH_SHORT).show();
                        }

                        break;
                }
                return false;
            }
        });



        return rootView;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    public class GetOrderHistory extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            transactionsList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("UserId", userId);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                response12 = "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (response12 != null){
                if (response12.equals("no internet")) {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                } else if (response12.equals("anyType{}")) {
                    dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                } else {

                    try {

                        Object property = result.getProperty(1);
                        if (property instanceof SoapObject) {
                            try {
                                SoapObject category_list = (SoapObject) property;
                                SoapObject storeObject = (SoapObject) category_list.getProperty(0);
                                for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                    OrderHistory oh = new OrderHistory();
                                    SoapObject sObj = (SoapObject) storeObject.getProperty(i);
//                            String TransactionNo = sObj.getPropertyAsString("TransactionNo");

                                    String orderId = sObj.getPropertyAsString("orderId");
                                    String storeId = sObj.getPropertyAsString("storeId");
                                    String orderDate = sObj.getPropertyAsString("orderDate");
                                    boolean isFavourite = (Boolean.valueOf(sObj.getPropertyAsString("IsFavorite")));
                                    String storeName = sObj.getPropertyAsString("StoreName");
                                    String storeName_ar = sObj.getPropertyAsString("StoreName_ar");
                                    String total_Price = sObj.getPropertyAsString("Total_Price");
                                    String status = sObj.getPropertyAsString("status");
                                    String orderStatus = sObj.getPropertyAsString("OrderStatus");
                                    String orderType = sObj.getPropertyAsString("OrderType");
                                    String address = sObj.getPropertyAsString("HouseNo") + "," +
                                            sObj.getPropertyAsString("CAddress");


                                    String invoiceNo = null;
                                    try {
                                        invoiceNo = sObj.getPropertyAsString("InvoiceNo");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        invoiceNo = "";
                                    }


                                    oh.setOrderId(orderId);
                                    oh.setStoreId(storeId);
                                    oh.setOrderDate(orderDate);
                                    oh.setOrderId(orderId);
                                    oh.setIsFavorite(isFavourite);
                                    oh.setStoreName(storeName);
                                    oh.setStoreName_ar(storeName_ar);
                                    oh.setTotalPrice(total_Price);
                                    oh.setStatus(status);
                                    oh.setOrderStatus(orderStatus);
                                    oh.setOrderType(orderType);
                                    oh.setAddress(address);
                                    oh.setInvoiceNo(invoiceNo);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    transactionsList.add(oh);

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//                    }

                    } catch (Exception e) {
                        Log.d("", "Error while parsing the results!");
                        e.printStackTrace();
                    }


                    dialog.dismiss();
                    if (language.equalsIgnoreCase("En")) {
                        mAdapter.notifyDataSetChanged();
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mAdapterArabic.notifyDataSetChanged();
                    }

                    super.onPostExecute(result1);
                }
        }
        }
    }



    public class DeleteOrderHistory extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_DELETE);
                    request.addProperty("OrderId", arg0[0]);
                    Log.i("TAG", ""+arg0[0]);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_DELETE, envelope);
//                    String deleteResult = envelope.bodyIn.toString();
//                    deleteResponse = deleteResult.toString();
                    Log.v("TAG", "" + envelope.bodyIn);
                    // SoapObject root = (SoapObject) result.getProperty(0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            dialog.dismiss();
            new GetOrderHistory().execute();
//            if(deleteResponse != null){
//
//            }else if(deleteResponse.equals("no internet")){
//                dialog.dismiss();
//                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
//            }
//            else if (deleteResponse.equals("anyType{}")) {
//                dialog.dismiss();
////                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
//            } else {
//
//
//            }
        }
    }



    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

}
