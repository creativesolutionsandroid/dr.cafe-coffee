package com.cs.drcafe.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.DecimalFormat;

/**
 * Created by SKT on 21-02-2016.
 */
public class OrderDetails extends Fragment implements OnMapReadyCallback {
    private String SOAP_ACTION_INSERT = "http://tempuri.org/InsertFavorieOrderName";
    private String METHOD_NAME_INSERT = "InsertFavorieOrderName";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapPrimitive insertResult;
    private String insertResponse = null;
    private GoogleMap map;
    MarkerOptions markerOptions;
    TextView addressType,storeNameTextView, storeAddressTextView, totalItems, totalAmount, estTime, orderNumber, paymentMode, orderType;
    Button getDirection,trackOrder, createOrder;
    ImageView favOrder;
    private String storeId, storeName, storeAddress, total_amt, sub_total, vat, total_items, expected_time, payment_mode, order_type, order_number, orderId;
    private Double latitude, longitude;
    LinearLayout recieptLayout, vatLayout;
    TextView netTotal, vatAmount, vatPercent, amount,  promotionAmount, subTotalAmount;
    ImageView receipt_close;
    SharedPreferences userPrefs;
    String response;
    SharedPreferences orderPrefs;
    AlertDialog alertDialog;
    SharedPreferences.Editor orderPrefsEditor;
    final DecimalFormat priceFormat = new DecimalFormat("0.00");
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_details, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.order_details_arabic, container,
                    false);
        }
        storeId = getArguments().getString("storeId");
        storeName = getArguments().getString("storeName");
        storeAddress = getArguments().getString("storeAddress");
        latitude = getArguments().getDouble("latitude");
        longitude = getArguments().getDouble("longitude");
        total_amt = getArguments().getString("total_amt");
        sub_total = getArguments().getString("sub_total");
        vat = getArguments().getString("vat");
        total_items = getArguments().getString("total_items");
        expected_time = getArguments().getString("expected_time");
        payment_mode = getArguments().getString("payment_mode");
        order_type = getArguments().getString("order_type");
        order_number = getArguments().getString("order_number");
        orderPrefs = getActivity().getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor  = orderPrefs.edit();
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        response = userPrefs.getString("user_profile", null);
        DrConstants.NO_BACKPRESS = true;

        addressType = (TextView) rootView.findViewById(R.id.address_type);
        storeNameTextView = (TextView) rootView.findViewById(R.id.store_detail_name);
        storeAddressTextView = (TextView) rootView.findViewById(R.id.store_detail_address);
        totalItems = (TextView) rootView.findViewById(R.id.total_items);
        totalAmount = (TextView) rootView.findViewById(R.id.total_amount);
        estTime = (TextView) rootView.findViewById(R.id.expected_time);
        orderNumber = (TextView) rootView.findViewById(R.id.order_number);
        paymentMode = (TextView) rootView.findViewById(R.id.payment_mode);
        orderType = (TextView) rootView.findViewById(R.id.order_type);
        getDirection = (Button) rootView.findViewById(R.id.get_direction);
        trackOrder = (Button) rootView.findViewById(R.id.track_order_btn);
        createOrder = (Button) rootView.findViewById(R.id.create_order_btn);
        favOrder = (ImageView) rootView.findViewById(R.id.fav_order);
        promotionAmount = (TextView) rootView.findViewById(R.id.promo_amount);
        subTotalAmount = (TextView) rootView.findViewById(R.id.subAmount);

        recieptLayout = (LinearLayout) rootView.findViewById(R.id.receipt_layout);
        receipt_close = (ImageView) rootView.findViewById(R.id.receipt_close);
        amount = (TextView) rootView.findViewById(R.id.amount);
        vatAmount = (TextView) rootView.findViewById(R.id.vatAmount);
        vatPercent = (TextView) rootView.findViewById(R.id.vatPercent);
        netTotal = (TextView) rootView.findViewById(R.id.net_total);
        vatLayout = (LinearLayout) rootView.findViewById(R.id.vatLayout);

        if (order_type.equalsIgnoreCase("Delivery")) {
            if (language.equalsIgnoreCase("En")) {
                addressType.setText("Delivery Address");
            }
            else {
                addressType.setText("عنوان التوصيل");
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(OrderDetails.this);

//        FragmentManager fm = getChildFragmentManager();
//        Fragment fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
//        if (fragment == null) {
//            fragment = SupportMapFragment.newInstance();
//            fm.beginTransaction().replace(R.id.map, fragment).commit();
//        }
//        map = ((SupportMapFragment) fragment).getMap();
//        markerOptions = new MarkerOptions();
//        markerOptions.position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
//        map.addMarker(markerOptions);
//        map.animateCamera(CameraUpdateFactory.newLatLngZoom
//                (new LatLng(latitude, longitude), 7.0f));

        Log.d("TAG", "onCreateView: "+order_number);
        String[] orderNo = order_number.split("-");
        String[] parts = order_number.split(",");
        orderId = parts[0];

        orderPrefsEditor.putString("order_id", orderId);
        orderPrefsEditor.putString("order_status", "open");
        orderPrefsEditor.commit();

        storeNameTextView.setText(storeName);
        storeAddressTextView.setText(storeAddress);
        totalItems.setText(total_items);
        totalAmount.setText(total_amt);
        estTime.setText(expected_time);
        amount.setText("" + sub_total);
        vatAmount.setText(""+vat);
        netTotal.setText(""+total_amt);
        vatPercent.setText("("+DrConstants.vatString+"%)");
        subTotalAmount.setText(""+getArguments().getString("subTotalAmount"));
        promotionAmount.setText("- "+getArguments().getString("promoAmt"));

//        if(payment_mode.equals("2")){
//            if(language.equalsIgnoreCase("En")) {
//                paymentMode.setText("Cash on Pick Up");
//            }else if(language.equalsIgnoreCase("Ar")){
//                paymentMode.setText("نقدي عند استلام الطلب");
//            }
//
//        }else if(payment_mode.equals("3")){
//            if(language.equalsIgnoreCase("En")) {
//                paymentMode.setText("Online Payment");
//            }else if(language.equalsIgnoreCase("Ar")){
//                paymentMode.setText("سداد أونلاين");
//            }
//
//        }

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recieptLayout.setVisibility(View.GONE);
            }
        });

        vatLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recieptLayout.setVisibility(View.VISIBLE);
            }
        });

        if(language.equalsIgnoreCase("En")) {
            if (payment_mode.equals("2") && !order_type.equalsIgnoreCase("Delivery")) {
                paymentMode.setText("Cash on Pick Up");
            } else if (payment_mode.equals("2") && order_type.equalsIgnoreCase("Delivery")) {
                paymentMode.setText("Cash on Delivery");
            } else if (payment_mode.equals("3")) {
                paymentMode.setText("Online Payment");
            }else if(payment_mode.equals("4")){
                paymentMode.setText("Free Drink");
            }

            orderType.setText(order_type);

        }else if(language.equalsIgnoreCase("Ar")) {
            if (payment_mode.equals("2") && !order_type.equalsIgnoreCase("Delivery")) {
                paymentMode.setText("نقدي عند استلام الطلب");
            }else if (payment_mode.equals("2") && order_type.equalsIgnoreCase("Delivery")) {
                paymentMode.setText("الدفع كاش عند التوصيل");
            } else if (payment_mode.equals("3")) {
                paymentMode.setText("سداد أونلاين");
            } else if(payment_mode.equals("4")){
                paymentMode.setText("مجاني");
            }

            if(order_type.equalsIgnoreCase("Dine In")){
                orderType.setText("الاستمتاع في الفرع");
            }else if(order_type.equalsIgnoreCase("Pick Up")){
                orderType.setText("الاستمتاع خارج الفرع");
            }
            else if(order_type.equalsIgnoreCase("Delivery")){
                orderType.setText("التوصيل");
            }
        }
//        paymentMode.setText(payment_mode);
//        orderType.setText(order_type);

        orderNumber.setText(orderNo[1]);


        getDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=24.70321657,46.68097073&daddr=" + latitude + "," + longitude));
                startActivity(intent);
            }
        });

        trackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrConstants.NO_BACKPRESS = false;
                Fragment merchandiseFragment = new TrackOrderFragment();
                Bundle mBundle7 = new Bundle();
                mBundle7.putString("orderId", orderId);
                mBundle7.putInt("flag", 0);
                merchandiseFragment.setArguments(mBundle7);

                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction merchandiseTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                merchandiseTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                merchandiseTransaction.add(R.id.realtabcontent, merchandiseFragment);
                merchandiseTransaction.hide(OrderDetails.this);
                merchandiseTransaction.addToBackStack(null);

                // Commit the transaction
                merchandiseTransaction.commit();
            }
        });

        createOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrConstants.NO_BACKPRESS = false;
                ((MainActivity) getActivity()).setCurrenTab(0);
                ((MainActivity) getActivity()).setCurrenTab(2);
            }
        });

        favOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.insert_fav_order_dialog, null);
                dialogBuilder.setView(dialogView);

                final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                TextView cancelBtn = (TextView) dialogView.findViewById(R.id.no_cancel);
                final TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                favName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(favName.getText().toString().length()>0){
                            saveOrder.setEnabled(true);
                        }else{
                            saveOrder.setEnabled(false);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {


                    }
                });


                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });



                saveOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new GetCardBalance().execute(favName.getText().toString());
                        alertDialog.dismiss();
                    }
                });


                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });


        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        try {
            markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
        map.addMarker(markerOptions);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom
                (new LatLng(latitude, longitude), 7.0f));
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }


    public class GetCardBalance extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        }

        @Override
        protected String doInBackground(String... arg0) {

            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_INSERT);
                    request.addProperty("orderID", orderId);
                    request.addProperty("FavoriteName", arg0[0]);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_INSERT, envelope);
                    insertResult = (SoapPrimitive) envelope.getResponse();
                    insertResponse = insertResult.toString();
                    Log.v("TAG", insertResponse);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + insertResult);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + insertResponse);
            }else{
                return "no internet";
            }
            return insertResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                if(result1.equalsIgnoreCase("no internet")){
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    favOrder.setImageResource(R.drawable.favourite_select);
                }
            }


            super.onPostExecute(result1);
        }
    }




    public void noBackPress(){
        ((MainActivity) getActivity()).onBackPressed();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DrConstants.NO_BACKPRESS = false;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
