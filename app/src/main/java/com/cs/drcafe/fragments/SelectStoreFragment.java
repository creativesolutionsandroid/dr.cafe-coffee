package com.cs.drcafe.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.GPSTracker;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.adapter.SelectStoreAdapter;
import com.cs.drcafe.adapter.SelectStoreAdapterArabic;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.Item;
import com.cs.drcafe.model.SectionItem;
import com.cs.drcafe.model.StoreInfo;
import com.cs.drcafe.widget.FlipAnimation;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by brahmam on 15/2/16.
 */
public class SelectStoreFragment extends Fragment implements View.OnClickListener, TextWatcher{

    private String SOAP_ACTION = "http://tempuri.org/getOnlineOrderStores";
    private String METHOD_NAME = "getOnlineOrderStores";
    private final String NAMESPACE = "http://tempuri.org/";

    private String SOAP_ACTION_TIME = "http://tempuri.org/getCurrentTime";
    private String METHOD_NAME_TIME = "getCurrentTime";
    private SoapPrimitive timeResult;
    private String timeResponse = null;
    private Timer timer = new Timer();
    boolean isFirstTime = true;

    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    private ArrayList<StoreInfo> favouriteStoresList = new ArrayList<>();
    private ArrayList<Item> totalStoresList = new ArrayList<>();
    private ArrayList<StoreInfo> tempStoresList = new ArrayList<>();
    private ArrayList<StoreInfo> filterStoresList = new ArrayList<>();
    private ListView mStoresListView;
    RelativeLayout buttonsLayout;
    private Button filterBtn;
    private TextView doneBtn;
    private LinearLayout mListviewLayout;
    private LinearLayout filterLayout, listLayout;
    private RelativeLayout storeLayout;
    String response12 = null;
    SoapObject result;
    SelectStoreAdapter mStoreListAdapter;
    SelectStoreAdapterArabic mStoreListAdapterArabic;
    private double lat, longi;
    private TextView wifiText, driveText, familyText, meetingText, patioText, v12Text, universityText, hospitalText, officeText, shoppingText, airportText, dineinText, ladiesText, neighborText;
    private EditText searchBar;
    public boolean isWifi, isDrive, isFamily, isMeeting, isPatio, isV12, isUniversity, isHospital, isOffice, isShopping, isAirport, isDineIn, isLadies, isNeighborhood;
    private boolean isAddedtoList, isFilterOn;
    ProgressDialog dialog;
    Button backBtn;
    GPSTracker gps;
    public static boolean isGPSRefreshed = false;
    private static final String STORE_URL = "http://www.drcafeonline.net/dcservices.asmx";
//    private static final String STORE_URL = "http://csadms.com/dcservices/DCServices.asmx";
    private static final String URL = DrConstants.URL;
    SharedPreferences favouritePrefs, locationPrefs;
    SharedPreferences.Editor favEditor;
    ArrayList<String> favoriteStores = new ArrayList<>();
    String favStoreIds, previousStoreIds;
    String mDistancePrefValue, mRadiusPrefValue, mDisplayPrefValue;
    AlertDialog alertDialog;
    String language;
    View rootView;
    SharedPreferences storePrefs;
    SharedPreferences.Editor storePrefsEditor;

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.select_store, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.select_store_arabic, container,
                    false);
        }

        storePrefs = getContext().getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);
        storePrefsEditor = storePrefs.edit();
        DrConstants.menuFlag = false;

        mStoresListView = (ListView) rootView.findViewById(R.id.store_listview);
//        mFavoriteListView = (ListView) rootView.findViewById(R.id.favourite_listview);

        mListviewLayout = (LinearLayout) rootView.findViewById(R.id.listview_layout);
        buttonsLayout = (RelativeLayout) rootView.findViewById(R.id.relativeLayout);
        filterBtn = (Button) rootView.findViewById(R.id.filter_btn);
        doneBtn = (TextView) rootView.findViewById(R.id.done_btn);
        listLayout = (LinearLayout) rootView.findViewById(R.id.list_layout);
        filterLayout = (LinearLayout) rootView.findViewById(R.id.filter_layout);
        storeLayout = (RelativeLayout) rootView.findViewById(R.id.store_layout);
        v12Text = (TextView) rootView.findViewById(R.id.filter_v12);

//        mFavouriteAdapter = new StoreListAdapter(getActivity(), favouriteStoresList);
//        mFavoriteListView.setAdapter(mFavouriteAdapter);
        wifiText = (TextView) rootView.findViewById(R.id.filter_hotspot);
        driveText = (TextView) rootView.findViewById(R.id.filter_drive);
        familyText = (TextView) rootView.findViewById(R.id.filter_family);
        meetingText = (TextView) rootView.findViewById(R.id.filter_meeting);
        patioText = (TextView) rootView.findViewById(R.id.filter_patio);
        universityText = (TextView) rootView.findViewById(R.id.filter_university);
        hospitalText = (TextView) rootView.findViewById(R.id.filter_hospital);
        officeText = (TextView) rootView.findViewById(R.id.filter_office);
        shoppingText = (TextView) rootView.findViewById(R.id.filter_shopping);
        airportText = (TextView) rootView.findViewById(R.id.filter_airport);
        dineinText = (TextView) rootView.findViewById(R.id.filter_dinein);
        ladiesText = (TextView) rootView.findViewById(R.id.filter_ladies);
        neighborText = (TextView) rootView.findViewById(R.id.filter_neighbohood);
        searchBar = (EditText) rootView.findViewById(R.id.search_bar);

        backBtn = (Button) rootView.findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrConstants.menuFlag = false;
                    ((MainActivity) getActivity()).setCurrenTab(0);
                    ((MainActivity) getActivity()).setCurrenTab(2);
            }
        });
        wifiText.setOnClickListener(this);
        driveText.setOnClickListener(this);
        familyText.setOnClickListener(this);
        meetingText.setOnClickListener(this);
        patioText.setOnClickListener(this);
        universityText.setOnClickListener(this);
        hospitalText.setOnClickListener(this);
        officeText.setOnClickListener(this);
        shoppingText.setOnClickListener(this);
        airportText.setOnClickListener(this);
        dineinText.setOnClickListener(this);
        ladiesText.setOnClickListener(this);
        neighborText.setOnClickListener(this);
        v12Text.setOnClickListener(this);
        searchBar.addTextChangedListener(this);

        searchBar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    searchBar.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                }else{
                    searchBar.setGravity(Gravity.CENTER);
                }
            }
        });

        favouritePrefs = getActivity().getSharedPreferences("FAV_PREFS", Context.MODE_PRIVATE);
        locationPrefs = getActivity().getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);
        favEditor  = favouritePrefs.edit();

        mDistancePrefValue = locationPrefs.getString("distance", "kilometer");
        mRadiusPrefValue = locationPrefs.getString("radius",DrConstants.storeDistance);
        mDisplayPrefValue = locationPrefs.getString("display","map");
        favStoreIds = favouritePrefs.getString("favourite_store_ids",null);
        previousStoreIds = favStoreIds;
        if(favStoreIds != null){
            String[] parts = favStoreIds.split(",");
            for(int i= 0;i < parts.length;i++){
                favoriteStores.add(parts[i]);
            }
        }else{
        }

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }

        mStoresListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Item listitem = totalStoresList.get(i);
                final StoreInfo si = (StoreInfo) listitem;

                storePrefsEditor.clear();
                storePrefsEditor.commit();
                if(si.isDineIn() || si.isPickUp() || si.isDeliverable()) {
                    if (!si.is24x7()) {
                        if (si.getOpenFlag() != 1 && !si.isDeliverable()) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    getActivity());

                            if (language.equalsIgnoreCase("En")) {
                                // set title
                                alertDialogBuilder.setTitle("dr.CAFE");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("Sorry Store is closed! You can't make the order from this store.")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                            } else if (language.equalsIgnoreCase("Ar")) {
                                // set title
                                alertDialogBuilder.setTitle("د. كيف");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
                                        .setCancelable(false)
                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                            }

                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        }
                        else if (si.isDeliverable() && !si.isPickUp() && !si.isDineIn()) {
                            {
                                Log.d("TAG", "onItemClick: 2");
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View dialogView = null;
                                if (language.equalsIgnoreCase("En")) {
                                    dialogView = inflater.inflate(R.layout.store_select_dialog_delivery, null);
                                } else {
                                    dialogView = inflater.inflate(R.layout.store_select_dialog_delivery_ar, null);
                                }
                                dialogBuilder.setView(dialogView);

                                ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                                ImageView pickup = (ImageView) dialogView.findViewById(R.id.select_pickup);
                                ImageView dineIn = (ImageView) dialogView.findViewById(R.id.select_dinein);
//                        ImageView pickupBig = (ImageView) dialogView.findViewById(R.id.select_pickup_big);
                                RelativeLayout pickupLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_layout);
                                RelativeLayout dineinLayout = (RelativeLayout) dialogView.findViewById(R.id.dinein_layout);
//                        RelativeLayout pickupBigLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_big_layout);
                                RelativeLayout deliveryLayout = (RelativeLayout) dialogView.findViewById(R.id.delivery_layout);
                                TextView deliveryTv = (TextView) dialogView.findViewById(R.id.delivery_txt);
                                TextView storeMsg = (TextView) dialogView.findViewById(R.id.store_msg);
                                TextView pickupTv = (TextView) dialogView.findViewById(R.id.pickup_txt);
                                TextView dineInTv = (TextView) dialogView.findViewById(R.id.dinein_txt);
//                        TextView pickupBigTv = (TextView) dialogView.findViewById(R.id.pickup_big_txt);
                                TextView heading = (TextView) dialogView.findViewById(R.id.order_type_heading);
//                        pickupBigLayout.setVisibility(View.GONE);
                                if (language.equalsIgnoreCase("En")) {
                                    pickupTv.setText("Pick Up");
                                    dineInTv.setText("Dine In");
//                            pickupBigTv.setText("Pick Up");
                                    deliveryTv.setText("Delivery");
                                    heading.setText("Order Type");
                                    deliveryTv.setText(si.getDeliveryMessageEn());
                                    if (si.getMessage().equalsIgnoreCase("")) {
                                        storeMsg.setVisibility(View.GONE);
                                    } else {
                                        storeMsg.setVisibility(View.VISIBLE);
                                        storeMsg.setText(si.getMessage());
                                    }
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    pickupTv.setText("الاستمتاع خارج الفرع");
                                    dineInTv.setText("الاستمتاع في الفرع");
//                            pickupBigTv.setText("الاستمتاع خارج الفرع");
                                    heading.setText("نوع الطلب");
                                    deliveryTv.setText(si.getDeliveryMessageAr());
                                    if (si.getMessage_ar().equalsIgnoreCase("")) {
                                        storeMsg.setVisibility(View.GONE);
                                    } else {
                                        storeMsg.setVisibility(View.VISIBLE);
                                        storeMsg.setText(si.getMessage_ar());
                                    }
                                }

                                dineinLayout.setVisibility(View.GONE);
                                pickupLayout.setVisibility(View.GONE);
                                if (si.isDeliverable()) {
                                    deliveryLayout.setVisibility(View.VISIBLE);
                                } else {
                                    deliveryLayout.setVisibility(View.GONE);
                                }

                                DrConstants.storeName = si.getStoreName();
                                DrConstants.storeAddress = si.getStoreAddress();
                                DrConstants.storeId = si.getStoreId();
                                DrConstants.latitude = si.getLatitude();
                                DrConstants.longitude = si.getLongitude();
                                DrConstants.fullHours = si.is24x7();
                                DrConstants.startTime = si.getStartTime();
                                DrConstants.endTime = si.getEndTime();
                                DrConstants.CashPayment = si.isCashPayment();
                                DrConstants.OnlinePayment = si.isOnlinePayment();

                                Gson gson = new Gson();
                                String jsonText = gson.toJson(si);

                                storePrefsEditor.putString("store", jsonText);
                                storePrefsEditor.putInt("position", i);
                                storePrefsEditor.apply();

                                pickup.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        DrConstants.orderNow = true;
                                        DrConstants.orderType = "Pick Up";

                                        storePrefsEditor.putString("orderType", DrConstants.orderType);
                                        storePrefsEditor.apply();

                                        alertDialog.dismiss();
                                        DrConstants.menuFlag = true;

                                        Fragment newFragment = new MenuFragment();
                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                        // Replace whatever is in the fragment_container view with this fragment,
                                        // and add the transaction to the back stack
                                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                        transaction.add(R.id.realtabcontent, newFragment, "menu");
                                        transaction.hide(SelectStoreFragment.this);
                                        transaction.addToBackStack(null);

                                        // Commit the transaction
                                        transaction.commit();
                                    }
                                });

//                        pickupBig.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                DrConstants.orderNow = true;
//                                DrConstants.orderType = "Pick Up";
//                                storePrefsEditor.putString("orderType", DrConstants.orderType);
//                                storePrefsEditor.commit();
//
//                                alertDialog.dismiss();
//                                DrConstants.menuFlag = true;
//
//                                Fragment newFragment = new MenuFragment();
//                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//
//                                // Replace whatever is in the fragment_container view with this fragment,
//                                // and add the transaction to the back stack
//                                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                                transaction.add(R.id.realtabcontent, newFragment, "menu");
//                                transaction.hide(SelectStoreFragment.this);
//                                transaction.addToBackStack(null);
//
//                                // Commit the transaction
//                                transaction.commit();
//                            }
//                        });

                                deliveryLayout.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        DrConstants.orderNow = true;
                                        DrConstants.orderType = "Delivery";
                                        DrConstants.MinOrderAmount = si.getMinOrderAmount();
                                        DrConstants.DeliveryCharge = si.getDeliveryCharge();
                                        DrConstants.FreeDeliveryAbove = si.getFreeDeliveryAbove();
                                        storePrefsEditor.putFloat("MinOrderAmount", DrConstants.MinOrderAmount);
                                        storePrefsEditor.putFloat("DeliveryCharge", DrConstants.DeliveryCharge);
                                        storePrefsEditor.putFloat("FreeDeliveryAbove", DrConstants.FreeDeliveryAbove);
                                        storePrefsEditor.putString("orderType", DrConstants.orderType);
                                        storePrefsEditor.commit();

                                        alertDialog.dismiss();
                                        DrConstants.menuFlag = true;

                                        Bundle args = new Bundle();
                                        args.putBoolean("store_details", false);
                                        args.putDouble("lat", si.getLatitude());
                                        args.putDouble("lng", si.getLongitude());
                                        args.putFloat("delDistance", si.getDistanceForDelivery());
                                        Fragment newFragment = new MyAddressActivity();
                                        newFragment.setArguments(args);
                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                        // Replace whatever is in the fragment_container view with this fragment,
                                        // and add the transaction to the back stack
                                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                        transaction.add(R.id.realtabcontent, newFragment, "address");
                                        transaction.hide(SelectStoreFragment.this);
                                        transaction.addToBackStack(null);

                                        // Commit the transaction
                                        transaction.commit();
                                    }
                                });


                                dineIn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                        DrConstants.orderNow = true;
                                        DrConstants.orderType = "Dine In";

                                        storePrefsEditor.putString("orderType", DrConstants.orderType);
                                        storePrefsEditor.apply();

                                        DrConstants.menuFlag = true;

                                        Fragment newFragment = new MenuFragment();
                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                        // Replace whatever is in the fragment_container view with this fragment,
                                        // and add the transaction to the back stack
                                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                        transaction.add(R.id.realtabcontent, newFragment, "menu");
                                        transaction.hide(SelectStoreFragment.this);
                                        transaction.addToBackStack(null);

                                        // Commit the transaction
                                        transaction.commit();
                                    }
                                });


                                cancelBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                    }
                                });

                                alertDialog = dialogBuilder.create();
                                alertDialog.show();

                                //Grab the window of the dialog, and change the width
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = alertDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        }
                        else {
                            Log.d("TAG", "onItemClick: isDeliverable"+si.isDeliverable());
                            Log.d("TAG", "onItemClick: 3isDineIn"+si.isDineIn());
                            Log.d("TAG", "onItemClick: 3isPickUp"+si.isPickUp());

                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = null;
                            if (language.equalsIgnoreCase("En")) {
                                dialogView = inflater.inflate(R.layout.store_select_dialog_delivery, null);
                            } else {
                                dialogView = inflater.inflate(R.layout.store_select_dialog_delivery_ar, null);
                            }
                            dialogBuilder.setView(dialogView);

                            ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                            ImageView pickup = (ImageView) dialogView.findViewById(R.id.select_pickup);
                            ImageView dineIn = (ImageView) dialogView.findViewById(R.id.select_dinein);
//                        ImageView pickupBig = (ImageView) dialogView.findViewById(R.id.select_pickup_big);
                            RelativeLayout pickupLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_layout);
                            RelativeLayout dineinLayout = (RelativeLayout) dialogView.findViewById(R.id.dinein_layout);
//                        RelativeLayout pickupBigLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_big_layout);
                            RelativeLayout deliveryLayout = (RelativeLayout) dialogView.findViewById(R.id.delivery_layout);
                            TextView deliveryTv = (TextView) dialogView.findViewById(R.id.delivery_txt);
                            TextView storeMsg = (TextView) dialogView.findViewById(R.id.store_msg);
                            TextView pickupTv = (TextView) dialogView.findViewById(R.id.pickup_txt);
                            TextView dineInTv = (TextView) dialogView.findViewById(R.id.dinein_txt);
//                        TextView pickupBigTv = (TextView) dialogView.findViewById(R.id.pickup_big_txt);
                            TextView heading = (TextView) dialogView.findViewById(R.id.order_type_heading);
//                        pickupBigLayout.setVisibility(View.GONE);
                            if (language.equalsIgnoreCase("En")) {
                                pickupTv.setText("Pick Up");
                                dineInTv.setText("Dine In");
//                            pickupBigTv.setText("Pick Up");
                                deliveryTv.setText("Delivery");
                                heading.setText("Order Type");
                                deliveryTv.setText(si.getDeliveryMessageEn());
                                if (si.getMessage().equalsIgnoreCase("")) {
                                    storeMsg.setVisibility(View.GONE);
                                } else {
                                    storeMsg.setVisibility(View.VISIBLE);
                                    storeMsg.setText(si.getMessage());
                                }
                            } else if (language.equalsIgnoreCase("Ar")) {
                                pickupTv.setText("الاستمتاع خارج الفرع");
                                dineInTv.setText("الاستمتاع في الفرع");
//                            pickupBigTv.setText("الاستمتاع خارج الفرع");
                                heading.setText("نوع الطلب");
                                deliveryTv.setText(si.getDeliveryMessageAr());
                                if (si.getMessage_ar().equalsIgnoreCase("")) {
                                    storeMsg.setVisibility(View.GONE);
                                } else {
                                    storeMsg.setVisibility(View.VISIBLE);
                                    storeMsg.setText(si.getMessage_ar());
                                }
                            }


                            if (si.isDineIn()) {
                                dineinLayout.setVisibility(View.VISIBLE);
                            } else {
                                dineinLayout.setVisibility(View.GONE);
                            }
                            if (si.isPickUp()) {
                                pickupLayout.setVisibility(View.VISIBLE);
                            } else {
                                pickupLayout.setVisibility(View.GONE);
                            }
                            if (si.isDeliverable()) {
                                deliveryLayout.setVisibility(View.VISIBLE);
                            } else {
                                deliveryLayout.setVisibility(View.GONE);
                            }

                            DrConstants.storeName = si.getStoreName();
                            DrConstants.storeAddress = si.getStoreAddress();
                            DrConstants.storeId = si.getStoreId();
                            DrConstants.latitude = si.getLatitude();
                            DrConstants.longitude = si.getLongitude();
                            DrConstants.fullHours = si.is24x7();
                            DrConstants.startTime = si.getStartTime();
                            DrConstants.endTime = si.getEndTime();
                            DrConstants.CashPayment = si.isCashPayment();
                            DrConstants.OnlinePayment = si.isOnlinePayment();

                            Gson gson = new Gson();
                            String jsonText = gson.toJson(si);

                            storePrefsEditor.putString("store", jsonText);
                            storePrefsEditor.putInt("position", i);
                            storePrefsEditor.apply();

                            pickup.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    DrConstants.orderNow = true;
                                    DrConstants.orderType = "Pick Up";

                                    storePrefsEditor.putString("orderType", DrConstants.orderType);
                                    storePrefsEditor.apply();

                                    alertDialog.dismiss();
                                    DrConstants.menuFlag = true;

                                    Fragment newFragment = new MenuFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack
                                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                    transaction.add(R.id.realtabcontent, newFragment, "menu");
                                    transaction.hide(SelectStoreFragment.this);
                                    transaction.addToBackStack(null);

                                    // Commit the transaction
                                    transaction.commit();
                                }
                            });

//                        pickupBig.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                DrConstants.orderNow = true;
//                                DrConstants.orderType = "Pick Up";
//                                storePrefsEditor.putString("orderType", DrConstants.orderType);
//                                storePrefsEditor.commit();
//
//                                alertDialog.dismiss();
//                                DrConstants.menuFlag = true;
//
//                                Fragment newFragment = new MenuFragment();
//                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//
//                                // Replace whatever is in the fragment_container view with this fragment,
//                                // and add the transaction to the back stack
//                                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
//                                transaction.add(R.id.realtabcontent, newFragment, "menu");
//                                transaction.hide(SelectStoreFragment.this);
//                                transaction.addToBackStack(null);
//
//                                // Commit the transaction
//                                transaction.commit();
//                            }
//                        });

                            deliveryLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    DrConstants.orderNow = true;
                                    DrConstants.orderType = "Delivery";
                                    DrConstants.MinOrderAmount = si.getMinOrderAmount();
                                    DrConstants.DeliveryCharge = si.getDeliveryCharge();
                                    DrConstants.FreeDeliveryAbove = si.getFreeDeliveryAbove();
                                    storePrefsEditor.putFloat("MinOrderAmount", DrConstants.MinOrderAmount);
                                    storePrefsEditor.putFloat("DeliveryCharge", DrConstants.DeliveryCharge);
                                    storePrefsEditor.putFloat("FreeDeliveryAbove", DrConstants.FreeDeliveryAbove);
                                    storePrefsEditor.putString("orderType", DrConstants.orderType);
                                    storePrefsEditor.commit();

                                    alertDialog.dismiss();
                                    DrConstants.menuFlag = true;

                                    Bundle args = new Bundle();
                                    args.putBoolean("store_details", false);
                                    args.putDouble("lat", si.getLatitude());
                                    args.putDouble("lng", si.getLongitude());
                                    args.putFloat("delDistance", si.getDistanceForDelivery());
                                    Fragment newFragment = new MyAddressActivity();
                                    newFragment.setArguments(args);
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack
                                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                    transaction.add(R.id.realtabcontent, newFragment, "address");
                                    transaction.hide(SelectStoreFragment.this);
                                    transaction.addToBackStack(null);

                                    // Commit the transaction
                                    transaction.commit();
                                }
                            });


                            dineIn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    DrConstants.orderNow = true;
                                    DrConstants.orderType = "Dine In";

                                    storePrefsEditor.putString("orderType", DrConstants.orderType);
                                    storePrefsEditor.apply();

                                    DrConstants.menuFlag = true;

                                    Fragment newFragment = new MenuFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack
                                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                    transaction.add(R.id.realtabcontent, newFragment, "menu");
                                    transaction.hide(SelectStoreFragment.this);
                                    transaction.addToBackStack(null);

                                    // Commit the transaction
                                    transaction.commit();
                                }
                            });


                            cancelBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                }
                            });

                            alertDialog = dialogBuilder.create();
                            alertDialog.show();

                            //Grab the window of the dialog, and change the width
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = alertDialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                        }
                    } else {

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = null;
                        if (language.equalsIgnoreCase("En")) {
                            dialogView = inflater.inflate(R.layout.store_select_dialog_delivery, null);
                        } else {
                            dialogView = inflater.inflate(R.layout.store_select_dialog_delivery_ar, null);
                        }
                        dialogBuilder.setView(dialogView);

                        ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                        ImageView pickup = (ImageView) dialogView.findViewById(R.id.select_pickup);
                        ImageView dineIn = (ImageView) dialogView.findViewById(R.id.select_dinein);
//                    ImageView pickupBig = (ImageView) dialogView.findViewById(R.id.select_pickup_big);
                        RelativeLayout pickupLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_layout);
                        RelativeLayout dineinLayout = (RelativeLayout) dialogView.findViewById(R.id.dinein_layout);
//                    RelativeLayout pickupBigLayout = (RelativeLayout) dialogView.findViewById(R.id.pickup_big_layout);
                        RelativeLayout deliveryLayout = (RelativeLayout) dialogView.findViewById(R.id.delivery_layout);
                        TextView deliveryTv = (TextView) dialogView.findViewById(R.id.delivery_txt);
                        TextView storeMsg = (TextView) dialogView.findViewById(R.id.store_msg);
                        TextView pickupTv = (TextView) dialogView.findViewById(R.id.pickup_txt);
                        TextView dineInTv = (TextView) dialogView.findViewById(R.id.dinein_txt);
//                    TextView pickupBigTv = (TextView) dialogView.findViewById(R.id.pickup_big_txt);
                        TextView heading = (TextView) dialogView.findViewById(R.id.order_type_heading);
//                    pickupBigLayout.setVisibility(View.GONE);
                        if (language.equalsIgnoreCase("En")) {
                            pickupTv.setText("Pick Up");
                            dineInTv.setText("Dine In");
//                        pickupBigTv.setText("Pick Up");
                            heading.setText("Order Type");
                            deliveryTv.setText(si.getDeliveryMessageEn());
                            if (si.getMessage().equalsIgnoreCase("")) {
                                storeMsg.setVisibility(View.GONE);
                            } else {
                                storeMsg.setVisibility(View.VISIBLE);
                                storeMsg.setText(si.getMessage());
                            }
                        } else if (language.equalsIgnoreCase("Ar")) {
                            pickupTv.setText("الاستمتاع خارج الفرع");
                            dineInTv.setText("الاستمتاع في الفرع");
//                        pickupBigTv.setText("الاستمتاع خارج الفرع");
                            heading.setText("نوع الطلب");
                            deliveryTv.setText(si.getDeliveryMessageAr());
                            if (si.getMessage_ar().equalsIgnoreCase("")) {
                                storeMsg.setVisibility(View.GONE);
                            } else {
                                storeMsg.setVisibility(View.VISIBLE);
                                storeMsg.setText(si.getMessage_ar());
                            }
                        }

//                        dineinLayout.setVisibility(View.GONE);
//                    pickupLayout.setVisibility(View.VISIBLE);
//                    pickupBigLayout.setVisibility(View.VISIBLE);

                        if (si.isDineIn()) {
                            dineinLayout.setVisibility(View.VISIBLE);
                        } else {
                            dineinLayout.setVisibility(View.GONE);
                        }
                        if (si.isPickUp()) {
                            pickupLayout.setVisibility(View.VISIBLE);
                        } else {
                            pickupLayout.setVisibility(View.GONE);
                        }
                        if (si.isDeliverable()) {
                            deliveryLayout.setVisibility(View.VISIBLE);
                        } else {
                            deliveryLayout.setVisibility(View.GONE);
                        }
//                        if (si.isDeliverable()) {
//                            deliveryLayout.setVisibility(View.VISIBLE);
//                        } else {
//                            deliveryLayout.setVisibility(View.GONE);
//                        }

                        DrConstants.storeName = si.getStoreName();
                        DrConstants.storeAddress = si.getStoreAddress();
                        DrConstants.storeId = si.getStoreId();
                        DrConstants.latitude = si.getLatitude();
                        DrConstants.longitude = si.getLongitude();
                        DrConstants.fullHours = si.is24x7();
                        DrConstants.startTime = si.getStartTime();
                        DrConstants.endTime = si.getEndTime();
                        DrConstants.CashPayment = si.isCashPayment();
                        DrConstants.OnlinePayment = si.isOnlinePayment();

                        Gson gson = new Gson();
                        String jsonText = gson.toJson(si);

                        storePrefsEditor.putString("store", jsonText);
                        storePrefsEditor.putInt("position", i);
                        storePrefsEditor.apply();


                        pickup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                DrConstants.orderNow = true;
                                DrConstants.orderType = "Pick Up";
                                storePrefsEditor.putString("orderType", DrConstants.orderType);
                                storePrefsEditor.apply();

                                alertDialog.dismiss();
                                DrConstants.menuFlag = true;

                                Fragment newFragment = new MenuFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                // Replace whatever is in the fragment_container view with this fragment,
                                // and add the transaction to the back stack
                                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                transaction.add(R.id.realtabcontent, newFragment, "menu");
                                transaction.hide(SelectStoreFragment.this);
                                transaction.addToBackStack(null);

                                // Commit the transaction
                                transaction.commit();
                            }
                        });

                        deliveryLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                DrConstants.orderNow = true;
                                DrConstants.orderType = "Delivery";
                                DrConstants.MinOrderAmount = si.getMinOrderAmount();
                                DrConstants.DeliveryCharge = si.getDeliveryCharge();
                                DrConstants.FreeDeliveryAbove = si.getFreeDeliveryAbove();
                                storePrefsEditor.putFloat("MinOrderAmount", DrConstants.MinOrderAmount);
                                storePrefsEditor.putFloat("DeliveryCharge", DrConstants.DeliveryCharge);
                                storePrefsEditor.putFloat("FreeDeliveryAbove", DrConstants.FreeDeliveryAbove);
                                storePrefsEditor.putString("orderType", DrConstants.orderType);
                                storePrefsEditor.commit();

                                alertDialog.dismiss();
                                DrConstants.menuFlag = true;

                                Bundle args = new Bundle();
                                args.putBoolean("store_details", false);
                                args.putDouble("lat", si.getLatitude());
                                args.putDouble("lng", si.getLongitude());
                                args.putFloat("delDistance", si.getDistanceForDelivery());
                                Fragment newFragment = new MyAddressActivity();
                                newFragment.setArguments(args);
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                // Replace whatever is in the fragment_container view with this fragment,
                                // and add the transaction to the back stack
                                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                transaction.add(R.id.realtabcontent, newFragment, "address");
                                transaction.hide(SelectStoreFragment.this);
                                transaction.addToBackStack(null);

                                // Commit the transaction
                                transaction.commit();
                            }
                        });

                        dineIn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                                DrConstants.orderNow = true;
                                DrConstants.orderType = "Dine In";

                                storePrefsEditor.putString("orderType", DrConstants.orderType);
                                storePrefsEditor.apply();

                                DrConstants.menuFlag = true;

                                Fragment newFragment = new MenuFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                                // Replace whatever is in the fragment_container view with this fragment,
                                // and add the transaction to the back stack
                                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                transaction.add(R.id.realtabcontent, newFragment, "menu");
                                transaction.hide(SelectStoreFragment.this);
                                transaction.addToBackStack(null);

                                // Commit the transaction
                                transaction.commit();
                            }
                        });

                        cancelBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });

                        alertDialog = dialogBuilder.create();
                        alertDialog.show();

                        //Grab the window of the dialog, and change the width
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = alertDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                }
                else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            getActivity());

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Right Now, This store can't take any orders.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("هذا الفرع لايستقبل الطلبات في الوقت الحالي.")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flipCard();
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                flipCard();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        filterData(response12);
                    }
                }, 500);
//                new FilterStoreInfo().execute();
            }
        });

        timer.schedule(new MyTimerTask(), 60000, 60000);

        return rootView;
    }


    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if(getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        Fragment currentFragment = getChildFragmentManager().findFragmentById(R.id.fragment_container);

                        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                        if (currentapiVersion >= Build.VERSION_CODES.M) {
                            if (!canAccessLocation()) {
                                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                            } else {
                                getGPSCoordinates();
                            }
                        }else {
                            getGPSCoordinates();
                        }
                    }
                });
            }
        }
    }

    public void getGPSCoordinates(){
        if(getActivity()!=null) {
            gps = new GPSTracker(getActivity());
        }
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);

                new GetCurrentTime().execute();

//                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }


    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_hotspot:
                if (isWifi) {
                    isWifi = false;
                    wifiText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isWifi = true;
                    wifiText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_drive:
                if (isDrive) {
                    isDrive = false;
                    driveText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isDrive = true;
                    driveText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_family:
                if (isFamily) {
                    isFamily = false;
                    familyText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isFamily = true;
                    familyText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_meeting:
                if (isMeeting) {
                    isMeeting = false;
                    meetingText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isMeeting = true;
                    meetingText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_patio:
                if (isPatio) {
                    isPatio = false;
                    patioText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isPatio = true;
                    patioText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_v12:
                if(isV12){
                    isV12 = false;
                    v12Text.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                }else{
                    isV12 = true;
                    v12Text.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check_mark,0);
                }
                break;
            case R.id.filter_university:
                if (isUniversity) {
                    isUniversity = false;
                    universityText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isUniversity = true;
                    universityText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_hospital:
                if (isHospital) {
                    isHospital = false;
                    hospitalText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isHospital = true;
                    hospitalText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;

            case R.id.filter_office:
                if (isOffice) {
                    isOffice = false;
                    officeText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isOffice = true;
                    officeText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_shopping:
                if (isShopping) {
                    isShopping = false;
                    shoppingText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isShopping = true;
                    shoppingText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_airport:
                if (isAirport) {
                    isAirport = false;
                    airportText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isAirport = true;
                    airportText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_dinein:
                if (isDineIn) {
                    isDineIn = false;
                    dineinText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isDineIn = true;
                    dineinText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_ladies:
                if (isLadies) {
                    isLadies = false;
                    ladiesText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isLadies = true;
                    ladiesText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
            case R.id.filter_neighbohood:
                if (isNeighborhood) {
                    isNeighborhood = false;
                    neighborText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isNeighborhood = true;
                    neighborText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check_mark, 0);
                }
                break;
        }
    }

    private void flipCard()
    {


        FlipAnimation flipAnimation = new FlipAnimation(listLayout, filterLayout);

        if (listLayout.getVisibility() == View.GONE)
        {
            flipAnimation.reverse();
        }
        storeLayout.startAnimation(flipAnimation);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String text = searchBar.getText().toString().toLowerCase();
        mStoresListView.invalidate();
        totalStoresList.clear();
        if (text.length() == 0)
        {
            searchBar.setGravity(Gravity.CENTER);
            if(isFilterOn){
                if(favouriteStoresList.size()==0){
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add((new SectionItem("Nearby Stores")));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add((new SectionItem(" الفروع القريبة")));
                    }
                    totalStoresList.addAll(filterStoresList);
//            mFavouriteLayout.setVisibility(View.VISIBLE);
                }else{
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add(new SectionItem("Favorite Stores"));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add(new SectionItem("  الفروع المفضلة"));
                    }
                    totalStoresList.addAll(favouriteStoresList);
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add((new SectionItem("Nearby Stores")));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add((new SectionItem(" الفروع القريبة")));
                    }
                    totalStoresList.addAll(filterStoresList);
                }
            }else {
                System.out.println("if statement");
                if(favouriteStoresList.size()==0){
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add((new SectionItem("Nearby Stores")));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add((new SectionItem(" الفروع القريبة")));
                    }
                    totalStoresList.addAll(tempStoresList);
//            mFavouriteLayout.setVisibility(View.VISIBLE);
                }else{
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add(new SectionItem("Favorite Stores"));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add(new SectionItem("  الفروع المفضلة"));
                    }
                    totalStoresList.addAll(favouriteStoresList);
                    if(language.equalsIgnoreCase("En")) {
                        totalStoresList.add((new SectionItem("Nearby Stores")));
                    }else if(language.equalsIgnoreCase("Ar")){
                        totalStoresList.add((new SectionItem(" الفروع القريبة")));
                    }
                    totalStoresList.addAll(tempStoresList);
                }
            }
            // System.out.println("if stmt"+theatersList.size());
        }else{
            searchBar.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            if(isFilterOn){
                for(StoreInfo si: filterStoresList){
                    if (si.getStoreAddress().toLowerCase().contains(text) || si.getStoreName().toLowerCase().contains(text) || si.getCityName().toLowerCase().contains(text) || si.getStoreAddress_ar().contains(text) || si.getStoreName_ar().contains(text)) {
                        totalStoresList.add(si);
                    }
                }
            }
            else {
                System.out.println("else statement");
                for (StoreInfo si: tempStoresList) {
                    System.out.println("else for loop");
                    if (si.getStoreAddress().toLowerCase().contains(text) || si.getStoreName().toLowerCase().contains(text) || si.getCityName().toLowerCase().contains(text) || si.getStoreAddress_ar().contains(text) || si.getStoreName_ar().contains(text)) {
                        totalStoresList.add(si);
                    }
                }
            }
        }
        Collections.sort(storesList, StoreInfo.storeDistance);
        if(language.equalsIgnoreCase("En")) {
            if(mStoreListAdapter != null) {
                mStoreListAdapter.notifyDataSetChanged();
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if(mStoreListAdapterArabic != null) {
                mStoreListAdapterArabic.notifyDataSetChanged();
            }
        }

    }



    public class GetStoreInfo extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus = "";
        String dayOfWeek;

        @Override
        protected void onPreExecute() {
//            storesList.clear();
//            totalStoresList.clear();
//            tempStoresList.clear();
//            favouriteStoresList.clear();
            if(getActivity() != null) {
                networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            }
            if(isFirstTime) {
                try {
                    dialog = ProgressDialog.show(getActivity(), "",
                            "Fetching stores...");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                isFirstTime = false;
            }
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("day", dayOfWeek.toLowerCase());

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(STORE_URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else{
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null ){
                Log.d("TAG", "onPostExecute: "+response12);
                if(result1.equalsIgnoreCase("no internet")){
                    Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                }else{
                    storesList.clear();
                    filterStoresList.clear();
                    totalStoresList.clear();
                    favouriteStoresList.clear();
                    if(response12 == null){

                    }else if (response12.equals("anyType{}")) {
                        Toast.makeText(getActivity(), "Failed to get stores info", Toast.LENGTH_LONG).show();
                    } else {

                        try {
//                    Log.d("Responce", "" + result);
//                    for (int i = 0; i < result.getPropertyCount(); i++) {
                            Object property = result.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject category_list = (SoapObject) property;
                                    SoapObject storeObject = (SoapObject) category_list.getProperty(0);
//                                    lat = 24.70321657;
//                                    longi = 46.68097073;
                                    for (int i = 0; i < storeObject.getPropertyCount(); i++) {
                                        isAddedtoList = false;
                                        SoapObject sObj = (SoapObject) storeObject.getProperty(i);
                                        StoreInfo si = new StoreInfo();

//                                    for (int j = 0; j < sObj.getPropertyCount(); j++) {
                                        try {
                                            si.setStoreId(sObj.getPropertyAsString(0));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        si.setStartTime(sObj.getPropertyAsString(1));
                                        si.setEndTime(sObj.getPropertyAsString(2));
                                        si.setStoreName(sObj.getPropertyAsString("StoreName"));
                                        si.setStoreAddress(sObj.getPropertyAsString("StoreAddress"));
                                        si.setLatitude(Double.valueOf(sObj.getPropertyAsString("Latitude")));
                                        si.setLongitude(Double.valueOf(sObj.getPropertyAsString("Longitude")));
                                        si.setCountryName(sObj.getPropertyAsString("CountryName"));
                                        si.setCityName(sObj.getPropertyAsString("CityName"));
                                        try {
                                            si.setImageURL(sObj.getPropertyAsString("imageURL"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        si.setFamilySection(Boolean.valueOf(sObj.getPropertyAsString("FamilySection")));
                                        si.setWifi(Boolean.valueOf(sObj.getPropertyAsString("Wifi")));
                                        si.setPatioSitting(Boolean.valueOf(sObj.getPropertyAsString("PatioSitting")));
                                        si.setV12(Boolean.valueOf(sObj.getPropertyAsString("V12")));
                                        si.setDriveThru(Boolean.valueOf(sObj.getPropertyAsString("DriveThru")));
                                        si.setMeetingSpace(Boolean.valueOf(sObj.getPropertyAsString("MeetingSpace")));
                                        si.setHospital(Boolean.valueOf(sObj.getPropertyAsString("Hospital")));
                                        si.setUniversity(Boolean.valueOf(sObj.getPropertyAsString("University")));
                                        si.setOffice(Boolean.valueOf(sObj.getPropertyAsString("Office")));
                                        si.setShoppingMall(Boolean.valueOf(sObj.getPropertyAsString("ShoppingMall")));
                                        try{
                                            si.setAirPort(Boolean.valueOf(sObj.getPropertyAsString("Airport")));
                                        }catch (Exception e){
                                            si.setAirPort(false);
                                        }
                                        try{
                                            si.setDineIn(Boolean.valueOf(sObj.getPropertyAsString("DineIn")));
                                        }catch (Exception e){
                                            si.setDineIn(false);
                                        }
                                        try{
                                            si.setPickUp(Boolean.valueOf(sObj.getPropertyAsString("Pickup")));
                                        }catch (Exception e){
                                            si.setPickUp(false);
                                        }
                                        try{
                                            si.setLadies(Boolean.valueOf(sObj.getPropertyAsString("Ladies")));
                                        }catch (Exception e){
                                            si.setLadies(false);
                                        }

                                        si.setNeighborhood(Boolean.valueOf(sObj.getPropertyAsString("Neighborhood")));
                                        si.setIs24x7(Boolean.valueOf(sObj.getPropertyAsString("is24x7")));
                                        si.setStatus(Boolean.valueOf(sObj.getPropertyAsString("status")));
                                        si.setDcCountry(sObj.getPropertyAsString("DCCountry"));
                                        si.setDcCity(sObj.getPropertyAsString("DCCity"));
                                        si.setStoreName_ar(sObj.getPropertyAsString("StoreName_ar"));
                                        si.setStoreAddress_ar(sObj.getPropertyAsString("StoreAddress_ar"));
                                        si.setCashPayment(Boolean.valueOf(sObj.getPropertyAsString("CashPayment")));
                                        si.setOnlinePayment(Boolean.valueOf(sObj.getPropertyAsString("OnlinePayment")));
                                        si.setDeliverable(Boolean.valueOf(sObj.getPropertyAsString("Deliverable")));
                                        si.setDistanceForDelivery(Float.valueOf(sObj.getPropertyAsString("Distance")));
                                        si.setMinOrderAmount(Float.valueOf(sObj.getPropertyAsString("MinOrderAmount")));
                                        si.setDeliveryCharge(Float.valueOf(sObj.getPropertyAsString("DeliveryCharge")));
                                        si.setFreeDeliveryAbove(Float.valueOf(sObj.getPropertyAsString("FreeDeliveryAbove")));
                                        si.setDeliveryMessageEn(sObj.getPropertyAsString("DeliveryMessageEn"));
                                        si.setDeliveryMessageAr(sObj.getPropertyAsString("DeliveryMessageAr"));
                                        si.setIemsJson(sObj.getPropertyAsString("ItemsJson"));

                                        try {
                                            si.setDeliveryItems(sObj.getPropertyAsString("DeliveryItems"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        try{
                                            si.setMessage(sObj.getPropertyAsString("Message"));
                                        }catch (Exception e){
                                            si.setMessage("");
                                        }

                                        try{
                                            si.setMessage_ar(sObj.getPropertyAsString("Message_ar"));
                                        }catch (Exception e){
                                            si.setMessage_ar("");
                                        }

                                        Location me   = new Location("");
                                        Location dest = new Location("");

                                        me.setLatitude(lat);
                                        me.setLongitude(longi);

                                        dest.setLatitude(Double.valueOf(sObj.getPropertyAsString("Latitude")));
                                        dest.setLongitude(Double.valueOf(sObj.getPropertyAsString("Longitude")));

                                        float dist = (me.distanceTo(dest))/1000;
                                        float mile = (float) (dist * 0.621);
//                                        Log.i("storename", "" + sObj.getPropertyAsString(1) + "   " + sObj.getPropertyAsString(2));
                                        if(mDistancePrefValue.equalsIgnoreCase("kilometer")){
                                            si.setDistance(dist);
                                        }else if(mDistancePrefValue.equalsIgnoreCase("mile")){
                                            si.setDistance(mile);
                                        }
                                        if(dist<Integer.parseInt(mRadiusPrefValue)) {
//                                            storesList.add(si);
                                            if(favoriteStores.size()>0){
                                                if(favoriteStores.contains(sObj.getPropertyAsString(0))){
                                                    favouriteStoresList.add(si);
                                                }
                                            }
                                            else {
                                            }
                                            String startTime = sObj.getPropertyAsString(1);
                                            String endTime = sObj.getPropertyAsString(2);
                                            if(Boolean.valueOf(sObj.getPropertyAsString("is24x7"))){
                                                si.setOpenFlag(1);
                                                storesList.add(si);
                                            }else if(sObj.getPropertyAsString(1).equals("anyType{}") && sObj.getPropertyAsString(2).equals("anyType{}")){
                                                si.setOpenFlag(-1);
                                                storesList.add(si);
                                            }else{
                                                if(endTime.equals("00:00AM")){
                                                    si.setOpenFlag(1);
                                                    storesList.add(si);

                                                    continue;
                                                }else if(endTime.equals("12:00AM")){
                                                    endTime = "11:59PM";
                                                }

                                                Calendar now = Calendar.getInstance();

                                                int hour = now.get(Calendar.HOUR_OF_DAY);
                                                int minute = now.get(Calendar.MINUTE);

                                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
                                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                                Date serverDate = null;
                                                Date end24Date = null;
                                                Date start24Date = null;
                                                Date current24Date = null;
                                                Date dateToday = null;
                                                Calendar dateStoreClose = Calendar.getInstance();

                                                try {
                                                    end24Date = timeFormat.parse(endTime);
                                                    start24Date = timeFormat.parse(startTime);
                                                    serverDate = dateFormat.parse(timeResponse);
                                                    dateToday = dateFormat1.parse(timeResponse);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                dateStoreClose.setTime(dateToday);
                                                dateStoreClose.add(Calendar.DATE, 1);
                                                String current24 = timeFormat1.format(serverDate);
                                                String end24 =timeFormat1.format(end24Date);
                                                String start24 = timeFormat1.format(start24Date);
                                                String startDateString = dateFormat1.format(dateToday);
                                                String endDateString = dateFormat1.format(dateToday);
                                                String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                                                dateStoreClose.add(Calendar.DATE, -2);
                                                String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

                                                Date startDate = null;
                                                Date endDate = null;

                                                try {
                                                    end24Date = timeFormat1.parse(end24);
                                                    start24Date = timeFormat1.parse(start24);
                                                    current24Date = timeFormat1.parse(current24);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                                String[] parts2 = start24.split(":");
                                                int startHour = Integer.parseInt(parts2[0]);
                                                int startMinute = Integer.parseInt(parts2[1]);

                                                String[] parts = end24.split(":");
                                                int endHour = Integer.parseInt(parts[0]);
                                                int endMinute = Integer.parseInt(parts[1]);

                                                String[] parts1 = current24.split(":");
                                                int currentHour = Integer.parseInt(parts1[0]);
                                                int currentMinute = Integer.parseInt(parts1[1]);


                                                if(startTime.contains("AM") && endTime.contains("AM")){
                                                    if(startHour < endHour){
                                                        startDateString = startDateString+ " "+ startTime;
                                                        endDateString = endDateString+"  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }else if(startHour > endHour){
                                                        if(timeResponse.contains("AM")){
                                                            if(currentHour > endHour){
                                                                startDateString = startDateString + " " + startTime;
                                                                endDateString = endDateTomorrow + "  " + endTime;
                                                                try {
                                                                    startDate = dateFormat2.parse(startDateString);
                                                                    endDate = dateFormat2.parse(endDateString);
                                                                } catch (ParseException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            } else {
                                                                startDateString = endDateYesterday + " " + startTime;
                                                                endDateString = endDateString + "  " + endTime;
                                                                try {
                                                                    startDate = dateFormat2.parse(startDateString);
                                                                    endDate = dateFormat2.parse(endDateString);
                                                                } catch (ParseException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }else {
                                                            startDateString = startDateString + " " + startTime;
                                                            endDateString = endDateTomorrow + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                }else if(startTime.contains("AM") && endTime.contains("PM")){
                                                    startDateString = startDateString+ " "+ startTime;
                                                    endDateString = endDateString+"  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }else if(startTime.contains("PM") && endTime.contains("AM")){
                                                    if(timeResponse.contains("AM")){
                                                        if(currentHour <= endHour){
                                                            startDateString = endDateYesterday+ " "+ startTime;
                                                            endDateString = endDateString+"  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }else{
                                                            startDateString = startDateString+ " "+ startTime;
                                                            endDateString = endDateTomorrow+"  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }else{
                                                        startDateString = startDateString+ " "+ startTime;
                                                        endDateString = endDateTomorrow+"  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                }else if(startTime.contains("PM") && endTime.contains("PM")){
                                                    startDateString = startDateString+ " "+ startTime;
                                                    endDateString = endDateString+"  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }


                                                String serverDateString = dateFormat2.format(serverDate);

                                                try {
                                                    serverDate = dateFormat2.parse(serverDateString);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                                if(serverDate.after(startDate) && serverDate.before(endDate)){
//                                                Log.i("TAG Visible" , "true");
                                                    si.setOpenFlag(1);
                                                    storesList.add(si);
                                                    continue;
                                                }else{
                                                    si.setOpenFlag(0);
                                                    storesList.add(si);
                                                    continue;
                                                }



//                                            if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
//                                                if(current24Date.after(start24Date) && current24Date.before(end24Date) ){
//                                                    si.setOpenFlag(1);
//                                                    storesList.add(si);
//                                                    Log.i("Store Name", "1  "+sObj.getPropertyAsString("StoreName"));
//                                                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//                                                    Log.i("END TAG", ""+ end24Date.toString());
//                                                    continue;
//                                                }else{
//                                                    si.setOpenFlag(0);
//                                                    storesList.add(si);
//                                                    Log.i("Store Name", "0  "+sObj.getPropertyAsString("StoreName"));
//                                                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//                                                    Log.i("END TAG", "" + end24Date.toString());
//                                                    continue;
//                                                }
//
//                                            }else{
//                                                if (currentHour < endHour || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
//                                                    si.setOpenFlag(1);
//                                                    storesList.add(si);
//                                                    Log.i("Store Name", "1  "+sObj.getPropertyAsString("StoreName"));
//                                                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//                                                    Log.i("END TAG", "" + end24Date.toString());
//                                                    continue;
//                                                }else{
//                                                    si.setOpenFlag(0);
//                                                    storesList.add(si);
//                                                    Log.i("Store Name", "0   "+sObj.getPropertyAsString("StoreName"));
//                                                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//                                                    Log.i("END TAG", "" + end24Date.toString());
//                                                    continue;
//                                                }
//                                            }
                                            }



                                        }
//                                    }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
//                    }
//                            map.animateCamera(CameraUpdateFactory.newLatLngZoom
//                                    (new LatLng(lat, longi), 11.0f));
                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }



                        filterStoresList.addAll(storesList);
                        Log.d("TAG", "storesList: "+storesList.size());

                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(favouriteStoresList, StoreInfo.storeDistance);
                        Collections.sort(storesList, StoreInfo.storeOpenSort);
                        Collections.sort(favouriteStoresList, StoreInfo.storeOpenSort);
                        if(favouriteStoresList.size() >0){
                            if(language.equalsIgnoreCase("En")) {
                                totalStoresList.add(new SectionItem("Favorite Stores"));
                            }else if(language.equalsIgnoreCase("Ar")){
                                totalStoresList.add(new SectionItem("  الفروع المفضلة"));
                            }
                            totalStoresList.addAll(favouriteStoresList);
                        }

                        if(language.equalsIgnoreCase("En")) {
                            totalStoresList.add((new SectionItem("Nearby Stores")));
                        }else if(language.equalsIgnoreCase("Ar")){
                            totalStoresList.add((new SectionItem(" الفروع القريبة")));
                        }
                        totalStoresList.addAll(storesList);

                        try {
                            if (totalStoresList!= null && totalStoresList.size() > 0) {
                                if (language.equalsIgnoreCase("En")) {
                                    mStoreListAdapter = new SelectStoreAdapter(getActivity(), totalStoresList, timeResponse);
                                    mStoresListView.setAdapter(mStoreListAdapter);
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    mStoreListAdapterArabic = new SelectStoreAdapterArabic(getActivity(), totalStoresList, timeResponse);
                                    mStoresListView.setAdapter(mStoreListAdapterArabic);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if(tempStoresList.size()==0){
                            tempStoresList.addAll(storesList);
                        }
                        if(language.equalsIgnoreCase("En")) {
                            if(mStoreListAdapter != null) {
                                mStoreListAdapter.notifyDataSetChanged();
                            }
                        }else if(language.equalsIgnoreCase("Ar")){
                            if(mStoreListAdapterArabic != null) {
                                mStoreListAdapterArabic.notifyDataSetChanged();
                            }
                        }
                        mStoresListView.smoothScrollToPosition(10);
                        mStoresListView.smoothScrollToPosition(0);

//                        mFavouriteAdapter.notifyDataSetChanged();

                        //detailsAdapter.notifyDataSetChanged();
                    }
                    if(dialog!=null) {
                        dialog.dismiss();
                    }
                    super.onPostExecute(result1);
                }
            }
        }
    }




    public void filterData(String response12){
        storesList.clear();
        filterStoresList.clear();

        for(StoreInfo si: tempStoresList) {
            isAddedtoList = false;
            if (isWifi || isDrive || isFamily || isMeeting || isPatio || isV12 || isUniversity || isHospital || isOffice || isShopping || isAirport || isDineIn || isLadies || isNeighborhood) {
                isFilterOn = true;
                if (isWifi) {
                    if (si.isWifi()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }
                if (isDrive) {
                    if (si.isDriveThru()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }
                if (isFamily) {
                    if (si.isFamilySection()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }
                if (isMeeting) {
                    if (si.isMeetingSpace()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }
                if (isPatio) {
                    if (si.isPatioSitting()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isV12) {
                    if (si.isV12()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isUniversity) {
                    if (si.isUniversity()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isHospital) {
                    if (si.isHospital()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isOffice) {
                    if (si.isOffice()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isShopping) {
                    if (si.isShoppingMall()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isAirport) {
                    if (si.isAirPort()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isDineIn) {
                    if (si.isDineIn()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isLadies) {
                    if (si.isLadies()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

                if (isNeighborhood) {
                    if (si.isNeighborhood()) {
                        if (!isAddedtoList) {
                            isAddedtoList = true;
                            storesList.add(si);
                        }
                    }
                }

            }else{
                storesList.add(si);
            }
        }


        if(favouriteStoresList.size()==0){
            totalStoresList.clear();
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add((new SectionItem("Nearby Stores")));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add((new SectionItem(" الفروع القريبة")));
            }
            totalStoresList.addAll(storesList);
//            mFavouriteLayout.setVisibility(View.VISIBLE);
        }else{
            totalStoresList.clear();
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add(new SectionItem("Favorite Stores"));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add(new SectionItem("  الفروع المفضلة"));
            }
            totalStoresList.addAll(favouriteStoresList);
            if(language.equalsIgnoreCase("En")) {
                totalStoresList.add((new SectionItem("Nearby Stores")));
            }else if(language.equalsIgnoreCase("Ar")){
                totalStoresList.add((new SectionItem(" الفروع القريبة")));
            }
            totalStoresList.addAll(storesList);
        }
//        Collections.sort(storesList, StoreInfo.storeDistance);
        filterStoresList.addAll(storesList);
        if(language.equalsIgnoreCase("En")) {
            if(mStoreListAdapter != null) {
                mStoreListAdapter.notifyDataSetChanged();
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if(mStoreListAdapterArabic != null) {
                mStoreListAdapterArabic.notifyDataSetChanged();
            }
        }
    }



    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy");

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if(isFirstTime) {
                if (dialog == null)
                dialog = ProgressDialog.show(getActivity(), "",
                        "Please Wait....");
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_TIME);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_TIME, envelope);
                    timeResult = (SoapPrimitive) envelope.getResponse();
                    timeResponse = timeResult.toString();
                    Log.v("TAG", timeResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + timeResponse);
            }else{
                timeResponse = "no internet";
            }
            return timeResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(timeResponse == null){
                if(dialog!=null) {
                    dialog.dismiss();
                }
            } else if(timeResponse.equals("no internet")){
                if(dialog!=null) {
                    dialog.dismiss();
                }
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                if(dialog!=null) {
                    dialog.dismiss();
                }
                new GetStoreInfo().execute();
                if (totalStoresList.size() > 0) {
                    if (language.equalsIgnoreCase("En")) {
                        mStoreListAdapter = new SelectStoreAdapter(getActivity(), totalStoresList, timeResponse);
                        mStoresListView.setAdapter(mStoreListAdapter);
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mStoreListAdapterArabic = new SelectStoreAdapterArabic(getActivity(), totalStoresList, timeResponse);
                        mStoresListView.setAdapter(mStoreListAdapterArabic);
                    }
                }
            }
            super.onPostExecute(result1);
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if(!isGPSRefreshed) {
                Bundle b = intent.getBundleExtra("Location");
                Location location = (Location) b.getParcelable("Location");
                if (location != null) {
                    lat = location.getLatitude();
                    longi = location.getLongitude();

                    if(!isFirstTime) {
                        new GetStoreInfo().execute();
                    }
                    gps.stopUsingGPS();

                    isGPSRefreshed = true;
                }
            }
        }
    };
}
