package com.cs.drcafe.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;

import com.cs.drcafe.R;
import com.cs.drcafe.model.FragmentUtils;


/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class OrderFragment  extends Fragment {

    Button mNewOrder, mFavouriteOrder, mOrderHistory;
    View rootView;
    String language;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_layout, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.order_layout_arabic, container,
                    false);
        }

        mNewOrder = (Button) rootView.findViewById(R.id.order_new);
        mFavouriteOrder = (Button) rootView.findViewById(R.id.order_favourite);
        mOrderHistory = (Button) rootView.findViewById(R.id.order_history);

        mNewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment menuFragment = new SelectStoreFragment();
//                Fragment menuFragment = new MenuFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction menuTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                menuTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                menuTransaction.add(R.id.realtabcontent, menuFragment, "selectstore");
                menuTransaction.hide(OrderFragment.this);
                menuTransaction.addToBackStack(null);

                // Commit the transaction
                menuTransaction.commit();
            }
        });

        mFavouriteOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment menuFragment = new FavouriteOrderFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction menuTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                menuTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                menuTransaction.add(R.id.realtabcontent, menuFragment);
                menuTransaction.hide(OrderFragment.this);
                menuTransaction.addToBackStack(null);

                // Commit the transaction
                menuTransaction.commit();
            }
        });

        mOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment menuFragment = new OrderHistoryFragment();
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction menuTransaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack

                menuTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                menuTransaction.add(R.id.realtabcontent, menuFragment);
                menuTransaction.hide(OrderFragment.this);
                menuTransaction.addToBackStack(null);

                // Commit the transaction
                menuTransaction.commit();
            }
        });

        return rootView;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {};
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}