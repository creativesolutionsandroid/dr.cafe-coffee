package com.cs.drcafe;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.cs.drcafe.fragments.CategoriesListFragment;
import com.cs.drcafe.fragments.FavouriteOrderFragment;
import com.cs.drcafe.fragments.MainFragment;
import com.cs.drcafe.fragments.ManageCardsFragment;
import com.cs.drcafe.fragments.MenuFragment;
import com.cs.drcafe.fragments.MoreFragment;
import com.cs.drcafe.fragments.MyGiftsFragment;
import com.cs.drcafe.fragments.OrderCheckoutFragment;
import com.cs.drcafe.fragments.OrderDetails;
import com.cs.drcafe.fragments.OrderFragment;
import com.cs.drcafe.fragments.SelectStoreFragment;
import com.cs.drcafe.fragments.StoreFragment;
import com.cs.drcafe.model.FragmentUtils;
import com.cs.drcafe.model.StoreInfo;
import com.daasuu.bl.BubbleLayout;
import com.google.gson.Gson;
import com.readystatesoftware.viewbadger.BadgeView;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MainActivity extends FragmentActivity {

	TabHost tHost;
    BadgeView badge, badge1;
    public static ImageView iv;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languageEditor;
    String language = "En";
    String qty = "";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    public static BubbleLayout bubbleLayout;
    TextView bubbleTextView;
    Boolean isCardsChecked = false;
    SharedPreferences storePrefs;
    SharedPreferences.Editor storePrefsEditor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        final String language = languagePrefs.getString("language", "En");
        setContentView(R.layout.activity_main);

        storePrefs = getSharedPreferences("STORE_PREFS", Context.MODE_PRIVATE);
        storePrefsEditor = storePrefs.edit();

        printHashKey(this);

        myDbHelper = new DataBaseHelper(MainActivity.this);
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {

        }

        try {
            myDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }


//        float f = 13.43f;
//        float netAmountRounded = (float) Math.round(f * 10)/10;
//        Log.d("round", ""+netAmountRounded);
//        Log.d("round", ""+(netAmountRounded - f));

//        ArrayList<SubCategories> localArray = myDbHelper.getItemDetails("455");
//        Log.d("TAG", "onCreate: "+localArray.get(0).getItemName());

//        Freshchat.getInstance(MainActivity.this).setPushRegistrationToken(SplashScreen.regid);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        Log.d("TAG", "onCreate: "+SplashScreen.regid);

        bubbleTextView = (TextView) findViewById(R.id.bubble_text);
        bubbleLayout = (BubbleLayout) findViewById(R.id.bubble_layout);
        tHost = (TabHost) findViewById(android.R.id.tabhost);
        tHost.setup();

        isCardsChecked = userPrefs.getBoolean("card", false);
        if(!isCardsChecked){
            bubbleLayout.setVisibility(View.VISIBLE);
        }

        /** Defining Tab Change Listener event. This is invoked when tab is changed */
        TabHost.OnTabChangeListener tabChangeListener = new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                try {
                    if (myDbHelper.getTotalOrderQty() > 0) {
                        DrConstants.menuFlag = true;
                    }
                } catch (SQLiteException sqle) {
                    sqle.printStackTrace();
                }
//                tHost.getCurrentTabView().setBackgroundColor(Color.parseColor("#00FFFFFF"));
                FragmentManager fm = getSupportFragmentManager();

                MainFragment mainFragment = (MainFragment) fm.findFragmentByTag("main");
                StoreFragment storeFragment = (StoreFragment) fm.findFragmentByTag("store");
                SelectStoreFragment selectStoreFragment = (SelectStoreFragment) fm.findFragmentByTag("select_store");
                OrderFragment orderFragment = (OrderFragment) fm.findFragmentByTag("order");
                ManageCardsFragment cardFragment = (ManageCardsFragment) fm.findFragmentByTag("card");
                MoreFragment moreFragment = (MoreFragment) fm.findFragmentByTag("more");
                MenuFragment menuFragment = (MenuFragment) fm.findFragmentByTag("menu");
                FavouriteOrderFragment favFragment = (FavouriteOrderFragment) fm.findFragmentByTag("fav_order");
                MyGiftsFragment myGiftsFragment = (MyGiftsFragment) fm.findFragmentByTag("gifts");
                CategoriesListFragment catFragment = (CategoriesListFragment) fm.findFragmentByTag("category_list");
                FragmentTransaction ft = fm.beginTransaction();

                clearBackStack();

//                Fragment fragment= null;
//                try {
//                    FragmentManager.BackStackEntry backEntry= fm.getBackStackEntryAt(fm.getBackStackEntryCount()-1);
//                    String str=backEntry.getName();
//                    Log.i("TAG", str+ "  "+fm.getBackStackEntryCount());
//                    fragment = fm.findFragmentByTag(str);
//                } catch (NullPointerException e) {
//                    e.printStackTrace();
//                }
//                if(fragment!= null){
//                    fm.popBackStackImmediate();
//                }
                /** Detaches the androidfragment if exists */
                if (mainFragment != null)
                    ft.detach(mainFragment);

                /** Detaches the applefragment if exists */
                if (storeFragment != null)
                    ft.detach(storeFragment);

                if (selectStoreFragment != null)
                    ft.detach(selectStoreFragment);
                if (orderFragment != null)
                    ft.detach(orderFragment);

                if (cardFragment != null)
                    ft.detach(cardFragment);

                if (moreFragment != null)
                    ft.detach(moreFragment);

                if (menuFragment != null)
                    ft.detach(menuFragment);

                if (favFragment != null) {
                    ft.detach(favFragment);
                }

                if (myGiftsFragment != null) {
                    ft.detach(myGiftsFragment);
                }

                if (catFragment != null) {
                    ft.detach(catFragment);
                }

                /** If current tab is android */
                if (tabId.equalsIgnoreCase("main")) {
//                    if(language.equalsIgnoreCase("Ar")) {
//                        bubbleLayout.setVisibility(View.GONE);
//                    }

                    DrConstants.offerFlag = false;
                    if (mainFragment == null) {
                        /** Create AndroidFragment and adding to fragmenttransaction */
                        ft.add(R.id.realtabcontent, new MainFragment(), "main");
                    } else {
                        /** Bring to the front, if already exists in the fragmenttransaction */
                        ft.attach(mainFragment);
                    }
//                    }

                } else if (tabId.equals("store")) {
                    bubbleLayout.setVisibility(View.GONE);
                    DrConstants.offerFlag = false;
                    /** If current tab is enter_from_left */
                    if (storeFragment == null) {
                        /** Create AppleFragment and adding to fragmenttransaction */
                        ft.add(R.id.realtabcontent, new StoreFragment(), "store");
                    } else {
                        /** Bring to the front, if already exists in the fragmenttransaction */
                        ft.attach(storeFragment);
                    }
                } else if (tabId.equals("order")) {
                    bubbleLayout.setVisibility(View.GONE);
                    if (DrConstants.favFlag) {
//                        DrConstants.favFlag = false;
                        if (favFragment == null) {
                            /** Create AppleFragment and adding to fragmenttransaction */
                            ft.add(R.id.realtabcontent, new FavouriteOrderFragment(), "fav_order");
                        } else {
                            /** Bring to the front, if already exists in the fragmenttransaction */
                            ft.attach(favFragment);
                        }
                    } else if (DrConstants.offerFlag) {
                        if (catFragment == null) {
                            /** Create AppleFragment and adding to fragmenttransaction */
                            ft.add(R.id.realtabcontent, new CategoriesListFragment(), "category_list");
                        } else {
                            /** Bring to the front, if already exists in the fragmenttransaction */
                            ft.attach(catFragment);
                        }
                    } else if (DrConstants.storeFlag) {
                        if (selectStoreFragment == null) {
                            /** Create AppleFragment and adding to fragmenttransaction */
                            ft.add(R.id.realtabcontent, new SelectStoreFragment(), "select_store");
                        } else {
                            /** Bring to the front, if already exists in the fragmenttransaction */
                            ft.attach(selectStoreFragment);
                        }
                    }
                        else if (DrConstants.menuFlag) {
//                        DrConstants.menuFlag = false;
                        StoreInfo si = null;
                        try {
                            Gson gson = new Gson();
                            String jsonText = storePrefs.getString("store", null);
                            si = gson.fromJson(jsonText, StoreInfo.class);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if(si != null) {
                            DrConstants.storeName = si.getStoreName();
                            DrConstants.storeAddress = si.getStoreAddress();
                            DrConstants.storeId = si.getStoreId();
                            DrConstants.latitude = si.getLatitude();
                            DrConstants.longitude = si.getLongitude();
                            DrConstants.fullHours = si.is24x7();
                            DrConstants.startTime = si.getStartTime();
                            DrConstants.endTime = si.getEndTime();
                            DrConstants.CashPayment = si.isCashPayment();
                            DrConstants.OnlinePayment = si.isOnlinePayment();
                            DrConstants.endTime = si.getEndTime();
                            DrConstants.orderNow = true;
                            DrConstants.orderType = storePrefs.getString("orderType", "Pick Up");
                            DrConstants.addressId = storePrefs.getString("addressId", "0");
                            DrConstants.addressHouseNo = storePrefs.getString("addressHouseNo", "");
                            DrConstants.address = storePrefs.getString("address", "");
                            DrConstants.delLatitude = storePrefs.getString("delLatitude", "");
                            DrConstants.delLongitude = storePrefs.getString("delLongitude", "");
                            DrConstants.MinOrderAmount = storePrefs.getFloat("MinOrderAmount", 0);
                            DrConstants.DeliveryCharge = storePrefs.getFloat("DeliveryCharge", 0);
                            DrConstants.FreeDeliveryAbove = storePrefs.getFloat("FreeDeliveryAbove", 0);
                        }

                        if (menuFragment == null) {
                            /** Create AppleFragment and adding to fragmenttransaction */
                            ft.add(R.id.realtabcontent, new MenuFragment(), "menu");
                        } else {
                            /** Bring to the front, if already exists in the fragmenttransaction */
                            ft.attach(menuFragment);
                        }
                    } else {
                        if (orderFragment == null) {
                            /** Create AppleFragment and adding to fragmenttransaction */
                            ft.add(R.id.realtabcontent, new OrderFragment(), "order");
                        } else {
                            /** Bring to the front, if already exists in the fragmenttransaction */
                            ft.attach(orderFragment);
                        }
                    }
                } else if (tabId.equals("card")) {
                    bubbleLayout.setVisibility(View.GONE);
                    userPrefsEditor = userPrefs.edit();
                    userPrefsEditor.putBoolean("card", true);
                    userPrefsEditor.commit();

                    DrConstants.offerFlag = false;
                    if (cardFragment == null) {
                        /** Create AppleFragment and adding to fragmenttransaction */
                        ft.add(R.id.realtabcontent, new ManageCardsFragment(), "card");
                    } else {
                        /** Bring to the front, if already exists in the fragmenttransaction */
                        ft.attach(cardFragment);
                    }
                } else if (tabId.equals("more")) {
//                    if(language.equalsIgnoreCase("En")) {
                        bubbleLayout.setVisibility(View.GONE);
//                    }
//                    else{
//                        bubbleLayout.setVisibility(View.VISIBLE);
//                    }
                    DrConstants.offerFlag = false;
                    if (moreFragment == null) {
                        /** Create AppleFragment and adding to fragmenttransaction */
                        ft.add(R.id.realtabcontent, new MoreFragment(), "more");
                    } else {
                        /** Bring to the front, if already exists in the fragmenttransaction */
                        ft.attach(moreFragment);
                    }
                }
                ft.commit();
            }
        };


        /** Setting tabchangelistener for the tab */
        tHost.setOnTabChangedListener(tabChangeListener);

        TabHost.TabSpec tSpecAndroid = tHost.newTabSpec("main");
        if (language.equals("En")) {
            tSpecAndroid.setIndicator("", getResources().getDrawable(R.drawable.tab));
        } else if (language.equals("Ar")) {
            tSpecAndroid.setIndicator("", getResources().getDrawable(R.drawable.tab_arabic));
        }
        tSpecAndroid.setContent(new DummyTabContent(getBaseContext()));


        TabHost.TabSpec tSpecApple = tHost.newTabSpec("store");
        if (language.equals("En")) {
            tSpecApple.setIndicator("", getResources().getDrawable(R.drawable.tab_store_selector));
        } else if (language.equals("Ar")) {
            tSpecApple.setIndicator("", getResources().getDrawable(R.drawable.tab_store_selector_arabic));
        }
        tSpecApple.setContent(new DummyTabContent(getBaseContext()));


        iv = new ImageView(this);
        iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        if (language.equals("En")) {
            iv.setImageResource(R.drawable.tab_order_selector);
        } else if (language.equals("Ar")) {
            iv.setImageResource(R.drawable.tab_order_selector_arabic);
        }


        TabHost.TabSpec tSpecOrder = tHost.newTabSpec("order");
        tSpecOrder.setIndicator(iv);
        tSpecOrder.setContent(new DummyTabContent(getBaseContext()));


        TabHost.TabSpec tSpecCard = tHost.newTabSpec("card");
        if (language.equals("En")) {
            tSpecCard.setIndicator("", getResources().getDrawable(R.drawable.tab_card_selector));
        } else if (language.equals("Ar")) {
            tSpecCard.setIndicator("", getResources().getDrawable(R.drawable.tab_card_selector_arabic));
        }
        tSpecCard.setContent(new DummyTabContent(getBaseContext()));


        TabHost.TabSpec tSpecMore = tHost.newTabSpec("more");
        if (language.equals("En")) {
            tSpecMore.setIndicator("", getResources().getDrawable(R.drawable.tab_more_selector));
        } else if (language.equals("Ar")) {
            tSpecMore.setIndicator("", getResources().getDrawable(R.drawable.tab_more_selector_arabic));
        }
        tSpecMore.setContent(new DummyTabContent(getBaseContext()));


        if (language.equals("En")) {
            tHost.addTab(tSpecAndroid);
            tHost.addTab(tSpecApple);
            tHost.addTab(tSpecOrder);
            tHost.addTab(tSpecCard);
            tHost.addTab(tSpecMore);
            setCurrenTab(0);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)bubbleLayout.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            bubbleLayout.setLayoutParams(params);
        } else if (language.equals("Ar")) {
            tHost.addTab(tSpecMore);
            tHost.addTab(tSpecCard);
            tHost.addTab(tSpecOrder);
            tHost.addTab(tSpecApple);
            tHost.addTab(tSpecAndroid);
            setCurrenTab(4);

            bubbleTextView.setText("الإعدادات الجديدة لإدارة البطاقة الائتمانية");
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)bubbleLayout.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

            bubbleLayout.setLayoutParams(params);
        }


//        tHost.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        tHost.getTabWidget().setStripEnabled(false);
//        tHost.getCurrentTabView().setBackgroundColor(Color.parseColor("#00FFFFFF"));
//        tHost.getTabContentView().setBackgroundColor(Color.parseColor("#00FFFFFF"));
        tHost.getTabWidget().getChildTabViewAt(0).setBackgroundColor(Color.parseColor("#00FFFFFF"));
        tHost.getTabWidget().getChildTabViewAt(1).setBackgroundColor(Color.parseColor("#00FFFFFF"));
        tHost.getTabWidget().getChildTabViewAt(2).setBackgroundColor(Color.parseColor("#00FFFFFF"));
        tHost.getTabWidget().getChildTabViewAt(3).setBackgroundColor(Color.parseColor("#00FFFFFF"));
        tHost.getTabWidget().getChildTabViewAt(4).setBackgroundColor(Color.parseColor("#00FFFFFF"));

//        View target = findViewById(R.id.target_view);
        badge = new BadgeView(this, tHost.getTabWidget().getChildTabViewAt(2));
//        badge1 = new BadgeView(this, tHost.getTabWidget().getChildTabViewAt(3));

//        badge1.setText("N");
//        badge1.setBadgePosition(BadgeView.POSITION_TOP_LEFT);

        try {
            setBadge("" + myDbHelper.getTotalOrderQty());
        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
        }

//        int currentapiVersion = Build.VERSION.SDK_INT;
//        if (currentapiVersion < Build.VERSION_CODES.O) {
//            try {
//                Intercom.client().registerIdentifiedUser(new Registration().withUserId(userId));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
     }


    @Override
    public void onBackPressed() {
        final OrderDetails myFragment1 = (OrderDetails) getSupportFragmentManager().findFragmentByTag("order_details");
        final CategoriesListFragment myFragment2 = (CategoriesListFragment) getSupportFragmentManager().findFragmentByTag("category");
        final MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag("menu");
        final SelectStoreFragment storeFragment = (SelectStoreFragment) getSupportFragmentManager().findFragmentByTag("selectstore");

        if (myFragment1 != null && myFragment1.isVisible()) {
            // add your code here
            Log.i("TAG", "" + "store Visible");
            if (myFragment1.isResumed()) {

            }
        }else if(DrConstants.NO_BACKPRESS || (DrConstants.orderNow && DrConstants.STOP_BACKPRESS)){

        }
        else if(storeFragment != null && storeFragment.isVisible()){
            Log.i("TAG", "" + "storefragment Visible");
            DrConstants.menuFlag = false;
            setCurrenTab(0);
            setCurrenTab(2);
        }
        else if(menuFragment != null && menuFragment.isVisible()){
            Log.i("TAG", "" + "menuFragment Visible");
            if(myDbHelper.getTotalOrderQty()>0){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                String title= "", msg= "", posBtn = "", negBtn = "";
                if(language.equalsIgnoreCase("En")){
                    posBtn = "Yes";
                    negBtn = "No";
                    title = "dr.CAFE";
                    msg = "You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear. Do you want to continue?";
                }else if(language.equalsIgnoreCase("Ar")){
                    posBtn = "نعم";
                    negBtn = "لا";
                    title = "د. كيف";
                    msg = "لديك " + myDbHelper.getTotalOrderQty()+ "  منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات . هل تود الاستمرار";
                }
                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                try {

                                    MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                    MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                    MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                                    CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                    CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                    CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                                    OrderCheckoutFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                    OrderCheckoutFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }

                                setBadge("" + myDbHelper.getTotalOrderQty());
                                FragmentManager manager = getSupportFragmentManager();
                                FragmentTransaction trans = manager.beginTransaction();
                                trans.remove(new MenuFragment());
                                trans.commit();

                                FragmentManager manager1 = getSupportFragmentManager();
                                FragmentTransaction trans1 = manager1.beginTransaction();
                                trans1.remove(new OrderFragment());
                                trans1.commit();

                                manager1.popBackStack();
                                manager.popBackStack();

                                Fragment radiusFragment = new SelectStoreFragment();
                                // consider using Java coding conventions (upper first char class names!!!)
                                FragmentTransaction radiusTransaction = getSupportFragmentManager().beginTransaction();

                                // Replace whatever is in the fragment_container view with this fragment,
                                // and add the transaction to the back stack
                                radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                                radiusTransaction.hide(new MenuFragment());
                                radiusTransaction.add(R.id.realtabcontent, radiusFragment, "selectstore");
                                radiusTransaction.addToBackStack(null);

                                // Commit the transaction
                                radiusTransaction.commit();
                            }
                        }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }else {
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(new MenuFragment());
                trans.commit();

                FragmentManager manager1 = getSupportFragmentManager();
                FragmentTransaction trans1 = manager1.beginTransaction();
                trans1.remove(new OrderFragment());
                trans1.commit();

                manager1.popBackStack();
                manager.popBackStack();

                Fragment radiusFragment = new SelectStoreFragment();

                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction radiusTransaction = getSupportFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                radiusTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                radiusTransaction.hide(new MenuFragment());
                radiusTransaction.add(R.id.realtabcontent, radiusFragment, "selectstore");
                radiusTransaction.addToBackStack(null);
                radiusTransaction.commit();
//                    ((MainActivity) getActivity()).setCurrenTab(0);
//                    ((MainActivity) getActivity()).setCurrenTab(2);
            }
        }
        else {
            try {
                super.onBackPressed();
            }catch (IllegalStateException ise){
                ise.printStackTrace();
            }
            final StoreFragment myFragment = (StoreFragment) getSupportFragmentManager().findFragmentByTag("store");
            if (myFragment != null && myFragment.isVisible()) {
//                // add your code here
                Log.i("TAG", "" + "store Visible");
//                if (myFragment.isResumed()) {
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            myFragment.refresh();
//                        }
//                    }, 300);
//
                }
//            }
            if(myFragment2 != null && myFragment2.isVisible()){
                if (myFragment2.isResumed()) {
                    myFragment2.refreshList();
                }
            }
//        Fragment fragmentBeforeBackPress = getCurrentFragment();
//        // Perform the usual back action
//        super.onBackPressed();
//        Fragment fragmentAfterBackPress = getCurrentFragment();
//        Log.i("TAG", ""+fragmentAfterBackPress.getTag());
//        if(fragmentAfterBackPress == new StoreFragment()){
//            Log.i("TAG", "StoreFragment");
//        }
        }

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
//                    if(((EditText) v).getText().length() == 0){
//                        ((EditText) v).setGravity(Gravity.CENTER);
//                    }else {
//                        ((EditText) v).setGravity(Gravity.CENTER);
//                    }
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    public void clearBackStack() {
        FragmentUtils.sDisableFragmentAnimations = true;
        try {
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }catch (IllegalStateException ise){
            ise.printStackTrace();
        }
        FragmentUtils.sDisableFragmentAnimations = false;
    }

//    private Fragment getCurrentFragment(){
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount()-1).getName();
//        Fragment currentFragment = getSupportFragmentManager()
//                .findFragmentByTag(fragmentTag);
//        return currentFragment;
//    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
//        super.onSaveInstanceState(outState, outPersistentState);
    }

    public void setTab(){

    }

    public void setBadge(String count){
        if(count.equals("0")){
            badge.hide();
            qty = count;
        }else {
            if(qty.equals(count)){

            }else{
                qty = count;
                Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
                badge.setText(count);
                badge.setBadgeBackgroundColor(Color.parseColor("#654113"));
                badge.setBadgePosition(BadgeView.POSITION_TOP_LEFT);
                badge.toggle();
                badge.show();
                badge.startAnimation(shake);
            }

        }
    }

    public void setCurrenTab(int tab){
        tHost.setCurrentTab(tab);
    }


    public void removeBadge(){
        badge.hide();
    }

    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("TAG", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("TAG", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("TAG", "printHashKey()", e);
        }
    }
}

