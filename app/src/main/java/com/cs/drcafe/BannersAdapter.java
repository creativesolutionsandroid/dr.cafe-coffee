package com.cs.drcafe;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.drcafe.model.Banner;

import java.util.ArrayList;

/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class BannersAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int[] mResources = {
            R.drawable.banner1,
            R.drawable.banner2,
            R.drawable.banner3
    };

    boolean offerBanner;
    ArrayList<Banner> bannerList;

    public BannersAdapter(Context context, boolean offerBanner, ArrayList<Banner> bannerList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.offerBanner = offerBanner;
        this.bannerList = bannerList;
    }

    @Override
    public int getCount() {
        if(offerBanner){
            return bannerList.size();
        }else {
            return mResources.length;
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.banner_pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        if (offerBanner) {
            Glide.with(mContext).load("http://www.drcafeonline.net/images/"+ bannerList.get(position).getImage()).into(imageView);
        }else {
            imageView.setImageResource(mResources[position]);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(offerBanner){
//                    if(bannerList.get(position).isClickable()) {
//                        DrConstants.offerFlag = true;
//                        ((MainActivity) mContext).setCurrenTab(2);
////                        Fragment pastryFragment = new CategoriesListFragment();
////                        Bundle mBundle2 = new Bundle();
////                        mBundle2.putInt("openfrom", 0);
////                        mBundle2.putString("catId", "2");
////                        pastryFragment.setArguments(mBundle2);
////                        // consider using Java coding conventions (upper first char class names!!!)
////                        FragmentTransaction pastryTransaction = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
////
////                        // Replace whatever is in the fragment_container view with this fragment,
////                        // and add the transaction to the back stack
////
////                        pastryTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
////                        pastryTransaction.add(R.id.realtabcontent, pastryFragment, "category");
////                        pastryTransaction.hide(new MainFragment());
////                        pastryTransaction.addToBackStack(null);
////
////                        // Commit the transaction
////                        pastryTransaction.commit();
//                    }
//                }

            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}