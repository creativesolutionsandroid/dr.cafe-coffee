package com.cs.drcafe.Dialogs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.ForgotPassword;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class ResetPasswordDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    EditText inputPassword, inputConfirmPassword;
    String strPassword, strConfirmPassword;
    Button buttonSubmit;
    TextView title;
    String userid;
    View rootView;
    String language;

    private final String NAMESPACE = "http://tempuri.org/";
    private String SOAP_ACTION = "http://tempuri.org/SetNewPassword";
    private String METHOD_NAME = "SetNewPassword";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    public static ResetPasswordDialog newInstance() {
        return new ResetPasswordDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        rootView = inflater.inflate(R.layout.dialog_reset_password, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        userid = getArguments().getString("userid");

        inputPassword = (EditText) rootView.findViewById(R.id.reset_input_password);
        inputConfirmPassword = (EditText) rootView.findViewById(R.id.reset_input_retype_password);
        title = (TextView) rootView.findViewById(R.id.reset_password_title);

        buttonSubmit = (Button) rootView.findViewById(R.id.reset_submit_button);
        buttonSubmit.setOnClickListener(this);

        if(language.equalsIgnoreCase("Ar")){
            inputPassword.setHint("كلمة السر الجديدة");
            inputConfirmPassword.setHint("أعد كتابة كلمة السر");
            title.setText("إعادة ضبط كلمة السر");
            buttonSubmit.setText("ارسال");
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.reset_submit_button:
                if(validations()) {
                    new ResetPasswordApi().execute();
                }
                break;
        }
    }

    private boolean validations(){
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0){
            if(language.equalsIgnoreCase("En")) {
                inputPassword.setError("Please enter Password");
            }else if(language.equalsIgnoreCase("Ar")){
                inputPassword.setError("من فضلك ادخل كلمة المرور");
            }
            return false;
        }
        else if (strPassword.length() < 8|| strPassword.length() > 20){
            if(language.equalsIgnoreCase("En")) {
                inputPassword.setError("Password must be at least 8 - 20 characters");
            }else if(language.equalsIgnoreCase("Ar")){
                inputPassword.setError("كلمة المرور يجب أن تتكون من 8 إلى 20 حرف على الأقل");
            }
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            if(language.equalsIgnoreCase("En")) {
                inputConfirmPassword.setError("Please enter Password");
            }else if(language.equalsIgnoreCase("Ar")){
                inputConfirmPassword.setError("من فضلك ادخل كلمة المرور");
            }
            return false;
        }
        else if (!strPassword.equals(strConfirmPassword)){
            if(language.equalsIgnoreCase("En")) {
                inputConfirmPassword.setError("Passwords not match, please retype");
            }else if(language.equalsIgnoreCase("Ar")){
                inputConfirmPassword.setError("كلمة المرور غير مطابقة ، من فضلك اعد الادخال");
            }
            return false;
        }
        return true;
    }

    public class ResetPasswordApi extends AsyncTask<String, String, String> {
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = ProgressDialog.show(getContext(), "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("userId", userid);
                    request.addProperty("password", strPassword);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    if (response12.equals("anyType{}")) {
                        dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                    } else {
                        dialog.dismiss();
                        try {
                            Object property = result.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject loginObject = (SoapObject) property;
                                    if(loginObject.getPropertyCount() == 0){
                                        dialog.dismiss();
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

                                        if(language.equalsIgnoreCase("En")) {
                                            // set title
                                            alertDialogBuilder.setTitle("dr.CAFE");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("Reset password unsuccessful.")
                                                    .setCancelable(false)
                                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                        }else if(language.equalsIgnoreCase("Ar")){
                                            // set title
                                            alertDialogBuilder.setTitle("د. كيف");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("فشل إعادة ضبط كلمة السر")
                                                    .setCancelable(false)
                                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                        }
                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        // show it
                                        alertDialog.show();
                                    }else {
                                        SoapObject userObject = (SoapObject) loginObject.getProperty(0);
                                        SoapObject userObject1 = (SoapObject) userObject.getProperty(0);
                                        String userId = userObject1.getPropertyAsString("userId");
                                        Log.i("TAG", "" + userId);

                                        String fullName = userObject1.getPropertyAsString("fullName");
                                        String phone = userObject1.getPropertyAsString("phone");
                                        String email = userObject1.getPropertyAsString("email");
//                                        String country = userObject1.getPropertyAsString("country");
//                                        String username = userObject1.getPropertyAsString("username");
                                        String mobile = userObject1.getPropertyAsString("mobile");
                                        String NickName = userObject1.getPropertyAsString("NickName");
                                        if (NickName.equalsIgnoreCase("anytype{}")) {
                                            NickName = "";
                                        }
                                        String Gender = userObject1.getPropertyAsString("Gender");
                                        String FamilyName = userObject1.getPropertyAsString("FamilyName");
                                        if (FamilyName.equalsIgnoreCase("anytype{}")) {
                                            FamilyName = "";
                                        }
//                                        boolean isVerified = Boolean.valueOf(userObject1.getPropertyAsString("isVerified"));

                                        try {
                                            JSONObject parent = new JSONObject();
                                            JSONObject jsonObject = new JSONObject();
                                            JSONArray jsonArray = new JSONArray();
                                            jsonArray.put("lv1");
                                            jsonArray.put("lv2");

                                            jsonObject.put("userId", userId);
                                            jsonObject.put("fullName", fullName);
                                            jsonObject.put("phone", phone);
                                            jsonObject.put("mobile", mobile);
                                            jsonObject.put("FamilyName", FamilyName);
                                            jsonObject.put("Gender", Gender);
                                            jsonObject.put("country", "");
                                            jsonObject.put("NickName", NickName);
                                            jsonObject.put("username", email);
                                            jsonObject.put("user_details", jsonArray);
                                            parent.put("profile", jsonObject);
                                            Log.d("output", parent.toString());
                                            userPrefEditor.putString("user_profile", parent.toString());
                                            userPrefEditor.putString("userId", userId);

                                            userPrefEditor.putString("user_email", email);
                                            userPrefEditor.putString("user_password", strPassword);

                                            userPrefEditor.commit();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();

                                        userPrefEditor.putString("login_status", "loggedin");
                                        userPrefEditor.commit();

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

                                        if(language.equalsIgnoreCase("En")) {
                                            // set title
                                            alertDialogBuilder.setTitle("dr.CAFE");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("Reset password successful.")
                                                    .setCancelable(false)
                                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                            ForgotPassword.isResetSuccessful = true;
                                                            getDialog().dismiss();
                                                        }
                                                    });
                                        }else if(language.equalsIgnoreCase("Ar")){
                                            // set title
                                            alertDialogBuilder.setTitle("د. كيف");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("تم تغيير كلمة المرور بنجاح")
                                                    .setCancelable(false)
                                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                            ForgotPassword.isResetSuccessful = true;
                                                            getDialog().dismiss();
                                                        }
                                                    });
                                        }
                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        // show it
                                        alertDialog.show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }
                    }
                }
            }
            super.onPostExecute(result1);
        }
    }
}
