package com.cs.drcafe.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cs.drcafe.ForgotPassword;
import com.cs.drcafe.R;


public class VerifyOtpDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    TextView textMobileNumber, textOtpTitle, body, insertText;
    String otpEntered="", strMobile, serverOtp;
    Button buttonVerify;
    Button imgEditMobile;
    String language;
    EditText otpView;
    View rootView;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;
    private Handler handler;
    private Runnable myRunnable;

    public static VerifyOtpDialog newInstance() {
        return new VerifyOtpDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_verify_otp, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        LanguagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userPrefsEditor = userPrefs.edit();

        textMobileNumber = (TextView) rootView.findViewById(R.id.otp_mobile_number);
        imgEditMobile = (Button) rootView.findViewById(R.id.edit_mobile_number);
        textOtpTitle = (TextView) rootView.findViewById(R.id.otp_title);
        otpView = (EditText) rootView.findViewById(R.id.otp_view);
        body = (TextView)rootView.findViewById(R.id.textView9);
        insertText = (TextView)rootView.findViewById(R.id.textView10);
        buttonVerify = (Button) rootView.findViewById(R.id.button_verify_otp);

        imgEditMobile.setVisibility(View.GONE);

        handler = new Handler();
        myRunnable = new Runnable() {
            public void run() {
                imgEditMobile.setVisibility(View.VISIBLE);
            }
        };
        handler.postDelayed(myRunnable,60000);

        serverOtp = getArguments().getString("otp");
        strMobile = getArguments().getString("mobile");
        textMobileNumber.setText("+966 "+strMobile);

        if(language.equalsIgnoreCase("Ar")){
            textOtpTitle.setText("رمز التفعيل للجوال");    /*title*/
            body.setText("من فضلك راجع هاتفك لإعادة تفعيل رمز الدخول");
            insertText.setText("ادخل رمز التفعيل");
            buttonVerify.setText("تحميل");
            imgEditMobile.setText("تغيير");
        }

        buttonVerify.setOnClickListener(this);
        imgEditMobile.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_verify_otp:
                otpEntered = otpView.getText().toString();
                if(otpEntered.length() != 4){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");
                    // set dialog message
                    alertDialogBuilder
                            .setMessage(R.string.otp_alert1)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                }
                else if(!otpEntered.equals(serverOtp)){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");
                    // set dialog message
                    alertDialogBuilder
                            .setMessage(R.string.otp_alert1)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                }
                else{
                    ForgotPassword.isOTPSuccessful = true;
                    getDialog().cancel();
                }
                break;

            case R.id.edit_mobile_number:
//                SignUpActivity.isOTPVerified = false;
                getDialog().cancel();
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(handler!=null){
            handler.removeCallbacks(myRunnable);
        }
    }
}
