package com.cs.drcafe;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.drcafe.Dialogs.ResetPasswordDialog;
import com.cs.drcafe.Dialogs.VerifyOtpDialog;
import com.cs.drcafe.adapter.CountriesAdapter;
import com.cs.drcafe.model.Countries;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by SKT on 03-01-2016.
 */
public class ForgotPassword extends AppCompatActivity {
//    private String SOAP_ACTION = "http://tempuri.org/forgetPassword";
//    private String METHOD_NAME = "forgetPassword";
    private final String NAMESPACE = "http://tempuri.org/";
    private String SOAP_ACTION = "http://tempuri.org/SendOTPtoChangePassword";
    private String METHOD_NAME = "SendOTPtoChangePassword";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    TextView mCancel, msend;
    EditText mPhoneNumber,mCountryCode;
    ProgressDialog dialog;
    String language;
    public static boolean isOTPSuccessful = false;
    public static boolean isResetSuccessful = false;
    private String ServerOTP = "";
    private String userId = "78004";

    private LinearLayout mCountryPicker;
    private ImageView mCountryFlag;
    private ArrayList<Countries> countriesList = new ArrayList<>();
    private Countries countries;
    private CountriesAdapter mCountriesAdapter;
    private int countryFlag;
    private int phoneNumberLength;
    private String countryCode = "+966";
    String strMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.forgot_password);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.forgot_password_arabic);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        prepareArrayLits();
        countryFlag = R.drawable.sa;
        mCountriesAdapter = new CountriesAdapter(this,countriesList);
        mCountryPicker = (LinearLayout) findViewById(R.id.country_picker);
        mCountryFlag = (ImageView) findViewById(R.id.country_flag);

        mCancel = (TextView) findViewById(R.id.cancel_button);
        msend = (TextView) findViewById(R.id.send_button);
        mPhoneNumber = (EditText) findViewById(R.id.forgot_email);
        mCountryCode = (EditText) findViewById(R.id.register_country_code);

        if(countryCode.equalsIgnoreCase("+966")){
            int maxLength = 9;
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(maxLength);
            mPhoneNumber.setFilters(fArray);
            phoneNumberLength = 9;
        }

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        msend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strMobile = mPhoneNumber.getText().toString();
                if(strMobile.length() == 0){
                    mPhoneNumber.setError("Please enter Mobile Number");
                }else if(strMobile.length() != phoneNumberLength){
                    mPhoneNumber.setError("Please enter valid Mobile Number");
                }else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPassword.this);

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("We will be verifying the mobile number: "+countryCode+" "+strMobile+"\n\nIs this OK, or would you like to edit the number?")
                            .setCancelable(false)
                            .setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mPhoneNumber.requestFocus();
                                    mPhoneNumber.setSelection(mPhoneNumber.length());
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    new GetVerificationDetails().execute();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });

        mPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = mPhoneNumber.getText().toString();
                if(!mPhoneNumber.hasWindowFocus() || mPhoneNumber.hasFocus() || s == null){
//                    if(countryCode.equalsIgnoreCase("+966")){
                    if(text.startsWith("0")){
                        mPhoneNumber.setText("");
                        return;
//                        }
                    }
                    return;
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mCountryPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayDialog();
            }
        });
    }


    public class GetVerificationDetails extends AsyncTask<String, String, String> {
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPassword.this);
            dialog = ProgressDialog.show(ForgotPassword.this, "",
                    "Please wait...");
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    String code = countryCode.replace("+","");
                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("mobile", code+strMobile);
                    request.addProperty("language", language);

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPassword.this);

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    if (response12.equals("anyType{}")) {
                        dialog.dismiss();
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                    } else {
                        dialog.dismiss();
                        try {
                            Object property = result.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject loginObject = (SoapObject) property;
                                    if(loginObject.getPropertyCount() == 0){
                                        dialog.dismiss();
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPassword.this);


                                        if(language.equalsIgnoreCase("En")) {
                                            // set title
                                            alertDialogBuilder.setTitle("dr.CAFE");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("Invalid Mobile Number")
                                                    .setCancelable(false)
                                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                        }else if(language.equalsIgnoreCase("Ar")){
                                            // set title
                                            alertDialogBuilder.setTitle("د. كيف");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("Invalid Mobile Number")
                                                    .setCancelable(false)
                                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                        }
                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        // show it
                                        alertDialog.show();
                                    }else {
                                        SoapObject userObject = (SoapObject) loginObject.getProperty(0);
                                        SoapObject userObject1 = (SoapObject) userObject.getProperty(0);
                                        userId = userObject1.getPropertyAsString("userId");
                                        String message = userObject1.getPropertyAsString("CustomMessage");
                                        if (userId.equals("0")){
                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPassword.this);
                                            if(language.equalsIgnoreCase("En")) {
                                                // set title
                                                alertDialogBuilder.setTitle("dr.CAFE");
                                                // set dialog message
                                                alertDialogBuilder
                                                        .setMessage(message)
                                                        .setCancelable(false)
                                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.dismiss();
//                                                                onBackPressed();
                                                            }
                                                        });
                                            }else if(language.equalsIgnoreCase("Ar")){
                                                // set title
                                                alertDialogBuilder.setTitle("د. كيف");

                                                // set dialog message
                                                alertDialogBuilder
                                                        .setMessage(message)
                                                        .setCancelable(false)
                                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.dismiss();
                                                            }
                                                        });
                                            }
                                            // create alert dialog
                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                            // show it
                                            alertDialog.show();
                                        }
                                        else{
                                            ServerOTP = userObject1.getPropertyAsString("OTP");
                                            Log.i("TAG","ServerOTP "+ServerOTP);
                                            displayVerifyDialog();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }
                    }
                }
            }
            super.onPostExecute(result1);
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    public void prepareArrayLits()
    {

        AddObjectToList(R.drawable.bh, "Bahrain", "+973", "18");
        AddObjectToList(R.drawable.bd, "Bangladesh", "+880", "19");
        AddObjectToList(R.drawable.cn, "China", "+86", "45");
        AddObjectToList(R.drawable.cy, "Cyprus", "+537" ,"57");
        AddObjectToList(R.drawable.eg, "Egypt", "+20", "64");
        AddObjectToList(R.drawable.fr, "France", "+33", "74");
        AddObjectToList(R.drawable.de, "Germany", "+49", "81");
        AddObjectToList(R.drawable.in, "India", "+91", "101");
        AddObjectToList(R.drawable.id, "Indonesia", "+62", "102");

        AddObjectToList(R.drawable.ir, "Iran", "+98", "103");
        AddObjectToList(R.drawable.iq, "Iraq", "+964", "104");
        AddObjectToList(R.drawable.jo, "Jordan", "+962", "112");
        AddObjectToList(R.drawable.kw, "Kuwait", "+965", "118");
        AddObjectToList(R.drawable.lb, "Lebanon", "+961", "122");
        AddObjectToList(R.drawable.my, "Malaysia", "+60", "133");
        AddObjectToList(R.drawable.ma, "Morocco", "+212", "147");
        AddObjectToList(R.drawable.np, "Nepal", "+977", "152");
        AddObjectToList(R.drawable.om, "Oman", "+968", "164");

        AddObjectToList(R.drawable.pk, "Pakistan", "+92", "165");
        AddObjectToList(R.drawable.ps, "Palestinian Territories", "", "167");
        AddObjectToList(R.drawable.ph, "Philippines", "+63", "172");
        AddObjectToList(R.drawable.qa, "Qatar", "+974", "177");
        AddObjectToList(R.drawable.sa, "Saudi Arabia", "+966", "190");
        AddObjectToList(R.drawable.sg, "Singapore", "+65", "195");
        AddObjectToList(R.drawable.es, "Spain", "+34", "202");
        AddObjectToList(R.drawable.lk, "Sri Lanka", "+94", "203");
        AddObjectToList(R.drawable.sd, "Sudan", "+249", "204");

        AddObjectToList(R.drawable.tr, "Syria", "+963", "210");
        AddObjectToList(R.drawable.tr, "Turkey", "+90", "221");
        AddObjectToList(R.drawable.ae, "United Arab Emirates", "+971", "227");
        AddObjectToList(R.drawable.gb, "United Kingdom", "+44", "228");
        AddObjectToList(R.drawable.us, "United States", "+1", "229");
        AddObjectToList(R.drawable.ye, "Yemen", "+967", "240");
    }

    // Add one item into the Array List
    public void AddObjectToList(int image, String countryName, String countryCode, String countrrId)
    {
        countries = new Countries();
        countries.setCountryName(countryName);
        countries.setCountryFlag(image);
        countries.setCountryCode(countryCode);
        countries.setCountryID(countrrId);
        countriesList.add(countries);
    }

    public void displayDialog() {

        final Dialog dialog2 = new Dialog(this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.setContentView(R.layout.countries_list);
        dialog2.setCanceledOnTouchOutside(true);
        ListView lv = (ListView) dialog2.findViewById(R.id.countries_list);
        lv.setAdapter(mCountriesAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                countryCode = countriesList.get(arg2).getCountryCode();
                countryFlag = countriesList.get(arg2).getCountryFlag();

                if(countryCode.equalsIgnoreCase("+966")){
                    int maxLength = 9;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mPhoneNumber.setFilters(fArray);
                    phoneNumberLength = 9;
                }else{
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mPhoneNumber.setFilters(fArray);
                    phoneNumberLength = 10;
                }

                mCountryFlag.setImageResource(countriesList.get(arg2).getCountryFlag());
                mCountryCode.setText("" + countriesList.get(arg2).getCountryCode());

                dialog2.dismiss();

            }
        });
        dialog2.show();
    }

    public void displayVerifyDialog(){
        Bundle args = new Bundle();
        args.putString("mobile", strMobile);
        args.putString("otp", ServerOTP);

        isOTPSuccessful = false;

        final VerifyOtpDialog newFragment = VerifyOtpDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "forgot");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed
                if (newFragment != null) {
                    newFragment.dismiss();
                }

                if(isOTPSuccessful){
                    displayResetPasswordDiaolg();
                }
            }
        });
    }

    public void displayResetPasswordDiaolg(){
        Bundle args = new Bundle();
        args.putString("userid", userId);

        final ResetPasswordDialog newFragment = ResetPasswordDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "forgot");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed
                if (newFragment != null) {
                    newFragment.dismiss();
                }

                if(isResetSuccessful){
                    Intent loginIntent = new Intent(ForgotPassword.this, MainActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(loginIntent);
                    finish();
                }
            }
        });
    }
}
