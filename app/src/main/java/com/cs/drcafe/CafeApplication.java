package com.cs.drcafe;

import android.app.Application;
import android.os.Build;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
/**
 * Created by SKT on 13-12-2015.
 */
public class CafeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
//        int currentapiVersion = Build.VERSION.SDK_INT;
//        if (currentapiVersion < Build.VERSION_CODES.O) {
//            try {
//                Intercom.initialize(this, "android_sdk-1e257a8ea6a7168703326383edffe9396847a558", "a909xa1j");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        TypefaceUtil.setDefaultFont(getApplicationContext(), "DEFAULT", "fonts/Avenir-Heavy.ttf");
        TypefaceUtil.setDefaultFont(getApplicationContext(), "MONOSPACE", "fonts/Avenir-Heavy.ttf");
        TypefaceUtil.setDefaultFont(getApplicationContext(), "SERIF", "fonts/Avenir-Heavy.ttf");
        TypefaceUtil.setDefaultFont(getApplicationContext(), "SANS_SERIF", "fonts/Avenir-Heavy.ttf");

    }
}
