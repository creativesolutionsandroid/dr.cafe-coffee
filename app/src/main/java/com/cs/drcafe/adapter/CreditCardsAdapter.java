package com.cs.drcafe.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.NetworkUtil;
import com.cs.drcafe.R;
import com.cs.drcafe.Rest.APIInterface;
import com.cs.drcafe.Rest.ApiClient;
import com.cs.drcafe.fragments.MainFragment;
import com.cs.drcafe.fragments.TrackOrderFragment;
import com.cs.drcafe.model.DeleteCardList;
import com.cs.drcafe.model.MyCards;
import com.cs.drcafe.model.SavedCards;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SKT on 20-02-2016.
 */
public class CreditCardsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    String language;
    List<SavedCards.Data> myGiftsList = new ArrayList<>();
    int pos;
    Activity activity;
    String id;

    ProgressDialog dialog;
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private String SOAP_ACTION_SETCARD = "http://tempuri.org/deleteUserCreditCard";
    private String METHOD_NAME_SETCARD = "deleteUserCreditCard";
    private SoapPrimitive getCardResult;
    private String getCardResponse = null;
    //public ImageLoader imageLoader;

    private static final String PAYMENTS_URL = DrConstants.PAYMENTS_LIVE_URL;
    private String SOAP_ACTION_HYPERPAYCARD = "http://tempuri.org/deleteCard";
    private String METHOD_NAME_HYPERPAYCARD = "deleteCard";
    private SoapPrimitive getHyperpayCardResult;
    private String getHyperpayCardResponse = null;

    public CreditCardsAdapter(Context context, List<SavedCards.Data> myGiftsList, String language, Activity activity) {
        this.context = context;
        this.myGiftsList = myGiftsList;
        this.activity = activity;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return myGiftsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView holderName, CardNumber, expiryDate;
        ImageView cardType, cardDelete;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_my_cards, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_my_cards, null);
            }

            holder.holderName = (TextView) convertView
                    .findViewById(R.id.name_on_card);
            holder.CardNumber = (TextView) convertView
                    .findViewById(R.id.card_number);
            holder.expiryDate = (TextView) convertView
                    .findViewById(R.id.card_expiry);
            holder.cardType = (ImageView) convertView.findViewById(R.id.card_type);
            holder.cardDelete = (ImageView) convertView.findViewById(R.id.delete_card);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.holderName.setText(myGiftsList.get(position).getCardHolder());
        holder.CardNumber.setText("**** **** **** "+myGiftsList.get(position).getLast4Digits());
        holder.expiryDate.setText(myGiftsList.get(position).getExpireMonth()+"/"+myGiftsList.get(position).getExpireYear());

        if(myGiftsList.get(position).getCardBrand().equalsIgnoreCase("visa")){
            holder.cardType.setImageDrawable(context.getResources().getDrawable(R.drawable.visa));
        }
        else {
            holder.cardType.setImageDrawable(context.getResources().getDrawable(R.drawable.master));
        }

        holder.cardDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                String title= "", msg= "", posBtn = "", negBtn = "";
                if(language.equalsIgnoreCase("En")){
                    posBtn = "Yes";
                    negBtn = "No";
                    title = "dr.CAFE";
                    msg = "Are you sure to delete the card?";
                }else if(language.equalsIgnoreCase("Ar")){
                    posBtn = "نعم";
                    negBtn = "لا";
                    title = "د. كيف";
                    msg = "هل انت متأكد من رغبتك في حذف البطاقة ؟";
                }
                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                Log.d("TAG", "onClick: "+myGiftsList.get(position).getToken());
                                new deleteCardApi().execute(myGiftsList.get(position).getToken(), ""+position);
                            }
                        }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        return convertView;
    }

    private class deleteCardApi extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(activity, "",
                    "Please wait....");
        }

        @Override
        protected String doInBackground(final String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            SharedPreferences userPrefs = activity.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
            String userId = userPrefs.getString("userId", null);

            Log.d("TAG", "params[0]: "+params[0]);
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("userId", userId);
                parentObj.put("registrationId", params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("TAG", "parentObj: " + parentObj.toString());

            Call<DeleteCardList> call = apiService.deleteCard(
                    RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));

            call.enqueue(new Callback<DeleteCardList>() {
                @Override
                public void onResponse(Call<DeleteCardList> call, Response<DeleteCardList> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        DeleteCardList orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                Log.d("TAG", "onResponse: true ");
                                myGiftsList.remove(Integer.parseInt(params[1]));
                                notifyDataSetChanged();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    showAlertDialog(failureResponse);
                                } else {
                                    String failureResponse = orderItems.getMessage();
                                    showAlertDialog(failureResponse);
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            showAlertDialog("Cannot reach server");
                        }
                    } else {
                        showAlertDialog("Cannot reach server");

                    }

                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<DeleteCardList> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    showAlertDialog("Cannot reach server");
                }
            });
            return null;
        }
    }

    public class DeleteHyperPayCardDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        SharedPreferences userPrefs;
        String userId;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(activity);
            dialog = ProgressDialog.show(activity, "",
                    "Please wait....");
            userPrefs = activity.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
            userId = userPrefs.getString("userId", null);
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                java.net.URL url;
                String urlString = null;
                HttpURLConnection connection = null;
                try {
//                    urlString = DrConstants.PAYMENTS_LIVE_URL + userId+ "&regdistrationId="+myGiftsList.get(pos).getToken();

//                    Log.i("TAG", "delete request url: " + urlString);

                    url = new URL(urlString);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(DrConstants.CONNECTION_TIMEOUT);

                    JsonReader jsonReader = new JsonReader(
                            new InputStreamReader(connection.getInputStream(), "UTF-8"));

                    jsonReader.beginObject();

                    while (jsonReader.hasNext()) {
                        String key = jsonReader.nextName();
                        if (key.equals("id")) {
                            getHyperpayCardResponse = jsonReader.nextString();
                        }
                        else {
                            jsonReader.skipValue();
                        }
                    }

                    jsonReader.endObject();
                    jsonReader.close();

                } catch (Exception e) {
                    Log.i("TAG", "Error: ", e);
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }else{
                getHyperpayCardResponse = "no internet";
            }
            return getHyperpayCardResponse;
        }

        @Override
        protected void onPostExecute(String result1) {

            if(result1 == null){
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Delete card failed, please try again.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }else if(result1.equalsIgnoreCase("no internet")){
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Communication Error! Please check the internet connection?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            } else {
                dialog.dismiss();
                if(result1.length() > 2) {
//                    new DeleteCardDetails().execute();
                    myGiftsList.remove(pos);
                    notifyDataSetChanged();
                }
            }

            super.onPostExecute(result1);
        }
    }

    public class DeleteCardDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        SharedPreferences userPrefs;
        String userId;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(activity);
//            dialog = ProgressDialog.show(activity, "",
//                    "Please wait....");
            userPrefs = activity.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
            userId = userPrefs.getString("userId", null);
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME_SETCARD);
                    request.addProperty("userId", userId);
//                    request.addProperty("registrationId", myGiftsList.get(pos).getToken());

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                    androidHttpTransport.call(SOAP_ACTION_SETCARD, envelope);
                    getCardResult = (SoapPrimitive) envelope.getResponse();
                    getCardResponse = getCardResult.toString();
                    Log.v("TAG", getCardResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + getCardResponse);
            }else{
                getCardResponse = "no internet";
            }
            return getCardResponse;
        }

        @Override
        protected void onPostExecute(String result1) {

            if(result1 == null){
                dialog.dismiss();

            }else if(result1.equalsIgnoreCase("no internet")){
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Communication Error! Please check the internet connection?")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            } else {
                dialog.dismiss();
                if(result1.equalsIgnoreCase("true")) {
                    myGiftsList.remove(pos);
                    notifyDataSetChanged();
                }
            }

            super.onPostExecute(result1);
        }
    }

    protected void showAlertDialog(String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, null)
                .setCancelable(false)
                .show();
    }

    protected void showAlertDialog(int messageId) {
        showAlertDialog(context.getString(messageId));
    }
}