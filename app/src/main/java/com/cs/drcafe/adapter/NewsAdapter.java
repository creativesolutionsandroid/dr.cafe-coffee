package com.cs.drcafe.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.News;

import java.util.ArrayList;

/**
 * Created by SKT on 05-01-2016.
 */
public class NewsAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<News> newsList = new ArrayList<>();

    public NewsAdapter(Context context, ArrayList<News> newsList) {
        this.context = context;
        this.newsList = newsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return newsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView newsTitle;
        TextView newsDesc;
        RelativeLayout newsItemLayout;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.news_listitem, null);

            holder.newsTitle = (TextView) convertView
                    .findViewById(R.id.news_title);
            holder.newsDesc = (TextView) convertView
                    .findViewById(R.id.news_desc);
            holder.newsItemLayout = (RelativeLayout) convertView.
                    findViewById(R.id.news_item_layout);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(position % 2 == 0){
            holder.newsItemLayout.setBackgroundColor(Color.parseColor("#F2EFE1"));
        }else {
            holder.newsItemLayout.setBackgroundColor(Color.parseColor("#EDEADC"));
        }

        holder.newsTitle.setText(newsList.get(position).getNewsHead());
        holder.newsDesc.setText(Html.fromHtml(newsList.get(position).getNewsDesc()));

        return convertView;
    }



}
