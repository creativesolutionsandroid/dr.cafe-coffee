package com.cs.drcafe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.Transactions;

import java.util.ArrayList;

/**
 * Created by BRAHMAM on 19-03-2016.
 */
public class CardTransactionsAdapterArabic extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Transactions> storesList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    //public ImageLoader imageLoader;

    public CardTransactionsAdapterArabic(Context context, ArrayList<Transactions> storesList) {
        this.context = context;
        this.storesList = storesList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return storesList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView storeName,tranasactionDate,totalBalance;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.transactions_list_item_arabic, null);

            holder.storeName = (TextView) convertView
                    .findViewById(R.id.transaction_storeame);
            holder.tranasactionDate = (TextView) convertView
                    .findViewById(R.id.transaction_date);
            holder.totalBalance = (TextView) convertView
                    .findViewById(R.id.transaction_balance);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.storeName.setText(storesList.get(position).getStoreName());
        holder.tranasactionDate.setText(storesList.get(position).getTransactionDate());

        holder.totalBalance.setText(String.format("%.2f", Float.parseFloat(storesList.get(position).getTotalBalance())));


        return convertView;
    }
}
