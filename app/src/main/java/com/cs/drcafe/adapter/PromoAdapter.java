package com.cs.drcafe.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.fragments.OrderConfimationFragment;
import com.cs.drcafe.model.Promos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 30-06-2016.
 */
public class PromoAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Promos> promosList = new ArrayList<>();
    String language;
    Activity activity;
    int maxApplicable = 10;

    public PromoAdapter(Context context, ArrayList<Promos> promosList, Activity activity) {
        this.context = context;
        this.promosList = promosList;
        this.activity = activity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        SharedPreferences languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
    }

    public int getCount() {
        return promosList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView offerName, remainingBonus, validityDate, qty, qtyText;
        LinearLayout layout;
        ImageView radioButton, plus, minus;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.promo_list_item, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.promo_list_item_arabic, null);
            }

            holder.offerName = (TextView) convertView
                    .findViewById(R.id.offer_title);
            holder.remainingBonus = (TextView) convertView
                    .findViewById(R.id.remainingBonus);
            holder.validityDate = (TextView) convertView
                    .findViewById(R.id.validityDate);
            holder.qty = (TextView) convertView
                    .findViewById(R.id.promoQty);
            holder.layout = (LinearLayout) convertView.
                    findViewById(R.id.layout);

            holder.radioButton = (ImageView) convertView
                    .findViewById(R.id.radio_button);
            holder.plus = (ImageView) convertView
                    .findViewById(R.id.plus_btn);
            holder.minus = (ImageView) convertView
                    .findViewById(R.id.minus_btn);
            holder.qtyText = (TextView) convertView
                    .findViewById(R.id.qty_text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(OrderConfimationFragment.position == position){
            holder.radioButton.setImageDrawable(context.getResources().getDrawable(R.drawable.promo_selected));
            holder.qty.setText(""+OrderConfimationFragment.promoQty);
            if(OrderConfimationFragment.promoQty == 1){
                holder.minus.setAlpha(0.4f);
                holder.minus.setFocusable(false);
                holder.minus.setFocusableInTouchMode(false);
                holder.minus.setClickable(false);
                if(promosList.get(position).getDailyOnce().equals("true")){
                    holder.plus.setAlpha(0.4f);
                    holder.plus.setFocusable(false);
                    holder.plus.setFocusableInTouchMode(false);
                    holder.plus.setClickable(false);
                }
                else {
                    holder.plus.setAlpha(1.0f);
                    holder.plus.setFocusable(true);
                    holder.plus.setFocusableInTouchMode(true);
                    holder.plus.setClickable(true);
                }
            }
            else if(OrderConfimationFragment.promoQty == 10){
                holder.plus.setAlpha(0.4f);
                holder.plus.setFocusable(false);
                holder.plus.setFocusableInTouchMode(false);
                holder.plus.setClickable(false);
                holder.minus.setAlpha(1.0f);
                holder.minus.setFocusable(true);
                holder.minus.setFocusableInTouchMode(true);
                holder.minus.setClickable(true);
            }
            else{
                holder.plus.setAlpha(1.0f);
                holder.plus.setFocusable(true);
                holder.plus.setFocusableInTouchMode(true);
                holder.plus.setClickable(true);
                holder.minus.setAlpha(1.0f);
                holder.minus.setFocusable(true);
                holder.minus.setFocusableInTouchMode(true);
                holder.plus.setClickable(true);
            }
        }
        else{
            holder.radioButton.setImageDrawable(context.getResources().getDrawable(R.drawable.promo_unselected));
            holder.qty.setText("1");
            holder.minus.setAlpha(0.4f);
            holder.minus.setFocusable(false);
            holder.minus.setFocusableInTouchMode(false);
            holder.plus.setClickable(false);
            holder.plus.setAlpha(0.2f);
            holder.plus.setFocusable(true);
            holder.plus.setFocusableInTouchMode(true);
            holder.plus.setClickable(true);
        }

        if(promosList.get(position).getPromoType().equalsIgnoreCase("7")){
            holder.plus.setVisibility(View.VISIBLE);
            holder.minus.setVisibility(View.VISIBLE);
        }
        else{
            holder.plus.setVisibility(View.INVISIBLE);
            holder.minus.setVisibility(View.INVISIBLE);
        }

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(OrderConfimationFragment.position!=-1){
                    if(OrderConfimationFragment.position == position) {
                        if (promosList.get(position).getDailyOnce().equals("false")) {
                            maxApplicable = 10;
                            float remBonus = Float.parseFloat(promosList.get(position).getRemainingBonus());
                            if(remBonus < 10){
                                maxApplicable = (int)remBonus;
                            }
                            if (OrderConfimationFragment.promoQty < maxApplicable) {
                                OrderConfimationFragment.promoQty = OrderConfimationFragment.promoQty + 1;
                                notifyDataSetChanged();
                            }
                        }
                    }
                }
                else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Please select any one promotion")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(OrderConfimationFragment.position == position) {
                    if (OrderConfimationFragment.promoQty > 1) {
                        OrderConfimationFragment.promoQty = OrderConfimationFragment.promoQty - 1;
                        notifyDataSetChanged();
                    }
                }
            }
        });

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!promosList.get(position).getValidDays().equals("0")) {
                    if(OrderConfimationFragment.position == position){

                    }
                    else {
                        OrderConfimationFragment.position = position;
                        OrderConfimationFragment.promoQty = 1;
                        notifyDataSetChanged();
                    }
                }
                else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("This Promotion is applicable only on "+promosList.get(position).getValidForDays())
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });

        if(language.equalsIgnoreCase("En")){
            holder.offerName.setText(promosList.get(position).getPromoTitle());

            SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
            Date dateObj = null;
            try {
                dateObj = curFormater.parse(promosList.get(position).getEndDate().replace("  ", " 0"));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
            String date = postFormater.format(dateObj);

            if (promosList.get(position).getPromoType().equalsIgnoreCase("2") ||
                    promosList.get(position).getPromoType().equalsIgnoreCase("3") ||
                    promosList.get(position).getPromoType().equalsIgnoreCase("6")) {

                if (promosList.get(position).getPromoType().equalsIgnoreCase("6")) {
                    if (promosList.get(position).getRemainingBonus().equalsIgnoreCase("0") || promosList.get(position).getRemainingBonus().equalsIgnoreCase("0.00")) {
                        holder.remainingBonus.setText("Remaining Free drinks 1");
                        holder.qty.setVisibility(View.VISIBLE);
                        holder.qtyText.setVisibility(View.VISIBLE);
                    } else {
                        holder.qty.setVisibility(View.INVISIBLE);
                        holder.qtyText.setVisibility(View.INVISIBLE);
                        holder.remainingBonus.setText("Remaining amount " + promosList.get(position).getRemainingBonus() + " SR");
                    }
                } else {
                    holder.qty.setVisibility(View.INVISIBLE);
                    holder.qtyText.setVisibility(View.INVISIBLE);
                    holder.remainingBonus.setText("Remaining amount " + promosList.get(position).getRemainingBonus() + " SR");
                }
            } else if (promosList.get(position).getPromoType().equalsIgnoreCase("1") ||
                    promosList.get(position).getPromoType().equalsIgnoreCase("5") ||
                    promosList.get(position).getPromoType().equalsIgnoreCase("7")) {
                holder.qty.setVisibility(View.VISIBLE);
                holder.qtyText.setVisibility(View.VISIBLE);
                float remBonus = Float.parseFloat(promosList.get(position).getRemainingBonus());
                holder.remainingBonus.setText("Remaining Free drinks " + (int) remBonus);
            }

            holder.validityDate.setText("Valid until\n"+date);
        }
        else{
            holder.offerName.setText(promosList.get(position).getPromoTitleAr());

            SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
            Date dateObj = null;
            try {
                dateObj = curFormater.parse(promosList.get(position).getEndDate().replace("  ", " 0"));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
            String date = postFormater.format(dateObj);

            holder.validityDate.setText("صالحة حتى\n"+date);

            if (promosList.get(position).getPromoType().equalsIgnoreCase("2") ||
                    promosList.get(position).getPromoType().equalsIgnoreCase("3") ||
                    promosList.get(position).getPromoType().equalsIgnoreCase("6")) {

                if (promosList.get(position).getPromoType().equalsIgnoreCase("6")) {
                    if (promosList.get(position).getRemainingBonus().equalsIgnoreCase("0") || promosList.get(position).getRemainingBonus().equalsIgnoreCase("0.00")) {
                        holder.remainingBonus.setText("المشروبات المجانية المتبقية 1");
                        holder.qty.setVisibility(View.VISIBLE);
                        holder.qtyText.setVisibility(View.VISIBLE);
                    } else {
                        holder.qty.setVisibility(View.INVISIBLE);
                        holder.qtyText.setVisibility(View.INVISIBLE);
                        holder.remainingBonus.setText("المبلغ المتبقي " + promosList.get(position).getRemainingBonus() + " SR");
                    }
                } else {
                    holder.qty.setVisibility(View.INVISIBLE);
                    holder.qtyText.setVisibility(View.INVISIBLE);
                    holder.remainingBonus.setText("المبلغ المتبقي " + promosList.get(position).getRemainingBonus() + " SR");
                }
            } else if (promosList.get(position).getPromoType().equalsIgnoreCase("1") ||
                    promosList.get(position).getPromoType().equalsIgnoreCase("5") ||
                    promosList.get(position).getPromoType().equalsIgnoreCase("7")) {
                holder.qty.setVisibility(View.VISIBLE);
                holder.qtyText.setVisibility(View.VISIBLE);
                float remBonus = Float.parseFloat(promosList.get(position).getRemainingBonus());
                holder.remainingBonus.setText("المشروبات المجانية المتبقية " + (int) remBonus);
            }
        }

//        holder.layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                holder.radioButton.setImageResource(R.drawable.promo_selected);
//            }
//        });
//        if(language.equalsIgnoreCase("En")) {
//            if(promosList.get(position).getPromoType().equals("1")){
//                holder.offerDesc.setText(promosList.get(position).getDescription());
//            }else if(promosList.get(position).getPromoType().equals("2")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("3")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("4")){
//                holder.offerDesc.setText(promosList.get(position).getDescription());
//            }else if(promosList.get(position).getPromoType().equals("5")){
//                holder.offerDesc.setText(promosList.get(position).getDescription());
//            }else if(promosList.get(position).getPromoType().equals("7")){
//            holder.offerDesc.setText(promosList.get(position).getDescription());
//            }
//            else if(promosList.get(position).getPromoType().equals("6")){
//                if(!promosList.get(position).getRemainingBonus().equals("0")) {
//                    holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//                }else{
//                    holder.offerDesc.setText("Free Drink");
//                }
//            }
//            holder.offerName.setText(promosList.get(position).getPromoTitle());
//        }else if(language.equalsIgnoreCase("Ar")){
//            if(promosList.get(position).getPromoType().equals("1")){
//                holder.offerDesc.setText(promosList.get(position).getDescriptionAr());
//            }else if(promosList.get(position).getPromoType().equals("2")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("3")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("4")){
//                holder.offerDesc.setText(promosList.get(position).getDescriptionAr());
//            }else if(promosList.get(position).getPromoType().equals("5")){
//                holder.offerDesc.setText(promosList.get(position).getDescriptionAr());
//            }else if(promosList.get(position).getPromoType().equals("7")){
//                holder.offerDesc.setText(promosList.get(position).getDescriptionAr());
//            }else if(promosList.get(position).getPromoType().equals("6")){
//                if(!promosList.get(position).getRemainingBonus().equals("0")) {
//                    holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//                }else{
//                    holder.offerDesc.setText("Free Drink");
//                }
//            }
//            holder.offerName.setText(promosList.get(position).getPromoTitleAr());
//        }



        return convertView;
    }

}
