package com.cs.drcafe.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.SubCategories;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.cs.drcafe.FreeGiftSelection.drinksSelected;
import static com.cs.drcafe.FreeGiftSelection.selectedGiftsCount;

/**
 * Created by SKT on 20-02-2016.
 */
public class FreeGiftsItemsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<SubCategories> itemsList = new ArrayList<>();
    int pos;
    Boolean isSingleItem;
    String Sizes;
    int drinksAvailble;

    public FreeGiftsItemsAdapter(Context context, ArrayList<SubCategories> itemsList, Boolean isSingleItem, String Sizes,
                                 int drinksAvailble) {
        this.context = context;
        this.itemsList = itemsList;
        this.Sizes = Sizes;
        this.drinksAvailble = drinksAvailble;
        this.isSingleItem = isSingleItem;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return itemsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView itemName, itemQty;
        ImageView itemImage, itemPlusBtn, itemMinusBtn, checkBox;
        LinearLayout qtyLayout, mainLayout, itemLayout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.free_gifts_selection_list, null);

            holder.itemName = (TextView) convertView
                    .findViewById(R.id.itemName);
            holder.itemQty = (TextView) convertView
                    .findViewById(R.id.itemQty);

            holder.itemPlusBtn = (ImageView) convertView.findViewById(R.id.itemPlusBtn);
            holder.itemMinusBtn = (ImageView) convertView.findViewById(R.id.itemMinusBtn);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.itemImage);
            holder.qtyLayout = (LinearLayout) convertView.findViewById(R.id.qtyLayout);
            holder.mainLayout = (LinearLayout) convertView.findViewById(R.id.mainLayout);
            holder.itemLayout = (LinearLayout) convertView.findViewById(R.id.itemLayout);
            holder.checkBox = (ImageView) convertView.findViewById(R.id.checkBox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.itemName.setText(itemsList.get(position).getItemName());

        if(selectedGiftsCount.size() != 0){
            int count = 0;
            for (String str:
                    selectedGiftsCount) {
                if(str.equalsIgnoreCase(itemsList.get(position).getItemName())){
                    count = count + 1;
                }

                if (count > 0){
                    holder.checkBox.setImageDrawable(context.getResources().getDrawable(R.drawable.payment_selected));
                    holder.itemQty.setText(""+count);
                }
                else{
                    holder.checkBox.setImageDrawable(context.getResources().getDrawable(R.drawable.payment_normal));
                    holder.itemQty.setText("0");
                }
            }
        }
        else{
            holder.checkBox.setImageDrawable(context.getResources().getDrawable(R.drawable.payment_normal));
            holder.itemQty.setText("0");
        }

        if(selectedGiftsCount.contains(itemsList.get(position).getItemName())){
            holder.checkBox.setImageDrawable(context.getResources().getDrawable(R.drawable.payment_selected));
            holder.itemMinusBtn.setClickable(true);
            holder.itemMinusBtn.setFocusableInTouchMode(true);
            holder.itemMinusBtn.setFocusable(true);

            if(holder.itemQty.getText().toString().equalsIgnoreCase("0")){
                holder.itemMinusBtn.setAlpha(0.3f);
                holder.itemPlusBtn.setAlpha(1.0f);
            }
            else {
                holder.itemMinusBtn.setAlpha(1.0f);
                holder.itemPlusBtn.setAlpha(1.0f);
            }
        }
        else{
            holder.checkBox.setImageDrawable(context.getResources().getDrawable(R.drawable.payment_normal));
            holder.itemMinusBtn.setAlpha(0.3f);
            holder.itemPlusBtn.setAlpha(0.3f);
            holder.itemMinusBtn.setClickable(false);
            holder.itemMinusBtn.setFocusableInTouchMode(false);
            holder.itemMinusBtn.setFocusable(false);
        }

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(selectedGiftsCount.contains(itemsList.get(position).getItemName())) {
                        for (int i = 0 ; i < selectedGiftsCount.size(); i ++) {
                            if(selectedGiftsCount.get(i).equalsIgnoreCase(itemsList.get(position).getItemName())) {
                                Log.i("TAG","selectedGiftsCount.size() "+selectedGiftsCount.size());
                                selectedGiftsCount.remove(i);
                                drinksSelected = drinksSelected - 1;
                                Log.i("TAG","drinksSelected "+selectedGiftsCount.size());
                            }
                        }
                        notifyDataSetChanged();
                    }
                    else {
                        if (drinksSelected < drinksAvailble) {
                            selectedGiftsCount.add(itemsList.get(position).getItemName());
                            drinksSelected = drinksSelected + 1;
                            notifyDataSetChanged();
                        } else {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    context);

//                    if (language.equalsIgnoreCase("En")) {
                            // set title
                            alertDialogBuilder.setTitle("dr.CAFE");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("You can select only " + drinksAvailble + " Free drinks.")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
//                    } else if (language.equalsIgnoreCase("Ar")) {
                            // set title
//                        alertDialogBuilder.setTitle("د. كيف");

                            // set dialog message
//                        alertDialogBuilder
//                                .setMessage("Promotion not applicable this store.")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        }
                    }

            }
        });

        holder.itemPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedGiftsCount.contains(itemsList.get(position).getItemName())) {
                    if (drinksSelected < drinksAvailble) {
                        selectedGiftsCount.add(itemsList.get(position).getItemName());
                        drinksSelected = drinksSelected + 1;
                        notifyDataSetChanged();
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                context);

//                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("dr.CAFE");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You can select only " + drinksAvailble + " Free drinks.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
//                        alertDialogBuilder.setTitle("د. كيف");

                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("Promotion not applicable this store.")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
                }
            }
        });

        holder.itemMinusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedGiftsCount.contains(itemsList.get(position).getItemName())) {
                    if (Integer.parseInt(holder.itemQty.getText().toString()) > 0) {
                        drinksSelected = drinksSelected - 1;
                        selectedGiftsCount.remove(itemsList.get(position).getItemName());
                        notifyDataSetChanged();
                    }
                }
            }
        });


        try {
            // get input stream
            InputStream ims = context.getAssets().open(itemsList.get(position).getItemId() + ".png");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            holder.itemImage.setImageDrawable(d);
        } catch (IOException ex) {
        }

        return convertView;
    }
}