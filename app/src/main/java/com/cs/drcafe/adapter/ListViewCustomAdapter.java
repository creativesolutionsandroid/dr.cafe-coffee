package com.cs.drcafe.adapter;

import java.util.ArrayList;
 
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.Countries;

public class ListViewCustomAdapter extends BaseAdapter{
 
    ArrayList<Object> itemList;
 
    public Activity context;
    public LayoutInflater inflater;
 
    public ListViewCustomAdapter(Activity context,ArrayList<Object> itemList) {
        super();
 
        this.context = context;
        this.itemList = itemList;
 
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return itemList.size();
    }
 
    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return itemList.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
 
    public static class ViewHolder
    {
        ImageView imgViewLogo;
        TextView txtViewTitle;
        TextView txtViewDescription;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
 
        ViewHolder holder;
        if(convertView==null)
        {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.countries_list_item, null);
 
            holder.imgViewLogo = (ImageView) convertView.findViewById(R.id.country_flag);
            holder.txtViewTitle = (TextView) convertView.findViewById(R.id.country_name);
 
            convertView.setTag(holder);
        }
        else
            holder=(ViewHolder)convertView.getTag();
 
        Countries bean = (Countries) itemList.get(position);
 
        holder.imgViewLogo.setImageResource(bean.getCountryFlag());
        holder.txtViewTitle.setText(bean.getCountryName());
 
        return convertView;
    }
 
}