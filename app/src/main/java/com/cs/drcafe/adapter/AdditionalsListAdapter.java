package com.cs.drcafe.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.R;
import com.cs.drcafe.fragments.OrderPriceFragment;
import com.cs.drcafe.model.Additionals;
import com.cs.drcafe.model.Modifiers;
import com.cs.drcafe.model.SelectedAddtionals;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by SKT on 31-01-2016.
 */
public class AdditionalsListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Modifiers> groups;
    private ArrayList<Additionals> nutritionList;
    private ArrayList<String> additionalsStrList = new ArrayList<>();
    public static ArrayList<String> additionalsIdList = new ArrayList<>();
    private ArrayList<String> additionalsPriceList = new ArrayList<>();
    public static ArrayList<SelectedAddtionals> additionalsList = new ArrayList<>();
    List<SelectedAddtionals> thingsToBeAdd = new ArrayList<>();
    ArrayList<Additionals> addList = new ArrayList<>();
    TextView price;
    TextView count;
    String addStr, modStr, modifiersId;
    int previousId;
    boolean isAlreadyAdded = false;
    public static boolean isMilkSelected = false, isCoffeeSelected = false, isFlavourSelected = false, isAdded = false, isCappuccinoSelected = true;
    private ExpandableListView listView;

    public AdditionalsListAdapter(Context context, ArrayList<Modifiers> groups, String modifersId, ExpandableListView listView) {
        this.context = context;
        this.groups = groups;
        this.modifiersId = modifersId;
        this.listView = listView;
        isMilkSelected = false;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<Additionals> chList = groups.get(groupPosition).getChildItems();

        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Additionals child = (Additionals) getChild(groupPosition, childPosition);
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
//            LayoutInflater infalInflater = (LayoutInflater) context
//                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.additional_child_item, null);
        }
        final DecimalFormat format = new DecimalFormat("0.00");
        TextView price = (TextView) convertView.findViewById(R.id.additional_child_price);
        TextView checkBoxText = (TextView) convertView.findViewById(R.id.additional_checkbox_text);
        final CheckBox childItem = (CheckBox) convertView.findViewById(R.id.checkBox);
        RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.additional_child_layout);
//        childItem.setId(Integer.parseInt(child.getAdditionalsId()));
//        childItem.setTextColor(Color.parseColor("#000000"));
//        rl.addView(childItem);
//
//        if(childItem.getId() == previousId){
//            childItem.setChecked(true);
//        }else{
//            childItem.setChecked(false);
//        }

//        if(additionalsList.size()>0) {
//
//            for (SelectedAddtionals sa : additionalsList) {
//                Log.i("TAG", "value"+ sa.getAdditionalId());
//                if (child.getAdditionalsId().equals(sa.getAdditionalId())) {
//                    Log.i("TAG", "if");
//                    childItem.setChecked(true);
//                } else {
//                    Log.i("TAG", "else");
//                    childItem.setChecked(false);
//                }
//            }
//        }

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!childItem.isChecked()) {
                    isAlreadyAdded = false;

                    isCoffeeSelected = true;
                    isFlavourSelected = true;
                    if(child.getModifierId().equals("1") || child.getModifierId().equals("9")
                            || child.getModifierId().equals("10")){
                        isMilkSelected = true;
                    }
                    if(child.getModifierId().equals("12")){
                        isCappuccinoSelected = true;
                    }
                    if (additionalsList.size() > 0) {
                        for(Iterator<SelectedAddtionals> it = additionalsList.iterator(); it.hasNext();) {
                            SelectedAddtionals sa1 = it.next();
                            Log.i("TAG","mod"+sa1.getModifierId()+"  "+sa1.getAdditionalId());
                            if (child.getModifierId().equals(sa1.getModifierId())) {
                                addStr = sa1.getAdditionalId();
                                modStr = sa1.getModifierId();
//                                additionalsList.remove(sa1);
                                thingsToBeAdd.remove(sa1);
                                float delPrice = Float.parseFloat(sa1.getPrice());
                                delPrice = delPrice * OrderPriceFragment.priceRatio;
                                OrderPriceFragment.price = OrderPriceFragment.price - delPrice;
                                Log.i("TAG","price 1 "+OrderPriceFragment.price);
                                OrderPriceFragment.finalPrice = OrderPriceFragment.finalPrice-(delPrice*OrderPriceFragment.quantity);
                                OrderPriceFragment.itemPrice.setText(format.format(OrderPriceFragment.finalPrice) + " SR");
                                additionalsIdList.remove(sa1.getAdditionalId());
                                if (!child.getAdditionalsId().equals(sa1.getAdditionalId())) {
                                    if(!isAlreadyAdded) {
                                        isAlreadyAdded = true;
                                        additionalsIdList.add(child.getAdditionalsId());

                                        float price = Float.parseFloat(child.getAdditionalPrice());
                                        price = price * OrderPriceFragment.priceRatio;
                                        OrderPriceFragment.price = OrderPriceFragment.price + price;
                                        Log.i("TAG","price 2 "+OrderPriceFragment.price);
                                        OrderPriceFragment.finalPrice = OrderPriceFragment.finalPrice+(price*OrderPriceFragment.quantity);
                                        OrderPriceFragment.itemPrice.setText(format.format(OrderPriceFragment.finalPrice) + " SR");

                                        SelectedAddtionals sa = new SelectedAddtionals();
                                        sa.setAdditionalId(child.getAdditionalsId());
                                        sa.setModifierId(child.getModifierId());
                                        sa.setPrice(child.getAdditionalPrice());
                                        sa.setAdditionalName(child.getAdditionalName());
                                        sa.setPriceRatio(OrderPriceFragment.priceRatio);
                                        thingsToBeAdd.add(sa);
                                    }
                                } else {

                                    if(!isAlreadyAdded) {
                                        isAlreadyAdded = true;
                                        additionalsIdList.add(child.getAdditionalsId());

                                        float price = Float.parseFloat(child.getAdditionalPrice());
                                        price = price * OrderPriceFragment.priceRatio;
                                        OrderPriceFragment.price = OrderPriceFragment.price + price;
                                        Log.i("TAG","price 3 "+OrderPriceFragment.price);
                                        OrderPriceFragment.finalPrice = OrderPriceFragment.finalPrice+(price*OrderPriceFragment.quantity);
                                        OrderPriceFragment.itemPrice.setText(format.format(OrderPriceFragment.finalPrice) + " SR");

                                        SelectedAddtionals sa = new SelectedAddtionals();
                                        sa.setAdditionalId(child.getAdditionalsId());
                                        sa.setModifierId(child.getModifierId());
                                        sa.setPrice(child.getAdditionalPrice());
                                        sa.setAdditionalName(child.getAdditionalName());
                                        sa.setPriceRatio(OrderPriceFragment.priceRatio);
                                        thingsToBeAdd.add(sa);
                                    }
                                }
                            } else {
                                if(!isAlreadyAdded) {
                                    isAlreadyAdded = true;
                                    additionalsIdList.add(child.getAdditionalsId());

                                    float price = Float.parseFloat(child.getAdditionalPrice());
                                    price = price * OrderPriceFragment.priceRatio;
                                    OrderPriceFragment.price = OrderPriceFragment.price + price;
                                    Log.i("TAG","price 4 "+OrderPriceFragment.price);
                                    OrderPriceFragment.finalPrice = OrderPriceFragment.finalPrice+(price*OrderPriceFragment.quantity);
                                    OrderPriceFragment.itemPrice.setText(format.format(OrderPriceFragment.finalPrice) + " SR");

                                    SelectedAddtionals sa = new SelectedAddtionals();
                                    sa.setAdditionalId(child.getAdditionalsId());
                                    sa.setModifierId(child.getModifierId());
                                    sa.setPrice(child.getAdditionalPrice());
                                    sa.setAdditionalName(child.getAdditionalName());
                                    sa.setPriceRatio(OrderPriceFragment.priceRatio);
                                    thingsToBeAdd.add(sa);

                                }
                            }
                        }
                    } else {
                        Log.i("TAG", "mod" + child.getModifierId() + "  " + child.getAdditionalsId());
                        if(!isAlreadyAdded) {
                            isAlreadyAdded = true;
                            additionalsIdList.add(child.getAdditionalsId());

                            float price = Float.parseFloat(child.getAdditionalPrice());
                            price = price * OrderPriceFragment.priceRatio;
                            OrderPriceFragment.price = OrderPriceFragment.price + price;
                            Log.i("TAG","price 5 "+OrderPriceFragment.price);
                            OrderPriceFragment.finalPrice = OrderPriceFragment.finalPrice+(price*OrderPriceFragment.quantity);
                            OrderPriceFragment.itemPrice.setText(format.format(OrderPriceFragment.finalPrice) + " SR");

                            SelectedAddtionals sa = new SelectedAddtionals();
                            sa.setAdditionalId(child.getAdditionalsId());
                            sa.setModifierId(child.getModifierId());
                            sa.setPrice(child.getAdditionalPrice());
                            sa.setAdditionalName(child.getAdditionalName());
                            sa.setPriceRatio(OrderPriceFragment.priceRatio);
                            thingsToBeAdd.add(sa);
                        }
                    }
                    additionalsList.clear();
                    additionalsList.addAll(thingsToBeAdd);
//                    thingsToBeAdd.clear();
                    notifyDataSetChanged();
//                    }

                } else {

                    if (modifiersId.equals("1") && child.getModifierId().equals("1")
                            || modifiersId.equals("9") && child.getModifierId().equals("9")
                            || modifiersId.equals("10") && child.getModifierId().equals("10")
                            || modifiersId.equals("12") && child.getModifierId().equals("12")
                            || modifiersId.equals("1,12") && child.getModifierId().equals("12")) {
//                        additionalsIdList.add(child.getAdditionalsId());
//
//                        float price = Float.parseFloat(child.getAdditionalPrice());
//                        price = price * OrderPriceFragment.priceRatio;
//                        OrderPriceFragment.price = OrderPriceFragment.price + price;
//                        Log.i("TAG", "price 6 " + OrderPriceFragment.price);
//
//                        SelectedAddtionals sa = new SelectedAddtionals();
//                        sa.setAdditionalId(child.getAdditionalsId());
//                        sa.setModifierId(child.getModifierId());
//                        sa.setPrice(child.getAdditionalPrice());
//                        sa.setAdditionalName(child.getAdditionalName());
//                        sa.setPriceRatio(OrderPriceFragment.priceRatio);
//                        thingsToBeAdd.add(sa);
//                    }
                    } else {
                    for (Iterator<SelectedAddtionals> it = additionalsList.iterator(); it.hasNext(); ) {
                        SelectedAddtionals sa1 = it.next();
                        if (child.getAdditionalsId().equals(sa1.getAdditionalId())) {
                            thingsToBeAdd.remove(sa1);
                            additionalsIdList.remove(child.getAdditionalsId());
                            float delPrice = Float.parseFloat(sa1.getPrice());
                            delPrice = delPrice * OrderPriceFragment.priceRatio;
                            OrderPriceFragment.price = OrderPriceFragment.price - delPrice;
                            Log.i("TAG", "price 7 " + OrderPriceFragment.price);
                            OrderPriceFragment.finalPrice = OrderPriceFragment.finalPrice - (delPrice * OrderPriceFragment.quantity);
                            OrderPriceFragment.itemPrice.setText(format.format(OrderPriceFragment.finalPrice) + " SR");
                        }
                    }
                    }
                    additionalsList.clear();
                    additionalsList.addAll(thingsToBeAdd);
                    notifyDataSetChanged();
                }
            }
        });

//        Log.i("TAG", "" + additionalsIdList.toString() + "  " + child.getAdditionalsId());
        if(additionalsIdList.contains(child.getAdditionalsId())){
            childItem.setChecked(true);
        }else{
            childItem.setChecked(false);
        }

        float price12 = Float.parseFloat(child.getAdditionalPrice());
        price12 = price12 * OrderPriceFragment.priceRatio;

        checkBoxText.setText(child.getAdditionalName());
//        price.setText(price12+" SR");
        if(price12 > 0) {
            price.setVisibility(View.VISIBLE);
            price.setText(DrConstants.format.format(price12));
        }
        else{
            price.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Additionals> chList = groups.get(groupPosition).getChildItems();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Modifiers group = (Modifiers) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.additionals_group_item, null);
        }

        TextView tv = (TextView) convertView.findViewById(R.id.additional_group_name);
        TextView price = (TextView) convertView.findViewById(R.id.additional_price);
        ImageView iv = (ImageView) convertView.findViewById(R.id.group_indicator);
        ImageView groupIcon = (ImageView) convertView.findViewById(R.id.additional_group_icon);
        tv.setText(group.getModifierName());

        try {
            // get input stream
            InputStream ims = context.getAssets().open(group.getImages().toLowerCase());
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            groupIcon.setImageDrawable(d);
        }
        catch(IOException ex) {
        }

        if (isExpanded) {
            iv.setImageResource(R.drawable.group_arrow_selected);
        } else {
            iv.setImageResource(R.drawable.group_arrow);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    Animation.AnimationListener animL = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            //this is just a method call you can create to delete the animated view or hide it until you need it again.
//            clearAnimation();
        }
    };

}