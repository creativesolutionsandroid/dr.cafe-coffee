package com.cs.drcafe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.R;

import java.util.ArrayList;

/**
 * Created by SKT on 27-12-2015.
 */
public class CardsAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<String> cardsList = new ArrayList<>();
    int pos;
    String id;
    //public ImageLoader imageLoader;

    public CardsAdapter(Context context, ArrayList<String> cardsList) {
        this.context = context;
        this.cardsList = cardsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return cardsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView totalBalance, cardsNumber;
        ImageView cardChecked;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.card_list_item, null);

            holder.totalBalance = (TextView) convertView
                    .findViewById(R.id.card_balance);
            holder.cardsNumber = (TextView) convertView
                    .findViewById(R.id.mycard_number);
            holder.cardChecked = (ImageView) convertView.findViewById(R.id.card_check);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String[] cards = cardsList.get(position).split(":");
        if(cards[1].equals(DrConstants.LATEST_CARD)){
            holder.cardChecked.setVisibility(View.VISIBLE);
        }else{
            holder.cardChecked.setVisibility(View.GONE);
        }

        holder.totalBalance.setText(cards[0]);
        holder.cardsNumber.setText("My card ("+cards[1]+")");



        return convertView;
    }
}
