package com.cs.drcafe.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.drcafe.FreeGiftSelection;
import com.cs.drcafe.R;
import com.cs.drcafe.model.FreeDrinkSizes;

import java.util.ArrayList;

/**
 * Created by CS on 27-03-2018.
 */

public class FreeDrinkSizeAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<FreeDrinkSizes> drinkSizes;
    public LayoutInflater inflater;

    public FreeDrinkSizeAdapter(Context mContext, ArrayList<FreeDrinkSizes> drinkSizes){
        this.mContext = mContext;
        this.drinkSizes = drinkSizes;
        this.inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return drinkSizes.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView sizeText;
        LinearLayout mainLayout;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.list_free_drink_size,  null);

            holder.sizeText = (TextView) convertView.findViewById(R.id.size_text);
            holder.mainLayout = (LinearLayout) convertView.findViewById(R.id.mainLayout);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.sizeText.setText(drinkSizes.get(position).getSizeName());

        if(FreeGiftSelection.drinkSizeSelected.equalsIgnoreCase(drinkSizes.get(position).getId())){
            holder.mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.free_drink_size_selected_shape));
            holder.sizeText.setTextColor(Color.parseColor("#FFFFFF"));
        }
        else{
            holder.mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.free_drink_size_unselected_shape));
            holder.sizeText.setTextColor(Color.parseColor("#000000"));
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FreeGiftSelection.drinkSizeSelected = drinkSizes.get(position).getId();
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
