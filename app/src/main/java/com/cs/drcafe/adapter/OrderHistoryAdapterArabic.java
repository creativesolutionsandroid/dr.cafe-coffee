package com.cs.drcafe.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.OrderHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SKT on 20-02-2016.
 */
public class OrderHistoryAdapterArabic extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderHistory> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    //public ImageLoader imageLoader;

    public OrderHistoryAdapterArabic(Context context, ArrayList<OrderHistory> favOrderList) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView orderNumber, storeName,orderDate,totalPrice;
        ImageView progress;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.order_history_listitem_arabic, null);

            holder.orderNumber = (TextView) convertView
                    .findViewById(R.id.order_number);
            holder.storeName = (TextView) convertView
                    .findViewById(R.id.store_name);
            holder.orderDate = (TextView) convertView
                    .findViewById(R.id.order_date);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.favorite_price);
            holder.progress = (ImageView) convertView.findViewById(R.id.order_step);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.orderNumber.setText("رقم الطلب : "+favOrderList.get(position).getInvoiceNo());
        if (!favOrderList.get(position).getOrderType().equalsIgnoreCase("Delivery")) {
            holder.storeName.setText(favOrderList.get(position).getStoreName_ar());
        }
        else {
            holder.storeName.setText(favOrderList.get(position).getAddress());
        }
        holder.totalPrice.setText(favOrderList.get(position).getTotalPrice()+" SR");
        Log.i("DATE TAG", "" + favOrderList.get(position).getOrderDate());
        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(" ");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getOrderDate().replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);


        holder.orderDate.setText(date);
        if (!favOrderList.get(position).getOrderType().equalsIgnoreCase("Delivery")) {
            if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                holder.progress.setImageResource(R.drawable.order_history_served_arabic);
            } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                holder.progress.setImageResource(R.drawable.order_history_accept_arabic);
            } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                holder.progress.setImageResource(R.drawable.order_history_ready_arabic);
            } else if (favOrderList.get(position).getOrderStatus().equals("Rejected")) {
                holder.progress.setImageResource(R.drawable.order_history_reject_arabic);
            } else {
                holder.progress.setImageResource(R.drawable.order_history_order_arabic);
            }
        }
        else {
            if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                holder.progress.setImageResource(R.drawable.order_history_served_delivery_ar);
            } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                holder.progress.setImageResource(R.drawable.order_history_accept_delivery_ar);
            } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                holder.progress.setImageResource(R.drawable.order_history_ready_delivery_ar);
            } else if (favOrderList.get(position).getOrderStatus().equals("Rejected")) {
                holder.progress.setImageResource(R.drawable.order_history_reject_delivery_ar);
            } else {
                holder.progress.setImageResource(R.drawable.order_history_order_delivery_ar);
            }
        }
//
//        holder.totalBalance.setText(String.format("%.2f", Float.parseFloat(storesList.get(position).getTotalBalance())));


        return convertView;
    }
}