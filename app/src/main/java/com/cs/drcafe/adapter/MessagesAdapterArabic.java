package com.cs.drcafe.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.Messages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SKT on 22-02-2016.
 */
public class MessagesAdapterArabic extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Messages> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    //public ImageLoader imageLoader;

    public MessagesAdapterArabic(Context context, ArrayList<Messages> favOrderList) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView status, date,desc;
        ImageView imageIcon;
        RelativeLayout itemLayout;
        View dot;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.messages_listitem_arabic, null);

            holder.status = (TextView) convertView
                    .findViewById(R.id.status);
            holder.date = (TextView) convertView
                    .findViewById(R.id.date);
            holder.desc = (TextView) convertView
                    .findViewById(R.id.desc);
            holder.imageIcon = (ImageView) convertView
                    .findViewById(R.id.item_icon);
            holder.itemLayout = (RelativeLayout) convertView.findViewById(R.id.message_item_layout);
            holder.dot =  convertView.findViewById(R.id.dot);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        holder.status.setText(favOrderList.get(position).getFavoriteName());
        holder.desc.setText(favOrderList.get(position).getPushMessage());
//
        if(position % 2 == 0){
            holder.itemLayout.setBackgroundColor(Color.parseColor("#F2EFE1"));
        }else{
            holder.itemLayout.setBackgroundColor(Color.parseColor("#EDEADC"));
        }
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date dateObj = null;
        String[] parts = favOrderList.get(position).getSentDate().split(" ");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getSentDate().replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.date.setText(date);
        if(favOrderList.get(position).getPushNotificationType().equals("1")){
            holder.status.setText("تم قبوله");
            holder.imageIcon.setImageResource(R.drawable.notification1);
        }else if(favOrderList.get(position).getPushNotificationType().equals("2")){
            holder.status.setText("دأ تحضيره");
            holder.imageIcon.setImageResource(R.drawable.notification2);
        }else if(favOrderList.get(position).getPushNotificationType().equals("3")){
            holder.status.setText("تم استلامه");
            holder.imageIcon.setImageResource(R.drawable.notification3);
        }else if(favOrderList.get(position).getPushNotificationType().equals("4")){
            holder.status.setText("رفض");
            holder.imageIcon.setImageResource(R.drawable.notification4);
        }else if(favOrderList.get(position).getPushNotificationType().equals("5")){
            holder.status.setText("عروض");
            holder.imageIcon.setImageResource(R.drawable.notification5);
        }else if(favOrderList.get(position).getPushNotificationType().equals("6")){
            holder.status.setText(" عرض خاص");
            holder.imageIcon.setImageResource(R.drawable.notification5);
        }else if(favOrderList.get(position).getPushNotificationType().equals("7")){
            holder.status.setText("المحفظة");
            holder.imageIcon.setImageResource(R.drawable.notification7);
        }else if(favOrderList.get(position).getPushNotificationType().equals("8")){
            holder.status.setText("تحياتنا");
            holder.imageIcon.setImageResource(R.drawable.notification8);
        }else if(favOrderList.get(position).getPushNotificationType().equals("9")){
            holder.status.setText("المعلومات");
            holder.imageIcon.setImageResource(R.drawable.notification9);
        }else if(favOrderList.get(position).getPushNotificationType().equals("10")){
            holder.status.setText("مكان الطلب");
            holder.imageIcon.setImageResource(R.drawable.notification10);
        }

        if(favOrderList.get(position).getIsRead().equals("true") && favOrderList.get(position).getPushNotificationType().equals("5")){
            holder.dot.setVisibility(View.VISIBLE);
        }else {
            holder.dot.setVisibility(View.INVISIBLE);
        }
//
//        holder.totalBalance.setText(String.format("%.2f", Float.parseFloat(storesList.get(position).getTotalBalance())));


        return convertView;
    }
}