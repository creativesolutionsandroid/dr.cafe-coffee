package com.cs.drcafe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.cs.drcafe.R;
import com.cs.drcafe.model.MyAddress;

import java.util.ArrayList;

/**
 * Created by CS on 20-06-2016.
 */
public class MyAddressAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<MyAddress> addressList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public MyAddressAdapter(Context context, ArrayList<MyAddress> addressList, String language) {
        this.context = context;
        this.addressList = addressList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return addressList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView address, addressName;
        ImageView addressType;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.list_my_address, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.list_my_address, null);
            }

            holder.addressName = (TextView) convertView
                    .findViewById(R.id.address_name);
            holder.address = (TextView) convertView
                    .findViewById(R.id.address_body);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.address.setText(addressList.get(position).getAddress());

        holder.addressName.setText("" + addressList.get(position).getHouseno());


        return convertView;
    }
}