package com.cs.drcafe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.R;
import com.cs.drcafe.model.FavouriteOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brahmam on 19/2/16.
 */
public class FavouriteOrderAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<FavouriteOrder> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    //public ImageLoader imageLoader;

    public FavouriteOrderAdapter(Context context, ArrayList<FavouriteOrder> favOrderList) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView favouriteName, storeName,orderDate,totalPrice;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.favourite_order_listitem, null);

            holder.favouriteName = (TextView) convertView
                    .findViewById(R.id.favorite_name);
            holder.storeName = (TextView) convertView
                    .findViewById(R.id.store_name);
            holder.orderDate = (TextView) convertView
                    .findViewById(R.id.order_date);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.favorite_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.favouriteName.setText(favOrderList.get(position).getFavoriteName());
        holder.storeName.setText(favOrderList.get(position).getStoreName());
        holder.totalPrice.setText(DrConstants.format.format(Float.parseFloat(favOrderList.get(position).getTotalPrice()))+" SR");

        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(" ");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getOrderDate().replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.orderDate.setText(date);
//
//        holder.totalBalance.setText(String.format("%.2f", Float.parseFloat(storesList.get(position).getTotalBalance())));


        return convertView;
    }
}