package com.cs.drcafe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.MyGifts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SKT on 20-02-2016.
 */
public class MyGiftsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    String language;
    ArrayList<MyGifts> myGiftsList = new ArrayList<>();
    int pos;
    String id;
    //public ImageLoader imageLoader;

    public MyGiftsAdapter(Context context, ArrayList<MyGifts> myGiftsList, String language) {
        this.context = context;
        this.myGiftsList = myGiftsList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return myGiftsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView title, description, endDate;
        ImageView imageView;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.mygifts_list, null);
            }
            else{
                convertView = inflater.inflate(R.layout.mygifts_list_ar, null);
            }

            holder.title = (TextView) convertView
                    .findViewById(R.id.giftTitle);
            holder.description = (TextView) convertView
                    .findViewById(R.id.giftDescription);
            holder.endDate = (TextView) convertView
                    .findViewById(R.id.validityDate);
            holder.imageView = (ImageView) convertView.findViewById(R.id.giftImage);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(" ");
        try {
            dateObj = curFormater.parse(myGiftsList.get(position).getEndDate().replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.endDate.setText(date);
        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(myGiftsList.get(position).getDesc_En());
            if (myGiftsList.get(position).getPromoType().equalsIgnoreCase("2") ||
                    myGiftsList.get(position).getPromoType().equalsIgnoreCase("3") ||
                    myGiftsList.get(position).getPromoType().equalsIgnoreCase("6")) {

                if (myGiftsList.get(position).getPromoType().equalsIgnoreCase("6")) {
                    if (myGiftsList.get(position).getRemainingBonus().equalsIgnoreCase("0")) {
                        holder.description.setText("Remaining Free drinks 1");
                    } else {
                        holder.description.setText("Remaining amount " + myGiftsList.get(position).getRemainingBonus() + " SR");
                    }
                } else {
                    holder.description.setText("Remaining amount " + myGiftsList.get(position).getRemainingBonus() + " SR");
                }
            } else if (myGiftsList.get(position).getPromoType().equalsIgnoreCase("1") ||
                    myGiftsList.get(position).getPromoType().equalsIgnoreCase("5") ||
                    myGiftsList.get(position).getPromoType().equalsIgnoreCase("7")) {
                holder.description.setText("Remaining Free drinks " + myGiftsList.get(position).getRemainingBonus());
            }
        }
        else{
            holder.title.setText(myGiftsList.get(position).getDesc_Ar());
            if (myGiftsList.get(position).getPromoType().equalsIgnoreCase("2") ||
                    myGiftsList.get(position).getPromoType().equalsIgnoreCase("3") ||
                    myGiftsList.get(position).getPromoType().equalsIgnoreCase("6")) {

                if (myGiftsList.get(position).getPromoType().equalsIgnoreCase("6")) {
                    if (myGiftsList.get(position).getRemainingBonus().equalsIgnoreCase("0")) {
                        holder.description.setText("المشروبات المجانية المتبقية 1");
                    } else {
                        holder.description.setText("المبلغ المتبقي " + myGiftsList.get(position).getRemainingBonus() + " SR");
                    }
                } else {
                    holder.description.setText("المبلغ المتبقي " + myGiftsList.get(position).getRemainingBonus() + " SR");
                }
            } else if (myGiftsList.get(position).getPromoType().equalsIgnoreCase("1") ||
                    myGiftsList.get(position).getPromoType().equalsIgnoreCase("5") ||
                    myGiftsList.get(position).getPromoType().equalsIgnoreCase("7")) {
                holder.description.setText("المشروبات المجانية المتبقية " + myGiftsList.get(position).getRemainingBonus());
            }
        }

        return convertView;
    }
}