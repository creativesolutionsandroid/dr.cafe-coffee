package com.cs.drcafe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.model.WalletHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SKT on 20-02-2016.
 */
public class WalletHistoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<WalletHistory> favOrderList = new ArrayList<>();
    int pos;
    String id;

    public WalletHistoryAdapter(Context context, ArrayList<WalletHistory> favOrderList) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView orderNumber, action,orderDate,totalPrice, actionNote;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.wallet_history_listitem, null);

            holder.orderNumber = (TextView) convertView
                    .findViewById(R.id.order_number);
            holder.action = (TextView) convertView
                    .findViewById(R.id.action);
            holder.actionNote = (TextView) convertView
                    .findViewById(R.id.action_note);
            holder.orderDate = (TextView) convertView
                    .findViewById(R.id.order_date);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.favorite_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.orderNumber.setText("Order No : "+favOrderList.get(position).getInvoiceNo());
        holder.action.setText(favOrderList.get(position).getAction());
//        holder.actionNote.setText(favOrderList.get(position).getAction_note());
        holder.totalPrice.setText(favOrderList.get(position).getTotalPrice()+" SR");

        holder.actionNote.setVisibility(View.GONE);

        SimpleDateFormat curFormater = new SimpleDateFormat("MMM dd yyyy hh:mma", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(" ");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getOrderDate());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.orderDate.setText(date);

        return convertView;
    }
}