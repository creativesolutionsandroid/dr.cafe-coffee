package com.cs.drcafe.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.drcafe.DrConstants;
import com.cs.drcafe.R;
import com.cs.drcafe.model.Item;
import com.cs.drcafe.model.SectionItem;
import com.cs.drcafe.model.StoreInfo;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SKT on 19-02-2016.
 */
public class SelectStoreAdapter extends BaseAdapter{

    private String SOAP_ACTION = "http://tempuri.org/getCurrentTime";
    private String METHOD_NAME = "getCurrentTime";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapPrimitive result;
    private String response12 = null;

    public static final String inputFormat = "HH:mm";
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Item> storesList = new ArrayList<>();
    int pos;
    SharedPreferences locationPrefs;
    String mDistancePrefValue;
    AnimationDrawable animation;
    ImageView cupImg;
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);
    private Handler handler = new Handler();
    boolean killHandler = false;
    String serverTime;

    //public ImageLoader imageLoader;

    public SelectStoreAdapter(Context context, ArrayList<Item> storesList, String serverTime) {
        this.context = context;
        this.storesList = storesList;
        this.serverTime = serverTime;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        locationPrefs = context.getSharedPreferences("LOCATION_PREFS", Context.MODE_PRIVATE);
        mDistancePrefValue = locationPrefs.getString("distance", "kilometer");
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return storesList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView storeName,storeAddress,startTime,endTime, storeDistance;
        //Switch subscribe;
        ImageView cupImg, wifi, drive, family, patio, meeting, university, hospital, office, shopping, airport, dineIn, ladies;
    }

    public View getView(int position, View convertView1, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;

        View convertView = convertView1;

        final Item i = storesList.get(position);
        if (i != null) {
//            new GetCurrentTime().execute();
            if(i.isSection()){
                SectionItem si = (SectionItem)i;
                convertView = inflater.inflate(R.layout.list_header_item, null);

                convertView.setOnClickListener(null);
                convertView.setOnLongClickListener(null);
                convertView.setLongClickable(false);

                final TextView sectionView = (TextView) convertView.findViewById(R.id.header_title);
                sectionView.setText(si.getTitle());

            }else{
                StoreInfo si = (StoreInfo)i;
                convertView = inflater.inflate(R.layout.store_list_item, null);
                TextView storeName = (TextView) convertView
                        .findViewById(R.id.storelist_name);
                TextView storeAddress = (TextView) convertView
                        .findViewById(R.id.storelist_address);
                TextView startTimeTxt = (TextView) convertView
                        .findViewById(R.id.storelist_starttime);
                TextView endTimeTxt = (TextView) convertView
                        .findViewById(R.id.storelist_endtime);
                TextView storeDistance = (TextView) convertView
                        .findViewById(R.id.distance_store);
                TextView storeDistanceDisplay = (TextView) convertView
                        .findViewById(R.id.distance_display);
                cupImg = (ImageView) convertView
                        .findViewById(R.id.cup_image);
                ImageView wifi = (ImageView) convertView
                        .findViewById(R.id.wifi);
                ImageView drive = (ImageView) convertView
                        .findViewById(R.id.drive);
                ImageView family = (ImageView) convertView
                        .findViewById(R.id.family);
                ImageView patio = (ImageView) convertView
                        .findViewById(R.id.patio);
                ImageView meeting = (ImageView) convertView
                        .findViewById(R.id.meeting);
                ImageView university = (ImageView) convertView
                        .findViewById(R.id.university);
                ImageView hospital = (ImageView) convertView
                        .findViewById(R.id.hospital);
                ImageView office = (ImageView) convertView
                        .findViewById(R.id.office);
                ImageView shopping = (ImageView) convertView
                        .findViewById(R.id.shopping);
                ImageView airport = (ImageView) convertView
                        .findViewById(R.id.airport);
                ImageView dineIn = (ImageView) convertView
                        .findViewById(R.id.dinein);
                ImageView ladies = (ImageView) convertView
                        .findViewById(R.id.ladies);
                LinearLayout freeDeliveryLayout = (LinearLayout) convertView.
                        findViewById(R.id.free_delivery_layout);

                startAnimation();

                if (si.isDeliverable()) {
                    freeDeliveryLayout.setVisibility(View.VISIBLE);
                }
                else {
                    freeDeliveryLayout.setVisibility(View.GONE);
                }

//                if (title != null)
//                    title.setText(ei.title);
//                if(subtitle != null)
//                    subtitle.setText(ei.subtitle);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
//
//
//
//
//                try {
//                    Date serverTime = dateFormat.parse(response12);
//                    response12 = timeFormat.format(serverTime);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }


                String startTime = si.getStartTime().replace(" ","");
                String endTime = si.getEndTime().replace(" ","");

//                Date d1 = timeFormat1.format(startTime);

//                if(endTime.equals("00:00AM")){
//                    cupImg.setVisibility(View.VISIBLE);
//                }else if(endTime.equals("12:00AM")){
//                    endTime = "11:59PM";
//                }
//
//                String[] parts = endTime.replace("AM","").replace("PM","").split(":");
//                int endHour = Integer.parseInt(parts[0]);
//                int endMinute = Integer.parseInt(parts[1]);
//
//                if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 )))) {
//
//
//                }



                storeName.setText(si.getStoreName().replaceAll("^\\s+", ""));
                storeAddress.setText(si.getStoreAddress());

                if(mDistancePrefValue.equalsIgnoreCase("kilometer")){
                    storeDistanceDisplay.setText(" KM");
                }else if(mDistancePrefValue.equalsIgnoreCase("mile")){
                    storeDistanceDisplay.setText(" Mi");
                }

                storeDistance.setText(String.format("%.1f", si.getDistance()));


//        Log.i("Adapter Tag",""+position);

                if(si.isWifi()){
                    wifi.setVisibility(View.VISIBLE);
                }
                if(si.isDriveThru()){
                    drive.setVisibility(View.VISIBLE);
                }
                if(si.isFamilySection()){
                    family.setVisibility(View.VISIBLE);
                }
                if(si.isPatioSitting()){
                    patio.setVisibility(View.VISIBLE);
                }
                if(si.isMeetingSpace()){
                    meeting.setVisibility(View.VISIBLE);
                }
                if(si.isUniversity()){
                    university.setVisibility(View.VISIBLE);
                }
                if(si.isHospital()){
                    hospital.setVisibility(View.VISIBLE);
                }
                if(si.isOffice()){
                    office.setVisibility(View.VISIBLE);
                }
                if(si.isShoppingMall()){
                    shopping.setVisibility(View.VISIBLE);
                }
                if(si.isAirPort()){
                    airport.setVisibility(View.VISIBLE);
                }
                if(si.isDineIn()){
                    dineIn.setVisibility(View.VISIBLE);
                }
                if(si.isLadies()){
                    ladies.setVisibility(View.VISIBLE);
                }

                if(si.is24x7()){
                    startTimeTxt.setText("24/7");
                    endTimeTxt.setText("Hours");
                    endTimeTxt.setVisibility(View.VISIBLE);
                    cupImg.setVisibility(View.VISIBLE);
                    startAnimation();
                }else if(startTime.equals("anyType{}") && endTime.equals("anyType{}")){
                    startTimeTxt.setText("Closed");
                    endTimeTxt.setVisibility(View.GONE);
                    cupImg.setVisibility(View.INVISIBLE);

                } else {
                    endTimeTxt.setVisibility(View.VISIBLE);
                    startTimeTxt.setText(si.getStartTime());
                    endTimeTxt.setText(si.getEndTime());


                    if(endTime.equals("00:00AM")){
                        cupImg.setVisibility(View.VISIBLE);
                        return convertView;
                    }else if(endTime.equals("12:00AM")){
                        endTime = "11:59PM";
                    }

                    Calendar now = Calendar.getInstance();

                    int hour = now.get(Calendar.HOUR_OF_DAY);
                    int minute = now.get(Calendar.MINUTE);


                    Date serverDate = null;
                    Date end24Date = null;
                    Date start24Date = null;
                    Date current24Date = null;
                    Date dateToday = null;
                    Calendar dateStoreClose = Calendar.getInstance();
                    try {
                        end24Date = timeFormat.parse(endTime);
                        start24Date = timeFormat.parse(startTime);
                        serverDate = dateFormat.parse(serverTime);
                        dateToday = dateFormat1.parse(serverTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    dateStoreClose.setTime(dateToday);
                    dateStoreClose.add(Calendar.DATE, 1);
                    String current24 = timeFormat1.format(serverDate);
                    String end24 =timeFormat1.format(end24Date);
                    String start24 = timeFormat1.format(start24Date);
                    String startDateString = dateFormat1.format(dateToday);
                    String endDateString = dateFormat1.format(dateToday);
                    String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                    dateStoreClose.add(Calendar.DATE, -2);
                    String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

                    Date startDate = null;
                    Date endDate = null;

                    try {
                        end24Date = timeFormat1.parse(end24);
                        start24Date = timeFormat1.parse(start24);
                        current24Date = timeFormat1.parse(current24);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String[] parts2 = start24.split(":");
                    int startHour = Integer.parseInt(parts2[0]);
                    int startMinute = Integer.parseInt(parts2[1]);

                    String[] parts = end24.split(":");
                    int endHour = Integer.parseInt(parts[0]);
                    int endMinute = Integer.parseInt(parts[1]);

                    String[] parts1 = current24.split(":");
                    int currentHour = Integer.parseInt(parts1[0]);
                    int currentMinute = Integer.parseInt(parts1[1]);


                    if(startTime.contains("AM") && endTime.contains("AM")){
                        if(startHour < endHour){
                            startDateString = startDateString+ " "+ startTime;
                            endDateString = endDateString+"  " + endTime;
                            try {
                                startDate = dateFormat2.parse(startDateString);
                                endDate = dateFormat2.parse(endDateString);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }else if(startHour > endHour){
                            if(serverTime.contains("AM")){
                                if(currentHour > endHour){
                                    startDateString = startDateString + " " + startTime;
                                    endDateString = endDateTomorrow + "  " + endTime;
                                    try {
                                        startDate = dateFormat2.parse(startDateString);
                                        endDate = dateFormat2.parse(endDateString);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    startDateString = endDateYesterday + " " + startTime;
                                    endDateString = endDateString + "  " + endTime;
                                    try {
                                        startDate = dateFormat2.parse(startDateString);
                                        endDate = dateFormat2.parse(endDateString);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }else {
                                startDateString = startDateString + " " + startTime;
                                endDateString = endDateTomorrow + "  " + endTime;
                                try {
                                    startDate = dateFormat2.parse(startDateString);
                                    endDate = dateFormat2.parse(endDateString);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }else if(startTime.contains("AM") && endTime.contains("PM")){
                        startDateString = startDateString+ " "+ startTime;
                        endDateString = endDateString+"  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }else if(startTime.contains("PM") && endTime.contains("AM")){
                        if(serverTime.contains("AM")){
                            if(currentHour <= endHour){
                                startDateString = endDateYesterday+ " "+ startTime;
                                endDateString = endDateString+"  " + endTime;
                                try {
                                    startDate = dateFormat2.parse(startDateString);
                                    endDate = dateFormat2.parse(endDateString);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }else{
                                startDateString = startDateString+ " "+ startTime;
                                endDateString = endDateTomorrow+"  " + endTime;
                                try {
                                    startDate = dateFormat2.parse(startDateString);
                                    endDate = dateFormat2.parse(endDateString);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }else{
                            startDateString = startDateString+ " "+ startTime;
                            endDateString = endDateTomorrow+"  " + endTime;
                            try {
                                startDate = dateFormat2.parse(startDateString);
                                endDate = dateFormat2.parse(endDateString);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    }else if(startTime.contains("PM") && endTime.contains("PM")){
                        startDateString = startDateString+ " "+ startTime;
                        endDateString = endDateString+"  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }


                    String serverDateString = dateFormat2.format(serverDate);

                    try {
                        serverDate = dateFormat2.parse(serverDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Log.i("TAG DATE" , ""+ startDate);
                    Log.i("TAG DATE1" , ""+ endDate);
                    Log.i("TAG DATE2" , ""+ serverDate);

                    if(serverDate.after(startDate) && serverDate.before(endDate)){
                        Log.i("TAG Visible" , "true");
                        cupImg.setVisibility(View.VISIBLE);
                        return convertView;
                    }else{
                        cupImg.setVisibility(View.INVISIBLE);
                        return convertView;
                    }



//                    Log.i("DATE TAG", ""+ start24Date.toString()+"  "+ current24Date.toString()+ " ");
//
//                    if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
//                        if(current24Date.after(start24Date) && current24Date.before(end24Date) ){
//                            cupImg.setVisibility(View.VISIBLE);
//                            return convertView;
//                        }else{
//
//                            cupImg.setVisibility(View.INVISIBLE);
//                            return convertView;
//                        }
//
//                    }else{
//                        if (currentHour < endHour || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
//                            cupImg.setVisibility(View.VISIBLE);
//                            return convertView;
//                        }else{
//
//                            cupImg.setVisibility(View.INVISIBLE);
//                            return convertView;
//                        }
//                    }

                }



//                Date d1 = timeFormat1.format(startTime);




            }
        }



        return convertView;
    }


    class Starter implements Runnable {
        public void run() {
            animation.start();
        }
    }

    private void startAnimation(){
        animation = new AnimationDrawable();

        animation.addFrame(context.getResources().getDrawable(R.drawable.c01), 300);
        animation.addFrame(context.getResources().getDrawable(R.drawable.c02), 300);
        animation.addFrame(context.getResources().getDrawable(R.drawable.c03), 300);
        animation.addFrame(context.getResources().getDrawable(R.drawable.c04), 300);
        animation.addFrame(context.getResources().getDrawable(R.drawable.c05), 300);
        animation.addFrame(context.getResources().getDrawable(R.drawable.c06), 300);


        animation.setOneShot(false);


        cupImg.setImageDrawable(animation);

        cupImg.post(new Starter());
    }

    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }



    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;

        @Override
        protected void onPreExecute() {

//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

                // request.addProperty("", ""); // incase you need to pass
                // parameters to the web-service

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                androidHttpTransport.call(SOAP_ACTION, envelope);
                result = (SoapPrimitive) envelope.getResponse();
                response12 = result.toString();
                Log.v("TAG", response12);

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Responce", "" + response12);

            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(response12 == null){

            } else {

            }
//            handler.post(timedTask);
            super.onPostExecute(result1);
        }
    }


//    Runnable timedTask = new Runnable(){
//        @Override
//        public void run() {
//            if(!killHandler){
//                new GetCurrentTime().execute();
//                handler.sendEmptyMessage(0);
//                handler.postDelayed(timedTask, 1000*60);
//            }
//        }};



}