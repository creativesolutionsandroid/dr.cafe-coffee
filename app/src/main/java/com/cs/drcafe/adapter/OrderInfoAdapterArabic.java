package com.cs.drcafe.adapter;

import android.content.Context;
import android.database.SQLException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.R;
import com.cs.drcafe.fragments.CategoriesListFragment;
import com.cs.drcafe.fragments.CommentFragment;
import com.cs.drcafe.fragments.MenuFragment;
import com.cs.drcafe.fragments.OrderCheckoutFragment;
import com.cs.drcafe.model.Order;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by SKT on 14-02-2016.
 */
public class OrderInfoAdapterArabic extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty;
    float price;

    public OrderInfoAdapterArabic(Context context, ArrayList<Order> orderList) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        myDbHelper = new DataBaseHelper(context);
        try {

            myDbHelper.createDataBase();

        } catch (IOException ioe) {


        }

        try {

            myDbHelper.openDataBase();

        } catch (SQLException sqle) {

            throw sqle;

        }
    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title, price, qty, type, additionalsName;
        ImageView itemBackground, plusBtn, minusBtn, itemComment, itemIcon;
        //ImageButton plus;
        LinearLayout orderInfoLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();


            convertView = inflater.inflate(R.layout.order_checkout_item_arabic, null);

            holder.itemBackground = (ImageView) convertView.findViewById(R.id.item_background);
            holder.orderInfoLayout = (LinearLayout) convertView.findViewById(R.id.order_info_layout);
            //animate();
            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.price = (TextView) convertView.findViewById(R.id.item_price);
            holder.qty = (TextView) convertView.findViewById(R.id.item_qty);
            holder.plusBtn = (ImageView) convertView.findViewById(R.id.plus_btn);
            holder.minusBtn = (ImageView) convertView.findViewById(R.id.minus_btn);
            holder.type = (TextView) convertView.findViewById(R.id.item_type);
            holder.itemComment = (ImageView) convertView.findViewById(R.id.item_comment);
            holder.additionalsName = (TextView) convertView.findViewById(R.id.additionals_name);
            holder.itemIcon = (ImageView) convertView.findViewById(R.id.item_icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ViewTreeObserver vto = holder.itemBackground.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                holder.itemBackground.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = holder.itemBackground.getMeasuredHeight();
                Log.i("Height TAG", "" + finalHeight);
                LinearLayout.LayoutParams newsCountParam = new
                        LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                RelativeLayout.LayoutParams params = new
                        RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                holder.orderInfoLayout.setLayoutParams(params);
                return true;
            }
        });
        try {
            // get input stream
            InputStream ims = context.getAssets().open(orderList.get(position).getItemId() + ".png");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            holder.itemIcon.setImageDrawable(d);
        } catch (IOException ex) {
        }

        if(orderList.get(position).getItemId().equalsIgnoreCase(OrderCheckoutFragment.itemId)){
            holder.plusBtn.setEnabled(false);
            holder.plusBtn.setClickable(false);
        }else{
            holder.plusBtn.setEnabled(true);
            holder.plusBtn.setClickable(true);
        }

        holder.plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qty = Integer.parseInt(orderList.get(position).getQty()) + 1;

                price = (Float.parseFloat(orderList.get(position).getTotalAmount())) + (Float.parseFloat(orderList.get(position).getPrice()));
                myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getOrderId());
                orderList = myDbHelper.getOrderInfo();
                notifyDataSetChanged();
                try {
                    MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                    MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                    CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                final DecimalFormat priceFormat = new DecimalFormat("0.00");
                OrderCheckoutFragment.amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice()*(DrConstants.vat/100);
                float netAmount = (Float.parseFloat(priceFormat.format(myDbHelper.getTotalOrderPrice())) + (Float.parseFloat(priceFormat.format(tax))));
                float netAmountRounded = (float) Math.round(netAmount * 10)/10 ;                final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
                OrderCheckoutFragment.roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(priceFormat.format(netAmount))));
                OrderCheckoutFragment.vatAmount.setText(""+priceFormat.format(tax));
                OrderCheckoutFragment.netTotal.setText(""+priceFormat.format((netAmountRounded)));
                OrderCheckoutFragment.orderPrice.setText("" + priceFormat.format((netAmountRounded)));
                OrderCheckoutFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());

                ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
            }
        });

        holder.minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qty = Integer.parseInt(orderList.get(position).getQty()) - 1;
                if(qty==0){
                    myDbHelper.deleteItemFromOrder(orderList.get(position).getOrderId());
                }else {
                    price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - (Float.parseFloat(orderList.get(position).getPrice()));
                    myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getOrderId());
                }
                if(orderList.get(position).getItemId().equalsIgnoreCase(OrderCheckoutFragment.itemId)){
//                    OrderCheckoutFragment.offerBanner.setVisibility(View.VISIBLE);
                }
                orderList = myDbHelper.getOrderInfo();

                try {
                    MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                    MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                    CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                final DecimalFormat priceFormat = new DecimalFormat("0.00");
                OrderCheckoutFragment.amount.setText("" + priceFormat.format((float) myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice()*(DrConstants.vat/100);
                float netAmount = (Float.parseFloat(priceFormat.format(myDbHelper.getTotalOrderPrice())) + (Float.parseFloat(priceFormat.format(tax))));
                float netAmountRounded = (float) Math.round(netAmount * 10)/10 ;                final DecimalFormat signFormat = new DecimalFormat("+ 0.00;- 0.00");
                OrderCheckoutFragment.roundUp.setText(""+signFormat.format(netAmountRounded - Float.parseFloat(priceFormat.format(netAmount))));
                OrderCheckoutFragment.vatAmount.setText(""+priceFormat.format(tax));
                OrderCheckoutFragment.netTotal.setText(""+priceFormat.format((netAmountRounded)));
                OrderCheckoutFragment.orderPrice.setText("" + priceFormat.format((netAmountRounded)));
                OrderCheckoutFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
                notifyDataSetChanged();
            }
        });

        holder.itemComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderList = myDbHelper.getOrderInfo();
                Fragment nutritionFragment = new CommentFragment();
                Bundle mBundle11 = new Bundle();
                mBundle11.putString("title", orderList.get(position).getItemName());
                mBundle11.putString("itemId", orderList.get(position).getItemId());
                mBundle11.putString("orderId", orderList.get(position).getOrderId());
                mBundle11.putString("comment", orderList.get(position).getComment());
                mBundle11.putString("screen", "food");
                nutritionFragment.setArguments(mBundle11);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                nutritionTransaction.hide(new OrderCheckoutFragment());
                nutritionTransaction.addToBackStack(null);

                // Commit the transaction
                nutritionTransaction.commit();
            }
        });


        if(orderList.get(position).getItemTypeId().equals("1")){
            holder.type.setText("الحجم : صغير | ");
        }else if(orderList.get(position).getItemTypeId().equals("3")){
            holder.type.setText("الحجم : عادي | ");
        }else if(orderList.get(position).getItemTypeId().equals("2")){
            holder.type.setText("الحجم : كبير | ");
        }else if(orderList.get(position).getItemTypeId().equals("6")){
            holder.type.setText("الحجم : جامبو | ");
        } else if (orderList.get(position).getItemTypeId().equals("7")) {
            holder.type.setText("الحجم : مفرد | ");
        } else if (orderList.get(position).getItemTypeId().equals("8")) {
            holder.type.setText("الحجم : مضاعف  | ");
        } else if (orderList.get(position).getItemTypeId().equals("9")) {
            holder.type.setText("الحجم : صندوق القهوة |");
        } else if (orderList.get(position).getItemTypeId().equals("10")) {
            holder.type.setText("الحجم : ثلاثي |");
        }

        if(orderList.get(position).getAdditionals().equals("")){
            holder.additionalsName.setVisibility(View.GONE);
        }else {
            holder.additionalsName.setVisibility(View.VISIBLE);
            holder.additionalsName.setText(orderList.get(position).getAdditionals());
        }
        if(orderList.get(position).getItemId().equalsIgnoreCase(OrderCheckoutFragment.itemId)){
//            OrderCheckoutFragment.offerBanner.setVisibility(View.GONE);
            DrConstants.IFMAdded = false;
        }
        holder.title.setText(orderList.get(position).getItemNameAr());
        holder.qty.setText(orderList.get(position).getQty());
        if(orderList.get(position).getPrice().equalsIgnoreCase("free")){
            DrConstants.IFMAdded = true;
            holder.price.setText("السعر : مجانا  | " + "لقيمة الكلية : مجانا");
        }else {
            holder.price.setText("SR " + DrConstants.format.format(Float.parseFloat(orderList.get(position).getTotalAmount())) + " : القيمة الكلية | " + "SR " + DrConstants.format.format(Float.parseFloat(orderList.get(position).getPrice())) + " : السعر");
        }
		/*ArrayList<HashMap<String, String>> List = SchoolAlertMain.DBcontroller.getAllAlerts();
		//System.out.println("adapter"+alertsArrayList.get(position).getTime());
		if(List.contains(alertsArrayList.get(position).getAlertId())){
			holder.alertList.setBackgroundColor(Color.GRAY);
		}*/


        return convertView;
    }
}
