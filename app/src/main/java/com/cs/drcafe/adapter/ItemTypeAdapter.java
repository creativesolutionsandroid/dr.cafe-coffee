package com.cs.drcafe.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.drcafe.R;
import com.cs.drcafe.fragments.NutritionInfoFragment;
import com.cs.drcafe.fragments.OrderConfimationFragment;
import com.cs.drcafe.model.Promos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 30-06-2016.
 */
public class ItemTypeAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<String> promosList = new ArrayList<>();
    String language;
    Activity activity;

    public ItemTypeAdapter(Context context, ArrayList<String> promosList) {
        this.context = context;
        this.promosList = promosList;
        this.activity = activity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        SharedPreferences languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
    }

    public int getCount() {
        return promosList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView title;
        ImageView selected;
        LinearLayout layout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_nutrition_item_type, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.list_nutrition_item_type_ar, null);
            }

            holder.title = (TextView) convertView
                    .findViewById(R.id.type_text);
            holder.selected = (ImageView) convertView
                    .findViewById(R.id.type_selected);
            holder.layout = (LinearLayout) convertView
                    .findViewById(R.id.layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(promosList.get(position));
        if(NutritionInfoFragment.selectedType == position){
            holder.selected.setVisibility(View.VISIBLE);
        }
        else {
            holder.selected.setVisibility(View.INVISIBLE);
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NutritionInfoFragment.selectedType = position;
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

}
