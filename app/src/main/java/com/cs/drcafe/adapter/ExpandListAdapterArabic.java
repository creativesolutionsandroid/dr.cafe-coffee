package com.cs.drcafe.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.SQLException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.drcafe.DataBaseHelper;
import com.cs.drcafe.DrConstants;
import com.cs.drcafe.MainActivity;
import com.cs.drcafe.R;
import com.cs.drcafe.fragments.CategoriesListFragment;
import com.cs.drcafe.fragments.CommentFragment;
import com.cs.drcafe.fragments.MenuFragment;
import com.cs.drcafe.fragments.NutritionInfoFragment;
import com.cs.drcafe.fragments.OrderPriceFragment;
import com.cs.drcafe.model.Categories;
import com.cs.drcafe.model.Nutrition;
import com.cs.drcafe.model.Order;
import com.cs.drcafe.model.SubCategories;
import com.readystatesoftware.viewbadger.BadgeView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class ExpandListAdapterArabic extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Categories> groups;
    private ArrayList<Nutrition> nutritionList;

    private DataBaseHelper myDbHelper;
    BadgeView badge;
    AlertDialog alertDialog;
    private int orderQuantity;
    float orderPrice;
    String prevId;


    public ExpandListAdapterArabic(Context context, ArrayList<Categories> groups) {
        this.context = context;
        this.groups = groups;
        myDbHelper = new DataBaseHelper(context);
        try {

            myDbHelper.createDataBase();

        } catch (IOException ioe) {


        }

        try {

            myDbHelper.openDataBase();

        } catch (SQLException sqle) {

            throw sqle;

        }

    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<SubCategories> chList = groups.get(groupPosition).getChildItems();

        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final SubCategories child = (SubCategories) getChild(groupPosition, childPosition);
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
//            LayoutInflater infalInflater = (LayoutInflater) context
//                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item_arabic, null);
        }
        final ArrayList<Order> orderList = new ArrayList<>();
        final View v1 = infalInflater.inflate(R.layout.product_info_dropdown_arabic, null);
        TextView tv = (TextView) convertView.findViewById(R.id.subcat_name);
        final TextView childBadgeQty = (TextView) convertView.findViewById(R.id.child_badge_qty);
        final RelativeLayout infoLayout = (RelativeLayout) v1.findViewById(R.id.info_layout);
        LinearLayout itemLayout = (LinearLayout) convertView.findViewById(R.id.item_layout);
        final LinearLayout childLayout = (LinearLayout) convertView.findViewById(R.id.child_layout);
        final TextView childQty = (TextView) convertView.findViewById(R.id.child_item_qty);
        final ImageView checkoutImage = (ImageView) v1.findViewById(R.id.info_bg);
        final TextView price = (TextView) v1.findViewById(R.id.product_price);
        final RelativeLayout qty_layout = (RelativeLayout) convertView.findViewById(R.id.qty_layout);
        final TextView foodPrice = (TextView) convertView.findViewById(R.id.item_price_food);
        final TextView dummyPrice = (TextView) convertView.findViewById(R.id.dummy_price);

        LinearLayout commentayout = (LinearLayout) v1.findViewById(R.id.comment_layout);
        ImageView plusBtn = (ImageView) v1.findViewById(R.id.plus_btn);
        ImageView minusBtn = (ImageView) v1.findViewById(R.id.minus_btn);
        final TextView count = (TextView) v1.findViewById(R.id.product_count);
        ImageView infoIcon = (ImageView) convertView.findViewById(R.id.info_icon);
        final ImageView itemIcon = (ImageView) convertView.findViewById(R.id.item_icon);
        childLayout.removeAllViews();
//        infoLayout.setVisibility(View.GONE);

        try {
            // get input stream
            InputStream ims = context.getAssets().open(child.getItemId() + ".png");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            itemIcon.setImageDrawable(d);
        } catch (IOException ex) {
        }

        ViewTreeObserver vto = checkoutImage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                checkoutImage.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = checkoutImage.getMeasuredHeight();
                Log.i("Height TAG", "" + finalHeight);
                LinearLayout.LayoutParams newsCountParam = new
                        LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                infoLayout.setLayoutParams(newsCountParam);
                return true;
            }
        });

        nutritionList = myDbHelper.getNutritionInfo(child.getCatId(), child.getItemId());
        price.setText(DrConstants.format.format(Float.parseFloat(nutritionList.get(0).getPrice())));


        if (nutritionList.get(0).getCatId().equals("1") && !nutritionList.get(0).getItemTypeId().equals("0")) {
            foodPrice.setVisibility(View.GONE);
            dummyPrice.setVisibility(View.GONE);
            qty_layout.setVisibility(View.VISIBLE);
        }else{
            qty_layout.setVisibility(View.GONE);
            foodPrice.setVisibility(View.VISIBLE);
            dummyPrice.setVisibility(View.GONE);
            foodPrice.setText(DrConstants.format.format(Float.parseFloat(nutritionList.get(0).getPrice())));
        }


        tv.setText(child.getItemNameAr());
        itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prevId = child.getItemId();
                orderList.clear();
                orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
                nutritionList = myDbHelper.getNutritionInfo(child.getCatId(), child.getItemId());
                if (nutritionList.get(0).getCatId().equals("1") && !nutritionList.get(0).getItemTypeId().equals("0")) {

                    AdditionalsListAdapter.isMilkSelected = false;
//                        nutritionList = child.getNutritionItems();
//                        price.setText(nutritionList.get(0).getPrice());
//                        count.setText("1");

                    Fragment nutritionFragment = new OrderPriceFragment();//**--
                    Bundle mBundle11 = new Bundle();
                    mBundle11.putString("title", child.getItemNameAr());
                    mBundle11.putString("image_id", child.getItemId());
                    mBundle11.putString("additionals_type", child.getAdditionalsType());
                    mBundle11.putSerializable("nutrition", nutritionList);
                    mBundle11.putString("modifiersId", child.getModifiersId());
                    mBundle11.putString("item_id", child.getItemId());
                    nutritionFragment.setArguments(mBundle11);
                    // consider using Java coding conventions (upper first char class names!!!)
                    FragmentTransaction nutritionTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                    nutritionTransaction.hide(new CategoriesListFragment());
                    nutritionTransaction.addToBackStack(null);

                    // Commit the transaction
                    nutritionTransaction.commit();

                } else {
//                    if (child.getItemId().equalsIgnoreCase("327") && !CategoriesListFragment.isValid) {
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//                        alertDialogBuilder.setTitle("د. كيف");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage(" نأسف هذا المنتج متوفر للطلب من "+CategoriesListFragment.sTime+" الى "+CategoriesListFragment.eTime)
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
//                    } else {
                        int getSavedCount = myDbHelper.getItemOrderCount(nutritionList.get(0).getItemId());
                        float getSavedPrice = Float.parseFloat(nutritionList.get(0).getPrice()) * getSavedCount;
                        Log.i("TAG", "saved count" + getSavedCount);

                        if (getSavedCount == 0) {
                            count.setText("1");
                            price.setText(DrConstants.format.format(Float.parseFloat(nutritionList.get(0).getPrice())));
                            childQty.setText("1");
                            childBadgeQty.setVisibility(View.VISIBLE);
                            childBadgeQty.setText("1");
                            orderQuantity = 1;
                            orderPrice = Float.parseFloat(nutritionList.get(0).getPrice());
                            HashMap<String, String> values = new HashMap<>();
                            values.put("itemId", nutritionList.get(0).getItemId());
                            values.put("itemTypeId", nutritionList.get(0).getItemTypeId());
                            values.put("subCategoryId", nutritionList.get(0).getSubCatId());
                            values.put("additionals", "");
                            values.put("qty", "1");
                            values.put("price", nutritionList.get(0).getPrice());
                            values.put("additionalsPrice", "");
                            values.put("additionalsTypeId", "");
                            values.put("totalAmount", nutritionList.get(0).getPrice());
                            values.put("comment", "");
                            values.put("status", "1");
                            values.put("creationDate", "14/07/2015");
                            values.put("modifiedDate", "14/07/2015");
                            values.put("categoryId", nutritionList.get(0).getCatId());
                            myDbHelper.insertOrder(values);
                            CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                            CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                            try {
                                MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                            if(child.getItemId().equalsIgnoreCase("327")){
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("د. كيف");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(" هذا المنتج متوفر من "+CategoriesListFragment.sTime+" الى "+CategoriesListFragment.eTime)
                                        .setCancelable(false)
                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }

                            ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
                        } else {
                            orderQuantity = getSavedCount;
                            orderPrice = Float.parseFloat(nutritionList.get(0).getPrice()) * orderQuantity;
                            count.setText(""+getSavedCount);
                            price.setText(DrConstants.format.format(getSavedPrice));
                            childQty.setText(""+getSavedCount);
                            CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                            CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                            try {
                                MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                                MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                            ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
                        }
                        childLayout.removeAllViews();
                        childLayout.addView(v1);
                    }
//                }


//                notifyDataSetChanged();
            }
        });

        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevId = child.getItemId();
                orderList.clear();
                orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
                if(orderList.size()>0) {
                    orderQuantity = orderQuantity + 1;
                    orderPrice = orderPrice + Float.parseFloat(nutritionList.get(0).getPrice());
                    count.setText("" + orderQuantity);
                    price.setText(DrConstants.format.format(orderPrice));
                    childQty.setText("" + orderQuantity);
                    myDbHelper.updateOrder(String.valueOf(orderQuantity), String.valueOf(orderPrice), orderList.get(0).getOrderId());
                    CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                    CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    try {
                        MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                        MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    int cnt = myDbHelper.getItemOrderCount(nutritionList.get(0).getItemId());
                    if (cnt != 0) {
                        childBadgeQty.setVisibility(View.VISIBLE);
                        childBadgeQty.setText("" + cnt);
                    } else {
                        childBadgeQty.setVisibility(View.GONE);
                    }
                    ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
                }else{
                    count.setText("1");
                    childQty.setText("1");
                    childBadgeQty.setVisibility(View.VISIBLE);
                    childBadgeQty.setText("1");
                    orderQuantity = 1;
                    orderPrice = Float.parseFloat(nutritionList.get(0).getPrice());
                    price.setText(DrConstants.format.format(orderPrice));
                    HashMap<String, String> values = new HashMap<>();
                    values.put("itemId", nutritionList.get(0).getItemId());
                    values.put("itemTypeId", nutritionList.get(0).getItemTypeId());
                    values.put("subCategoryId", nutritionList.get(0).getSubCatId());
                    values.put("additionals", "");
                    values.put("qty", "1");
                    values.put("price", nutritionList.get(0).getPrice());
                    values.put("additionalsPrice", "");
                    values.put("additionalsTypeId", "");
                    values.put("totalAmount", nutritionList.get(0).getPrice());
                    values.put("comment", "");
                    values.put("status", "1");
                    values.put("creationDate", "14/07/2015");
                    values.put("modifiedDate", "14/07/2015");
                    values.put("categoryId", nutritionList.get(0).getCatId());
                    myDbHelper.insertOrder(values);
                    CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                    CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    try {
                        MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                        MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());

                }
//                notifyDataSetChanged();
            }
        });

        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevId = child.getItemId();
                if(orderQuantity > 0) {
                    orderList.clear();
                    orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
                    if(orderList.size()>0) {
                        orderQuantity = orderQuantity - 1;
                        orderPrice = orderPrice - Float.parseFloat(nutritionList.get(0).getPrice());
                        count.setText("" + orderQuantity);
                        price.setText(DrConstants.format.format(orderPrice));
                        childQty.setText("" + orderQuantity);
                        myDbHelper.updateOrder(String.valueOf(orderQuantity), String.valueOf(orderPrice), orderList.get(0).getOrderId());
                        CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                        CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                        try {
                            MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                            MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        int cnt = myDbHelper.getItemOrderCount(nutritionList.get(0).getItemId());
                        if (cnt != 0) {
                            childBadgeQty.setVisibility(View.VISIBLE);
                            childBadgeQty.setText("" + cnt);
                        } else {
                            childBadgeQty.setVisibility(View.GONE);
                        }
                        ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
                        if (orderQuantity == 0) {
                            notifyDataSetChanged();
                            myDbHelper.deleteItemFromOrder(orderList.get(0).getOrderId());
                        }
//                        notifyDataSetChanged();
                    }
                }
            }
        });

        infoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nutritionList = myDbHelper.getNutritionInfo(child.getCatId(), child.getItemId());
                Fragment nutritionFragment = new NutritionInfoFragment();
                Bundle mBundle11 = new Bundle();
                mBundle11.putString("title", child.getItemNameAr());
                mBundle11.putString("description", child.getDescriptionAr());
                mBundle11.putSerializable("nutrition", nutritionList);
                nutritionFragment.setArguments(mBundle11);
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction nutritionTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                nutritionTransaction.hide(new CategoriesListFragment());
                nutritionTransaction.addToBackStack(null);

                // Commit the transaction
                nutritionTransaction.commit();
            }
        });


        commentayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(orderQuantity > 0) {
                    orderList.clear();
                    orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
                    if(orderList.size()>0) {
                        Fragment nutritionFragment = new CommentFragment();
                        Bundle mBundle11 = new Bundle();
                        mBundle11.putString("title", child.getItemNameAr());
                        mBundle11.putString("itemId", child.getItemId());
                        mBundle11.putString("orderId", orderList.get(0).getOrderId());
                        mBundle11.putString("comment", orderList.get(0).getComment());
                        mBundle11.putString("screen", "food");
                        nutritionFragment.setArguments(mBundle11);
                        // consider using Java coding conventions (upper first char class names!!!)
                        FragmentTransaction nutritionTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();

                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack
                        nutritionTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        nutritionTransaction.add(R.id.realtabcontent, nutritionFragment);
                        nutritionTransaction.hide(new CategoriesListFragment());
                        nutritionTransaction.addToBackStack(null);

                        // Commit the transaction
                        nutritionTransaction.commit();
                    }
                }else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                    // set title
                    alertDialogBuilder.setTitle("د. كيف");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("من فضلك اضف الكمية")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });
        int cnt = myDbHelper.getItemOrderCount(child.getItemId());
        childQty.setText(""+cnt);
        if(cnt !=0){
            childBadgeQty.setVisibility(View.VISIBLE);
            childBadgeQty.setText(""+cnt);
            orderQuantity = cnt;
        }else {
            childBadgeQty.setVisibility(View.GONE);
        }


//        if(child.getItemId().equals(prevId)){
//            childLayout.removeAllViews();
//            childLayout.addView(v1);
//            orderList.clear();
//            orderList.addAll(myDbHelper.getItemOrderInfo(child.getItemId()));
//            nutritionList = myDbHelper.getNutritionInfo(child.getCatId(), child.getItemId());
//        }

        childBadgeQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Order> orderList = new ArrayList<>();
                orderList = myDbHelper.getItemOrderInfo(child.getItemId());
                orderQuantity = myDbHelper.getItemOrderCount(child.getItemId());
                nutritionList = myDbHelper.getNutritionInfo(child.getCatId(), child.getItemId());
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(context.LAYOUT_INFLATER_SERVICE);
                View dialogView = inflater.inflate(R.layout.order_dialog_listview, null);
                dialogBuilder.setView(dialogView);
                ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_button);
                OrderInfoAdapter orderInfoAdapter = new OrderInfoAdapter(context, orderList);
                ListView listView = (ListView) dialogView.findViewById(R.id.order_dialog_list);
                listView.setAdapter(orderInfoAdapter);
//                TextView title = (TextView) dialogView.findViewById(R.id.item_name);
//                final TextView price = (TextView) dialogView.findViewById(R.id.item_price);
//                final TextView qty = (TextView) dialogView.findViewById(R.id.item_qty);
//                ImageView plusBtn = (ImageView) dialogView.findViewById(R.id.plus_btn);
//                ImageView minusBtn = (ImageView) dialogView.findViewById(R.id.minus_btn);
//
//                try {
//                    // get input stream
//                    InputStream ims = context.getAssets().open(nutritionList.get(0).getItemId() + ".png");
//                    // load image as Drawable
//                    Drawable d = Drawable.createFromStream(ims, null);
//                    // set image to ImageView
//                    title.setCompoundDrawablesWithIntrinsicBounds(d,null,null,null);
//                } catch (IOException ex) {
//                }
//                title.setText(child.getItemName());
//                qty.setText("" + orderQuantity);
//                int totalAmount = Integer.parseInt(nutritionList.get(0).getPrice())*orderQuantity;
//                price.setText("Price : "+nutritionList.get(0).getPrice() +"  |  Total Amount : "+totalAmount+ "SR");
//
//
//                plusBtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        orderQuantity = orderQuantity + 1;
//                        orderPrice = orderPrice + Integer.parseInt(nutritionList.get(0).getPrice());
//                        count.setText("" + orderQuantity);
//                        childQty.setText("" + orderQuantity);
//                        qty.setText("" + orderQuantity);
//                        int totalAmount = Integer.parseInt(nutritionList.get(0).getPrice())*orderQuantity;
//                        price.setText("Price : "+nutritionList.get(0).getPrice() +"  |  Total Amount : "+totalAmount+ "SR");
//                        myDbHelper.updateOrder(String.valueOf(orderQuantity), String.valueOf(orderPrice), nutritionList.get(0).getItemId());
//                        CategoriesListFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                        CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        notifyDataSetChanged();
//                        ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
//                    }
//                });
//
//                minusBtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (orderQuantity > 0) {
//                            orderQuantity = orderQuantity - 1;
//                            orderPrice = orderPrice - Integer.parseInt(nutritionList.get(0).getPrice());
//                            count.setText("" + orderQuantity);
//                            childQty.setText("" + orderQuantity);
//                            qty.setText("" + orderQuantity);
//                            int totalAmount = Integer.parseInt(nutritionList.get(0).getPrice())*orderQuantity;
//                            price.setText("Price : "+nutritionList.get(0).getPrice() +"  |  Total Amount : "+totalAmount+ "SR");
//                            myDbHelper.updateOrder(String.valueOf(orderQuantity), String.valueOf(orderPrice), nutritionList.get(0).getItemId());
//                            CategoriesListFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                            CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            notifyDataSetChanged();
//                            ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());
//                            if(orderQuantity == 0){
//                                alertDialog.dismiss();
//                            }
//
//                        }else{
//                        }
//                    }
//                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<SubCategories> chList = groups.get(groupPosition).getChildItems();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Categories group = (Categories) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.group_item_arabic, null);
        }
        RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.grp_layout);
        TextView tv = (TextView) convertView.findViewById(R.id.group_name);
        TextView groupItemQty  = (TextView) convertView.findViewById(R.id.group_item_qty);
        ImageView iv = (ImageView) convertView.findViewById(R.id.group_indicator);
        ImageView groupIcon = (ImageView) convertView.findViewById(R.id.group_icon);

        tv.setText(group.getSubCatAr());

        try {
            // get input stream
            InputStream ims = context.getAssets().open(group.getImages());
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            groupIcon.setImageDrawable(d);
        } catch (IOException ex) {
        }

        if (isExpanded) {
            iv.setImageResource(R.drawable.group_arrow_selected);
        } else {
            iv.setImageResource(R.drawable.group_arrow_left);
        }

        if(group.getSubCatId().equalsIgnoreCase("71")){
            rl.setBackgroundResource(R.drawable.bg_offer_banner_arabic);
            groupIcon.setVisibility(View.GONE);
            tv.setVisibility(View.GONE);
        }else{
            rl.setBackgroundResource(0);
            groupIcon.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
        }

        int cnt = myDbHelper.getSubcatOrderCount(group.getSubCatId());
        if(cnt !=0) {
            groupItemQty.setVisibility(View.VISIBLE);
            groupItemQty.setText(""+cnt);
        }else{
            groupItemQty.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    Animation.AnimationListener animL = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            //this is just a method call you can create to delete the animated view or hide it until you need it again.
//            clearAnimation();
        }
    };



    public class OrderInfoAdapter extends BaseAdapter {
        public Context context;
        public LayoutInflater inflater;
        ArrayList<Order> orderList = new ArrayList<>();
        int qty;
        float price;

        public OrderInfoAdapter(Context context, ArrayList<Order> orderList) {
            this.context = context;
            this.orderList = orderList;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        public int getCount() {
            return orderList.size();

        }

        public String getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            TextView title, price, qty, type;
            ImageView itemBackground, plusBtn, minusBtn;
            //ImageButton plus;
            LinearLayout orderInfoLayout;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();


                convertView = inflater.inflate(R.layout.order_item_dialog, null);

                holder.itemBackground = (ImageView) convertView.findViewById(R.id.item_background);
                holder.orderInfoLayout = (LinearLayout) convertView.findViewById(R.id.order_info_layout);
                //animate();
                holder.title = (TextView) convertView.findViewById(R.id.item_name);
                holder.price = (TextView) convertView.findViewById(R.id.item_price);
                holder.qty = (TextView) convertView.findViewById(R.id.item_qty);
                holder.plusBtn = (ImageView) convertView.findViewById(R.id.plus_btn);
                holder.minusBtn = (ImageView) convertView.findViewById(R.id.minus_btn);
                holder.type = (TextView) convertView.findViewById(R.id.item_type);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ViewTreeObserver vto = holder.itemBackground.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    holder.itemBackground.getViewTreeObserver().removeOnPreDrawListener(this);
                    int finalHeight = holder.itemBackground.getMeasuredHeight();
                    Log.i("Height TAG", "" + finalHeight);
                    LinearLayout.LayoutParams newsCountParam = new
                            LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                    RelativeLayout.LayoutParams params = new
                            RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
                    holder.orderInfoLayout.setLayoutParams(params);
                    return true;
                }
            });
            try {
                // get input stream
                InputStream ims = context.getAssets().open(orderList.get(position).getItemId() + ".png");
                // load image as Drawable
                Drawable d = Drawable.createFromStream(ims, null);
                // set image to ImageView
                holder.title.setCompoundDrawablesWithIntrinsicBounds(d,null,null,null);
            } catch (IOException ex) {
            }

            holder.plusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("TAG", "" + position);
                    qty = Integer.parseInt(orderList.get(position).getQty()) + 1;

                    price = (Float.parseFloat(orderList.get(position).getTotalAmount())) + (Float.parseFloat(orderList.get(position).getPrice()));
                    myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getOrderId());
                    orderList = myDbHelper.getItemOrderInfo(orderList.get(position).getItemId());
                    notifyDataSetChanged();
                    ExpandListAdapterArabic.this.notifyDataSetChanged();
                    CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                    CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    try {
                        MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                        MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());

                }
            });

            holder.minusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    qty = Integer.parseInt(orderList.get(position).getQty()) - 1;
                    if(qty==0){
                        myDbHelper.deleteItemFromOrder(orderList.get(position).getOrderId());
                    }else {
                        price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - (Float.parseFloat(orderList.get(position).getPrice()));
                        myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getOrderId());
                    }

                    orderList = myDbHelper.getItemOrderInfo(orderList.get(position).getItemId());
                    if(orderList.size() == 0){
                        alertDialog.dismiss();
                    }
                    notifyDataSetChanged();
                    ExpandListAdapterArabic.this.notifyDataSetChanged();
                    CategoriesListFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                    CategoriesListFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoriesListFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    try {
                        MenuFragment.orderPrice.setText(DrConstants.format.format(myDbHelper.getTotalOrderPrice()));
                        MenuFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        MenuFragment.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    ((MainActivity) context).setBadge("" + myDbHelper.getTotalOrderQty());

                }
            });

            if(orderList.get(position).getItemTypeId().equals("1")){
                holder.type.setText("size : Short |");
            }else if(orderList.get(position).getItemTypeId().equals("3")){
                holder.type.setText("size : Tall |");
            }else if(orderList.get(position).getItemTypeId().equals("2")){
                holder.type.setText("size : Grande |");
            }else if(orderList.get(position).getItemTypeId().equals("6")){
                holder.type.setText("size : Jumbo |");
            } else if (orderList.get(position).getItemTypeId().equals("7")) {
                holder.type.setText("size : Single |");
            } else if (orderList.get(position).getItemTypeId().equals("8")) {
                holder.type.setText("size : Double |");
            }

            holder.title.setText(orderList.get(position).getItemName());
            holder.qty.setText(orderList.get(position).getQty());
            holder.price.setText("Price : "+orderList.get(position).getPrice() +"  |  Total Amount : "+orderList.get(position).getTotalAmount()+ "SR");
		/*ArrayList<HashMap<String, String>> List = SchoolAlertMain.DBcontroller.getAllAlerts();
		//System.out.println("adapter"+alertsArrayList.get(position).getTime());
		if(List.contains(alertsArrayList.get(position).getAlertId())){
			holder.alertList.setBackgroundColor(Color.GRAY);
		}*/


            return convertView;
        }
    }

}