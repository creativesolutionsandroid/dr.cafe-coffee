package com.cs.drcafe;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by brahmam on 28/12/15.
 */
public class LoginActivity extends Activity {

    private String SOAP_ACTION = "http://tempuri.org/userLogin";
    private String METHOD_NAME = "userLogin";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapObject result;
    private String response12 = null;
    private Button backBtn, loginBtn;
    private EditText mEmail, mPassword;
    private TextView mSignUp, mForgotPassword;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    ProgressDialog dialog;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("lifecycle","login onCreate invoked");
        SharedPreferences languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.login_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.login_layout_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        backBtn = (Button) findViewById(R.id.back_btn);
        loginBtn = (Button) findViewById(R.id.login_button);
        mEmail = (EditText) findViewById(R.id.email_field);
        mPassword = (EditText) findViewById(R.id.password_field);
        mSignUp = (TextView) findViewById(R.id.login_signup_btn);
        mForgotPassword = (TextView) findViewById(R.id.forgot_password);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                if (email.length() == 0) {
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please enter Email / Mobile Number");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError(" من فضلك ادخل البريد الالكتروني / رقم الجوال");
                    }
                    mEmail.requestFocus();
//                } else if (!isValidEmail(email)) {
//                    if(language.equalsIgnoreCase("En")) {
//                        mPhoneNumber.setError("Enter valid email address");
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        mPhoneNumber.setError("ادخل البريد الالكتروني كما هو موضح");
//                    }
//
//                    mPhoneNumber.requestFocus();
                } else if (password.length() == 0) {
                    if(language.equalsIgnoreCase("En")) {
                        mPassword.setError("Please enter Password");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPassword.setError("من فضلك ادخل كلمة المرور");
                    }

                    mPassword.requestFocus();
                } else {
                    new CheckLoginDetails().execute(email, password);
                }

//                Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
//                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(loginIntent);
//                finish();
            }
        });

        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(signUpIntent);
            }
        });

        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(intent);
            }
        });
    }


    public class CheckLoginDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus, email, password;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            dialog = ProgressDialog.show(LoginActivity.this, "",
                    "Please wait...");
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                    request.addProperty("username", arg0[0]);
                    request.addProperty("password", arg0[1]);
                    request.addProperty("deviceToken", SplashScreen.regid);
                    request.addProperty("language", "En");
                    email = arg0[0];
                    password = arg0[1];

                    // request.addProperty("", ""); // incase you need to pass
                    // parameters to the web-service

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                            SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL, 180000);
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    result = (SoapObject) envelope.getResponse();
                    response12 = result.toString();
                    Log.v("TAG", response12);
                    // SoapObject root = (SoapObject) result.getProperty(0);
                    System.out.println("********Count : " + result.getPropertyCount());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);

                    // set title
                    alertDialogBuilder.setTitle("dr.CAFE");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    if (response12.equals("anyType{}")) {
//                Toast.makeText(getActivity(), "Please Enter Valid Store Id", Toast.LENGTH_LONG).show();
                    } else {

                        try {

                            Object property = result.getProperty(1);
                            if (property instanceof SoapObject) {
                                try {
                                    SoapObject loginObject = (SoapObject) property;
                                    SoapObject userObject = (SoapObject) loginObject.getProperty(0);
                                    SoapObject userObject1 = (SoapObject) userObject.getProperty(0);
                                    String userId = userObject1.getPropertyAsString("userId");
                                    Log.i("TAG", "" + userId);
                                    if (userId.equalsIgnoreCase("0")) {
                                        dialog.dismiss();
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);

                                        if(language.equalsIgnoreCase("En")) {
                                            // set title
                                            alertDialogBuilder.setTitle("dr.CAFE");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("Invalid email / mobile number or password.")
                                                    .setCancelable(false)
                                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                        }else if(language.equalsIgnoreCase("Ar")){
                                            // set title
                                            alertDialogBuilder.setTitle("د. كيف");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("البريد الالكتروني غير موجود/ رقم الجوال او كلمة المرور غير صحيحة")
                                                    .setCancelable(false)
                                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                        }


                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();

                                        // show it
                                        alertDialog.show();
                                    } else {

                                        String fullName = userObject1.getPropertyAsString("fullName");
                                        String fullAddress = userObject1.getPropertyAsString("fullAddress");
                                        String phone = userObject1.getPropertyAsString("phone");
                                        String countryId = userObject1.getPropertyAsString("countryId");
                                        String country = userObject1.getPropertyAsString("country");
                                        String username = userObject1.getPropertyAsString("username");
                                        String mobile = userObject1.getPropertyAsString("mobile");
                                        String NickName = userObject1.getPropertyAsString("NickName");
                                        if(NickName.equalsIgnoreCase("anytype{}")){
                                            NickName = "";
                                        }
                                        String Gender = userObject1.getPropertyAsString("Gender");
                                        String FamilyName = userObject1.getPropertyAsString("FamilyName");
                                        if(FamilyName.equalsIgnoreCase("anytype{}")){
                                            FamilyName = "";
                                        }
                                        boolean isVerified = Boolean.valueOf(userObject1.getPropertyAsString("isVerified"));

                                        try {
                                            JSONObject parent = new JSONObject();
                                            JSONObject jsonObject = new JSONObject();
                                            JSONArray jsonArray = new JSONArray();
                                            jsonArray.put("lv1");
                                            jsonArray.put("lv2");

                                            jsonObject.put("userId", userId);
                                            jsonObject.put("fullName", fullName);
                                            jsonObject.put("phone", phone);
                                            jsonObject.put("mobile", mobile);
                                            jsonObject.put("FamilyName", FamilyName);
                                            jsonObject.put("Gender", Gender);
                                            jsonObject.put("country", country);
                                            jsonObject.put("NickName", NickName);
                                            jsonObject.put("username", username);
                                            jsonObject.put("user_details", jsonArray);
                                            parent.put("profile", jsonObject);
                                            Log.d("output", parent.toString());
                                            userPrefEditor.putString("user_profile", parent.toString());
                                            userPrefEditor.putString("userId", userId);

                                            userPrefEditor.putString("user_email", email);
                                            userPrefEditor.putString("user_password", password);

                                            userPrefEditor.commit();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        dialog.dismiss();



                                        if(isVerified) {
                                        userPrefEditor.putString("login_status","loggedin");
                                        userPrefEditor.commit();
                                        Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
                                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(loginIntent);
                                        finish();
                                    }else{
                                        Intent loginIntent = new Intent(LoginActivity.this, VerifyRandomNumber.class);
                                        loginIntent.putExtra("phone_number", phone);
                                        loginIntent.putExtra("country_flag", 0);
                                        loginIntent.putExtra("userId", userId);
                                        startActivity(loginIntent);
                                        finish();
                                    }
                                }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            Toast.makeText(LoginActivity.this, "cannot reach server, please try again.", Toast.LENGTH_LONG).show();
                            Log.d("", "Error while parsing the results!");
                            e.printStackTrace();
                        }
                    }
                }
            }

            if(dialog!=null){
                dialog.dismiss();
            }

                super.onPostExecute(result1);
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("lifecycle","login onStart invoked");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d("lifecycle","login onResume invoked");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d("lifecycle","login onPause invoked");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d("lifecycle","login onStop invoked");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("lifecycle","login onRestart invoked");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("lifecycle","login onDestroy invoked");
    }
}
