package com.cs.drcafe;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import java.text.DecimalFormat;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by SKT on 29-12-2015.
 */
public class DrConstants {
    public static String storeDistance = "50";
    public static int messageCount = 0;
    public static String comment = "";
    public static String itemId = "null";
    public static boolean NO_BACKPRESS = false;
    public static boolean favFlag = false;
    public static boolean giftsFlag = false;
    public static boolean menuFlag = false;
    public static boolean storeFlag = false;
    public static boolean offerFlag = false;
    public static String LATEST_CARD = "";
    public static String LATEST_BALANCE = "";
    public static String PAYMENTS_LIVE_URL = "http://drcafeonline.net/payment/Api/Payments/deleteCard?userId=";
    public static String URL = "http://www.drcafeonline.net/dcservices.asmx";
//    public static String URL = "http://csadms.com/dcservices/DCServices.asmx";
    public static boolean IFMAdded = false;
    public static boolean STOP_BACKPRESS = false;
    public static boolean orderNow = false;
    public static String storeId = "";
    public static String storeName = "";
    public static String storeAddress = "";
    public static String startTime = "";
    public static String endTime = "";
    public static String addressId = "";
    public static String addressHouseNo = "";
    public static String address = "";
    public static String delLatitude = "";
    public static String delLongitude = "";
    public static String orderType = "DineIn";
    public static Double latitude, longitude;
    public static float MinOrderAmount = 0;
    public static float DeliveryCharge = 0;
    public static float FreeDeliveryAbove = 0;
    public static boolean CashPayment = true;
    public static boolean OnlinePayment= true;
    public static boolean fullHours= false;
    public static boolean updatePopupShown= false;
    public static boolean firstOrderDone= false;
    public static float vat = 5;
    public static String vatString = "5";
    public static DecimalFormat format = new DecimalFormat("0.00");

    /* The configuration values to change across the app */
    public static class Config {

        /* The payment brands for Ready-to-Use UI and Payment Button */
        public static final Set<String> PAYMENT_BRANDS;

        static {
            PAYMENT_BRANDS = new LinkedHashSet<>();

            PAYMENT_BRANDS.add("VISA");
            PAYMENT_BRANDS.add("MASTER");
        }

        /* The default payment brand for payment button */
        public static final String PAYMENT_BUTTON_BRAND = "VISA";

        /* The default amount and currency */
        public static final String AMOUNT = "49.99";
        public static final String CURRENCY = "SAR";

        /* The card info for SDK & Your Own UI*/
        public static final String CARD_BRAND = "VISA";
        public static final String CARD_HOLDER_NAME = "JOHN DOE";
        public static final String CARD_NUMBER = "4200000000000000";
        public static final String CARD_EXPIRY_MONTH = "07";
        public static final String CARD_EXPIRY_YEAR = "21";
        public static final String CARD_CVV = "123";
    }

    public static final int CONNECTION_TIMEOUT = 5000;

//    public static final String CHECKOUT_URL = "http://csadms.com/DCPaymentService/api/Payments/CheckoutRequestFromSDK?";

//    public static final String PAYMENT_STATUS_URL = "http://csadms.com/dcpaymentservice/Api/Payments/paymentStatus?resourcePath=";

//    public static final String CARD_REGISTRATION_STATUS_URL = "http://csadms.com/dcpaymentservice/Api/Payments/AddCardStatus?resourcePath=";

    public static final String CHECKOUT_URL = "http://drcafeonline.net/payment/Api/Payments/CheckoutRequestFromSDK?";

    public static final String PAYMENT_STATUS_URL = "http://drcafeonline.net/payment/Api/Payments/paymentStatus?resourcePath=";

    public static final String CARD_REGISTRATION_STATUS_URL = "http://drcafeonline.net/payment/Api/Payments/AddCardStatus?resourcePath=";

    public static final String SHOPPER_RESULT_URL = "com.creativesols.drcafe.payments://result";

    public static String convertToArabic(String value)
    {
        String newValue = (value
                .replaceAll("١","1" ).replaceAll("٢","2" )
                .replaceAll("٣","3" ).replaceAll("٤","4" )
                .replaceAll("٥","5" ).replaceAll("٦","6" )
                .replaceAll("٧","7" ).replaceAll("٨","8")
                .replaceAll("٩","9" ).replaceAll("٠","0" )
                .replaceAll("٫",".").replaceAll(",","."));
        return newValue;
    }

    public class LocationConstants {
        public static final int SUCCESS_RESULT = 0;

        public static final int FAILURE_RESULT = 1;

        public static final String PACKAGE_NAME = "com.cs.drcafe.";

        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

        public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

        public static final String LOCATION_DATA_AREA = PACKAGE_NAME + ".LOCATION_DATA_AREA";
        public static final String LOCATION_DATA_CITY = PACKAGE_NAME + ".LOCATION_DATA_CITY";
        public static final String LOCATION_DATA_STREET = PACKAGE_NAME + ".LOCATION_DATA_STREET";
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }
}
