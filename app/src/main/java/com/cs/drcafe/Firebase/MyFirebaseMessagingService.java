package com.cs.drcafe.Firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import android.util.Log;

import com.cs.drcafe.MainActivity;
import com.cs.drcafe.R;
import com.cs.drcafe.SplashScreen;
//import com.google.firebase.messaging.FirebaseMessagingService;
//import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService
//        extends FirebaseMessagingService
{
//    private static final String TAG = "TAG";
//    String title, icon;
//    Intent intent;
//    Context mContext;
//    SharedPreferences userPrefs;
//
//    @Override
//    public void onNewToken(String token) {
//        Log.d(TAG, "Refreshed token: " + token);
//
//        // If you want to send messages to this application instance or
//        // manage this apps subscriptions on the server side, send the
//        // Instance ID token to your app server.
//        storeRegIdInPref(token);
//
////        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
////        registrationComplete.putExtra("Token", token);
////        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
//
////        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
//    }
//
//    private void storeRegIdInPref(String Token) {
//        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putString("regId", Token);
//        SplashScreen.regid = Token;
//        editor.commit();
//    }
//
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.e(TAG, "From: " + remoteMessage.getFrom());
//
//        if (remoteMessage == null)
//            return;
//
//        if (remoteMessage.getNotification() != null) {
//            Log.d(TAG, "notification: "+remoteMessage.getNotification().getBody());
//            userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
//            String userId = userPrefs.getString("userId", "-1");
//            if(!userId.equals("-1")) {
//                // Creates an explicit intent for an ResultActivity to receive.
//                Intent resultIntent = new Intent(this, MainActivity.class);
//                resultIntent.putExtra("push", true);
//
//                // This ensures that the back button follows the recommended
//                // convention for the back key.
//                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//
//                // Adds the back stack for the Intent (but not the Intent itself).
//                stackBuilder.addParentStack(MainActivity.class);
//
//                // Adds the Intent that starts the Activity to the top of the stack.
//                stackBuilder.addNextIntent(resultIntent);
//                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
//                        0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//                Notification myNotification = new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.drawable.notification_icon)
//                        .setColor(Color.parseColor("#000000"))
//                        .setAutoCancel(true)
//                        .setContentIntent(resultPendingIntent)
//                        .setContentTitle("dr.CAFE")
//                        .setContentText(remoteMessage.getNotification().getBody())
//                        .setTicker(remoteMessage.getNotification().getBody())
//                        .setPriority(NotificationCompat.PRIORITY_HIGH)
//                        .setDefaults(NotificationCompat.DEFAULT_ALL)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getNotification().getBody()))
//                        .setChannelId("dr.CAFE")
//                        .build();
//
//                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//
//                int NOTIFICATION_ID = (int) System.currentTimeMillis();
//                mNotificationManager.notify(NOTIFICATION_ID, myNotification);
//            }
//            else{
//                Notification myNotification = new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.drawable.notification_icon)
//                        .setColor(Color.parseColor("#000000"))
//                        .setAutoCancel(true)
//                        .setContentTitle("dr.CAFE")
//                        .setContentText(remoteMessage.getNotification().getBody())
//                        .setTicker(remoteMessage.getNotification().getBody())
//                        .setPriority(NotificationCompat.PRIORITY_HIGH)
//                        .setDefaults(NotificationCompat.DEFAULT_ALL)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getNotification().getBody()))
//                        .setChannelId("dr.CAFE")
//                        .build();
//
//                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                int NOTIFICATION_ID = (int) System.currentTimeMillis();
//
//                mNotificationManager.notify(NOTIFICATION_ID, myNotification);
//            }
//        }
//
//        if (remoteMessage.getData().size() > 0) {
//            Log.d(TAG, "onMessageReceived: "+remoteMessage.getData());
//
//            String message = "";
//            int pnType = 1;
//            Map<String, String> notificationMap = remoteMessage.getData();
//            for (Map.Entry<String, String> e : notificationMap.entrySet()) {
//                if(e.getKey().equals("text")){
//                    message = e.getValue();
//                }
//                if(e.getKey().equals("PnType")){
//                    pnType = Integer.parseInt(e.getValue());
//                }
//            }
//
//            userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
//            String userId = userPrefs.getString("userId", "-1");
//            if(!userId.equals("-1")) {
//                // Creates an explicit intent for an ResultActivity to receive.
//                Intent resultIntent = new Intent(this, MainActivity.class);
//                resultIntent.putExtra("push", true);
//
//                // This ensures that the back button follows the recommended
//                // convention for the back key.
//                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//
//                // Adds the back stack for the Intent (but not the Intent itself).
//                stackBuilder.addParentStack(MainActivity.class);
//
//                // Adds the Intent that starts the Activity to the top of the stack.
//                stackBuilder.addNextIntent(resultIntent);
//                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
//                        0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//                Notification myNotification = new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.drawable.notification_icon)
//                        .setColor(Color.parseColor("#000000"))
//                        .setAutoCancel(true)
//                        .setContentIntent(resultPendingIntent)
//                        .setContentTitle("dr.CAFE")
//                        .setContentText(message)
//                        .setTicker(message)
//                        .setPriority(NotificationCompat.PRIORITY_HIGH)
//                        .setDefaults(NotificationCompat.DEFAULT_ALL)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
//                        .setChannelId("dr.CAFE")
//                        .build();
//
//                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//
//                int NOTIFICATION_ID = (int) System.currentTimeMillis();
//                mNotificationManager.notify(NOTIFICATION_ID, myNotification);
//            }
//            else{
//                Notification myNotification = new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.drawable.notification_icon)
//                        .setColor(Color.parseColor("#000000"))
//                        .setAutoCancel(true)
//                        .setContentTitle("dr.CAFE")
//                        .setContentText(message)
//                        .setTicker(message)
//                        .setPriority(NotificationCompat.PRIORITY_HIGH)
//                        .setDefaults(NotificationCompat.DEFAULT_ALL)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
//                        .setChannelId("dr.CAFE")
//                        .build();
//
//                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//
//                int NOTIFICATION_ID = (int) System.currentTimeMillis();
//                mNotificationManager.notify(NOTIFICATION_ID, myNotification);
//            }
//        }
//    }
}
