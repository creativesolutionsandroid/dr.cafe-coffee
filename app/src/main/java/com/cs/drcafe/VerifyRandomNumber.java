package com.cs.drcafe;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by SKT on 02-01-2016.
 */
public class VerifyRandomNumber extends Activity {
    private String SOAP_ACTION = "http://tempuri.org/verifyRandomNumber";
    private String METHOD_NAME = "verifyRandomNumber";
    private final String NAMESPACE = "http://tempuri.org/";
    private static final String URL = DrConstants.URL;
    private SoapPrimitive result;
    private String response12 = null;
    EditText mMobileNumber, mActivationCode, mCountryCode;
    ImageView mCountryFlag;
    Button mVerifySubmit;
    String phoneNumber, userId;
    ProgressDialog dialog;
    int countryFlag;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_random_number);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        phoneNumber = getIntent().getExtras().getString("phone_number");
        countryFlag = getIntent().getExtras().getInt("country_flag");
        userId = getIntent().getExtras().getString("userId");

        String[] parts = phoneNumber.split("-");

        mMobileNumber = (EditText) findViewById(R.id.mobile_number);
        mCountryCode = (EditText) findViewById(R.id.country_code);
        mActivationCode = (EditText) findViewById(R.id.verify_code);
        mCountryFlag = (ImageView) findViewById(R.id.country_flag);
        mVerifySubmit = (Button) findViewById(R.id.submit);

        mCountryCode.setText(parts[0]);
        mMobileNumber.setText(parts[1]);
        mCountryFlag.setImageResource(countryFlag);

        mVerifySubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String activationCode = mActivationCode.getText().toString();

                new GetVerificationDetails().execute(userId, phoneNumber, activationCode);
            }
        });

    }


    public class GetVerificationDetails extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            dialog = ProgressDialog.show(VerifyRandomNumber.this, "",
                    "Loading. Please Wait....");
        }


        @Override
        protected String doInBackground(String... arg0) {

            try {

                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                request.addProperty("userId", arg0[0]);
                request.addProperty("mobile", arg0[1]);
                request.addProperty("RandomNumber", arg0[2]);

                // request.addProperty("", ""); // incase you need to pass
                // parameters to the web-service

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
                androidHttpTransport.call(SOAP_ACTION, envelope);
                result = (SoapPrimitive) envelope.getResponse();
                response12 = result.toString();
                Log.v("TAG", response12);
                // SoapObject root = (SoapObject) result.getProperty(0);
                System.out.println("********Count : " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Responce", "" + response12);

            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(response12 == null){
                dialog.dismiss();
            }else if (response12.equalsIgnoreCase("0")) { //TODO in empty quotes
                dialog.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        VerifyRandomNumber.this);

                // set title
                alertDialogBuilder.setTitle("dr.CAFE");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Invalid Activation Code")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.dismiss();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            } else if(response12.equalsIgnoreCase("1")){
                dialog.dismiss();
                userPrefEditor.putString("login_status","loggedin");
                userPrefEditor.commit();
                Intent loginIntent = new Intent(VerifyRandomNumber.this, MainActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginIntent);
                finish();
            }

            super.onPostExecute(result1);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
