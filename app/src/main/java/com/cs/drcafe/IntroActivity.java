package com.cs.drcafe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by brahmam on 28/12/15.
 */
public class IntroActivity extends Activity {

    TextView changeLanguage;
    Button signinButton, signupButton;
    SharedPreferences.Editor languagePrefsEditor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("lifecycle","intro onCreate invoked");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        SharedPreferences languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        final String language = languagePrefs.getString("language", "En");
//        if(SplashScreen.regid != null){

//        }
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.intro_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.intro_layout_arabic);
        }

        signinButton = (Button) findViewById(R.id.signin_btn);
        signupButton = (Button) findViewById(R.id.signup_btn);
        changeLanguage = (TextView) findViewById(R.id.language_selection);

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signinIntent = new Intent(IntroActivity.this, LoginActivity.class);
                startActivity(signinIntent);
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signupIntent = new Intent(IntroActivity.this, RegistrationActivity.class);
                startActivity(signupIntent);
            }
        });


        changeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    recreate();
                }else if(language.equalsIgnoreCase("Ar")){
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    recreate();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("lifecycle","intro onStart invoked");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d("lifecycle","intro onResume invoked");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d("lifecycle","intro onPause invoked");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d("lifecycle","intro onStop invoked");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("lifecycle","intro onRestart invoked");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("lifecycle","intro onDestroy invoked");
    }
}
