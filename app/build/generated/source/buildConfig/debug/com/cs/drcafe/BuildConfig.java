/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.cs.drcafe;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.cs.drcafe";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 133;
  public static final String VERSION_NAME = "1.9.58";
}
